//google map
/*function initialize() {
 var mapCanvas = document.getElementById('map-packages');
 var mapOptions = {
 center: new google.maps.LatLng(44.5403, -78.5463),
 zoom: 8,
 mapTypeId: google.maps.MapTypeId.ROADMAP
 }
 var map = new google.maps.Map(mapCanvas, mapOptions)
 }
 google.maps.event.addDomListener(window, 'load', initialize);*/
//Package Full Calendar
$(document).ready(function() {
    var url = window.location.href;
    var urlArr = url.split('/');
    var isPackage = 'no';
    if (urlArr.indexOf('package') > -1) {
        isPackage = 'yes';
    }
    if (isPackage == 'yes') {
        $('#package-calendar').fullCalendar({
            theme: false,
            header: {
                left: 'prev',
                center: 'title',
                right: 'next'
            },
            titleFormat: 'YYYY MMMM',
            fixedWeekCount: false,
            defaultDate: new Date(),
            selectable: true,
            selectHelper: true,
            eventLimit: true, // allow "more" link when too many events
            contentHeight: 'auto',
            events: {
                //url: $("#base_url").val() + 'getSavedEvents',
            },
            eventColor: 'rgb(1, 175, 239)',
            eventClick: function(event) {
                /*if(event.show_type == 'calendar') {
                 window.location.href = $("#base_url").val() + 'calendar/' + event.alias;
                 } else {
                 window.location.href = $("#base_url").val() + 'events/' + event.alias;
                 }*/
            }
        });
    }

    $('body').on('click','.btn-book-now',function() {
        var _this = $(this);
        var package = _this.attr('rel');
        $.ajax({
            url: $("#baseUrl").val() + 'bookingm/' + package,
            type: "POST",
            dataType: "JSON",
            success: function(response) {
                var data = response.data;
                var hidden_selected_dates = '';
                var dd = data.package_description.departure_dates;
                var dinfo = data.package_description.info;
                var enquire_url = $("#baseUrl").val() + 'enquire' + package;
                if (!dd) {
                    $('[name="package_booking"]').hide();
                    $('#current-booking-staus').hide();
                    $('#current-booking-staus-no').show();
                    $('#current-booking-staus-no').find('a').attr('href', enquire_url);
                    $('#hidden_selected_dates').val('0');
                } else {
                    ($.each(dd, function(i, b) {
                        hidden_selected_dates += b.departure_date + ',';
                    }));
                    hidden_selected_dates = hidden_selected_dates.slice(0, -1);
                    $('#hidden_selected_dates').val(hidden_selected_dates);
                    $('[name="hidden_package_name"]').val(dinfo.name);
                    $('[name="hidden_package_destination_id"]').val(dinfo.destination_id);
                    $('[name="hidden_package_experience_id"]').val(dinfo.experience_id);
                    $('[name="hidden_package_id"]').val(dinfo.id);
                    $('[name="hidden_package_code"]').val(dinfo.code);

                    var selected_date = data.selected_date;
                    if (selected_date) {
                        $('[name="departure_date"]').val(selected_date);
                        $('#selected_departure_date').text(selected_date);
                    } else {
                        $('#selected_departure_date').text(dd[0].departure_date);
                    }
                    var selected_all_allocation = data.selected_all_allocation;
                    (selected_all_allocation) ? $('#total_booking_available').text(selected_all_allocation) : $('#total_booking_available').text(dd[0].all_allocation);
                    var selected_booked = data.selected_booked;
                    (selected_booked) ? $('#total_booked').text(selected_booked) : $('#total_booked').text(dd[0].booked);
                    var selected_available = data.selected_available;
                    (selected_available) ? $('#total_available').text(selected_available) : $('#total_available').text(dd[0].available);


                    //load calender now
                    var lastPart = 'booking';
                    var availableDates = $("input#hidden_selected_dates").val();
                    if (availableDates != '0' || availableDates != 'NULL' || availableDates == 'undefined') {
                        availableDates = availableDates.split(",");
                    }

                    console.log(availableDates);
                    if (lastPart == 'booking') {
                        $("#departure_date").datepicker('remove');
                        $('#departure_date').datepicker({
                            format: "yyyy-mm-dd",
                            autoclose: true,
                            beforeShowDay: function(date) {
                                return enableAvailableDates(date, availableDates);
                            }
                        });
                    } else {
                        $('#departure_date').datepicker({
                            format: "yyyy-mm-dd",
                            autoclose: true,
                            beforeShowDay: disablePrevious
                        });
                    }

                    //# show hide form elements
                    $('[name="package_booking"]').show();
                    $('#current-booking-staus').show();
                    $('#current-booking-staus-no').hide();
                    //# load modal now
                    $('#bookingModal').modal('show');
                }
            },
            error: function() {
                alert('Error occured');
                return false;
            }
        });
    });

});



function enableAvailableDates(date, availableDates) {
    dmy = date.getFullYear() + "-" + (('0' + (date.getMonth() + 1)).slice(-2)) + "-" + (('0' + (date.getDate())).slice(-2));

    if ($.inArray(dmy, availableDates) == -1) {
        return {
            enabled: false, classes: '', tooltip: 'Unavailable'
        };
    }
    return;
}

function disablePrevious(date) {
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    if (date.valueOf() < now.valueOf()) {
        return {
            enabled: false
        };
    }
    return
}
