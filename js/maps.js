var marker;
var infowindow;
var markers = {};
var infowindows = [];
var edit = 0;
var linecordinates = [];
var map;
var lineSymbol;
var flightPath;
var line = [];
var saved_label;
var mapnotLoaded = 1;

$('a.tab-map').on('shown.bs.tab', function (e) {
    var saved_locations = $("#hidden_lat_long").val();
    if(saved_locations != '0') {
        if (mapnotLoaded) {
            mapnotLoaded = 0;
            initialize();
        }
    }
});

function initialize() {
    var saved_locations = $("#hidden_lat_long").val();
    
    lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
    };
    /* edit portion */
    if(saved_locations != '') {
        edit = 1;
        saved_locations = JSON.parse(saved_locations);
    }
    /* edit portion */
    var latlng = new google.maps.LatLng(saved_locations[0].lat,saved_locations[0].long);
    var options = {
        zoom: 12,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map-packages"), options);
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    /* edit portion */
    if(edit) {
        console.log(saved_locations.length);
        for(var i = 0; i < saved_locations.length; i++ ) {
            var position = new google.maps.LatLng(saved_locations[i].lat, saved_locations[i].long);
            var id = saved_locations[i].label;
            saved_label = id;
            marker = new google.maps.Marker({
                id: id,
                position: position,
                map: map,
                icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + saved_label + '|FF0000|000000'
            });
            infowindow = ['<div class="info_content">' +
            '<span> Lat :' + saved_locations[i].lat + '/ Long : ' + saved_locations[i].long + '</span>' +
            '<h3 class="map-title">' + saved_locations[i].titl + '</h3>' +
            '<p class="map-address">' + saved_locations[i].addr + '</p>' +
            '<p class="map-location">' + saved_locations[i].desc + '</p>' +
            '</div>']
            infowindows.push(infowindow);

            markers[i] = marker;

            linecordinates.push({
                lat: parseFloat(saved_locations[i].lat),
                lng: parseFloat(saved_locations[i].long)
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infoWindow.setContent(String(infowindows[i]));
                    infoWindow.open(map, marker);
                }
            })(marker, i));
        }

        flightPath = new google.maps.Polyline({
            path: linecordinates,
            strokeColor: '#FF0000',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            icons: [{
                icon: lineSymbol,
                offset: '100%'
            }],
        });
    }

    flightPath.setMap(map);
    /* edit portion */
}
