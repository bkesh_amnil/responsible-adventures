$(document).on("click", "a.download-tour-detail, a.btn-download-pdf", function() {
    var modal = $('#exampleModal').modal('show');
})

$(document).off('click', '#send-pdf');
$(document).on('click', '#send-pdf', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var form_name = $('form#comit_form').attr('name');
    var form = $('form#comit_form');

    var file = $('a.btn-download-pdf').attr('file');
    $('#fileid').val($('#hidden_package_id').val());
    $('#filename').val(file);

    var data = form.serialize();
    var action = form.attr('action');

    if(form.valid()) {
        $("#btn-send").attr("disabled", "disabled");
        $('.popup-processing').css('display','block');
        $('html, body').animate({
            scrollTop: $('.processing').offset().top - 100
        }, 700);
        $.ajax({
            url: action,
            data: data,
            type: 'post',
            dataType: 'JSON',
            success: function (data) {
                $("#send-pdf").attr("disabled", "");
                $('.popup-processing').css('display','none');
                $('form#comit_form')[0].reset();
                $('#exampleModal').modal('hide');
                $("#messageModal .modal-body").html('Package pdf sent to your mail.');
                $("#messageModal").modal('show');
            },
            error: function() {
                alert('Error occured');
                return false;
            }
        });
    }
});