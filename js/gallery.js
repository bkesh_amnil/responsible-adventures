$(document).ready(function() {
    $('.fancybox').fancybox({
        padding: 5,
        helpers: {
            title: {type: 'inside'},
            buttons: {}
        },
        beforeShow: function () {
            this.title = $(this.element).data("caption");
        }
    });

    $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
            openEffect : 'none',
            closeEffect : 'none',
            prevEffect : 'none',
            nextEffect : 'none',

            arrows : false,
            helpers : {
                title	: { type : 'inside' },
                media : {},
                buttons : {}
            },
            beforeShow : function(){
                this.title =  $(this.element).data("caption");
            }
        });
});