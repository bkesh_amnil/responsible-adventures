//Main Banner
$('#main-banner').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items :1,
    loop: owlLoop('#main-banner'),
    nav: true,
    dot: true,
    smartSpeed: 500
});

//Destination Banner
$('#destinaiton').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items :1,
    loop: owlLoop('#destinaiton'),
    nav: true,
    dot: true,
    smartSpeed: 500
});

//Testimonial Slider
$('#testimonial').owlCarousel({
    autoplay: true,
    autoplayTimeout: 5000,
    items :1,
    loop: owlLoop('#testimonial'),
    nav: true,
    dot: true,
    smartSpeed: 500
});

//Package
//tour detail slider
$("#package-detail-slide").owlCarousel({
    autoplay: true,
    items: 1,
    loop: owlLoop('#package-detail-slide'),
    nav: true
});

function owlLoop(owlElementID) {
    if (!$(owlElementID).length){
        return false;
    }
    if ($(owlElementID).children().length < 2) {
        return false;
    } else {
        return true;
    }
}
// the following to the end is whats needed for the thumbnails.
jQuery( document ).ready(function() {
	 
  
// 1) ASSIGN EACH 'DOT' A NUMBER
dotcount = 1;

jQuery('#package-detail-slide .owl-dot').each(function() {
    jQuery( this ).addClass( 'dotnumber' + dotcount);
    jQuery( this ).attr('data-info', dotcount);
    dotcount=dotcount+1;
});

// 2) ASSIGN EACH 'SLIDE' A NUMBER
slidecount = 1;

jQuery('#package-detail-slide .owl-item').not('.cloned').each(function() {
    jQuery( this ).addClass( 'slidenumber' + slidecount);
    slidecount=slidecount+1;
});

// SYNC THE SLIDE NUMBER IMG TO ITS DOT COUNTERPART (E.G SLIDE 1 IMG TO DOT 1 BACKGROUND-IMAGE)
jQuery('#package-detail-slide .owl-dot').each(function() {

    grab = jQuery(this).data('info');

    slidegrab = jQuery('.slidenumber'+ grab +' img').attr('src');
//    console.log(slidegrab);

    jQuery(this).css("background-image", "url("+slidegrab+")");  

});

// THIS FINAL BIT CAN BE REMOVED AND OVERRIDEN WITH YOUR OWN CSS OR FUNCTION, I JUST HAVE IT
// TO MAKE IT ALL NEAT 
    amount = jQuery('#package-detail-slide .owl-dot').length;
    gotowidth = 100/amount;

    jQuery('#package-detail-slide .owl-dot').css("width", gotowidth+"%");
    newwidth = jQuery('#package-detail-slide .owl-dot').width();
    jQuery('#package-detail-slide .owl-dot').css("height", newwidth /1.5+"px");	
	
});