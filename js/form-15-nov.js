$(document).ready(function() {
    $("#btn-send").css("display", "block");
})

var url = window.location.href,
    lastPart = url.substring(url.lastIndexOf('/') + 1);

/* form submit starts */
$(document).off('click', '#btn-send');
$(document).on('click', '#btn-send', function (e) {
    e.stopPropagation();
    e.preventDefault();
    var $this = $(this);
    var form_name = $(this).parents('form').attr('name');
    var form = $(this).parents('form');
    var data = form.serialize();
    var action = form.attr('action');

    if(lastPart == 'book' || lastPart == 'enquire') {
        if ($("select#nos_of_pax").val() == '0') {
            alert('Number of Pax is Required');
            return false;
        }
    }

    if(lastPart == 'book') {
        if(($("input.price_per_person").val() == '') || ($("input.price_per_person").val() == 'NULL')) {
            alert('Price Per Person is Required');
            return false;
        }
    }

    form.validate({
        rules: {
            captchaResult: {
                math: true
            }
        }
    });

    $.validator.addMethod("math", function(value, element, params) {
        var a = parseInt($("input[name=firstNumber]").val());
        var b = parseInt($("input[name=secondNumber]").val());
        var c = $("input[name=captchaResult]").val();
        value = parseInt(a + b);

        if(c == value) {
            return true;
        } else {
            return false;
        }
    }, $.validator.format("Wrong Sum"));


    if(form.valid()) {
        $('.processing').css('display','block');
        $('html, body').animate({
            scrollTop: $('.processing').offset().top - 100
        }, 700);
        $.ajax({
            url: action,
            data: data,
            type: 'post',
            dataType: 'JSON',
            success: function (data) {
                $('.processing').css('display','none');
                $("#messageModal .modal-body").html(data.msg);
                $("#messageModal").modal('show');
                $this.parents('form')[0].reset();
            }
        });
    }
});
/* form submit ends */
/* back button in form as close icon starts  */
$(document).on("click", "a.close-btn", function() {
    var cur_url = document.location.href,
        url = cur_url.substring(0,cur_url.lastIndexOf("/"));
    window.location.href = url;
})
/* back button in form as close icon ends */