var item_per_page = $('#hidden_item_per_page').val();
var url = window.location.pathname,
    value = url.substring(url.lastIndexOf('/') + 1);
var count;

$(document).ready(function(){
    if(item_per_page != '') {
        $('#paging_container2').pajinate({
            num_page_links_to_display : 20,
            items_per_page : item_per_page,
            wrap_around: false,
            show_first_last: false
        });

        if(value == 'news') {
            count = $('ul.content div.contents').size();
        } else {
            count = $('ul.content div.tour-package-wrap').size();
        }
        if(count <= item_per_page){
            $('.page_navigation').hide();
        }
    }

    var num_items = $(".page_link").length;
    $("span.total_page_number").text(num_items);
});
$(document).on('click', 'a.previous_link, a.page_link, a.next_link', function() {
    if(value == 'news') {
        $('html, body').animate({
            scrollTop: $("div.col-lg-9").offset().top
        }, 1000);
    } else {
        $('html, body').animate({scrollTop: 0});
    }
})

$(document).on('click', 'a.page_link', function() {
    var nos = $(this).attr("longdesc");
    nos++;
    $("span.current_page_number").text(nos);
})

$(document).on('click', 'a.next_link, a.previous_link', function() {
    var nos = $('a.active_page').attr("longdesc");
    nos++;
    $("span.current_page_number").text(nos);
})