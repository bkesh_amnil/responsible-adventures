var destination_id;
var experience_id;
$(document).on("click", "input.go", function() {
    destination_id = $("select#destination option:selected").val();
    experience_id = $("select#experience option:selected").val();

    if(destination_id == '0' && experience_id == '0') {
        alert('Select value first');
        return false;
    } else {
        $("#hidden_package_ids").val('');
        $.ajax({
            url: $("#baseUrl").val() + 'search/packageexperience',
            data: {destination_id : destination_id, experience_id : experience_id},
            type: 'POST',
            dataType: 'JSON',
            success: function (result) {
                $('html, body').animate({
                    scrollTop: $('section.featured-experiences').offset().top - 0
                }, 700);
                $("section.featured-experiences div.tour-package-wrap").remove();
                $("a#load-more").text("Load More");
                if(result.data != 'empty') {
                    $("#hidden_package_ids").val(result.ids);
                    $("section.featured-experiences div.featured-header").after(result.data);
                    $("select#all_packages_filter")
                        .find('option')
                        .remove()
                        .end()
                        .append(result.search_data);

                    $("a#load-more").css("display", "inline-block");
                } else {
                    $("section.featured-experiences div.featured-header").after("<div class='tour-package-wrap'><p>Search Result not found</p></div>");
                    $("a#load-more").css("display", "none");
                }
            }
        });
        return false;
    }
})
$(document).on("change", 'select#destination', function() {
    destination_id = $(this).find("option:selected").val();
    $.ajax({
        url: $("#baseUrl").val() + 'search/experience',
        data: {destination_id : destination_id},
        type: 'POST',
        dataType: 'JSON',
        success: function(data) {
            $("select#experience")
                .find('option')
                .remove()
                .end()
                .append(data);

            return false;
        }
    })
})
