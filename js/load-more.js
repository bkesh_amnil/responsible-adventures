var destination;
var experience;
var ids;
var search_type;

/* load more */
$(document).on("click", "a#load-more", function() {
    $(this).hide(); //hide load more button on click
    $('.animation_image').show();
    destination = $("select#destination option:selected").val()
    experience = $("select#experience option:selected").val()
    ids = $("#hidden_package_ids").val();
    search_type = $("#hidden_search_type").val();

    $.ajax({
        url : $("#baseUrl").val() + 'content/load_more',
        type : "POST",
        dataType : "JSON",
        //data : {type : type, page : track_click, destination : destination, experience : experience},
        data : {destination : destination, experience : experience, ids : ids},
        success : function(result) {
            $('.animation_image').hide(); //hide loading image once data is received
            if(result.data != 'empty') {
                $("section.featured-experiences div.container div.tour-package-wrap:last-child").after(result.data);
                $("#hidden_package_ids").val(result.ids);
                //scroll page to button element
                //$("html, body").animate({scrollTop: $("section.featured-experiences div.tour-package-wrap").offset().top}, 500);
                $("a#load-more").css("display", "inline-block");
            } else {
                $("a#load-more").css("display", "inline-block");
                $("a#load-more").text("No More Data Available");
            }
        },
        error : function(xhr, ajaxOptions, thrownError) {
            alert(thrownError); //alert any HTTP error
            $(".btn-more").show(); //bring back load more button
            $('.animation_image').hide(); //hide loading image once data is received
        }
    });
});