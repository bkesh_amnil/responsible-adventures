jQuery(function($) {
    // Asynchronously Load the map API
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var data = $("div.contact-contents-wrap").html();
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    map.setTilt(45);

    // Multiple Markers
    var markers = [
        ['Responsible Adventures', 27.740132, 85.331406]/*,
         ['Newroad, Kathmandu', 27.7035,85.3107],
         ['Chabahil, Kathmandu', 27.7132,85.3449],
         ['Durbarthok, Pokhara', 28.21,83.98]*/
    ];

    // Info Window Content
    var infoWindowContent = [
         ['<div class="info_content">' +
         '<h3>RAJ TAMANG</h3>' +
         '<p>P.O Box 2455, Naya Bazaar, Kathmandu, Nepal</p>' +
         '<p>+977-9841768892, +977-9801082108</p><p>info@responsibleadventures.com</p>' +
         '</div>']/*,
         ['<div class="info_content">' +
         '<h3>Chabahil Branch Office</h3>' +
         '<p>Chabahil, Kathmandu</p><p>Mitrapark(Pipal Bot)</p>' +
         '</div>'],
         ['<div class="info_content">' +
         '<h3>Regional Office</h3>' +
         '<p>Durbarthok, Pokhara</p><p>Samsung Galli</p>' +
         '</div>']*/
    ];

    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;

    // Loop through our array of markers & place each one on the map
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });

        // Allow each marker to have an info window
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(8);
        google.maps.event.removeListener(boundsListener);
    });

}