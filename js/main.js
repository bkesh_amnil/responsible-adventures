//Select
$(document).ready(function() {
  $(".js-example-basic-single").select2({
       minimumResultsForSearch: Infinity
  });
});

//search Show/Hide
$(".search-btn").click(function() {
    $(".form-control").toggleClass("active");
});
$(document).on("click", function(e) {
    if ($(e.target).hasClass("search-btn")) return false
    if ($(e.target).hasClass("form-control")) return false
    $(".form-control").removeClass("active");
});
/* to top starts */
$(document).ready(function($){
    // browser window scroll (in pixels) after which the "back to top" link is shown
    var offset = 300,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
    //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
    //grab the "back to top" link
        $back_to_top = $('.cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function(){
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if( $(this).scrollTop() > offset_opacity ) {
            $back_to_top.addClass('cd-fade-out');
        }
    });
    //smooth scroll to top
    $back_to_top.on('click', function(event){
        event.preventDefault();
        $('body,html').animate({
                scrollTop: 0 ,
            }, scroll_top_duration
        );
    });
});
/* to top ends */
// Autocomplete search
$("input#search").autocomplete({
    serviceUrl: $("#baseUrl").val() + 'search/',
    minChars: 1,
    delimiter: /(,|;)\s*/,
    maxHeight: 400,
    width: 200,
    deferRequestBy: 0,
    onSelect: function(value, data) {
        window.location = data;
    },
})