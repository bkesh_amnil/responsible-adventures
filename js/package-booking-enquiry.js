////Package Full Calendar
$(document).ready(function() {
    /**
     * enquire pop up on click
     */
    $('body').on('click', '.btn-enquire', function() {
        var _this = $(this);
        var package = _this.attr('rel');
        var formID = $('[name="package_enquire"]');
        formID[0].reset();
        formID.validate({
            focusCleanup: true
        }).resetForm();
        formID.find('input').removeClass('error');
        $('#bookingModal').modal('hide');
        loadEnquireModal(package, '');
    });
    /**
     * booking pop up on click
     */
    $('body').on('click', '.btn-book-package-now', function() {
        var _this = $(this);
        var package = _this.attr('rel');
        var formID = $('[name="package_booking"]');

        var moment = $('#package-calendar').fullCalendar('getDate');
        var currentCalenderMonth = moment.format();
        formID[0].reset();
        formID.validate({
            focusCleanup: true
        }).resetForm();
        formID.find('input').removeClass('error');
        var data = "current_calender_month=" + currentCalenderMonth;
        loadBookingModal(package, data);
    });
    /**
     * booking pop up on click
     */
    $('body').on('click', '.btn-book-now', function() {
        var _this = $(this);
        var package = _this.attr('rel');
        var formID = $('[name="package_booking"]');
        formID[0].reset();
        formID.validate({
            focusCleanup: true
        }).resetForm();
        formID.find('input').removeClass('error');
        loadBookingModal(package);
    });
});
/**
 * get month from the fullcalendar
 * @returns {undefined}
 */
function getMonth() {
    var date = $("#calendarID").fullCalendar('getDate');
    var month_int = date.getMonth();
    //you now have the visible month as an integer from 0-11
}

/**
 * loads the booking form modal
 * @param {type} package
 * @param {type} data
 * @returns {undefined}
 */
function loadBookingModal(package, data) {
    $.ajax({
        url: $("#baseUrl").val() + 'bookingm/' + package,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(response) {
            var data = response.data;
            var hidden_selected_dates = '';
            var dd = data.package_description.departure_dates;
            var dinfo = data.package_description.info;
//            var enquire_url = $("#baseUrl").val() + 'enquire/' + package;
            if ((dd.length == 0) || data.current_month=='no') {
//                $('[name="package_booking"]').hide();
                $('#current-booking-staus').hide();
                $('.btn-booking-submit').hide();
                $('#current-booking-staus-no').show();
                $('#current-booking-staus-no').find('a').attr('rel', package);
                $('#hidden_selected_dates').val('0');
                //# load modal now
                $('#bookingModal').modal('show');
            } else {
                ($.each(dd, function(i, b) {
                    hidden_selected_dates += b.departure_date + ',';
                }));
                hidden_selected_dates = hidden_selected_dates.slice(0, -1);
                $('#hidden_selected_dates').val(hidden_selected_dates);
                $('[name="hidden_package_name"]').val(dinfo.name);
                $('[name="hidden_package_destination_id"]').val(dinfo.destination_id);
                $('[name="hidden_package_experience_id"]').val(dinfo.experience_id);
                $('[name="hidden_package_id"]').val(dinfo.id);
                $('[name="hidden_package_code"]').val(dinfo.code);

                var selected_date = data.selected_date;
                if (selected_date) {
                    $('[name="departure_date"]').val(selected_date);
                    $('#selected_departure_date').text(selected_date);
                } else {
                    $('#selected_departure_date').text(dd[0].departure_date);
                }
                var selected_all_allocation = data.selected_all_allocation;
                (selected_all_allocation) ? $('#total_booking_available').text(selected_all_allocation) : $('#total_booking_available').text(dd[0].all_allocation);
                var selected_booked = data.selected_booked;
                (selected_booked) ? $('#total_booked').text(selected_booked) : $('#total_booked').text(dd[0].booked);
                var selected_available = data.selected_available;
                (selected_available) ? $('#total_available').text(selected_available) : $('#total_available').text(dd[0].available);


                //load calender now
                var lastPart = 'booking';
                var availableDates = $("input#hidden_selected_dates").val();
                if (availableDates != '0' || availableDates != 'NULL' || availableDates == 'undefined') {
                    availableDates = availableDates.split(",");
                }

                console.log(availableDates);
                if (lastPart == 'booking') {
                    $("#departure_date").datepicker('remove');
                    $('#departure_date').datepicker({
                        format: "yyyy-mm-dd",
                        autoclose: true,
                        beforeShowDay: function(date) {
                            return enableAvailableDates(date, availableDates);
                        }
                    });
                } else {
                    $('#departure_date').datepicker({
                        format: "yyyy-mm-dd",
                        autoclose: true,
                        beforeShowDay: disablePrevious
                    });
                }

                //# show hide form elements
//                $('[name="package_booking"]').show();
                $('#current-booking-staus').show();
                $('.btn-booking-package_booking').show();
                $('#current-booking-staus-no').hide();
                //# load modal now
                $('#bookingModal').modal('show');
            }
        },
        error: function() {
            alert('Error occured');
            return false;
        }
    });
}
/**
 * loads the booking form modal
 * @param {type} package
 * @param {type} data
 * @returns {undefined}
 */
function loadEnquireModal(package) {
    $.ajax({
        url: $("#baseUrl").val() + 'enquire/' + package,
        type: "POST",
        data: '',
        dataType: "JSON",
        success: function(response) {
            var data = response.data;
            var dinfo = data.package_description.info;

            $('[name="hidden_package_name"]').val(dinfo.name);
            $('[name="hidden_package_destination_id"]').val(dinfo.destination_id);
            $('[name="hidden_package_experience_id"]').val(dinfo.experience_id);
            $('[name="hidden_package_id"]').val(dinfo.id);
            $('[name="hidden_package_code"]').val(dinfo.code);
            //load calender now
            $('#departure_date_enquire').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true,
                beforeShowDay: disablePrevious
            });
            //# load modal now
            $('#enquireModal').modal('show');
            return false;
        },
        error: function() {
            alert('Error occured');
            return false;
        }
    });
}


/**
 * checks the dates to enable the only dates available in list
 * @param {type} date
 * @param {type} availableDates
 * @returns {enableAvailableDates.packageAnonym$3|undefined}
 */
function enableAvailableDates(date, availableDates) {
    dmy = date.getFullYear() + "-" + (('0' + (date.getMonth() + 1)).slice(-2)) + "-" + (('0' + (date.getDate())).slice(-2));

    if ($.inArray(dmy, availableDates) == -1) {
        return {
            enabled: false, classes: '', tooltip: 'Unavailable'
        };
    }
    return;
}


/**
 * disables the previous dates in calendar
 * @param {type} date
 * @returns {undefined|disablePrevious.packageAnonym$4}
 */
function disablePrevious(date) {
    var nowTemp = new Date();
    var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
    if (date.valueOf() < now.valueOf()) {
        return {
            enabled: false
        };
    }
    return;
}