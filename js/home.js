$(document).ready(function(){
    var slides = $("#hidden_json_data").val();
    slides = $.parseJSON(slides);

    var country;
    $('.tours').off("mouseover");
    $('.tours').on("mouseover", function(e) {
        // Do not proceed further if different country is not hovered
        if($(this).attr("data-country") == country) return;
        // set country
        country  = $(this).attr("data-country");

        carouselElm = $(this).parents(".tour-highlighted-wraps").find(".carousel");

        if(($(this).parents(".tour-highlighted-wraps").find(".activity-slider")).hasClass("slider-animation")) {
            $(this).parents(".tour-highlighted-wraps").find(".tour-detail-slide-in").removeClass("slider-animation");
        } else {
            $(this).parents(".tour-highlighted-wraps").find(".tour-detail-slide-in").addClass("slider-animation");
        }

        var owl = carouselElm.data('owlCarousel');
        owl.destroy();
        var countrySlides = $.grep(slides, function( slide ) {
            return slide.country === country;
        });

        renderSlide(carouselElm, countrySlides);
    })

    $(".tour-highlighted-wraps").off("mouseleave")
    $(".tour-highlighted-wraps").on("mouseleave", function(e) {
        var carouselElm = $(this).find(".carousel");

        var countries = [];
        $.each($(this).find(".destintaion_name"), function (j, co) {
            var country = $(co).attr("data-country");

            var co = $.grep(slides, function (slide) {
                return slide.country == country
            });
            $.merge(countries, co);
        })

        var owl = carouselElm.data('owlCarousel');

        if(owl.itemsAmount == countries.length) return
        owl.destroy();
        renderSlide(carouselElm, countries);
        country = ''; // set country to blank after mouse leave
    })

    $.each($(".tour-detail-slide-in-wrap"), function(i, v) {
        var carouselElm = $(v).find(".carousel");
        var countries = [];
        $.each($(v).find(".country"), function (j, co) {
            var country = $(co).attr("data-country");
            var co = $.grep(slides, function (slide) {
                return slide.country == country
            });
            $.merge(countries, co);
        })
        renderSlide(carouselElm, countries);
    })
})

$(document).on("click", "div.activity-slider", function() {
    var url = $(this).data("url");
//    alert(url);
    if(url != '') {
        window.location.href = url;
    }
})

function renderSlide(slideElm, slides) {
    var content = '';
    for (i in slides) {
        if(i == 0) {
            content += '<div class="activity-slider slider-animation" data-country="' + slides[i].country +'" data-url="' + slides[i].link + '"><img class="img-responsive" src="' + slides[i].url +'"><div class="trek-short-desc"><h3>' + slides[i].title + '</h3><p>' + slides[i].description + '</p></div></div></div>';
        } else {
            content += '<div class="activity-slider" data-country="' + slides[i].country + '" data-url="' + slides[i].link + '"><img class="img-responsive" src="' + slides[i].url + '"><div class="trek-short-desc"><h3>' + slides[i].title + '</h3><p>' + slides[i].description + '</p></div></div></div>';
        }
    }
    slideElm.html(content)
    slideElm.owlCarousel({
        autoplay: true,
        autoplayTimeout: 4000,
        loop: owlLoop(slideElm),
        items: 1
    });
}

function owlLoop(owlElementID) {
    if (!$(owlElementID).length){
        return false;
    }
    if ($(owlElementID).children().length < 2) {
        return false;
    } else {
        return true;
    }
}