var sel_date;
var nos_of_pax;
/* tour booking */
//var package_id = $("input[name=hidden_package_id]").val();
$(document).on("change", "input#departure_date", function(e) {

    var package_id = $("input[name=hidden_package_id]").val();
    sel_date = $(this).val();
    nos_of_pax = $("select#nos_of_pax option:selected").val();
    if (sel_date != 'NULL' && sel_date != "") {
        $.ajax({
            url: $("#baseUrl").val() + 'booking/package/',
            type: "POST",
            dataType: "JSON",
            data: {package_id: package_id, sel_date: sel_date, nos_of_pax: nos_of_pax},
            success: function(data) {
                if (data != '') {
                    appendData(data, sel_date, nos_of_pax);
                }
            },
            error: function() {
                alert('Error occured');
                return false;
            }
        })
    }
})

$(document).on("change", "select#nos_of_pax", function(e) {

    var package_id = $("input[name=hidden_package_id]").val();
    sel_date = $("input#departure_date").val();
    nos_of_pax = $(this).find("option:selected").val();
    if (sel_date != 'NULL' && nos_of_pax != '0') {
        $.ajax({
            url: $("#baseUrl").val() + 'booking/package/',
            type: "POST",
            dataType: "JSON",
            data: {package_id: package_id, sel_date: sel_date, nos_of_pax: nos_of_pax},
            success: function(data) {
                if (data != '') {
                    appendData(data, sel_date, nos_of_pax);
                }
            },
            error: function() {
                alert('Error occured');
                return false;
            }
        })
    }
    if (nos_of_pax == '0') {
        $("input.price_per_person").val('');
        $("input[name=total_cost]").val('');
    }
})

function appendData(data, sel_date, nos_of_pax) {
    $("span#selected_departure_date").text(sel_date);
    $("span#total_booking_available").text(data.all_allocation);
    $("span#total_booked").text(data.booked);
    $("span#total_available").text(data.available);
    $("input.price_per_person").val(data.package_unit_price);

    if (data.package_unit_price != '') {
        var total_cost = nos_of_pax * data.package_unit_price;
        $("input[name=total_cost]").val(total_cost);
    } else {
        $("input[name=total_cost]").val('');
    }
    $("input.hidden_fixed_departure").val(data.package_is_fixed_departure_id);
}