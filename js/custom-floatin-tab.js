$(document).ready(function() {
    var count = $("#hidden_count").val();
    if(count > 1) {
        var slug = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        var tab = $("ul#myTab li#" +slug + " a").attr("href").split("#");
        var tabId = $("ul#myTab li#" +slug + " a").attr("href").split("-")[1];

        $("ul#myTab li").removeClass("active");
        $("ul#myTab li#" + slug).addClass("active");
        $("div#myTabContent > div").removeClass("active").removeClass("in");
        $("div#myTabContent div#" + tab[1]).addClass("active in");

        setTimeout(function(){
            $("#team-image-slider-"+tabId).owlCarousel({
                autoplay: true,
                autoplayTimeout: 5000,
                items: 1,
                nav: true,
                smartSpeed: 500,
            });
        }, 200);
    } else {
        var tabId = $("ul#myTab a:first").attr("href").split("-")[1];

        $("#team-image-slider-"+tabId).owlCarousel({
            autoplay: true,
            autoplayTimeout: 5000,
            items: 1,
            nav: true,
            smartSpeed: 500,
        });
    }
})
$(document).on("click", "ul#myTab li a", function() {
    var count = $("#hidden_count").val();
    var slug = $(this).attr("rel");
    var tabId = $(this).attr("href").split("-")[1];

    if(count > 1) {
        var url = $("#hidden_current_url").val();
        var current_url = url.substring(0,url.lastIndexOf("/"));
    } else {
        var current_url = $("#hidden_current_url").val();
    }

    setTimeout(function(){
        $("#team-image-slider-"+tabId).owlCarousel({
            autoplay: true,
            autoplayTimeout: 5000,
            items: 1,
            nav: true,
            smartSpeed: 500,
        });
    }, 200);

    history.pushState(null, null,  current_url + '/' + slug);
})
$('button').on('click', function(){
});
$('#team').tabCollapse();