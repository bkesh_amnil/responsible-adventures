/* package search begins */
var location_id,
    experience_id,
    order_id,
    year_id,
    month_id;
var item_per_page = $('#hidden_item_per_page').val();

$(document).on("change", "select#destination", function(){
    $("div.processing").css("display", "block");
    $("select#experience").val("0").change();
    location_id = $(this).find("option:selected").val();
    experience_id = $("select#experience option:selected").val();
    order_id = $("select#price option:selected").val();
    year_id = $("select#years option:selected").val();
    month_id = $("select#months option:selected").val();

    if(location_id != '0') {
        $.ajax({
            url: $("#baseUrl").val() + 'search/experience',
            data: {destination_id: location_id},
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
                $("select#experience")
                    .find('option')
                    .remove()
                    .end()
                    .append(data);

                return false;
            }
        })
    } else {
        $.ajax({
            url: $("#baseUrl").val() + 'search/experience',
            data: {destination_id: location_id},
            type: 'POST',
            dataType: 'JSON',
            success: function (data) {
                $("select#experience")
                    .find('option')
                    .remove()
                    .end()
                    .append(data);

                return false;
            }
        })
    }

    searchPackage();
});
$(document).on("change", "select#experience", function(){
    $("div.processing").css("display", "block");
    location_id = $("select#destination option:selected").val();
    experience_id = $(this).find("option:selected").val();
    order_id = $("select#price option:selected").val();
    year_id = $("select#years option:selected").val();
    month_id = $("select#months option:selected").val();

    searchPackage();
});
$(document).on("change", "select#price", function(){
    $("div.processing").css("display", "block");
    location_id = $("select#destination option:selected").val();
    experience_id = $("select#experience option:selected").val();
    order_id = $(this).find("option:selected").val();
    year_id = $("select#years option:selected").val();
    month_id = $("select#months option:selected").val();

    searchPackage();
});
$(document).on("change", "select#years", function(){
    $("div.processing").css("display", "block");
    location_id = $("select#destination option:selected").val();
    experience_id = $("select#experience option:selected").val();
    order_id = $("select#price option:selected").val();
    year_id = $(this).find("option:selected").val();
    month_id = $("select#months option:selected").val();

    searchPackage();
});
$(document).on("change", "select#months", function(){
    $("div.processing").css("display", "block");
    location_id = $("select#destination option:selected").val();
    experience_id = $("select#experience option:selected").val();
    order_id = $("select#price option:selected").val();
    year_id = $("select#years option:selected").val();
    month_id = $(this).find("option:selected").val();

    searchPackage();
});

function searchPackage() {
    $.ajax({
        url : $("#baseUrl").val() + 'search/package',
        data : {location_id : location_id, experience_id : experience_id, order_id : order_id, year_id : year_id, month_id : month_id},
        type : "POST",
        dataType : "JSON",
        success : function(res) {
            $("div.processing").css("display", "none");

            $("div.packages div.tour-package-wrap").remove();
            if(res.count != '0') {
                $("div.paging-information").css("display", "block");
                $("div.packages div.container_srch ul.content").append(res.data);
                $("p.total-count").text('Total Packages : ' + res.count + ' , ');
            } else {
                $("div.paging-information").css("display", "none");
            }

            $('#paging_container2').pajinate({
                num_page_links_to_display : 4,
                items_per_page : item_per_page,
                wrap_around: false,
                show_first_last: false
            });

            var count = $('ul.content div.tour-package-wrap').size();

            if(count <= item_per_page){
                $('.page_navigation').hide();
                $("span.current_page_number").text('1');
                $("span.total_page_number").text('1');
            } else {
                $('.page_navigation').show();
                $("span.current_page_number").text('1');
                var num_items = $(".page_link").length;
                $("span.total_page_number").text(num_items);
            }
            return false;
        },
        error : function() {
            alert("error occured");
            return false;
        }
    })
}
/* package search begins */
/* package search reset */
$(document).on("click", "input.btn-reset-package-search", function() {
    $("select#destination, select#experience, select#years, select#months").val("0").change();
    $("select#price").val("ASC").change();
})
/* package search reset */