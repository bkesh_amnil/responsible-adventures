////Package Full Calendar
$(document).ready(function() {

    /**
     * loads the full calender on package page right side
     */
    $('#package-calendar').fullCalendar({
        theme: false,
        header: {
            left: 'prev',
            center: 'title',
            right: 'next'
        },
        titleFormat: 'YYYY MMMM',
        fixedWeekCount: false,
        defaultDate: new Date(),
        selectable: true,
        selectHelper: true,
        eventLimit: true, // allow "more" link when too many events
        contentHeight: 'auto',
        events: {
            url: $("#baseUrl").val() + 'getPackageBookingDate/' + $("#hidden_package_id").val(),
        },
        eventColor: 'rgb(1, 175, 239)',
        eventClick: function(event) {
            console.log(event.start, 'bkesh');
//            $("input#date").val(event.start);
//            $("input#allocation").val(event.allocation);
//            $("input#booked").val(event.booked);
//            $("input#available").val(event.available);
//            $("#book_now_date").attr("action", $("#baseUrl").val() + 'booking'+'/'+lastPart);
//            $('#book_now_date').submit();
            var url = window.location.href;
            var package = url.substr(url.lastIndexOf('/') + 1);
            var data = "date=" + event.start + "&allocation=" + event.allocation + "&booked=" + event.booked + "&available=" + event.available;
            loadBookingModal(package, data);
        }
    });


    /**
     * event for book now when clicked from itenery tabs
     */
    $(document).on("click", ".book-now", function() {
        var date = $(this).attr("rel");
        var timestamp = 'no';
        var allocation = $(this).data("allocation");
        var booked = $(this).data("booked");
        var available = $(this).data("available");
        var data = "date=" + date + "&allocation=" + allocation + "&booked=" + booked + "&available=" + available + "&timestamp=" + timestamp;
        var url = window.location.href;
        var package = url.substr(url.lastIndexOf('/') + 1);
        loadBookingModal(package, data);
    });


    if ($('#hidden_count').val() > 1) {
        $('button').on('click', function() {
        });
        $('#package-detail-tab').tabCollapse();
    }

});