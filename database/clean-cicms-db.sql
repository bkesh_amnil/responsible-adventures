/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.5.24-0ubuntu0.12.04.1 : Database - db_icimod_vaca
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ci_sessions` */

insert  into `ci_sessions`(`session_id`,`ip_address`,`user_agent`,`last_activity`,`user_data`) values ('077663875b477b67989a2c7fcf165241','::1','Mozilla/5.0 (Windows NT 6.3; WOW64; rv:40.0) Gecko/20100101 Firefox/40.0',1442828614,'a:6:{s:9:\"user_data\";s:0:\"\";s:8:\"username\";s:10:\"superadmin\";s:7:\"user_id\";s:1:\"7\";s:7:\"role_id\";s:1:\"1\";s:5:\"email\";s:20:\"satish@amniltech.com\";s:4:\"name\";s:21:\"Satish Shanker Dongol\";}');

/*Table structure for table `tbl_banner` */

DROP TABLE IF EXISTS `tbl_banner`;

CREATE TABLE `tbl_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_banner` */

/*Table structure for table `tbl_category` */

DROP TABLE IF EXISTS `tbl_category`;

CREATE TABLE `tbl_category` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `status` enum('Active','InActive') DEFAULT 'Active',
  `type` enum('Activity','Others') NOT NULL DEFAULT 'Others',
  `slug` varchar(100) NOT NULL,
  `created_on` int(15) DEFAULT NULL,
  `updated_on` int(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_category` */

/*Table structure for table `tbl_configuration` */

DROP TABLE IF EXISTS `tbl_configuration`;

CREATE TABLE `tbl_configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) NOT NULL,
  `site_email` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` text NOT NULL,
  `meta_keyword` varchar(160) NOT NULL,
  `meta_description` text NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `gplus` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_configuration` */

insert  into `tbl_configuration`(`id`,`site_title`,`site_email`,`address`,`phone`,`meta_keyword`,`meta_description`,`facebook`,`twitter`,`gplus`,`skype`,`youtube`) values (1,'ICIMOD Vaca','satish@amniltech.com','<p>Test Address</p>\r\n','Test Phone','ICIMOD Vaca','ICIMOD Vaca','http://facebook.com','http://twitter.com','','','http://youtube.com');

/*Table structure for table `tbl_content` */

DROP TABLE IF EXISTS `tbl_content`;

CREATE TABLE `tbl_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `short_description` varchar(255) NOT NULL,
  `long_description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `meta_description` text NOT NULL,
  `meta_keyword` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `status` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  `created_on` int(15) NOT NULL,
  `source` varchar(255) NOT NULL,
  `updated_on` int(15) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_content` */

/*Table structure for table `tbl_gallery` */

DROP TABLE IF EXISTS `tbl_gallery`;

CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_gallery` */

/*Table structure for table `tbl_module` */

DROP TABLE IF EXISTS `tbl_module`;

CREATE TABLE `tbl_module` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `priority` int(10) NOT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `icon_class` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_module` */

insert  into `tbl_module`(`id`,`name`,`slug`,`priority`,`parent_id`,`icon_class`) values (1,'CMS','#',0,0,'fa fa-files-o'),(2,'Contents','content',0,1,''),(3,'Banners','banner',0,1,''),(4,'Gallery','gallery',0,1,''),(5,'Categories','category',0,1,''),(6,'Media','media',0,1,''),(7,'Settings','#',0,0,'fa fa-cogs'),(8,'Site Configuration','configuration',0,7,''),(9,'Module Manager','module',0,7,''),(10,'Role Module Manager','rolemodule',0,7,''),(20,'Users','user',0,0,'fa fa-users');

/*Table structure for table `tbl_role` */

DROP TABLE IF EXISTS `tbl_role`;

CREATE TABLE `tbl_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_role` */

insert  into `tbl_role`(`id`,`name`,`description`) values (1,'Super Administrator','Super Administrator'),(2,'Administrator','Administrator'),(3,'Moderator','Moderator');

/*Table structure for table `tbl_role_module` */

DROP TABLE IF EXISTS `tbl_role_module`;

CREATE TABLE `tbl_role_module` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `module_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK Module Role` (`module_id`),
  KEY `FK User Role` (`role_id`),
  CONSTRAINT `FK User Role` FOREIGN KEY (`role_id`) REFERENCES `tbl_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK Module Role` FOREIGN KEY (`module_id`) REFERENCES `tbl_module` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=210 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_role_module` */

insert  into `tbl_role_module`(`id`,`module_id`,`role_id`) values (192,1,3),(193,2,3),(194,3,3),(195,4,3),(196,5,3),(197,6,3),(198,1,2),(199,2,2),(200,3,2),(201,4,2),(202,5,2),(203,6,2),(204,7,2),(205,10,2),(206,20,2);

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) NOT NULL,
  `status` enum('Active','InActive') COLLATE utf8_unicode_ci DEFAULT 'Active',
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tbl_role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`id`,`name`,`email`,`username`,`password`,`role_id`,`status`) values (7,'Satish Shanker Dongol','satish@amniltech.com','superadmin','amniltech',1,'Active');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
