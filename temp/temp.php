
<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta content="width=300, initial-scale=1" name="viewport">
  <meta name="description" content="Create a new spreadsheet and edit with others at the same time -- from your computer, phone or tablet. Get stuff done with or without an internet connection. Use Sheets to edit Excel files. Free from Google.">
  <meta name="google-site-verification" content="LrdTUW9psUAMbh4Ia074-BPEVmcpBxF6Gwf0MSgQXZs">
  <title>Google Sheets - create and edit spreadsheets online, for free.</title>
  <style>
  @font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 300;
  src: local('Open Sans Light'), local('OpenSans-Light'), url(//fonts.gstatic.com/s/opensans/v13/DXI1ORHCpsQm3Vp6mXoaTYnF5uFdDttMLvmWuJdhhgs.ttf) format('truetype');
}
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local('Open Sans'), local('OpenSans'), url(//fonts.gstatic.com/s/opensans/v13/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
}
  </style>
  <style>
  h1, h2 {
  -webkit-animation-duration: 0.1s;
  -webkit-animation-name: fontfix;
  -webkit-animation-iteration-count: 1;
  -webkit-animation-timing-function: linear;
  -webkit-animation-delay: 0;
  }
  @-webkit-keyframes fontfix {
  from {
  opacity: 1;
  }
  to {
  opacity: 1;
  }
  }
  </style>
<style>
  html, body {
  font-family: Arial, sans-serif;
  background: #fff;
  margin: 0;
  padding: 0;
  border: 0;
  position: absolute;
  height: 100%;
  min-width: 100%;
  font-size: 13px;
  color: #404040;
  direction: ltr;
  -webkit-text-size-adjust: none;
  }
  button,
  input[type=button],
  input[type=submit] {
  font-family: Arial, sans-serif;
  font-size: 13px;
  }
  a,
  a:hover,
  a:visited {
  color: #427fed;
  cursor: pointer;
  text-decoration: none;
  }
  a:hover {
  text-decoration: underline;
  }
  h1 {
  font-size: 20px;
  color: #262626;
  margin: 0 0 15px;
  font-weight: normal;
  }
  h2 {
  font-size: 14px;
  color: #262626;
  margin: 0 0 15px;
  font-weight: bold;
  }
  input[type=email],
  input[type=number],
  input[type=password],
  input[type=tel],
  input[type=text],
  input[type=url] {
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  display: inline-block;
  height: 36px;
  padding: 0 8px;
  margin: 0;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-top: 1px solid #c0c0c0;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  -moz-border-radius: 1px;
  -webkit-border-radius: 1px;
  border-radius: 1px;
  font-size: 15px;
  color: #404040;
  }
  input[type=email]:hover,
  input[type=number]:hover,
  input[type=password]:hover,
  input[type=tel]:hover,
  input[type=text]:hover,
  input[type=url]:hover {
  border: 1px solid #b9b9b9;
  border-top: 1px solid #a0a0a0;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  }
  input[type=email]:focus,
  input[type=number]:focus,
  input[type=password]:focus,
  input[type=tel]:focus,
  input[type=text]:focus,
  input[type=url]:focus {
  outline: none;
  border: 1px solid #4d90fe;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  }
  input[type=checkbox],
  input[type=radio] {
  -webkit-appearance: none;
  display: inline-block;
  width: 13px;
  height: 13px;
  margin: 0;
  cursor: pointer;
  vertical-align: bottom;
  background: #fff;
  border: 1px solid #c6c6c6;
  -moz-border-radius: 1px;
  -webkit-border-radius: 1px;
  border-radius: 1px;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  position: relative;
  }
  input[type=checkbox]:active,
  input[type=radio]:active {
  background: #ebebeb;
  }
  input[type=checkbox]:hover {
  border-color: #c6c6c6;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  }
  input[type=radio] {
  -moz-border-radius: 1em;
  -webkit-border-radius: 1em;
  border-radius: 1em;
  width: 15px;
  height: 15px;
  }
  input[type=checkbox]:checked,
  input[type=radio]:checked {
  background: #fff;
  }
  input[type=radio]:checked::after {
  content: '';
  display: block;
  position: relative;
  top: 3px;
  left: 3px;
  width: 7px;
  height: 7px;
  background: #666;
  -moz-border-radius: 1em;
  -webkit-border-radius: 1em;
  border-radius: 1em;
  }
  input[type=checkbox]:checked::after {
  content: url(https://ssl.gstatic.com/ui/v1/menu/checkmark.png);
  display: block;
  position: absolute;
  top: -6px;
  left: -5px;
  }
  input[type=checkbox]:focus {
  outline: none;
  border-color: #4d90fe;
  }
  .stacked-label {
  display: block;
  font-weight: bold;
  margin: .5em 0;
  }
  .hidden-label {
  position: absolute !important;
  clip: rect(1px 1px 1px 1px); /* IE6, IE7 */
  clip: rect(1px, 1px, 1px, 1px);
  height: 0px;
  width: 0px;
  overflow: hidden;
  visibility: hidden;
  }
  input[type=checkbox].form-error,
  input[type=email].form-error,
  input[type=number].form-error,
  input[type=password].form-error,
  input[type=text].form-error,
  input[type=tel].form-error,
  input[type=url].form-error {
  border: 1px solid #dd4b39;
  }
  .error-msg {
  margin: .5em 0;
  display: block;
  color: #dd4b39;
  line-height: 17px;
  }
  .help-link {
  background: #dd4b39;
  padding: 0 5px;
  color: #fff;
  font-weight: bold;
  display: inline-block;
  -moz-border-radius: 1em;
  -webkit-border-radius: 1em;
  border-radius: 1em;
  text-decoration: none;
  position: relative;
  top: 0px;
  }
  .help-link:visited {
  color: #fff;
  }
  .help-link:hover {
  color: #fff;
  background: #c03523;
  text-decoration: none;
  }
  .help-link:active {
  opacity: 1;
  background: #ae2817;
  }
  .wrapper {
  position: relative;
  min-height: 100%;
  }
  .content {
  padding: 0 44px;
  }
  .main {
  padding-bottom: 100px;
  }
  /* For modern browsers */
  .clearfix:before,
  .clearfix:after {
  content: "";
  display: table;
  }
  .clearfix:after {
  clear: both;
  }
  /* For IE 6/7 (trigger hasLayout) */
  .clearfix {
  zoom:1;
  }
  .google-header-bar {
  height: 71px;
  border-bottom: 1px solid #e5e5e5;
  overflow: hidden;
  }
  .header .logo {
  background-image: url(https://ssl.gstatic.com/accounts/ui/logo_1x.png);
  background-size: 116px 38px;
  background-repeat: no-repeat;
  margin: 17px 0 0;
  float: left;
  height: 38px;
  width: 116px;
  }
  .header .logo-w {
  background-image: url(https://ssl.gstatic.com/images/branding/googlelogo/1x/googlelogo_color_112x36dp.png);
  background-size: 112px 36px;
  margin: 21px 0 0;
  }
  .header .secondary-link {
  margin: 28px 0 0;
  float: right;
  }
  .header .secondary-link a {
  font-weight: normal;
  }
  .google-header-bar.centered {
  border: 0;
  height: 108px;
  }
  .google-header-bar.centered .header .logo {
  float: none;
  margin: 40px auto 30px;
  display: block;
  }
  .google-header-bar.centered .header .secondary-link {
  display: none
  }
  .google-footer-bar {
  position: absolute;
  bottom: 0;
  height: 35px;
  width: 100%;
  border-top: 1px solid #e5e5e5;
  overflow: hidden;
  }
  .footer {
  padding-top: 7px;
  font-size: .85em;
  white-space: nowrap;
  line-height: 0;
  }
  .footer ul {
  float: left;
  max-width: 80%;
  min-height: 16px;
  padding: 0;
  }
  .footer ul li {
  color: #737373;
  display: inline;
  padding: 0;
  padding-right: 1.5em;
  }
  .footer a {
  color: #737373;
  }
  .lang-chooser-wrap {
  float: right;
  display: inline;
  }
  .lang-chooser-wrap img {
  vertical-align: top;
  }
  .lang-chooser {
  font-size: 13px;
  height: 24px;
  line-height: 24px;
  }
  .lang-chooser option {
  font-size: 13px;
  line-height: 24px;
  }
  .hidden {
  height: 0px;
  width: 0px;
  overflow: hidden;
  visibility: hidden;
  display: none !important;
  }
  .banner {
  text-align: center;
  }
  .card {
  background-color: #f7f7f7;
  padding: 20px 25px 30px;
  margin: 0 auto 25px;
  width: 304px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
  -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  }
  .card > *:first-child {
  margin-top: 0;
  }
  .rc-button,
  .rc-button:visited {
  display: inline-block;
  min-width: 46px;
  text-align: center;
  color: #444;
  font-size: 14px;
  font-weight: 700;
  height: 36px;
  padding: 0 8px;
  line-height: 36px;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  -o-transition: all 0.218s;
  -moz-transition: all 0.218s;
  -webkit-transition: all 0.218s;
  transition: all 0.218s;
  border: 1px solid #dcdcdc;
  background-color: #f5f5f5;
  background-image: -webkit-linear-gradient(top,#f5f5f5,#f1f1f1);
  background-image: -moz-linear-gradient(top,#f5f5f5,#f1f1f1);
  background-image: -ms-linear-gradient(top,#f5f5f5,#f1f1f1);
  background-image: -o-linear-gradient(top,#f5f5f5,#f1f1f1);
  background-image: linear-gradient(top,#f5f5f5,#f1f1f1);
  -o-transition: none;
  -moz-user-select: none;
  -webkit-user-select: none;
  user-select: none;
  cursor: default;
  }
  .card .rc-button {
  width: 100%;
  padding: 0;
  }
  .rc-button.disabled,
  .rc-button[disabled] {
  opacity: .5;
  filter: alpha(opacity=50);
  cursor: default;
  pointer-events: none;
  }
  .rc-button:hover {
  border: 1px solid #c6c6c6;
  color: #333;
  text-decoration: none;
  -o-transition: all 0.0s;
  -moz-transition: all 0.0s;
  -webkit-transition: all 0.0s;
  transition: all 0.0s;
  background-color: #f8f8f8;
  background-image: -webkit-linear-gradient(top,#f8f8f8,#f1f1f1);
  background-image: -moz-linear-gradient(top,#f8f8f8,#f1f1f1);
  background-image: -ms-linear-gradient(top,#f8f8f8,#f1f1f1);
  background-image: -o-linear-gradient(top,#f8f8f8,#f1f1f1);
  background-image: linear-gradient(top,#f8f8f8,#f1f1f1);
  -moz-box-shadow: 0 1px 1px rgba(0,0,0,0.1);
  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.1);
  box-shadow: 0 1px 1px rgba(0,0,0,0.1);
  }
  .rc-button:active {
  background-color: #f6f6f6;
  background-image: -webkit-linear-gradient(top,#f6f6f6,#f1f1f1);
  background-image: -moz-linear-gradient(top,#f6f6f6,#f1f1f1);
  background-image: -ms-linear-gradient(top,#f6f6f6,#f1f1f1);
  background-image: -o-linear-gradient(top,#f6f6f6,#f1f1f1);
  background-image: linear-gradient(top,#f6f6f6,#f1f1f1);
  -moz-box-shadow: 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: 0 1px 2px rgba(0,0,0,0.1);
  }
  .rc-button-submit,
  .rc-button-submit:visited {
  border: 1px solid #3079ed;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.1);
  background-color: #4d90fe;
  background-image: -webkit-linear-gradient(top,#4d90fe,#4787ed);
  background-image: -moz-linear-gradient(top,#4d90fe,#4787ed);
  background-image: -ms-linear-gradient(top,#4d90fe,#4787ed);
  background-image: -o-linear-gradient(top,#4d90fe,#4787ed);
  background-image: linear-gradient(top,#4d90fe,#4787ed);
  }
  .rc-button-submit:hover {
  border: 1px solid #2f5bb7;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.3);
  background-color: #357ae8;
  background-image: -webkit-linear-gradient(top,#4d90fe,#357ae8);
  background-image: -moz-linear-gradient(top,#4d90fe,#357ae8);
  background-image: -ms-linear-gradient(top,#4d90fe,#357ae8);
  background-image: -o-linear-gradient(top,#4d90fe,#357ae8);
  background-image: linear-gradient(top,#4d90fe,#357ae8);
  }
  .rc-button-submit:active {
  background-color: #357ae8;
  background-image: -webkit-linear-gradient(top,#4d90fe,#357ae8);
  background-image: -moz-linear-gradient(top,#4d90fe,#357ae8);
  background-image: -ms-linear-gradient(top,#4d90fe,#357ae8);
  background-image: -o-linear-gradient(top,#4d90fe,#357ae8);
  background-image: linear-gradient(top,#4d90fe,#357ae8);
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  }
  .rc-button-red,
  .rc-button-red:visited {
  border: 1px solid transparent;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.1);
  background-color: #d14836;
  background-image: -webkit-linear-gradient(top,#dd4b39,#d14836);
  background-image: -moz-linear-gradient(top,#dd4b39,#d14836);
  background-image: -ms-linear-gradient(top,#dd4b39,#d14836);
  background-image: -o-linear-gradient(top,#dd4b39,#d14836);
  background-image: linear-gradient(top,#dd4b39,#d14836);
  }
  .rc-button-red:hover {
  border: 1px solid #b0281a;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.3);
  background-color: #c53727;
  background-image: -webkit-linear-gradient(top,#dd4b39,#c53727);
  background-image: -moz-linear-gradient(top,#dd4b39,#c53727);
  background-image: -ms-linear-gradient(top,#dd4b39,#c53727);
  background-image: -o-linear-gradient(top,#dd4b39,#c53727);
  background-image: linear-gradient(top,#dd4b39,#c53727);
  }
  .rc-button-red:active {
  border: 1px solid #992a1b;
  background-color: #b0281a;
  background-image: -webkit-linear-gradient(top,#dd4b39,#b0281a);
  background-image: -moz-linear-gradient(top,#dd4b39,#b0281a);
  background-image: -ms-linear-gradient(top,#dd4b39,#b0281a);
  background-image: -o-linear-gradient(top,#dd4b39,#b0281a);
  background-image: linear-gradient(top,#dd4b39,#b0281a);
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.3);
  }
  .secondary-actions {
  text-align: center;
  }
</style>
<style media="screen and (max-width: 800px), screen and (max-height: 800px)">
  .google-header-bar.centered {
  height: 83px;
  }
  .google-header-bar.centered .header .logo {
  margin: 25px auto 20px;
  }
  .card {
  margin-bottom: 20px;
  }
</style>
<style media="screen and (max-width: 580px)">
  html, body {
  font-size: 14px;
  }
  .google-header-bar.centered {
  height: 73px;
  }
  .google-header-bar.centered .header .logo {
  margin: 20px auto 15px;
  }
  .content {
  padding-left: 10px;
  padding-right: 10px;
  }
  .hidden-small {
  display: none;
  }
  .card {
  padding: 20px 15px 30px;
  width: 270px;
  }
  .footer ul li {
  padding-right: 1em;
  }
  .lang-chooser-wrap {
  display: none;
  }
</style>
<style media="screen and (-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3 / 2), (min-device-pixel-ratio: 1.5)">
  .header .logo {
  background-image: url(https://ssl.gstatic.com/accounts/ui/logo_2x.png);
  }
  .header .logo-w {
  background-image: url(https://ssl.gstatic.com/images/branding/googlelogo/2x/googlelogo_color_112x36dp.png);
  }
</style>
<style>
  pre.debug {
  font-family: monospace;
  position: absolute;
  left: 0;
  margin: 0;
  padding: 1.5em;
  font-size: 13px;
  background: #f1f1f1;
  border-top: 1px solid #e5e5e5;
  direction: ltr;
  white-space: pre-wrap;
  width: 90%;
  overflow: hidden;
  }
</style>
<style>
  .banner h1 {
  font-family: 'Open Sans', arial;
  -webkit-font-smoothing: antialiased;
  color: #555;
  font-size: 42px;
  font-weight: 300;
  margin-top: 0;
  margin-bottom: 20px;
  }
  .banner h2 {
  font-family: 'Open Sans', arial;
  -webkit-font-smoothing: antialiased;
  color: #555;
  font-size: 18px;
  font-weight: 400;
  margin-bottom: 20px;
  }
  .signin-card {
  width: 274px;
  padding: 40px 40px;
  }
  .signin-card .profile-img {
  width: 96px;
  height: 96px;
  margin: 0 auto 10px;
  display: block;
  -moz-border-radius: 50%;
  -webkit-border-radius: 50%;
  border-radius: 50%;
  }
  .signin-card .profile-name {
  font-size: 16px;
  font-weight: bold;
  text-align: center;
  margin: 10px 0 0;
  min-height: 1em;
  }
  .signin-card .profile-email {
  font-size: 16px;
  text-align: center;
  margin: 10px 0 20px 0;
  min-height: 1em;
  }
  .signin-card input[type=email],
  .signin-card input[type=password],
  .signin-card input[type=text],
  .signin-card input[type=submit] {
  width: 100%;
  display: block;
  margin-bottom: 10px;
  z-index: 1;
  position: relative;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  }
  .signin-card #Email,
  .signin-card #Passwd,
  .signin-card .captcha {
  direction: ltr;
  height: 44px;
  font-size: 16px;
  }
  .signin-card #Email + .stacked-label {
  margin-top: 15px;
  }
  .signin-card #reauthEmail {
  display: block;
  margin-bottom: 10px;
  line-height: 36px;
  padding: 0 8px;
  font-size: 15px;
  color: #404040;
  line-height: 2;
  margin-bottom: 10px;
  font-size: 14px;
  text-align: center;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  }
  .one-google p {
  margin: 0 0 10px;
  color: #555;
  font-size: 14px;
  text-align: center;
  }
  .one-google p.create-account,
  .one-google p.switch-account {
  margin-bottom: 60px;
  }
  .one-google .logo-strip {
  background-repeat: no-repeat;
  display: block;
  margin: 10px auto;
  background-image: url(https://ssl.gstatic.com/accounts/ui/wlogostrip_230x17_1x.png);
  background-size: 230px 17px;
  width: 230px;
  height: 17px;
  }
</style>
<style media="screen and (max-width: 800px), screen and (max-height: 800px)">
  .banner h1 {
  font-size: 38px;
  margin-bottom: 15px;
  }
  .banner h2 {
  margin-bottom: 15px;
  }
  .one-google p.create-account,
  .one-google p.switch-account {
  margin-bottom: 30px;
  }
  .signin-card #Email {
  margin-bottom: 0;
  }
  .signin-card #Passwd {
  margin-top: -1px;
  }
  .signin-card #Email.form-error,
  .signin-card #Passwd.form-error {
  z-index: 2;
  }
  .signin-card #Email:hover,
  .signin-card #Email:focus,
  .signin-card #Passwd:hover,
  .signin-card #Passwd:focus {
  z-index: 3;
  }
</style>
<style media="screen and (max-width: 580px)">
  .banner h1 {
  font-size: 22px;
  margin-bottom: 15px;
  }
  .signin-card {
  width: 260px;
  padding: 20px 20px;
  margin: 0 auto 20px;
  }
  .signin-card .profile-img {
  width: 72px;
  height: 72px;
  -moz-border-radius: 72px;
  -webkit-border-radius: 72px;
  border-radius: 72px;
  }
</style>
<style media="screen and (-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3 / 2), (min-device-pixel-ratio: 1.5)">
  .one-google .logo-strip {
  background-image: url(https://ssl.gstatic.com/accounts/ui/wlogostrip_230x17_2x.png);
  }
</style>
<style>
  .jfk-tooltip {
  background-color: #fff;
  border: 1px solid;
  color: #737373;
  font-size: 12px;
  position: absolute;
  z-index: 800 !important;
  border-color: #bbb #bbb #a8a8a8;
  padding: 16px;
  width: 250px;
  }
 .jfk-tooltip h3 {
  color: #555;
  font-size: 12px;
  margin: 0 0 .5em;
  }
 .jfk-tooltip-content p:last-child {
  margin-bottom: 0;
  }
  .jfk-tooltip-arrow {
  position: absolute;
  }
  .jfk-tooltip-arrow .jfk-tooltip-arrowimplbefore,
  .jfk-tooltip-arrow .jfk-tooltip-arrowimplafter {
  display: block;
  height: 0;
  position: absolute;
  width: 0;
  }
  .jfk-tooltip-arrow .jfk-tooltip-arrowimplbefore {
  border: 9px solid;
  }
  .jfk-tooltip-arrow .jfk-tooltip-arrowimplafter {
  border: 8px solid;
  }
  .jfk-tooltip-arrowdown {
  bottom: 0;
  }
  .jfk-tooltip-arrowup {
  top: -9px;
  }
  .jfk-tooltip-arrowleft {
  left: -9px;
  top: 30px;
  }
  .jfk-tooltip-arrowright {
  right: 0;
  top: 30px;
  }
  .jfk-tooltip-arrowdown .jfk-tooltip-arrowimplbefore,.jfk-tooltip-arrowup .jfk-tooltip-arrowimplbefore {
  border-color: #bbb transparent;
  left: -9px;
  }
  .jfk-tooltip-arrowdown .jfk-tooltip-arrowimplbefore {
  border-color: #a8a8a8 transparent;
  }
  .jfk-tooltip-arrowdown .jfk-tooltip-arrowimplafter,.jfk-tooltip-arrowup .jfk-tooltip-arrowimplafter {
  border-color: #fff transparent;
  left: -8px;
  }
  .jfk-tooltip-arrowdown .jfk-tooltip-arrowimplbefore {
  border-bottom-width: 0;
  }
  .jfk-tooltip-arrowdown .jfk-tooltip-arrowimplafter {
  border-bottom-width: 0;
  }
  .jfk-tooltip-arrowup .jfk-tooltip-arrowimplbefore {
  border-top-width: 0;
  }
  .jfk-tooltip-arrowup .jfk-tooltip-arrowimplafter {
  border-top-width: 0;
  top: 1px;
  }
  .jfk-tooltip-arrowleft .jfk-tooltip-arrowimplbefore,
  .jfk-tooltip-arrowright .jfk-tooltip-arrowimplbefore {
  border-color: transparent #bbb;
  top: -9px;
  }
  .jfk-tooltip-arrowleft .jfk-tooltip-arrowimplafter,
  .jfk-tooltip-arrowright .jfk-tooltip-arrowimplafter {
  border-color:transparent #fff;
  top:-8px;
  }
  .jfk-tooltip-arrowleft .jfk-tooltip-arrowimplbefore {
  border-left-width: 0;
  }
  .jfk-tooltip-arrowleft .jfk-tooltip-arrowimplafter {
  border-left-width: 0;
  left: 1px;
  }
  .jfk-tooltip-arrowright .jfk-tooltip-arrowimplbefore {
  border-right-width: 0;
  }
  .jfk-tooltip-arrowright .jfk-tooltip-arrowimplafter {
  border-right-width: 0;
  }
  .jfk-tooltip-closebtn {
  background: url("//ssl.gstatic.com/ui/v1/icons/common/x_8px.png") no-repeat;
  border: 1px solid transparent;
  height: 21px;
  opacity: .4;
  outline: 0;
  position: absolute;
  right: 2px;
  top: 2px;
  width: 21px;
  }
  .jfk-tooltip-closebtn:focus,
  .jfk-tooltip-closebtn:hover {
  opacity: .8;
  cursor: pointer;
  }
  .jfk-tooltip-closebtn:focus {
  border-color: #4d90fe;
  }
</style>
<style media="screen and (max-width: 580px)">
  .jfk-tooltip {
  display: none;
  }
</style>
<style>
  .need-help-reverse {
  float: right;
  }
  .remember .bubble-wrap {
  position: absolute;
  padding-top: 3px;
  -o-transition: opacity .218s ease-in .218s;
  -moz-transition: opacity .218s ease-in .218s;
  -webkit-transition: opacity .218s ease-in .218s;
  transition: opacity .218s ease-in .218s;
  left: -999em;
  opacity: 0;
  width: 314px;
  margin-left: -20px;
  }
  .remember:hover .bubble-wrap,
  .remember input:focus ~ .bubble-wrap,
  .remember .bubble-wrap:hover,
  .remember .bubble-wrap:focus {
  opacity: 1;
  left: inherit;
  }
  .bubble-pointer {
  border-left: 10px solid transparent;
  border-right: 10px solid transparent;
  border-bottom: 10px solid #fff;
  width: 0;
  height: 0;
  margin-left: 17px;
  }
  .bubble {
  background-color: #fff;
  padding: 15px;
  margin-top: -1px;
  font-size: 11px;
  -moz-border-radius: 2px;
  -webkit-border-radius: 2px;
  border-radius: 2px;
  -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  }
  #stay-signed-in {
  float: left;
  }
  #stay-signed-in-tooltip {
  left: auto;
  margin-left: -20px;
  padding-top: 3px;
  position: absolute;
  top: 0;
  visibility: hidden;
  width: 314px;
  z-index: 1;
  }
  .dasher-tooltip {
  position: absolute;
  left: 50%;
  top: 380px;
  margin-left: 150px;
  }
  .dasher-tooltip .tooltip-pointer {
  margin-top: 15px;
  }
  .dasher-tooltip p {
  margin-top: 0;
  }
  .dasher-tooltip p span {
  display: block;
  }
</style>
<style media="screen and (max-width: 800px), screen and (max-height: 800px)">
  .dasher-tooltip {
  top: 340px;
  }
</style>
  </head>
  <body>
  <div class="wrapper">
  <div class="google-header-bar  centered">
  <div class="header content clearfix">
  <div class="logo logo-w" aria-label="Google"></div>
  </div>
  </div>
  <div class="main content clearfix">
<div class="banner">
<h1>
  One account. All of Google.
</h1>
  <h2 class="hidden-small">
  Sign in to continue to Sheets
  </h2>
</div>
<div class="card signin-card clearfix">
<img class="profile-img" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" alt="">
<p class="profile-name"></p>
  <form novalidate method="post" action="https://accounts.google.com/ServiceLoginAuth" id="gaia_loginform">
  <input name="Page" type="hidden" value="SignIn">
  <input type="hidden" name="GALX" value="QOdRxiLMKHI">
  <input type="hidden" name="gxf" value="AFoagUV_70xHCDxxfkSho63qFLbu_3Muqg:1452598083383">
  <input type="hidden" name="continue" value="https://docs.google.com/spreadsheets/d/1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w/export?format=csv&amp;id=1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w">
  <input type="hidden" name="followup" value="https://docs.google.com/spreadsheets/d/1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w/export?format=csv&amp;id=1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w">
  <input type="hidden" name="service" value="wise">
  <input type="hidden" name="ltmpl" value="sheets">
  <input type="hidden" id="_utf8" name="_utf8" value="&#9731;"/>
  <input type="hidden" name="bgresponse" id="bgresponse" value="js_disabled">
<label class="hidden-label" for="Email">Email</label>
<input id="Email" name="Email" type="email"
       placeholder="Email"
       value=""
       spellcheck="false"
       class="">
<label class="hidden-label" for="Passwd">Password</label>
<input id="Passwd" name="Passwd" type="password"
       placeholder="Password"
       class="">
<input id="signIn" name="signIn" class="rc-button rc-button-submit" type="submit" value="Sign in">
  <input type="hidden" name="PersistentCookie" value="yes">
  <a id="link-forgot-passwd" href="https://accounts.google.com/RecoverAccount?service=wise&amp;continue=https%3A%2F%2Fdocs.google.com%2Fspreadsheets%2Fd%2F1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w%2Fexport%3Fformat%3Dcsv%26id%3D1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w&amp;hl=en"
       
       >
  Need help?
  </a>
  </form>
</div>
<div class="one-google">
  <p class="create-account">
  <a id="link-signup" href="https://accounts.google.com/SignUp?service=wise&amp;continue=https%3A%2F%2Fdocs.google.com%2Fspreadsheets%2Fd%2F1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w%2Fexport%3Fformat%3Dcsv%26id%3D1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w&amp;ltmpl=sheets">
  Create an account
  </a>
  </p>
<p class="tagline">
  One Google Account for everything Google
</p>
<div class="logo-strip"></div>
</div>
  </div>
  <div class="google-footer-bar">
  <div class="footer content clearfix">
  <ul id="footer-list">
  <li>
  <a href="https://www.google.com/intl/en/about" target="_blank">
  About Google
  </a>
  </li>
  <li>
  <a href="https://accounts.google.com/TOS?loc=NP&hl=en&privacy=true" target="_blank">
  Privacy
  </a>
  </li>
  <li>
  <a href="https://accounts.google.com/TOS?loc=NP&hl=en" target="_blank">
  Terms
  </a>
  </li>
  <li>
  <a href="http://www.google.com/support/accounts?hl=en" target="_blank">
  Help
  </a>
  </li>
  </ul>
  </div>
</div>
  </div>
<script type="text/javascript">
  var gaia_attachEvent = function(element, event, callback) {
  if (element.addEventListener) {
  element.addEventListener(event, callback, false);
  } else if (element.attachEvent) {
  element.attachEvent('on' + event, callback);
  }
  };
  (function() {
  var gaia_hideNavBar = function() {
  setTimeout(function() {
  window.scrollTo(0, 1);
  }, 0);
  };
  gaia_attachEvent(window, 'load', gaia_hideNavBar);
  })();
</script>
  <script type="text/javascript">/* Anti-spam. Want to say hello? Contact (base64) Ym90Z3VhcmQtY29udGFjdEBnb29nbGUuY29t */(function(){eval('var D=this,Y=function(E,z,k,B,y){k=E.split("."),B=D,k[0]in B||!B.execScript||B.execScript("var "+k[0]);for(;k.length&&(y=k.shift());)k.length||void 0===z?B=B[y]?B[y]:B[y]={}:B[y]=z},A=function(E,z){return E<z?-1:E>z?1:0},I,b=function(E,z,k){if(z=typeof E,"object"==z)if(E){if(E instanceof Array)return"array";if(E instanceof Object)return z;if(k=Object.prototype.toString.call(E),"[object Window]"==k)return"object";if("[object Array]"==k||"number"==typeof E.length&&"undefined"!=typeof E.splice&&"undefined"!=typeof E.propertyIsEnumerable&&!E.propertyIsEnumerable("splice"))return"array";if("[object Function]"==k||"undefined"!=typeof E.call&&"undefined"!=typeof E.propertyIsEnumerable&&!E.propertyIsEnumerable("call"))return"function"}else return"null";else if("function"==z&&"undefined"==typeof E.call)return"object";return z},J;a:{var r=D.navigator;if(r){var j2=r.userAgent;if(j2){I=j2;break a}}I=""}var q="",Eb=function(E){return(E=D.document)?E.documentMode:void 0},S=/\\b(?:MSIE|rv)[: ]([^\\);]+)(\\)|;)/.exec(I),zc=(S&&(q=S?S[1]:""),Eb()),BO=zc>parseFloat(q)?String(zc):q,a=function(E,z,k,B,y,g,t,c,X,Z,l,v){if(!(z=yJ[E])){for(z=0,k=String(BO).replace(/^[\\s\\xa0]+|[\\s\\xa0]+$/g,"").split("."),B=String(E).replace(/^[\\s\\xa0]+|[\\s\\xa0]+$/g,"").split("."),y=Math.max(k.length,B.length),g=0;0==z&&g<y;g++){c=B[g]||"",t=k[g]||"",X=RegExp("(\\\\d*)(\\\\D*)","g"),Z=RegExp("(\\\\d*)(\\\\D*)","g");do{if(l=X.exec(t)||["","",""],v=Z.exec(c)||["","",""],0==l[0].length&&0==v[0].length)break;z=A(0==l[1].length?0:parseInt(l[1],10),0==v[1].length?0:parseInt(v[1],10))||A(0==l[2].length,0==v[2].length)||A(l[2],v[2])}while(0==z)}z=yJ[E]=0<=z}return z},kC=D.document,yJ={},gF=kC?Eb()||("CSS1Compat"==kC.compatMode?parseInt(BO,10):5):void 0,Q=(a("9"),new function(){},9<=gF),tq=function(E,z,k,B,y){for(z=[],B=k=0;B<E.length;B++)y=E.charCodeAt(B),128>y?z[k++]=y:(2048>y?z[k++]=y>>6|192:(55296==(y&64512)&&B+1<E.length&&56320==(E.charCodeAt(B+1)&64512)?(y=65536+((y&1023)<<10)+(E.charCodeAt(++B)&1023),z[k++]=y>>18|240,z[k++]=y>>12&63|128):z[k++]=y>>12|224,z[k++]=y>>6&63|128),z[k++]=y&63|128);return z},DG=!a("9"),K=(a("8"),a("9"),function(E,z){this.type=E,this.currentTarget=this.target=z,this.defaultPrevented=false}),L=function(E,z,k,B,y){K.call(this,E?E.type:""),this.relatedTarget=this.currentTarget=this.target=null,this.charCode=this.keyCode=this.button=this.screenY=this.screenX=this.clientY=this.clientX=this.offsetY=this.offsetX=0,this.metaKey=this.shiftKey=this.altKey=this.ctrlKey=false,this.m=this.state=null,E&&(k=this.type=E.type,y=E.relatedTarget,this.currentTarget=z,this.target=E.target||E.srcElement,B=E.changedTouches?E.changedTouches[0]:null,y||("mouseover"==k?y=E.fromElement:"mouseout"==k&&(y=E.toElement)),this.relatedTarget=y,null===B?(this.offsetY=void 0!==E.offsetY?E.offsetY:E.layerY,this.screenY=E.screenY||0,this.clientX=void 0!==E.clientX?E.clientX:E.pageX,this.screenX=E.screenX||0,this.clientY=void 0!==E.clientY?E.clientY:E.pageY,this.offsetX=void 0!==E.offsetX?E.offsetX:E.layerX):(this.screenY=B.screenY||0,this.screenX=B.screenX||0,this.clientY=void 0!==B.clientY?B.clientY:B.pageY,this.clientX=void 0!==B.clientX?B.clientX:B.pageX),this.altKey=E.altKey,this.keyCode=E.keyCode||0,this.shiftKey=E.shiftKey,this.m=E,this.charCode=E.charCode||("keypress"==k?E.keyCode:0),this.ctrlKey=E.ctrlKey,this.metaKey=E.metaKey,this.state=E.state,this.button=E.button,E.defaultPrevented&&this.preventDefault())},YC=(K.prototype.preventDefault=function(){this.defaultPrevented=true},function(){function E(){}E.prototype=K.prototype,L.Pu=K.prototype,L.prototype=new E,L.xU=function(E,k,B,y,g){for(y=Array(arguments.length-2),g=2;g<arguments.length;g++)y[g-2]=arguments[g];return K.prototype[k].apply(E,y)}}(),L.prototype.preventDefault=function(E){if(L.Pu.preventDefault.call(this),E=this.m,E.preventDefault)E.preventDefault();else if(E.returnValue=false,DG)try{if(E.ctrlKey||112<=E.keyCode&&123>=E.keyCode)E.keyCode=-1}catch(z){}},"closure_listenable_"+(1E6*Math.random()|0)),Xv=function(E,z,k,B,y){this.H=!!B,this.src=z,this.type=k,this.g=null,this.V=y,this.listener=E,this.key=++cO,this.v=this.K=false},Aq=function(E){E.V=null,E.src=null,E.listener=null,E.g=null,E.v=true},w=function(E){this.o=0,this.F={},this.src=E},cO=(w.prototype.add=function(E,z,k,B,y,g,t,c){g=E.toString(),E=this.F[g],E||(E=this.F[g]=[],this.o++);a:{for(t=0;t<E.length;++t)if(c=E[t],!c.v&&c.listener==z&&c.H==!!B&&c.V==y)break a;t=-1}return-1<t?(z=E[t],k||(z.K=false)):(z=new Xv(z,this.src,g,!!B,y),z.K=k,E.push(z)),z},0),u="closure_lm_"+(1E6*Math.random()|0),ZG=function(E){return E in P?P[E]:P[E]="on"+E},lE=function(E){return E=E[u],E instanceof w?E:null},Iu=function(E,z){return E=vO,z=Q?function(k){return E.call(z.src,z.listener,k)}:function(k){if(k=E.call(z.src,z.listener,k),!k)return k}},qV=function(E,z,k,B,y,g,t,c,X,Z){if(B=E.V||E.src,k=E.listener,E.K&&"number"!=typeof E&&E&&!E.v)if((y=E.src)&&y[YC])y.CP(E);else if(t=E.g,g=E.type,y.removeEventListener?y.removeEventListener(g,t,E.H):y.detachEvent&&y.detachEvent(ZG(g),t),rF--,g=lE(y)){if(t=E.type,c=t in g.F){c=g.F[t];b:if("string"==typeof c)X="string"==typeof E&&1==E.length?c.indexOf(E,0):-1;else{for(X=0;X<c.length;X++)if(X in c&&c[X]===E)break b;X=-1}(Z=0<=X)&&Array.prototype.splice.call(c,X,1),c=Z}c&&(Aq(E),0==g.F[t].length&&(delete g.F[t],g.o--)),0==g.o&&(g.src=null,y[u]=null)}else Aq(E);return k.call(B,z)},rF=0,vO=function(E,z,k,B,y){if(E.v)returntrue;if(!Q){if(!(k=z))a:{for(B=D,k=["window","event"];y=k.shift();)if(null!=B[y])B=B[y];else{k=null;break a}k=B}return k=new L(k,this),B=true,B=qV(E,k)}return qV(E,new L(z,this))},W=function(E,z,k,B,y,g,t){if("array"==b(z))for(g=0;g<z.length;g++)W(E,z[g],k,B,y);else if(k=S2(k),E&&E[YC])E.KP(z,k,B,y);else{if(!z)throw Error("Invalid event type");if(g=!!B,!g||Q)if((t=lE(E))||(E[u]=t=new w(E)),k=t.add(z,k,false,B,y),!k.g){if(B=Iu(),B.listener=k,B.src=E,k.g=B,E.addEventListener)E.addEventListener(z.toString(),B,g);else if(E.attachEvent)E.attachEvent(ZG(z.toString()),B);else throw Error("addEventListener and attachEvent are unavailable.");rF++}}},P={},h="__closure_events_fn_"+(1E9*Math.random()>>>0),f=function(E){try{au(this,E)}catch(z){n(this,z)}},e={},QJ=function(E,z){E.Z.push(E.B.slice()),E.B[E.M]=void 0,C(E,E.M,z)},mB=(J=f.prototype,J.D=227,J.P=194,f.prototype.j=function(E,z,k,B){B=this.L(this.P),E=[E,B>>8&255,B&255],void 0!=k&&E.push(k),0==this.L(this.c).length&&(this.B[this.c]=void 0,C(this,this.c,E)),k="",z&&(z.message&&(k+=z.message),z.stack&&(k+=":"+z.stack)),z=this.L(this.I),3<z&&(k=k.slice(0,z-3),z-=k.length+3,k=tq(k.replace(/\\r\\n/g,"\\n")),T(this,this.S,N(k.length,2).concat(k),this.SN)),C(this,this.I,z)},function(E,z,k,B,y,g,t,c,X){return B=function(){return k()},y=f.prototype,X=y.j,c=f,g=y.l,t=y.U,k=function(E,l,v){for(v=0,E=B[y.f],l=E===z,E=E&&E[y.f];E&&E!=g&&E!=t&&E!=c&&E!=X&&20>v;)v++,E=E[y.f];return k[y.Wu+l+!(!E+(v+3>>3))]},B[y.u]=y,k[y.kU]=E,E=void 0,B}),K9=(J.SN=12,function(E,z,k,B,y,g,t){for(k=0,z=[];k<E.length;){if(B=p[E.charAt(k++)],y=k<E.length?p[E.charAt(k)]:0,++k,g=k<E.length?p[E.charAt(k)]:64,++k,t=k<E.length?p[E.charAt(k)]:64,++k,null==B||null==y||null==g||null==t)throw Error();z.push(B<<2|y>>4),64!=g&&(z.push(y<<4&240|g>>2),64!=t&&z.push(g<<6&192|t))}return z}),F=(J.u="toString",function(E,z){for(z=Array(E);E--;)z[E]=255*Math.random()|0;return z}),N=function(E,z,k,B){for(B=z-1,k=[];0<=B;B--)k[z-1-B]=E>>8*B&255;return k},p=(J.J=203,{}),n=function(E,z){E.R=("E:"+z.message+":"+z.stack).slice(0,2048)},au=function(E,z){E.B=[],C(E,E.M,0),C(E,E.P,0),C(E,E.a2,E),C(E,E.D,0),C(E,E.I,2048),C(E,E.w,0),C(E,E.J,{}),C(E,E.OW,"object"==typeof window?window:D),C(E,E.jN,[]),C(E,E.c,[]),E.A=true,C(E,E.S,F(4)),C(E,E.a,[]),C(E,E.$,0),C(E,E.s,E.s),C(E,39,function(E){R(E,1)}),C(E,90,function(E,B,y,z,t){B=O(E),y=O(E),z=O(E),B=E.L(B),t=E.L(O(E)),y=E.L(y),z=E.L(z),0!==B&&W(B,y,L9(E,z,t,true))}),C(E,32,function(E,B,y,z,t,c,X){B=U(E),t=B.h,z=B.G,y=B.W,X=y.length,0==X?c=new z[t]:1==X?c=new z[t](y[0]):2==X?c=new z[t](y[0],y[1]):3==X?c=new z[t](y[0],y[1],y[2]):4==X?c=new z[t](y[0],y[1],y[2],y[3]):E.j(E.N),C(E,B.b,c)}),C(E,107,function(){}),C(E,4,function(E,B,z,g,t,c,X){B=U(E),t=B.h,g=B.G,z=B.W,X=z.length,0==X?c=g[t]():1==X?c=g[t](z[0]):2==X?c=g[t](z[0],z[1]):3==X?c=g[t](z[0],z[1],z[2]):E.j(E.N),C(E,B.b,c)}),C(E,117,function(E,B,z,g,t,c){if(B=O(E),z=O(E),g=O(E),t=O(E),B=E.L(B),z=E.L(z),g=E.L(g),E=E.L(t),"object"==b(B)){for(c in t=[],B)t.push(c);B=t}for(t=0,c=B.length;t<c;t+=g)z(B.slice(t,t+g),E)}),C(E,13,function(E,B,z){B=O(E),z=O(E),C(E,z,E.L(z)-E.L(B))}),C(E,6,function(E){V(E,1)}),C(E,41,function(E,B,z,g){B=O(E),z=O(E),g=O(E),C(E,g,E.L(B)<<z)}),C(E,124,function(E,B,z,g,t){for(B=O(E),z=O(E)<<8|O(E),g=Array(z),t=0;t<z;t++)g[t]=O(E);C(E,B,g)}),C(E,44,function(E,B,z,g){B=O(E),z=O(E),g=O(E),E.L(B)>E.L(z)&&C(E,g,E.L(g)+1)}),C(E,115,function(E,B){B=U(E),C(E,B.b,B.h.apply(B.G,B.W))}),C(E,14,function(E,B,z,g){B=O(E),z=O(E),g=O(E),C(E,g,(E.L(B)in E.L(z))+0)}),C(E,43,function(E){G(E,0)}),C(E,51,function(E){G(E,7)}),C(E,54,function(E,B,z){B=O(E),z=O(E),0!=E.L(B)&&C(E,E.M,E.L(z))}),C(E,123,function(E){R(E,2)}),C(E,118,function(E,B,z,g){B=O(E),z=O(E),g=E.L(O(E)),z=E.L(z),C(E,B,L9(E,z,g))}),C(E,1,function(E,z,y){z=O(E),y=O(E),C(E,y,E.L(y)+E.L(z))}),C(E,29,function(E,z,y){z=O(E),y=O(E),z=E.L(z),C(E,y,z)}),C(E,33,function(E,z,y,g,t,c,X,Z,l,v){if(z=O(E),y=O(E)<<8|O(E),g="",void 0!=E.B[E.X])for(t=E.L(E.X);y--;)c=t[O(E)<<8|O(E)],g+=c;else{for(g=Array(y),t=0;t<y;t++)g[t]=O(E);for(y=[],c=t=0;t<g.length;)X=g[t++],128>X?y[c++]=String.fromCharCode(X):191<X&&224>X?(Z=g[t++],y[c++]=String.fromCharCode((X&31)<<6|Z&63)):239<X&&365>X?(Z=g[t++],l=g[t++],v=g[t++],X=((X&7)<<18|(Z&63)<<12|(l&63)<<6|v&63)-65536,y[c++]=String.fromCharCode(55296+(X>>10)),y[c++]=String.fromCharCode(56320+(X&1023))):(Z=g[t++],l=g[t++],y[c++]=String.fromCharCode((X&15)<<12|(Z&63)<<6|l&63));g=y.join("")}C(E,z,g)}),C(E,9,function(E,z){z=E.L(O(E)),QJ(E,z)}),C(E,69,function(E,z,y,g){z=O(E),y=O(E),g=O(E),C(E,g,E.L(z)||E.L(y))}),C(E,31,function(E,z,y,g){if(z=E.Z.pop()){for(y=O(E);0<y;y--)g=O(E),z[g]=E.B[g];z[E.c]=E.B[E.c],E.B=z}else C(E,E.M,E.O.length)}),C(E,73,function(E,z,y){z=O(E),y=O(E),C(E,y,function(E){return eval(E)}(E.L(z)))}),C(E,47,function(E,z,y,g){z=O(E),y=O(E),g=O(E),C(E,g,E.L(z)|E.L(y))}),C(E,56,function(E,z,y,g){z=O(E),y=O(E),g=O(E),E.L(z)[E.L(y)]=E.L(g)}),C(E,35,function(E){R(E,4)}),C(E,120,function(E,z,y,g){z=O(E),y=O(E),g=O(E),E.L(z)==E.L(y)&&C(E,g,E.L(g)+1)}),C(E,85,function(E){V(E,4)}),C(E,36,function(E,z,y){z=O(E),y=O(E),z=E.L(z),C(E,y,b(z))}),C(E,125,function(E,z,y){z=O(E),y=O(E),C(E,y,E.L(y)%E.L(z))}),C(E,16,function(E,z,y,g){z=O(E),y=O(E),g=O(E),C(E,g,E.L(z)>>y)}),C(E,38,function(E,z,y){z=O(E),y=O(E),C(E,y,E.L(y)*E.L(z))}),C(E,2,function(E){G(E,4)}),C(E,94,function(E,z,y){z=O(E),y=O(E),C(E,y,""+E.L(z))}),C(E,99,function(){}),C(E,95,function(E){V(E,2)}),C(E,111,function(E){G(E,3)}),C(E,25,function(E,z,y,g){z=O(E),y=O(E),g=O(E),y=E.L(y),z=E.L(z),C(E,g,z[y])}),wF(),z&&"!"==z.charAt(0)?E.R=z:(E.O=K9(z),E.O&&E.O.length?(E.Z=[],E.l()):E.j(E.LP))},uE=(J.X=226,J.S=135,J.w=247,J.a=153,function(E,z,k,B){return k=E.L(E.M),E.O&&k<E.O.length?(C(E,E.M,E.O.length),QJ(E,z)):C(E,E.M,z),B=E.l(),C(E,E.M,k),B}),M=function(E,z){return E[z]<<24|E[z+1]<<16|E[z+2]<<8|E[z+3]},PO=function(E,z,k,B){try{for(B=0;84941944608!=B;)E+=(z<<4^z>>>5)+z^B+k[B&3],B+=2654435769,z+=(E<<4^E>>>5)+E^B+k[B>>>11&3];return[E>>>24,E>>16&255,E>>8&255,E&255,z>>>24,z>>16&255,z>>8&255,z&255]}catch(y){throw y;}},wF=(J.a2=248,f.prototype.L=function(E,z){if(z=this.B[E],void 0===z)throw this.j(this.Bu,0,E),this.T;return z()},f.prototype.TH=function(E,z,k,B){try{B=E[(z+2)%3],E[z]=E[z]-E[(z+1)%3]-B^(1==z?B<<k:B>>>k)}catch(y){throw y;}},function(E){for(E=0;64>E;++E)e[E]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(E),p["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(E)]=E;e[64]="",p["+"]=62,p["/"]=63,p["="]=64}),C=(J.kU=36,f.prototype.JG=function(E,z){z.push(E[0]<<24|E[1]<<16|E[2]<<8|E[3]),z.push(E[4]<<24|E[5]<<16|E[6]<<8|E[7]),z.push(E[8]<<24|E[9]<<16|E[10]<<8|E[11])},function(E,z,k){if(z==E.M||z==E.P)E.B[z]?E.B[z].R2(k):E.B[z]=WO(k);else if(z!=E.a&&z!=E.S&&z!=E.c||!E.B[z])E.B[z]=mB(k,E.L);z==E.$&&(E.i=void 0,C(E,E.M,E.L(E.M)+4))}),U=(J.$=196,J.c=128,J.Wu=34,J.EW=42,f.prototype.fP=function(E,z,k,B){if(3==E.length){for(k=0;3>k;k++)z[k]+=E[k];for(k=0,B=[13,8,13,12,16,5,3,10,15];9>k;k++)z[3](z,k%3,B[k])}},J.I=132,J.tG=31,J.jN=173,J.N=22,J.Bu=30,function(E,z,k,B,y,g){for(z={},k=O(E),z.b=O(E),z.W=[],B=O(E)-1,y=O(E),g=0;g<B;g++)z.W.push(O(E));for(z.h=E.L(k),z.G=E.L(y);B--;)z.W[B]=E.L(z.W[B]);return z}),O=function(E,z,k){if(z=E.L(E.M),!(z in E.O))throw E.j(E.tG),E.T;return void 0==E.i&&(E.i=M(E.O,z-4),E.C=void 0),E.C!=z>>3&&(E.C=z>>3,k=[0,0,0,E.L(E.$)],E.pP=PO(E.i,E.C,k)),C(E,E.M,z+1),E.O[z]^E.pP[z%8]},L9=function(E,z,k,B){return function(){if(!B||E.A)return C(E,E.eN,B?[arguments[0].m]:arguments),C(E,E.J,k),uE(E,z)}},T=(J.MK=33,J.cu=15,J.eN=217,J.OW=143,J.T={},function(E,z,k,B,y,g){for(y=E.L(z),z=z==E.S?function(z,B,k,g){if(B=y.length,k=B-4>>3,y.I2!=k){y.I2=k,k=(k<<3)-4,g=[0,0,0,E.L(E.D)];try{y.FD=PO(M(y,k),M(y,k+4),g)}catch(l){throw l;}}y.push(y.FD[B&7]^z)}:function(E){y.push(E)},B&&z(B&255),g=0,B=k.length;g<B;g++)z(k[g])}),WO=(J.zH=21,J.f="caller",J.sW=10,J.s=235,J.LP=17,J.M=197,J.Y=133,function(E,z,k){return k=function(){return E},z=function(){return k()},z.R2=function(z){E=z},z}),S2=(J=f.prototype,function(E){if("function"==b(E))return E;return E[h]||(E[h]=function(z){return E.handleEvent(z)}),E[h]}),V=(J.vu=function(E){return(E=window.performance)&&E.now?function(){return E.now()|0}:function(){return+new Date}}(),function(E,z,k,B){for(k=O(E),B=0;0<z;z--)B=B<<8|O(E);C(E,k,B)}),R=(J.QC=function(E,z,k,B,y,g){for(k=[],g=B=0;g<E.length;g++)for(y=y<<z|E[g],B+=z;7<B;)B-=8,k.push(y>>B&255);return k},J.U=function(E,z,k,B,y,g,t,c,X,Z,l,v,bE,H,Jq,d,x,m){if(this.R)return this.R;try{for(this.A=false,z=this.L(this.a).length,k=this.L(this.S).length,B=this.L(this.I),this.B[this.Y]&&uE(this,this.L(this.Y)),y=this.L(this.c),0<y.length&&T(this,this.a,N(y.length,2).concat(y),this.cu),g=this.L(this.w)&511,g-=this.L(this.a).length+5,t=this.L(this.S),4<t.length&&(g-=t.length+3),0<g&&T(this,this.a,N(g,2).concat(F(g)),this.sW),4<t.length&&T(this,this.a,N(t.length,2).concat(t),this.EW),c=F(2).concat(this.L(this.a)),c[1]=c[0]^3,g=0,y=[];g<c.length;g+=3)Z=c[g],v=(l=g+1<c.length)?c[g+1]:0,H=(bE=g+2<c.length)?c[g+2]:0,x=H&63,Jq=(Z&3)<<4|v>>4,d=(v&15)<<2|H>>6,t=Z>>2,bE||(x=64,l||(d=64)),y.push(e[t],e[Jq],e[d],e[x]);if(X=y.join(""))X="!"+X;else for(Z=0,X="";Z<c.length;Z++)m=c[Z][this.u](16),1==m.length&&(m="0"+m),X+=m;this.L(this.a).length=z,this.L(this.S).length=k,C(this,this.I,B),E=X,this.A=true}catch(hq){n(this,hq),E=this.R}return E},J.gI=function(E,z,k){return z^=z<<13,z^=z>>17,(z=(z^z<<5)&k)||(z=1),E^z},J.l=function(E,z,k,B,y,g){try{for(B=0,z=5001,k=void 0,E=this.O.length;--z&&(B=this.L(this.M))<E;)try{C(this,this.P,B),y=O(this),(k=this.L(y))&&k.call?k(this):this.j(this.zH,0,y)}catch(t){t!=this.T&&(g=this.L(this.s),g!=this.s?(C(this,g,t),C(this,this.s,this.s)):this.j(this.N,t))}z||this.j(this.MK)}catch(t){try{this.j(this.N,t)}catch(c){n(this,c)}}return this.L(this.J)},function(E,z,k,B){k=O(E),B=O(E),T(E,B,N(E.L(k),z))}),G=(J.NK=function(E,z){return z=this.U(),E&&E(z),z},function(E,z,k,B,y,g){B=z&3,k=z&4,y=O(E),g=O(E),y=E.L(y),k&&(y=tq((""+y).replace(/\\r\\n/g,"\\n"))),B&&T(E,g,N(y.length,2)),T(E,g,y)});J.$U=function(E,z,k,B,y){for(y=B=0;y<E.length;y++)B+=E.charCodeAt(y),B+=B<<10,B^=B>>6;return B+=B<<3,B^=B>>11,E=B+(B<<15)>>>0,B=new Number(E&(1<<z)-1),B[0]=(E>>>z)%k,B};try{W(window,"unload",function(){})}catch(E){}Y("botguard.bg",f),Y("botguard.bg.prototype.invoke",f.prototype.NK);')})()</script>
  <script type="text/javascript">
  document.bg = new botguard.bg('GN7zqzjvKMqVAXNJZldOoQkfuTqUY30+OUCw5/s1HGAcsh0m7Olp65XKj12n1HaC69D5mImBLhZ02bfX7aTx6xEQjhch5DZXpfUt8KcmY7YQ73NasUnaTyFxhlBADRzzBtR+3gGOTmIF5k8ciyqYvMwS84erpE27v/CrfDF0mOn/2/dgzMf6MjJWcFQ7ryPAA3XldW9fxJqXaKCgGKCF6+osArSdAUchZtV+x2bQCPrhWKNtftzGKMduX0jdM4237K6/zjRWcFghA29fbbN7AqUVkEIwnIYLthtlDcEa8PoWTqaPgVCZI2+cUxf4cL2/cB5dU3zFN2IJ/opWhEFb/OjKmJ4HOhLyTBHSwYy8d6BjTwjAK1TX7fC0M/ldcfZN5t46TE0PjPBZYuIzZw13NuTX+3roq/L643W/78kXuys+qyJDPFQsSCxdmwFcY414OZXZDya2GBG3Cz9K/oJwjlufhCqtypumK6D8agNazqCSo9ggnX+cBtTJFl1siGjE+qA7tKfi4sbqrcHqSfkkKOooVHAIkaGeqZM6+SKaSGPo8uHTyM41vtXphErHhQhZYtBOCR37fwCjPPmhUUWV/BrO0E02S9xOdF1aixBeKmCYzNJJGJT9Q2Yj2kzUGOX1qb0heQZC8/NgL6yoc7LYcZ1iQFMYeRRBz/On599yqR7t9M1LXSj7sguIBXAcaLy3Tt8B9EUr4tZsy/ZzIQHyKH2PYhmO+ib7zd5QYiQjlV0jDhukjacUynh6Cf0mL84mhEMavb8xiMkV2Wvj4+nEaylsOZdkz412pzt/ni0egHEaieeBJtJaP1+MWXziN52o3vHRacWib+oRLTt2q3co+9COovI5lopA7tpLit/NddFpY/x3f7hheBsLDXOvMLY3O8E/ia4urOEo1FwVwB9Yiy/0la4uZMY1/L3P54JlzFW+4/f2oxaYCa3xhHz9rxnZuJMYVq0oJY9aG4AhUyRIHuCtsWa0nrLfuFUuq6QVX967XeaxHIQ6pvdMt4B7xzTcITWEHYRtzFiqmgnZIUBJ3tRdmlazgSLUb5LPfI9j6EMmJYzFlK7PNuuznVvtYz3sTXhzV2ykOCKG0gmIXz4qyTo/aaISrbFrQF/QcK3UaWPZjLl2CnuO4qvOMV8+6BPVnawgkLzYmJq35tZKHOtKzSRakU7AAxSQt3kdtxNzeKjzxi+kcXHnTABHYH26z+JeyPb04aMMMq27Rzp6RWmCzPY2YeYTqqpf7duKAHgPwB8OECmNNs1fOKrI0uSsVPtHjYaG4POq459//H4Zyk9iNY0qTX4XHAdkmTFekSineYYY+cIGlaTrbFeWrJ9loC+/3GDK84M7V1axToZbGzT0CeR5QOeTNJoM1DW0S6lF9L7DQnGyII8AdyPYirDjjObXUyBDHZhV5Zj/uw/9rCpIE7qsptv/yuoqXHNH0XgBx2xAZGdWjK6dAIh5AabkDiu7cfOsKpT3mPSB7Hg4iTN0M2hkBvaux72K6NvLOML/qByC/emRNEVKPGUT49/qkKzgtDGsq6HjfjHFA5FyYm4cWBKGREeNk0YAqfneNwbX2lJgkAVE8yhIV6RtFiXhgAsC9gbq3iLW4lP+JiLsegvyf2O+rPtlFqKFhDuDG/JheUFnE3RPlNWilVW/2axdgPFZ/JX2IHHWfpjdbIYEFWDRS1zWvIT/IcHEaRgMbJQqThhHc6/biynB34C5Kd2MM7VJBVkAN1YlwW9ampTSZogiXZ3uvYXLZl9oIqyAh07SutFwL7SGhWEZQXeBzGsTPOjNapmcnglQi7aPk0WVggmPA8BZ00wiaGDYo0VZ2KV4Lahq4eCfdLgHrjvP7KO1Dhdlvrcq63yNO2qVnSGfC92Un4rywiQ6MYziWllpq+XG8s1RYp5jgNRz9AnObw0wfmjbLrVf1TuM536br3ao5X/aMKCbebJyDABYK7r04T79R+Fcqc+K8o6f7EfxRVuytt+T/EUatxbPWIiic7URc5ruqm2Q4GO1KvJO/0jtVMsidcDRt1EHq6Dye6IiG+dQk+iQTx8ROusFaCjkhI34F2jCER46GauukXP9H/8I8a/MyzsbG5IQL3MRwO4yZQn//5M7+GyI7Pcr6mkEZ4gZoeeUGKeMB+XL0hpNmHeVpQCLGUz8tLpf8UaBekLaz9gamxpKzDcVctlop1VlIU7wgPniBLrZInrbbrgOfNDctdVRVsEF68xC7BLOFEikxXd9mhcmWlR416mkL2M1Scxl+/wftThsYHTuCKozU6Vc1BvOYnxEKxluOqWwB+BdUIENaAG1TLEG3isJ7QbmgRXkMKDmasTEIxhF/XAro/0y5qzDzJ9ESSWK43l/AT1BAAidYe+AgcjaqS/4zbQvDqVT9/xcwtAOsKqlls8NkkkS7Ay/N/P2PVfMHEOzj8ggmWtzqd+/iZcNoJaiMc4kcfeeF6vJWrl54ygnRHekTTI1x1OXikZH5ITay6zZbziMMu4eTq+gu0V9zYlCQeeKtG9Rls5glLA/6UwvmJi7rE5PO8VijtN9680j4li7LG2qpY2a9DyAG3KwVWZLMCeLwiQkDksGkQm8DggUvHZeC4k+klU5EjYGQ8IuNd4H5o73mzidUtQ7xzHGUAivQHKL4LnIXdq1NuZ032RXo7VtUrmiz44BnYT2nz0TGEf+vC/9HDTJNrGr1bj8puw2145W2olSehb67oXd+OHW/tWkvDUGQWeU4eohtZjQuk9HXrqsRTfl8qxb8leSVgEDRxDwIRpO9npQPlVkU+hGNso3NeqnNpQVsmo1DSuQuuK7G80oLgLsKOuFhKtussGx6IFPWqeKoD4YlwbHwa1THlH7xvum/t8Ya3iAs7TSeAki4UjQW0GdvwbeviEgZRbAzKg3b0r0C0KtV50flI68FtTlJtBUFvv8MOY22Mp73/mZkVaHSR69TLYvRqY6xqgMl8B0Tf8a9egp1mmOfVu+1X3qLpcs1JBRB3gAJuRS2r9A4N/x9N2WktsibPPW0nBNEhMTjgs87Yyuvio2yEln+8ddg3EW8Q+rIMV9nbO2IvdADjptez3U9x7avo5JkiZsHdqNVCqv48G6knSmxklGbqmXgCoCs8Hf/Hgzagb9PN6hjvx7c/U0lP1Zcrw46etMARKecWHLwE+mpOZloEl6DdPXkjvHkPGIpSN1wtjsQ5aOY6QggSTbxl49gJj6zBp/LydWcitgOfmvwU4rs+985dmTjs52Z6AyskYTuuS+vai2Kh/iKIv4Aq9prUZHGRa7GmYQHe86HmNw9FDj7JUA05ybrEf2QpgFJEvlgcUZlYCueaOzQ1g5WuWdKLn6mzllC7uKYrlWSWr002436DrsyCXyyBlscY14T3sePUU4UlBhUMNg7NX7KZMR99M4BA/wktz6i3vkF7+65bQe0BiKDZFakWXqcQuzdL/N5cTZsV5L/XAq4rkmr8zZczB03Rp5+h4BgNud9jdIZdBOszTx+WxsmDvGiI+vsT8VYnutKbeSn3Xl0C8UvI1unjdW9BR5jVQcab5k32N/Goa6Akmx9XSsVk6lJ92HYs6Z4l3V5nRjQ7uCRPAwfTCYqWskNuF/5JwTnn4tJUKFhEEGJmDa7oGXVqFYWKdfOKrIjYBdPyVZv90ScTiChVqnnHYBF0CeU35ZWUF47H+RFm3Amzv+IvSN6lh/OjZ+O4RtEVhi2cp4MC1bhYZZW39JxebDBSMMTFkda2tWsu30sIaSWtLlbIKo+E/+Mqf00I0hB1APhKpCrRKPDatP71xm7MKbBXO1GJkSgMtX2M6QQdSZtB+jN7cAEZtXoPzMW+Kbx/T53CcfIffCcW7oLMuhvUCcuy4id+Lqmrlhk3sKhipED+yuVLB0Rh4PY/UxF23hDhMJ21nuwT/gBx+0M22RGrlQaqdaf7zi860injdbygEBkPYwiGepl5t1dyU7+TmfJDxy7NBkKUmSd3OVl1IBBucEKbWwrStHP9UCd8Erj4Ujr+aYOQp0yTfV2YY4oaHkGfeyn7EoIrHmPHAt7dzln4L01CstJEYZWIlpzXwcuF+RxBpAMFwE+NBfiKMZyWtQ42BZxw1BmbIi4b/7jJM+zK2Che3Q17mfiIkaa4VUmULxQVyWPqvFlmvZAuw/QwTeFxPNrjveyhF9iShHBx/4eVxjUYDVfTCeKktwjnUKBmpYWvCWua4RqF91lSK79WNQwDbsNMR4WbxGQTgQKn0t5uoZxEm+rzUsSain75V5hjPEfg7QCoYt7tLTmF0TiKR5NUrk4u75nqkIJS3hoZujmXSOmSQA2EGT7XJ+dPPY0W9+nbO2r5Z1YsIfVQLZiVJIDdxy/6TzhbeDuHOReghSy0Vb6y/SxPBrZ6Z/znAB5GMhQBKpu3P4O1coNXOPP1pNUYYMDCWAw8acl/1lHwM5e4ml2moLNmabuIHV655NLPzm6SmmpK9xz5PFCuvyq14s+I6JpjdMAiAw9SMI5+oLVAm4wgk+zC9AfN3ZQXiu4ncCmSActoQ+rLpJyYBNANM/YP4Fi9C+T5kdidNVpZRLNeJ7z5bgnwyIlWheintBT5it/q65vWMSOogm6Zqz9ZCocU4P+/EaGcQZxdoSSQxBZ/x/gmuOPccCPymZ1TEi3kEoFoN/uow6T4Jy/M0xCYgR1RkeXirCf9X3bNgAyTo3iCGmFQhahhbx3xFy01wAIDlhVlERjHt/k///0BUSvSlrmZVEJm8qV2BmKHabvdw/cixjHgsOHsiM7rfTMuwGXGkM6BPZV8LvbqP6Iwgaby7B+LtHMjN9Dnb0HTjGJaJqKMBTHqUFA/FyRP/isMp707e50/C0lxxfrYGp/3yh8D6/8YaL3JUqWuSrmcHmKdqgZ6Rf+GcEssfQoJndy4q92NJpQ/rtmJtFIMxCl7U01u4C/SyOTJc8Dh5DOBRzkIzvps3avJ2STWVhos0R5tSWbiwsEuobMKcQP5RttuqwADEce/kzZiw2BSCujWIEdnyL++m5fMGVpNHe3+AwZQMTo1WTTi46jS+E43jM/tLgmFsaDlRwQct2Pp8rgBjKh1KiViXITlA6KI/Fmkv02golUG/mVBVg4/tFvQsmZES6yOgD+lMV7zaiU4LD9IEYdUqxhOj9938JQF0M4/f+hbcOBC44y8ut9ZRVAdFlaDHR3p0rC+gpJDSp5N9rFpg+t+JEnhhD+H40SzhylF8eFTKH1jfm1Ru1SUAM7yT9B0bYpwAQVKmiPEpBKhaN6bKvwoWF2/BgaLK365h5J0//5xohLG9CI1iEe8l8PcqM3MNda+LWUpz7/i+01iA49odWz/0EF9U/DhefhCgJOxnOJVkcTY7gM6suqSfKHIGtjXCzMXxTLpYCqRGGTYq8lNBpP4KbdprJEYatIfHlNwGA9Ya9RAeFwgJzg9tBskELG3tG0D14jadHtkyxlO2927Y8Pqx0+aqYlS5ZppcTAeMuKOvaePRRpuHCcaAtwbhdrs7vvVPk9+8O8k06e/OnO4XDX1o1yiBxqA8Jbr/vxLaEIWv/RH8JOVZkW8WdWl5eLIbwmil4vXa72aZvI5O1n0xUijOmgkJTYpEHTS2QcEC0Ge6yZp1HYonUR2KgTdITvDw9aCzgSX5y/eXQbuXggXT5LnlebtdcuXrsP3EBF6HGuRjH0oW5m64ig/0pu4Itavz9v7MLBCp6K5ejEnuvwv9uYHCn1FxU+4Vbpl7w1aYXPB9jKg3wUKpqeEZzriQJLsy8+8WEcjfBMdnMW8yJnaWFRu8lVoISo6ZWexomkxh7Lep+Z+S/FOjRXKyPmk5ubBe/CDSUTxAlPUuzRUrfGK7aU5fDR8xtbqF4TmD6qbEcnZtQXgkee3Idw6QD1EYiFHfk9e5uppWj0SfE/9vcrfwhpZmEnsF/7SCdueyNydoUEvohS4vOAdM0bE0JklyXDkQ3MM0RjMI1JtZsgH/i1aAQ+g72X/7587a1Xu03tozIjDJchN3iDfPoGALTji6WFVRfoAYqdWbYG5S4MOsjSEfmdRwJFMUV/C28RHM+mEYOTGYPm/JJo9AaF9bZUdluulPUgMkYenwvCG+kqJFQkrHJ07WNfqW+G1eLIx0MIprB1icNvMDTZ0+/3cwNeYiZ+zGUJ1u33By5KPuADeltAxdKi+8ZEvbhxFz7ce1LefpKn2STXyKeqK8QY/NNK7boEiotisWIo/hxE/hX/HdjrEQBw9b8uJluGDltFh1BipOt4lByXQJq9V4LFZ4rmXrTKY+09O5BWfRn6F6a+8wJghjhq/ZgmJEye5zUyXnSdIVtZFeW9fnAEHIVL6TOCzcfgRHPZ9UMtS3CLIvKael0M9SWsmrd+n36Tivi+MMQlESV/kj6Vzj1mTunki0G2XkRgeZblcI3oo5oj2WQKWDSvk8gePF17kY8mu8xiPt7MuDU2KWPhMz+SWBoobKyAulXniNcWEaadd5Y8L7DnII8bac/gj8bnX3gPJmzyRQloQ/o68IkgiEgdH0nf30OUY506dAtCYH5C6R7u0Qm4PxH6WFbvIwyT4O8g2HjVedLtsZX/WZ/I6eyTDbtQN6mtq+P3siBFaeuvz5Z9DKBpycBhRexCJXnknf6k1gtd9F+IGIwpzBFcp1eu9tUclSJEI2NEFiyomVJxjyN5ZirFc8Px21N15t5LkS+kq4fHZ3Ppf09/oiZOQvK1kY=');
  </script>
<script>
  function gaia_parseFragment() {
  var hash = location.hash;
  var params = {};
  if (!hash) {
  return params;
  }
  var paramStrs = decodeURIComponent(hash.substring(1)).split('&');
  for (var i = 0; i < paramStrs.length; i++) {
      var param = paramStrs[i].split('=');
      params[param[0]] = param[1];
    }
    return params;
  }

  function gaia_prefillEmail() {
    var form = null;
    if (document.getElementById) {
      form = document.getElementById('gaia_loginform');
    }

    if (form && form.Email &&
        (form.Email.value == null || form.Email.value == '')
        && (form.Email.type != 'hidden')) {
      hashParams = gaia_parseFragment();
      if (hashParams['Email'] && hashParams['Email'] != '') {
        form.Email.value = hashParams['Email'];
      }
    }
  }

  
  try {
    gaia_prefillEmail();
  } catch (e) {
  }
  
</script>
<script>
  function gaia_setFocus() {
  var form = null;
  var isFocusableField = function(inputElement) {
  if (!inputElement) {
  return false;
  }
  if (inputElement.type != 'hidden' && inputElement.focus &&
  inputElement.style.display != 'none') {
  return true;
  }
  return false;
  };
  var isFocusableErrorField = function(inputElement) {
  if (!inputElement) {
  return false;
  }
  var hasError = inputElement.className.indexOf('form-error') > -1;
  if (hasError && isFocusableField(inputElement)) {
  return true;
  }
  return false;
  };
  var isFocusableEmptyField = function(inputElement) {
  if (!inputElement) {
  return false;
  }
  var isEmpty = inputElement.value == null || inputElement.value == '';
  if (isEmpty && isFocusableField(inputElement)) {
  return true;
  }
  return false;
  };
  if (document.getElementById) {
  form = document.getElementById('gaia_loginform');
  }
  if (form) {
  var userAgent = navigator.userAgent.toLowerCase();
  var formFields = form.getElementsByTagName('input');
  for (var i = 0; i < formFields.length; i++) {
        var currentField = formFields[i];
        if (isFocusableErrorField(currentField)) {
          currentField.focus();
          
          var currentValue = currentField.value;
          currentField.value = '';
          currentField.value = currentValue;
          return;
        }
      }
      
      
      
        for (var j = 0; j < formFields.length; j++) {
          var currentField = formFields[j];
          if (isFocusableEmptyField(currentField)) {
            currentField.focus();
            return;
          }
        }
      
    }
  }

  
  
  
</script>
<script>
  var gaia_scrollToElement = function(element) {
  var calculateOffsetHeight = function(element) {
  var curtop = 0;
  if (element.offsetParent) {
  while (element) {
  curtop += element.offsetTop;
  element = element.offsetParent;
  }
  }
  return curtop;
  }
  var siginOffsetHeight = calculateOffsetHeight(element);
  var scrollHeight = siginOffsetHeight - window.innerHeight +
  element.clientHeight + 0.02 * window.innerHeight;
  window.scroll(0, scrollHeight);
  }
</script>
  <script>var f=this,g=function(a,c){var b=a.split("."),d=f;b[0]in d||!d.execScript||d.execScript("var "+b[0]);for(var e;b.length&&(e=b.shift());)b.length||void 0===c?d[e]?d=d[e]:d=d[e]={}:d[e]=c};var h=function(){try{return new XMLHttpRequest}catch(b){for(var a=["MSXML2.XMLHTTP.6.0","MSXML2.XMLHTTP.3.0","MSXML2.XMLHTTP","Microsoft.XMLHTTP"],c=0;c<a.length;c++)try{return new ActiveXObject(a[c])}catch(d){}}return null};g("gaia.ajax.newXmlHttpRequest",h);var k=function(){this.a=h();this.parameters={}};g("gaia.ajax.XmlHttpFormRequest",k);
k.prototype.send=function(a,c){var b=[],d;for(d in this.parameters)b.push(d+"="+encodeURIComponent(this.parameters[d]));var b=b.join("&"),e=this.a;e.open("POST",a,!0);e.setRequestHeader("Content-type","application/x-www-form-urlencoded");e.onreadystatechange=function(){4==e.readyState&&c({status:e.status,text:e.responseText})};e.send(b)};k.prototype.send=k.prototype.send;
k.prototype.c=function(a,c,b){var d=this.a;d.open("POST",a,!0);d.setRequestHeader("Content-type","application/json");d.onreadystatechange=function(){4==d.readyState&&b({status:d.status,text:d.responseText})};d.send(c)};k.prototype.sendJson=k.prototype.c;k.prototype.b=function(a,c){var b=this.a;b.open("GET",a,!0);b.onreadystatechange=function(){4==b.readyState&&c({status:b.status,text:b.responseText})};b.send()};k.prototype.get=k.prototype.b;var l=/\s*;\s*/,m=function(){if(!document.cookie)return"";for(var a=document.cookie.split(l),c=0;c<a.length;c++){var b=a[c],b=b.replace(/^\s+/,""),b=b.replace(/\s+$/,"");if(0==b.indexOf("APISID="))return b.substr(7)}return""};var n=null,p=function(a,c){this.g=a;this.f=c;this.c=m();this.b=!1},q=function(){var a=n,c=m();c==a.c||a.b||(a.c=c,(new k).b(a.f,function(b){var a=n;b&&b.status&&200==b.status&&"OK"==b.text&&(a.a&&clearInterval(a.a),a.b||(window.location=a.g))}))},r=function(a){var c=n;if(c.a)return!1;c.a=setInterval(function(){q()},a);return!0};g("gaia.loginAutoRedirect.start",function(a,c,b){if(n||!b||!c||0>=a)return!1;n=new p(c,b);return r(a)});
g("gaia.loginAutoRedirect.stop",function(){var a=n;a.b=!0;a.a&&(clearInterval(a.a),a.a=null)});
</script>
  <script type="text/javascript">
  gaia.loginAutoRedirect.start(5000,
  'https:\x2F\x2Faccounts.google.com\x2FServiceLogin?continue=https%3A%2F%2Fdocs.google.com%2Fspreadsheets%2Fd%2F1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w%2Fexport%3Fformat%3Dcsv%26id%3D1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w\x26followup=https%3A%2F%2Fdocs.google.com%2Fspreadsheets%2Fd%2F1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w%2Fexport%3Fformat%3Dcsv%26id%3D1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w\x26service=wise\x26ltmpl=sheets\x26passive=1209600\x26noautologin=true',
  'https:\x2F\x2Faccounts.google.com\x2FPassiveLoginProber?continue=https%3A%2F%2Fdocs.google.com%2Fspreadsheets%2Fd%2F1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w%2Fexport%3Fformat%3Dcsv%26id%3D1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w\x26followup=https%3A%2F%2Fdocs.google.com%2Fspreadsheets%2Fd%2F1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w%2Fexport%3Fformat%3Dcsv%26id%3D1bFOHIoxZY4BXU3l_KOoW9NYpUidPfZE-h07kAIuv_-w\x26service=wise\x26ltmpl=sheets\x26passive=1209600');
  </script>
<script>
  (function(){
  var signinInput = document.getElementById('signIn');
  gaia_onLoginSubmit = function() {
  try {
  gaia.loginAutoRedirect.stop();
  } catch (err) {
  // do not prevent form from being submitted
  }
  try {
  document.bg.invoke(function(response) {
  document.getElementById('bgresponse').value = response;
  });
  } catch (err) {
  document.getElementById('bgresponse').value = '';
  }
  return true;
  }
  document.getElementById('gaia_loginform').onsubmit = gaia_onLoginSubmit;
  var signinButton = document.getElementById('signIn');
  gaia_attachEvent(window, 'load', function(){
  gaia_scrollToElement(signinButton);
  });
  })();
</script>
  </body>
</html>
