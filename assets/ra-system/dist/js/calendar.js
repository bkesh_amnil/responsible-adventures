var nowTemp = new Date();
var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);

var valid_from = $('#valid_from').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: now,
    onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
}).on('changeDate', function (ev) {
    if (ev.date.valueOf() > valid_to.date.valueOf()) {
        var newDate = new Date(ev.date)
        newDate.setDate(newDate.getDate() + 1);
        valid_to.setValue(newDate);
    }
    valid_from.hide();
    $('#valid_to')[0].focus();
}).data('datepicker');

var valid_to = $('#valid_to').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    startDate: valid_from,
    onRender: function (date) {
        return date.valueOf() <= valid_from.date.valueOf() ? 'disabled' : '';
    }
}).on('changeDate', function (ev) {
    valid_to.hide();
}).data('datepicker');

/* event start on */
$('#departure_date').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
    }
}).data('datepicker');
$('#customer_dob').datepicker({
format: "yyyy-mm-dd",
    autoclose: true,
}).data('datepicker');