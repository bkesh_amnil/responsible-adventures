(function() {
	'use strict';

	$('#publish_date').datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true
	});
	$('#unpublish_date').datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true
	});
})();

var options = {
	'maxCharacterSize': 255,
	'originalStyle': 'originalDisplayInfo',
	'warningStyle': 'warningDisplayInfo',
	'warningNumber': 40,
	'displayFormat': '#input Characters | #left Characters Left | #words Words'
};
$('textarea[name=short_description]').textareaCount(options);


$("input:radio[name=is_highlighted]").click(function() {
	var value = $(this).val();
    if(value == 1) {
    	$.ajax({
    		url : $("#base-url").val() + $("#backend_folder").val() + '/news/checkHighlightedNews',
    		dataType : "JSON",
    		success : function(data) {
    			if(data.action == 'error') {
    				if(confirm(data.msg)) {
    					$.ajax({
    						url : $("#base-url").val() + $("#backend_folder").val() + '/news/removeHighlight',
    						type : "POST",
    						dataType : "JSON",
                            data : {id : data.id},
    						success : function(data1) {
                                alert(data1.msg);
                                return false;
    						},
    						error : function() {
    							alert('Error occured');
    							return false;
    						}
    					})
    				} else {
                        $("input:radio[name=news_is_highlighted][value='1']").parent('span').removeClass("checked");
                        $("input:radio[name=news_is_highlighted][value='0']").parent('span').addClass("checked");
                        $("input#hidden_highlight_event").val('0');
                    }
    			}
    		},
    		error : function() {
    			alert('Error occured');
    			return false;
    		}
    	})
    }
});
$(".multi-select").chosen({
	width:"100%"
});