$(document).on("change", "select#link_type", function() {
    var value = $(this).find("option:selected").val();
    if(value == 'menu'){
        $("#menuId").css("display", "block");
        $("#destinationIdId, #experienceId, #areaId, #packageId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'package') {
        $("#packageId").css("display", "block");
        $("#destinationIdId, #experienceId, #areaId, #menuId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'destination') {
        $("#destinationId").css("display", "block");
        $("#packageId, #experienceId, #areaId, #menuId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'experience') {
        $("#experienceId").css("display", "block");
        $("#packageId, #destinationId, #areaId, #menuId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'area') {
        $("#areaId").css("display", "block");
        $("#packageId, #destinationId, #experienceId, #menuId, #urlId").css("display", "none");
        $("select#opens").css("display", "block");
    } else if(value == 'url') {
        $("#urlId").css("display", "block");
        $("#packageId, #destinationId, #experienceId, #menuId, #areaId").css("display", "none");
        $("select#opens").css("display", "block");
    } else {
        $("#packageId, #destinationId, #experienceId, #menuId, #areaId, #urlId").css("display", "none");
        $("select#opens").css("display", "none");
    }
})
