var base_url = $("#base-url").val();
var backend_folder = $("#backend_folder").val();
var current_page = $("#current_page").val();

$(document).ready(function() {
    $("#export_filter_package, #export_filter_download_date").css("display", "none");
})

$(document).on("change", "select[name=export_filter_id]", function() {
    var sel_val = $(this).find("option:selected").val();

    $("#export_filter_" + sel_val).css("display", "block");
    $(".searchFields select").not("#export_filter_" + sel_val).css("display", "none");
    $(".searchFields input").not("#export_filter_" + sel_val).css("display", "none");
})
/* package book list js ends */