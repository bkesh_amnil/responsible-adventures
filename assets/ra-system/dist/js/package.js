$(document).on("change", "select#destination_id", function() {
    var id = $(this).find("option:selected").val();

    $.ajax({
        url : $("#base-url").val() + $("#backend_folder").val() + '/package/getExperiences/',
        type : "POST",
        data : {destination_id : id},
        dataType : "JSON",
        success : function(data) {
            $("select#experience_id")
                .find('option')
                .remove()
                .end()
                .append(data);
            return false;
        },
        error : function(){
            alert("error occured");
            return false;
        }
    })
});