/*
(function(){
    "use strict";

    var menuLinkType = $("#menu-link-type");
    menuLinkType.off("change");
    menuLinkType.on("change", function(){
        $("#menu-module, #menu-content, #menu-url").hide();
        var menuType = $(this).val();
        $("#menu-" + menuType).show();
    });

    menuLinkType.trigger("change");
})();
*/
var base_url = $("#base-url").val();
var backend_folder = $("#backend_folder").val();

$(document).on("change", '#menu_link_type', function(){
    var valSelected = $(this).val();
    $('.setingsRow').hide('fast', function(){
        $('.'+valSelected).show();
    });
});
/* add remove module for menu link type = 'page' starts */
$(document).on("click", ".add_module", function(e){
    var cloned = $('.div_main_content:eq(0)').clone();
    var length = parseInt($(this).attr('length'));
    var newLength = length + 1;
    var hiddenElm = "";
    $(this).attr('length', newLength);
    cloned.find('select:first').attr('name', "module_id["+ length +"]").val("");
    cloned.find('select:last').attr('name', "category_id["+ length +"]").val("");
    cloned.find('.div_module_detail').html("");
    cloned.attr("position", length);
    cloned.find('input[type=hidden]').attr('name', "selectedModulesIds["+ length +"]").attr('class', "selectedValue selectedModulesIds_"+length).val("");
    cloned.find('input[type=text]').attr('name', "module_title["+ length +"]").val("");
    cloned.find('.addRemove').text("[ Remove ]").addClass("remove_module");
    $(cloned).appendTo('#moduleSettings');
    return false;
});
$(document).on("click", ".remove_module", function() {
    if(confirm("Are you sure you are removing the selected module?")){
        $(this).parents('.div_main_content').remove();
    }
    return false;
});
/* add remove module for menu link type = 'page' ends */
/* data according to module and category starts */
$(document).on("change", ".module_id, .category_id", function(e) {
    var element = $(this);
    var parents = $(this).parents('.div_main_content');
    //var site_id = parseInt($('#site_id').val());
    var site_id = 1;
    var moduleId = element.parents('.div_main_content').find('.module_id').val();
    var categoryId = element.parents('.div_main_content').find('.category_id').val();
    var selecteds = element.parents('.div_main_content').find('.selectedValue').val();
    if(moduleId == "" || moduleId == "0"){
        element.parents('.div_main_content').find('.div_module_detail').html("");
    }else{
        var position = $(this).parents('.div_main_content').attr('position');
        element.parents('.div_main_content').find('.div_module_detail').html("Loading.... Please Wait!!");
        $.ajax({
            type : 'POST',
            url : base_url + backend_folder + '/menu/getModuleContents/'+moduleId+'/',
            dataType : 'html',
            data: {"to": "getModuleContents", "siteId":site_id, "moduleId": moduleId, "position": position, "category_id":categoryId, "selecteds" : selecteds},
            success : function(data){
                console.log(data);
                parents.find('.div_module_detail').html(data)/*.find("input, textarea, select").uniform()*/;
                checkClickedModules(element.parents('.div_main_content'));
                return false;
            },
            error : function(XMLHttpRequest, textStatus, errorThrown) {
                //$("#list"+id).html('<center>Unknown Error Occurred</center>');
            }
        });
    }
});
$(document).on("change", ".selectAllWithinCategory", function() {
    var parentElm = $(this).parent('li');
    if($(this).is(':checked')){
        $(this).parents('ul').find('li').each(function(){
            if(!$(this).find('input[type=checkbox]').hasClass('selectAllWithinCategory')){
                $(this).hide();
            }
        })
    }else{
        $(this).parents('ul').find('li').each(function(){
            $(this).show();
        })
    }
});
$('.div_main_content').find('input[type=checkbox]').live('click',function(){
    var checkedIds = "";
    var elm = $(this).parents('.div_main_content');
    var position = elm.attr('position');
    elm.find('input[type=checkbox]').each(function(){
        if($(this).is(':checked')){
            if($(this).val() != "true"){
                $(this).parents('li').addClass('selectedRow');
                if(checkedIds == ""){
                    checkedIds = $(this).val();
                }else{
                    checkedIds = checkedIds + "," + $(this).val();
                }
            }
        }else{
            $(this).parents('li').removeClass('selectedRow');//$(this).remove();
        }
    })
    $('.selectedModulesIds_'+position).val(checkedIds);
});
function checkClickedModules(elm){
    var checkedIds = "";
    var position = elm.attr('position');
    elm.find('input[type=checkbox]').each(function(){
        if($(this).is(':checked')){
            if($(this).val() != "true"){
                $(this).parents('li').addClass('selectedRow');
                if(checkedIds == ""){
                    checkedIds = $(this).val();
                }else{
                    checkedIds = checkedIds + "," + $(this).val();
                }
            }
        }else{
            $(this).parents('li').removeClass('selectedRow');//$(this).remove();
        }
    })
    $('.selectedModulesIds_'+position).val(checkedIds);
}
$(document).on("click", ".selectAllWithinCategory", function(e) {
    var parentElm = $(this).parent('li');
    if($(this).is(':checked')){
        $(this).parents('ul').find('li').each(function(){
            if(!$(this).find('input[type=checkbox]').hasClass('selectAllWithinCategory')){
                $(this).hide();
            }
        })
    }else{
        $(this).parents('ul').find('li').each(function(){
            $(this).show();
        })
    }
})
/* data according to module and category starts */
/* change in menu according to menu type starts */
$(document).on("change", "select#menu_type", function(){
    var menuType = $(this).val();
    $("#parentList").html("Loading.... Please Wait!!");
    $.ajax({
        type : 'POST',
        url : base_url + backend_folder + '/menu/getAvailableMenus',
        dataType : 'html',
        data: {"to": "GetAvailableMenus", "siteId": 1, "menuType": menuType},
        success : function(data){
            $("#parentList").html(data);
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            //$("#list"+id).html('<center>Unknown Error Occurred</center>');
        }
    });
})
/* change in menu according to menu type ends */