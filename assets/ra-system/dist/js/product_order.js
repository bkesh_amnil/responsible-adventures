var base_url = $("#base-url").val();
var backend_folder = $("#backend_folder").val();
var current_page = $("#current_page").val();
var type;

$(document).on("change", "select#product_category_id", function() {
    var val = $(this).find("option:selected").val();
    type = 'product_category_id';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/load_products',
        type : "POST",
        data : {product_category_id : val, type : type},
        dataType : "JSON",
        success : function(res) {
            $("#product_id")
                .find("option")
                .remove()
                .end()
                .append(res.products);
            $("select#product_quantity").val('1');
            $("span#selected_quantity").text('1');
            appendData(res);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#product_id", function() {
    var val = $(this).find("option:selected").val();
    type = 'product';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/load_products',
        type : "POST",
        data : {product_id : val, type : type},
        dataType : "JSON",
        success : function(res) {
            $("select#product_quantity").val('1');
            $("span#selected_quantity").text('1');
            appendData(res);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#product_quantity", function() {
    var quan = $(this).find("option:selected").val();
    var unit_price = $("input#product_price").val();
    var total = quan * unit_price;

    $("input[name=total_price]").val(total);
    $("span#selected_quantity").text(quan);
    $("span#selected_unit_price").text(unit_price);
    $("span#selected_total_price").text(total);
})

function appendData(res) {
    $("span#selected_product_name").text(res.product_name);
    $("input[name=product_model]").val(res.model);
    $("input[name=product_price]").val(res.price)
    $("span#selected_unit_price").text(res.price);
    $("input[name=total_price]").val(res.price);
    $("span#selected_total_price").text(res.price);
}
