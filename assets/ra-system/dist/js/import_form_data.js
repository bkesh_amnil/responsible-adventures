$(document).on("change", "select#type", function() {
    var type = $(this).find("option:selected").val();

    $.ajax({
        url : $("#base-url").val() + $("#backend_folder").val() + '/' + $("#current_page").val() + '/loadData',
        type : "POST",
        dataType : "JSON",
        data : {type : type},
        success : function(res) {
            console.log(res);
            console.log(res.data);
            $("select#id")
                .find('option')
                .remove()
                .end()
                .append(res.data);
            return false;
        },
        error : function() {
            alert('error');
            return false;
        }
    })
})
