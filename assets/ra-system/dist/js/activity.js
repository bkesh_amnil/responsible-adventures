$(document).on("change", "select#link_type", function() {
    var value = $(this).find("option:selected").val();
    if(value == 'experience'){
        $("#experienceId").css("display", "block");
        $("#urlId").css("display", "none");
        $("#linkOpens").css("display", "block");
    } else if(value == 'url') {
        $("#urlId").css("display", "block");
        $("#experienceId").css("display", "none");
        $("#linkOpens").css("display", "block");
    } else {
        $("#experienceId #urlId").css("display", "none");
        $("#linkOpens").css("display", "none");
    }
})