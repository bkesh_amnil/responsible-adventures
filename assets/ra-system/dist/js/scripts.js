var base_url = $('#base-url').val();
var backend_folder = $('#backend_folder').val();
var current_page = $('#current_page').val();

$(function () {
    /* datatable starts */
    var table = $('.list-datatable').DataTable({
        "bPaginate": true,
        "bLengthChange": false,
        "bFilter": true,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false,
        "columnDefs": [ { "targets": 1, "orderable": false } ]
    });
    // Setup - add a text input to each footer cell
    $('#table-search-row th').each( function () {
        var title = $(this).text();
        if(title != '')
            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        else
            $(this).html('');
    });
    // Apply the search
    table.columns().every( function () {
        var that = this;

        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    });
    /* datatable ends */
    /* elfinder for images starts */
    var elf = $('#media-manager').elfinder({
        url : base_url+'assets/elfinder/php/connector.php', // connector URL (REQUIRED)
        lang: 'en' // language (OPTIONAL)
    }).elfinder('instance');
    /* elfinder for images ends */
});
/* load ckeditor starts */
function load_ckeditor(textarea, customConfig)
{
    if(customConfig) {
        configFile = base_url+'assets/ckeditor/custom/minimal.js';
    } else {
        configFile = base_url+'assets/ckeditor/custom/full.js';
    }

    CKEDITOR.replace(textarea, {
        customConfig: configFile
    });
    console.log(configFile);
}
/* load ckeditor ends */
/* corresponding title-alias starts */
$(document).on("keyup", ".title", function () {
    var txtValue = $(this).val();
    var newValue = txtValue.toLowerCase().replace(/[~!@#$%\^\&\*\(\)\+=|'"|\?\/;:.,<>\-\\\s]+/gi, '-');
    $('.slug').val(newValue);
});
/* corresponding title-alias starts */
/* social data starts */
$('body').on('click', '#submitSocialData', function(e) {
    e.preventDefault();
    var that = $(this),
        form = that.parents('form'),
        url = form.attr('action'),
        data = form.serialize();

    $.ajax({
        url: url,
        data: data,
        type: 'post',
        dataType: 'json',
        success: function(res) {
            $("div#social-result").css("display", "block");
            $('html, body').animate({
                'scrollTop' : $("div#social-result").position().top + 800
            }, 4000);
            if(res.action == 'success') {
                $("div#social-result p").html('social data saved successully');
            } else {
                $("div#social-result p").html('error in saving data');
            }
        },
        error: function() {
            alert('error occured');
            return false;
        }
    })
});


(function() {
    $('body').on('change', '#selectData', function(e) {
        var val = $(this).find("option:selected").val();
        if(val != '') {
            var form = $(this).parents('form'),
                url = form.attr('action'),
                moduleId = $('#moduleId').val();
            $.ajax({
                url: url + '/getSocialData',
                data: 'dataId=' + $(this).val() + '&moduleId=' + moduleId,
                type: 'post',
                dataType: 'json',
                success: function (res) {
                    if (res) {
                        $('#facebook_title').val(res.Facebook.title);
                        $('#facebook_link').val(res.Facebook.link);
                        $('#facebook_image').val(res.Facebook.image);
                        $('#facebook_description').val(res.Facebook.description);

                        $('#twitter_title').val(res.Twitter.title);
                        $('#twitter_link').val(res.Twitter.link);
                        $('#twitter_image').val(res.Twitter.image);
                        $('#twitter_description').val(res.Twitter.description);
                    } else {
                        $('#facebook_title').val('');
                        $('#facebook_link').val('');
                        $('#facebook_image').val('');
                        $('#facebook_description').val('');

                        $('#twitter_title').val('');
                        $('#twitter_link').val('');
                        $('#twitter_image').val('');
                        $('#twitter_description').val('');
                    }
                }
            });
        } else {
            $("div.image-wrapper img.img-responsive").attr('src', '');
            $('#facebook_title').val('');
            $('#facebook_link').val('');
            $('#facebook_image').val('');
            $('#facebook_description').val('');

            $('#twitter_title').val('');
            $('#twitter_link').val('');
            $('#twitter_image').val('');
            $('#twitter_description').val('');
        }
    });
})();
/* social data ends */
/* select all checkbox for listing page starts */
$('.selectAll').change(function () {
    var set = ".rowCheckBox";
    var checked = $(this).is(":checked");
    $(set).each(function () {
        if (checked) {
            $(this).attr("checked", true);
        } else {
            $(this).attr("checked", false);
        }
    });
});
/* select all checkbox for listing page starts */
/* package, bike extra nav button starts */
$("#inclusion_exclusion_navigation, #map_navigation, #gallery_navigation, #departure_navigation, #price_navigation").click(function() {
    var checked = parseInt($(".rowCheckBox:checked").length);
    if(checked > 0)
    {
        if(checked > 1){
            var excessCount = confirm("You have selected more than 1 item, Only the top one selected will be edited. Do you want to continue?");
        }else{
            var excessCount = true;
        }
        if(excessCount){
            var url = $(this).attr('rel') + '/' + $(".rowCheckBox:checked:first").val();
            window.location = url;
        }
    } else{
        alert("Please Select Some Items First");
    }
    return false;
})
/* package, bike extra nav button ends */
/* multi-delete starts */
$("#deleteIcon").click(function() {
    var checked = parseInt($(".rowCheckBox:checked").length);

    if(checked == 1 )
    {
        if(confirm("Are you sure to delete data?"))
        {
            var url = $(this).attr('rel') + '/' + $(".rowCheckBox:checked:first").val();
            window.location = url;
        }
    }
    else if(checked > 1){
        if(confirm("Are you sure to delete data?"))
        {
            $('#gridForm').attr('method', 'post');
            $("#gridForm").attr("action", $(this).attr('rel'));
            $("#gridForm").submit();
        }
    }else{
        alert("Please Select Some Items To Delete");
    }
    return false;
})
/* multi-delete ends */
/* multi-change-status starts */
$("#activeIcon, #inactiveIcon").click(function(){
    var checked = parseInt($(".rowCheckBox:checked").length);

    if(checked > 0){
        if(confirm("Are you sure to change status of data?"))
        {
            $('#gridForm').attr('method', 'post');
            $("#gridForm").attr("action", $(this).attr('rel'));
            $("#gridForm").submit();
        }
    }else{
        alert("Please Select Some Items To Change Status of");
    }
    return false;
})
/* multi-change-status ends */
/* multi-change-departure starts */
$("#fixedDepartureIcon, #unfixedDepartureIcon").click(function(){
    var checked = parseInt($(".rowCheckBox:checked").length);

    /*if(checked == 1 )
    {
        if(confirm("Are you sure to change status of data?"))
        {
            var url = $(this).attr('rel') + '/' + $(".rowCheckBox:checked:first").val();
            window.location = url;
        }
    }
    else*/ if(checked > 1 || checked == 1){
        if(confirm("Are you sure to change status of data?"))
        {
            $('#gridForm').attr('method', 'post');
            $("#gridForm").attr("action", $(this).attr('rel'));
            $("#gridForm").submit();
        }
    }else{
        alert("Please Select Some Items To Change Status of");
    }
    return false;
})
/* multi-change-departure ends */
/* structure and data button starts */
$(".structureIcon").click(function(){
    var checked = parseInt($(".rowCheckBox:checked").length);

    if(checked > 0){
        if(checked > 1){
            var excessCount = confirm("You have selected more than 1 item, Only the top one selected will be edited. Do you want to continue?");
        }else{
            var excessCount = true;
        }
        if(excessCount){
            var url = base_url + backend_folder + "/form_fields/" + $(".rowCheckBox:checked:first").val() + "";
            window.location = url;
        }
    }else{
        alert("Please Select Some Items First");
    }
    return false;
});
$(".dataIcon").click(function(){
    var checked = parseInt($(".rowCheckBox:checked").length);

    if(checked > 0){
        if(checked > 1){
            var excessCount = confirm("You have selected more than 1 item, Only the top one selected will be edited. Do you want to continue?");
        }else{
            var excessCount = true;
        }
        if(excessCount){
            var url = base_url + backend_folder + "/form_data/" + $(".rowCheckBox:checked:first").val() + "";
            window.location = url;
        }
    } else{
        alert("Please Select Some Items First");
    }
    return false;
});
/* permission button for role starts */
$(".permissionIcon").click(function() {
    var checked = parseInt($(".rowCheckBox:checked").length);

    if(checked > 0) {
        if(checked > 1) {
            alert("You have selected more than 1 item");
            var excessCount = false;
        } else {
            var excessCount = true;
        }

        if($(".rowCheckBox:checked:first").val() == '1') {
            alert("You can\'t set permission for Super Administrator");
            var excessCount = false;
        }

        if(excessCount) {
            var url = base_url + backend_folder + "/rolemodule/index/" + $(".rowCheckBox:checked:first").val();
            window.location = url;
        }

    } else {
        alert('Please Select Some Items First');
    }
    return false;
})
/* permission button for role ends */
/* structure and data button ends */
/* send mail and export button starts */
$(".mailIcon").click(function() {
    var checked = parseInt($(".rowCheckBox:checked").length);

    if(checked > 0) {
        $('#gridForm').attr('method', 'post');
        $("#gridForm").attr("action", $(this).attr('rel'));
        $("#gridForm").submit();
    } else {
        alert("Please Select Some Items First");
    }
    return false;
})
$(".exportIcon").click(function() {
    var url = $(this).attr("rel");
    window.location = url;
    return false;
})
/* send mail and export button ends */
/* sort starts */
$('[data-toggle="ajaxModal"]').on('click',function (e) {
    $('#ajaxModal').remove();
    e.preventDefault();

    if(current_page == 'menu') {
        if($(this).hasClass('sortMenu')) {
            var href = $(this).attr('href');
        } else {
            var menu_type_id = $("select[name=menu_type_id] option:selected").val();
            if (menu_type_id == '0') {
                alert('Select Menu Type');
                return false;
            }
            var href = $(this).attr('href') + '/' + menu_type_id;
        }

        var $this = $(this)
            , $remote = $this.data('remote') || href
            , $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
        $('body').append($modal);
    } else {
        var $this = $(this)
            , $remote = $this.data('remote') || $this.attr('href')
            , $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
        $('body').append($modal);
    }

    $modal.modal({backdrop: 'static', keyboard: false});
    $modal.load($remote, function () {
        // sortable
        $("#sortable-data").sortable({
            update: function (event, ui) {
                var data = $(this).sortable('serialize');
                // POST to server using $.post or $.ajax
                $.ajax({
                    data: data,
                    type: 'POST',
                    url: base_url + backend_folder + '/' + current_page + '/sort',
                    success: function () {
                        $('#msg').remove();
                        $("#sortable-data").prepend('<span id="msg"></span>');
                        $('#msg').html('Order Updated')
                    }
                });
            }
        }).disableSelection();
    });
});
/* sort ends */