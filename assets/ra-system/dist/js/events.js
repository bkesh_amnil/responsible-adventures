(function() {
    'use strict';

    $('#event_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('#publish_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
    $('#unpublish_date').datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true
    });
})();

$(document).ready(function () {
    var options = {
        'maxCharacterSize': 413,
        'originalStyle': 'originalDisplayInfo',
        'warningStyle': 'warningDisplayInfo',
        'warningNumber': 40,
        'displayFormat': '#input Characters | #left Characters Left | #words Words'
    };
    $('textarea[name=short_description]').textareaCount(options);
});

$("input:radio[name=event_is_highlighted]").click(function () {
    var value = $(this).val();

    if (value == 1) {
        $.ajax({
            url: $("#base_url").val() + 'event/checkHighlightedEvents',
            dataType: "JSON",
            success: function (data) {
                if (data.action == 'error') {
                    if (confirm(data.msg)) {
                        $.ajax({
                            url: $("#base_url").val() + 'event/removeHighlight',
                            type: "POST",
                            dataType: "JSON",
                            data: {id: data.id},
                            success: function (data1) {
                                alert(data1.msg);
                                return false;
                            },
                            error: function () {
                                alert('Error occured');
                                return false;
                            }
                        })
                    } else {
                        $("input:radio[name=event_is_highlighted][value='1']").parent('span').removeClass("checked");
                        $("input:radio[name=event_is_highlighted][value='0']").parent('span').addClass("checked");
                        $("input#hidden_highlight_event").val('0');
                    }
                }
            },
            error: function () {
                alert('Error occured');
                return false;
            }
        })
    }
});
