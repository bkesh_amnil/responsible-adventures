$(document).ready(function(){
    var selected_role_id = $("#hidden_selected_role_id").val();

    if(selected_role_id != '0') {
        $('html, body').animate({
            'scrollTop' : $("section#" + selected_role_id).position().top
        }, 4000);
    }

    $('.parent-check').on('click', function(){
        var parent_id = $(this).val();
        var role = $(this).data('role-type');
        var child_checkbox = $('.'+role+'-'+parent_id);
        checkBoxes($(this), child_checkbox);
    });
    function checkBoxes(element, child_checkbox) {
        if(element.is(':checked'))
            child_checkbox.prop("checked", true);
        else
            child_checkbox.prop("checked", false);
    }
    $('.child-check').on('click', function(){
        var child_id = $(this).val();
        var role = $(this).data('role-type');
        var child_checkbox = $('#permission-'+role+'-'+child_id+' input[type=checkbox]');
        checkBoxes($(this), child_checkbox);
    });

    $('.submit-form').on('click', function(event){
        event.preventDefault();
        var form = $(this).parents('form:first');
        var url = form.attr('action');
        var data = form.serialize();
        var role_id = $('#role-id').val();
        $.ajax({
            url: url,
            type: 'post',
            data: data,
            datatype: 'JSON',
            success: function(res) {
                res = $.parseJSON(res);

                $('.message-'+ res.role_id).html(res.message);

                $('html, body').animate({
                    'scrollTop' : $("section#" + res.role_id).position().top
                }, 4000);
            }
        })
    })
});