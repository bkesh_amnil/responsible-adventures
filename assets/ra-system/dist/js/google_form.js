$(document).on("change", "#edit_google_form", function() {
    if ($(this).is(':checked') == true) {
        if(confirm('You cant view previous form data if any')){
            $("input#google_form_link").removeAttr("disabled");
        } else {
            $(this).attr("checked", false);
        }
    } else {
        $("input#google_form_link").attr("disabled", "disabled");
    }
})