$(document).ready(function() {
    $("#export_filter_package_destination, #export_filter_package_experience, #export_filter_package, #export_filter_customer_name, #export_filter_enquire_date").css("display", "none");
})
$(document).on("change", "select[name=export_filter_id]", function() {
    var sel_val = $(this).find("option:selected").val();

    $("#export_filter_" + sel_val).css("display", "block");
    $(".searchFields select").not("#export_filter_" + sel_val).css("display", "none");
    $(".searchFields input").not("#export_filter_" + sel_val).css("display", "none");
})