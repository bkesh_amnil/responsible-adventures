/**
 * Created by satish on 12/3/2015.
 */

(function() {
    'use strict';

    var mediaTypeSelect = $('.mediaTypeSelect'),
        selectVideos = $('#selectVideos'),
        selectImages = $('#selectImages');

    mediaTypeSelect.on('click', function() {
        var that = $(this),
            selectedMedia = that.val();

        if(selectedMedia == 'video') {
            selectImages.hide();
            selectVideos.show();
        } else {
            selectVideos.hide();
            selectImages.show();
        }
    });

    $('body').on('click', '.deleteMedia', function(e) {
        if(confirm('Do you want to delete?')) {
            e.preventDefault();
            var that = $(this);
            $.ajax({
                url: that.data('url'),
                success: function (res) {
                    if (res)
                        that.parents('.mediaWrapper')
                            .fadeOut('slow', function () {
                                $(this).remove();
                            });
                }
            });
        }
    });
})();
