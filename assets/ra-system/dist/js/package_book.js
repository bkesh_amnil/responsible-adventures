var base_url = $("#base-url").val();
var backend_folder = $("#backend_folder").val();
var current_page = $("#current_page").val();

var package_name;
var departure_date;
var unit_price;
var total_cost;

var package_id;
var nos_of_pax;
var date_id;
var sel_date;
var type;

$(document).on("change", 'select#package_destination_id', function() {
    var val = $(this).find("option:selected").val();
    type = 'package_destination';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {package_destination_id : val, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", 'select#package_experience_id', function() {
    var val = $(this).find("option:selected").val();
    type = 'package_experience';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {package_experience_id : val, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#package_id", function() {
    package_id = $(this).find("option:selected").val();
    type = 'package';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {package_id : package_id, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#departure_date_id", function() {
    date_id = $(this).find("option:selected").val();
    sel_date = $(this).find("option:selected").text();
    nos_of_pax = $("select#number_of_pax option:selected").val();
    package_id = $("select#package_id option:selected").val();
    type = 'departure_date';

    $.ajax({
        url : base_url + backend_folder + '/' + current_page + '/' + 'load_data',
        type : "POST",
        dataType : "JSON",
        data : {date_id : date_id, sel_date : sel_date, nos_of_pax : nos_of_pax, package_id : package_id, type : type},
        success : function(data) {
            appendData(data, type);
            return false;
        },
        error : function() {
            alert('error occured');
            return false;
        }
    })
})

$(document).on("change", "select#number_of_pax", function(e) {
    date_id = $("select#departure_date_id").find("option:selected").val();
    sel_date = $("select#departure_date_id").find("option:selected").text();
    nos_of_pax = $(this).find("option:selected").val();
    package_id = $("select#package_id option:selected").val();
    type = 'number_of_person';

    if(nos_of_pax != 'Select') {
        $.ajax({
            url: base_url + backend_folder + '/' + current_page + '/' + 'load_data',
            type: "POST",
            dataType: "JSON",
            data : {date_id : date_id, sel_date : sel_date, nos_of_pax : nos_of_pax, package_id : package_id, type : type},
            success: function (data) {
                appendData(data, type);
                return false;
            },
            error: function () {
                alert('error occured');
                return false;
            }
        })
    }
})

function appendData(data, type) {
    if(type == 'package_destination') {
        if(data.experience_html != '') {
            $("select#package_experience_id")
                .find('option')
                .remove()
                .end()
                .append(data.experience_html);
        } else {
            $("select#package_experience_id")
                .find('option')
                .remove()
                .end()
                .append('<option>No Package Experience Available</option>');
        }

        if(data.package_html != '') {
            $("select#package_id")
                .find('option')
                .remove()
                .end()
                .append(data.package_html);
        } else {
            $("select#package_id")
                .find('option')
                .remove()
                .end()
                .append('<option>No Package Available</option>');
        }
    }
    if(type == 'package_experience') {
        if(data.package_html != '') {
            $("select#package_id")
                .find('option')
                .remove()
                .end()
                .append(data.package_html);
        } else {
            $("select#package_id")
                .find('option')
                .remove()
                .end()
                .append('<option>No Package Available</option>');
        }
    }

    if(type == 'package_experience' || type == 'package') {
        $("select#number_of_pax").val('Select'); /* change Number of pax to default everytime package experience or package changes */
        /* change in departure date */
        if (data.package_dates != '') {
            $("select#departure_date_id")
                .find('option')
                .remove()
                .end()
                .append(data.package_dates);
        } else {
            $("select#departure_date_id")
                .find('option')
                .remove()
                .end()
                .append('<option>No Date/Price Available</option>');
        }
        /* change in departure date */
    }
    /* Price Info */
    nos_of_pax = $("select#number_of_pax option:selected").val();
    unit_price = data.package_unit_price;

    if(unit_price != '') {
        total_cost = nos_of_pax * unit_price;
        $("input[name=total_price]").val(total_cost);
    } else {
        $("input[name=total_price]").val('');
    }
    $("input[name=price_per_person]").val(data.package_unit_price);
    /* Price Info */
    /* Booking Status Info */
    package_name = $("select#package_id option:selected").text();
    departure_date = $("select#departure_date_id option:selected").text();
    if(package_name == 'No Package Available') {
        package_name = '';
    }
    if(departure_date == 'No Date/Price Available') {
        departure_date = '';
    }

    $("span#selected_package_name").text(package_name);
    $("span#selected_departure_date").text(departure_date);
    if(type != 'number_of_person') {
        $("span#selected_booking_available").text(data.package_all_allocation);
        $("span#selected_total_booked").text(data.package_booked);
        $("span#selected_total_available").text(data.package_available);
    }
    /* Booking Status Info */
}
/* package book list js starts */
$(document).ready(function() {
    $("select#export_filter_package_destination, select#export_filter_package_experience, select#export_filter_package, select#export_filter_customer_name, select#export_filter_booking_status, input#export_filter_booking_date, input#export_filter_departure_date").css("display", "none");
})
$(document).on("change", "select[name=export_filter_id]", function() {
    var sel_val = $(this).find("option:selected").val();

    $("#export_filter_" + sel_val).css("display", "block");
    $(".searchFields select").not("#export_filter_" + sel_val).css("display", "none");
    $(".searchFields input").not("#export_filter_" + sel_val).css("display", "none");
})
/* package book list js ends */