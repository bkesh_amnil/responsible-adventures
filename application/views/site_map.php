<section class="sitemap-wrap">
    <?php if(isset($content) && !empty($content)) { ?>
        <div class="container">
            <h2>Site Map</h2>
            <?php if(isset($content['main_menus']) && !empty($content['main_menus'])) { ?>
            <ul class="nav sitemap-nav">
            <?php
            foreach($content['main_menus'] as $menu) {
                $class = '';
                $child = 0;
                if(!empty($menu['childs'])) {
                    $child = 1;
                    $class = ' group-menu';
                }
                if($child) {
                    ?>
                    <li>
                        <span class="sitemap-head"><?php echo $menu['name'] ?></span>
                        <?php foreach($menu['childs'] as $child) { ?>
                            <ul class="clearfix">
                                <li>
                                    <a href="<?php echo site_url($child['slug']) ?>">
                                        <span><?php echo $child['name'] ?></span>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                    </li>
                <?php
                } else {
                    ?>
                    <li>
                        <span class="sitemap-head"></span>
                        <ul class="clearfix">
                            <li>
                                <a href="<?php echo site_url($menu['slug']) ?>">
                                    <span><?php echo $menu['name'] ?></span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php
                }
            } ?>
            </ul>
            <?php } ?>
            <?php if(isset($content['footer_menus']) && !empty($content['footer_menus'])) { ?>
                <ul class="nav sitemap-nav">
                    <?php
                    foreach($content['footer_menus'] as $menu) {
                        $class = '';
                        $child = 0;
                        if(!empty($menu['childs'])) {
                            $child = 1;
                            $class = ' group-menu';
                        }
                        if($child) {
                            ?>
                            <li>
                                <span class="sitemap-head"><?php echo $menu['name'] ?></span>
                                <?php foreach($menu['childs'] as $child) { ?>
                                    <ul class="clearfix">
                                        <li>
                                            <a href="<?php echo site_url($child['slug']) ?>">
                                                <span><?php echo $child['name'] ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </li>
                            <?php
                        } else {
                            ?>
                            <li>
                                <span class="sitemap-head"></span>
                                <ul class="clearfix">
                                    <li>
                                        <a href="<?php echo site_url($menu['slug']) ?>">
                                            <span><?php echo $menu['name'] ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php
                        }
                    } ?>
                </ul>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="container">
            <p>No Content Added as of Yet.</p>
        </div>
    <?php } ?>
</section>