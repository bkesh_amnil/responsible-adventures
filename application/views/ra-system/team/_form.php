<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Team Name *</label>
                <input type="text" name="name" value="<?php echo $team->name ?>" class="form-control title"
                       id="name" placeholder="Team Name">
            </div>
            <div class="form-group">
                <label for="slug">Slug *</label>
                <input type="text" name="slug" value="<?php echo $team->slug ?>" class="form-control slug" id="slug" placeholder="Slug">
            </div>
            <div class="form-group">
                <label for="post">Post </label>
                <input type="text" name="post" value="<?php echo $team->post ?>" class="form-control" id="post" placeholder="Post">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $team->status == '1' || $team->status == '' ? 'selected' : '' ?>>
                        Publish
                    </option>
                    <option value="0" <?php echo $team->status == '0' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group" id="selectImages">
                <label for="image">Image (690 X 350)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="true" name="image" data-show-detail="true" value="<?php echo $team->image ?>" class="form-control" id="image" placeholder="Image">
            </div>
            <?php
            if(isset($savedMedia) && !empty($savedMedia)) { ?>
                <?php foreach($savedMedia as $media) { ?>
                    <div class="mediaWrapper">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url($media->media) ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $media->media ?>" name="media[]"/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title" value="<?php echo $media->title ?>" name="title[]"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Description" name="description[]"><?php echo $media->caption ?></textarea>
                                </div>
                            </div>
                        </div>
                        <a href="javascript:void(0);" class="deleteMedia"
                           data-url="<?php echo base_url(BACKENDFOLDER . '/gallery/deleteMedia/' . $media->id) ?>">
                            Delete
                        </a>
                    </div>
                <?php } ?>
            <?php }
            ?>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="team_description">Team Description *</label>
                <textarea rows="7" name="team_description" class="form-control" id="team_description"
                          placeholder="Team Description"><?php echo $team->team_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        load_ckeditor('team_description');
    };
</script>