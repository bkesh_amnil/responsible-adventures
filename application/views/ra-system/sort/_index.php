<ul id="sortable-data">
    <?php if(is_array($allDataSort)) { ?>
        <?php foreach($allDataSort as $data) { ?>
            <li id='id_<?php echo $data->id ?>'>
                <?php
                if(isset($data->destination_id) && !(isset($data->menu_type_id))) {
                    $destination_name = $this->db->select('name')->where('id', $data->destination_id)->get('tbl_destination')->row();
                    echo $data->name . '|' . $destination_name->name;
                } else if(isset($data->menu_type_id)) {
                    echo $data->name;
                } else if(isset($data->name))
                    echo $data->name;
                elseif(isset($data->menu_title))
                    echo $data->menu_title;
                elseif(isset($data->title))
                    echo $data->title;
                elseif(isset($data->question))
                    echo $data->question;
                elseif(isset($data->customer_full_name))
                    echo $data->customer_full_name;
                ?>
            </li>
        <?php } ?>
    <?php } else {
        echo "<p>No data to sort.</p>";
    } ?>
</ul>