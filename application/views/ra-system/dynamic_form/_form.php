<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="form_title">Form Title *</label>
                <input type="text" name="form_title" value="<?php echo $dynamic_form->form_title ?>" class="form-control" id="form_title" placeholder="Form Title">
                
            </div>
            <div class="form-group">
                <label for="form_name">Form Name *</label>
                <input type="text" name="form_name" value="<?php echo $dynamic_form->form_name ?>" class="form-control" id="form_name" placeholder="Form Name">
            </div>
            <div class="form-group">
                <label for="form_related">Form Related To</label>
                <select name="form_related" id="form_related" class="form-control">
                    <option value="0">None</option>
                </select>
            </div>
            <div class="form-group">
                <label for="form_relation_link">Relation Link Text</label>
                <input type="text" name="form_relation_link" value="<?php echo $dynamic_form->form_relation_link ?>" class="form-control form_relation_link" id="form_relation_link" placeholder="Relation Link Text">
            </div>
            <div class="form-group">
                <label for="form_description">Form Description</label>
                <textarea rows="7" name="form_description" class="form-control" id="form_description" placeholder="Form Description"><?php echo $dynamic_form->form_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="form_class">Form Class</label>
                <input type="text" name="form_class" value="<?php echo $dynamic_form->form_class ?>" class="form-control" id="form_class" placeholder="Form Class">
            </div>
            <div class="form-group">
                <label for="form_attribute">Attributes</label>
                <input type="text" name="form_attribute" value="<?php echo $dynamic_form->form_attribute ?>" class="form-control" id="form_attribute" placeholder="Attributes">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $dynamic_form->status == '1' || $dynamic_form->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $dynamic_form->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
            <div class="form-group">
                <label for="submit_action">Submit Action</label>
                <select name="submit_action" id="submit_action" class="form-control">
                    <option value="database" <?php echo (isset($dynamic_form->submit_action) && $dynamic_form->submit_action == 'database') ? 'selected="selected"' : '' ?>>Save to Database</option>
                    <option value="email" <?php echo (isset($dynamic_form->submit_action) && $dynamic_form->submit_action == 'email') ? 'selected="selected"' : '' ?>>Send Email</option>
                    <option value="both" <?php echo (isset($dynamic_form->submit_action) && $dynamic_form->submit_action == 'both') ? 'selected="selected"' : '' ?>>Save to Database & send Email</option>
                </select>
            </div>
            <div class="form-group">
                <label for="success_msg">Success Message</label>
                <textarea rows="7" name="success_msg" class="form-control" id="success_msg" placeholder="Success Message"><?php echo $dynamic_form->success_msg ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group" id="selectImages">
                <label for="admin_email">Admin Email</label>
                <input type="text" name="admin_email" value="<?php echo $dynamic_form->admin_email ?>" class="form-control" id="admin_email" placeholder="Admin Email">
            </div>
            <div class="form-group" id="selectImages">
                <label for="admin_email_subject">Admin Email Subject</label>
                <input type="text" name="admin_email_subject" value="<?php echo $dynamic_form->admin_email_subject ?>" class="form-control" id="admin_email_subjects" placeholder="Admin Email Subject">
            </div>
            <div class="form-group">
                <label for="admin_email_msg">Admin Email Message</label>
                <textarea rows="7" name="admin_email_msg" class="form-control" id="admin_email_msg" placeholder="Admin Email Message"><?php echo $dynamic_form->admin_email_msg ?></textarea>
            </div>
            <div class="form-group">
                <label for="email_to_user">Email to User ?</label>
                <select name="email_to_user" id="email_to_user" class="form-control">
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                </select>
            </div>
            <div class="form-group">
                <label for="user_email_subject">User Email Subject</label>
                <input type="text" name="user_email_subject" value="<?php echo $dynamic_form->user_email_subject ?>" class="form-control" id="user_email_subject" placeholder="User Email Subject">
            </div>
            <div class="form-group">
                <label for="user_email_msg">User Email Message</label>
                <textarea rows="7" name="user_email_msg" class="form-control" id="user_email_msg" placeholder="User Email Message"><?php echo $dynamic_form->user_email_msg ?></textarea>
            </div>
            
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('form_description');
        load_ckeditor('success_msg');
        load_ckeditor('admin_email_msg');
        load_ckeditor('user_email_msg');
    };
</script>