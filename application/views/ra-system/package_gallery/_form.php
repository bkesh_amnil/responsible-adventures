<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo isset($package_gallery->status) && ($package_gallery->status == '1' || $package_gallery->status == '') ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo isset($package_gallery->status) && $package_gallery->status == '0' ? 'selected' : '' ?>>InActive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
           <div class="form-group">
                <label for="image">Images (1920x1323)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="true"
                       name="image" data-show-detail="true" class="form-control" id="image" placeholder="Images"
                        <?php echo isset($package_gallery->image) && $package_gallery->image != '' ? 'readonly="readonly"' : ''?>>
            </div>
            <?php if(isset($package_gallery->image) && $package_gallery->image != '') { ?>
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-responsive" src="<?php echo base_url($package_gallery->image) ?>"/>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <input type="text" class="form-control" readonly="readonly" value="<?php echo $package_gallery->image ?>" name="media[]"/>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Title" value="<?php echo $package_gallery->title ?>" name="title[]"/>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Description" name="description[]"><?php echo $package_gallery->caption ?></textarea>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/' . $package_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>

<!-- Modal -->
<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Media Details</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url(BACKENDFOLDER . '/gallery/saveMediaDetail') ?>" method="post">
                    <div class="form-group">
                        <label for="mediaTitle">Title</label>
                        <input class="form-control" type="text" name="mediaTitle" id="mediaTitle" placeholder="Media Title"/>
                    </div>
                    <div class="form-group">
                        <label for="mediaCaption">Caption</label>
                        <textarea rows="8" class="form-control" name="mediaCaption" id="mediaCaption" placeholder="Media Caption"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="mediaDescription">Description</label>
                        <textarea rows="8" class="form-control" name="mediaDescription" id="mediaDescription" placeholder="Media Description"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>