<?php
$url = $this->uri->segment_array();
$count = count($url);
$last_part = $url[$count];

if($count == 2 || ($count == 3 && is_numeric($last_part))) {
if(isset($this->header['page_name']) && $this->header['page_name'] == 'subscription') { ?>
    <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'/send_mail') ?>" class="btn btn-success btn-xs mailIcon">
        <i class="fa fa-plus fa-fw"></i>Send Mail
    </a>
    <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'/export_excel') ?>" class="btn btn-success btn-xs exportIcon">
        <i class="fa fa-plus fa-fw"></i>Export
    </a>
<?php } ?>
<?php if($show_add_link) {
    $rolemanagerId = get_userdata('rolemanager_id');
    // all access to superadmin
    if($this->header['page_name'] == 'dynamic_form') {
        if($rolemanagerId == '1') {
        ?>
        <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/form_fields/index/') ?>" class="btn btn-success btn-xs structureIcon">
            <i class="fa fa-film fa-fw"></i>Structure
        </a>
        <?php } ?>
        <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/form_controller/') ?>" class="btn btn-success btn-xs dataIcon">
            <i class="fa fa-database fa-fw"></i>Data
        </a>
        <?php
    }

    if($activeModulePermission['add']) {
        switch($this->header['page_name']) {
            case 'package_inclusion_exclusion':
            case 'package_map':
            case 'package_price':
            case 'package_departure':
            case 'package_gallery':
            case 'bike_price':
            case 'bike_gallery':
            case 'bike_rent':
            case 'product_gallery':
            case 'form_fields':
            case 'form_data':
                $hrefUrl = base_url(BACKENDFOLDER.'/'.segment(2).'/create/'.segment(3).'/0');
                $activeStatusUrl = base_url(BACKENDFOLDER.'/'.segment(2).'/status/0/'.segment(3));
                $inactiveStatusUrl = base_url(BACKENDFOLDER.'/'.segment(2).'/status/1/'.segment(3));
                $deleteUrl = base_url(BACKENDFOLDER.'/'.segment(2).'/delete/'.segment(3));
                $backUrl = base_url(BACKENDFOLDER . '/' . explode('_', segment(2))[0]);
                if(segment(2) == 'form_fields') {
                    $backUrl = base_url(BACKENDFOLDER . '/dynamic_form');
                } else if(segment(2) == 'form_data') {
                    $backUrl = base_url(BACKENDFOLDER . '/dynamic_form');
                }
                break;
            default:
                $hrefUrl = base_url(BACKENDFOLDER.'/'.segment(2).'/create');
                $activeStatusUrl = base_url(BACKENDFOLDER.'/'.segment(2).'/status/0');
                $inactiveStatusUrl = base_url(BACKENDFOLDER.'/'.segment(2).'/status/1');
                $deleteUrl = base_url(BACKENDFOLDER.'/'.segment(2).'/delete');
                $backUrl = '';
                break;
        }
        ?>
        <?php if($this->header['page_name'] != 'form_data') { ?>
        <a href="<?php echo $hrefUrl ?>" class="btn btn-success btn-xs">
            <i class="fa fa-plus fa-fw"></i>
        </a>
        <?php } ?>
        <?php if($activeModulePermission['edit'] && $this->header['page_name'] != 'role' && $this->header['page_name'] != 'package_map' && $this->header['page_name'] != 'module' && $this->header['page_name'] != 'user' && $this->header['page_name'] != 'form_fields' && $this->header['page_name'] != 'package_book' && $this->header['page_name'] != 'bike_book' && $this->header['page_name'] != 'product_order' && $this->header['page_name'] != 'form_data') { ?>
        <a href="javascript:void(0);" rel="<?php echo $activeStatusUrl ?>" class="btn btn-success btn-xs" id="activeIcon">
            <i class="fa fa-circle fa-fw"></i> Active
        </a>
        <a href="javascript:void(0);" rel="<?php echo $inactiveStatusUrl ?>" class="btn btn-info btn-xs" id="inactiveIcon">
            <i class="fa fa-circle-o fa-fw"></i> Inactive
        </a>
        <?php } ?>
        <?php if($this->header['page_name'] == 'package_departure' || $this->header['page_name'] == 'bike_rent') { ?>
        <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'/fixed_departure/0/'.segment(3)) ?>" class="btn btn-success btn-xs" id="fixedDepartureIcon">
            <i class="fa fa-circle fa-fw"></i> Fixed Departure
        </a>
        <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'/fixed_departure/1/'.segment(3)) ?>" class="btn btn-info btn-xs" id="unfixedDepartureIcon">
            <i class="fa fa-circle-o fa-fw"></i> Unfixed Departure
        </a>
        <?php } ?>
        <?php if($activeModulePermission['delete'] && $this->header['page_name'] != 'role' && $this->header['page_name'] != 'user' && $this->header['page_name'] != 'form_data' && $this->header['page_name'] != 'package_book') { ?>
        <a href="javascript:void(0);" rel="<?php echo $deleteUrl ?>" class="btn btn-danger btn-xs" id="deleteIcon">
            <i class="fa fa-trash fa-fw"></i>
        </a>
        <?php } ?>

        <?php if($this->header['page_name'] == 'package' || $this->header['page_name'] == 'bike' || $this->header['page_name'] == 'product') { ?>
            <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'_gallery') ?>" class="btn btn-success btn-xs" id="gallery_navigation">
                <i class="fa fa-camera fa-fw"></i>Gallery
            </a>
        <?php } ?>
        <?php if($this->header['page_name'] == 'package' || $this->header['page_name'] == 'bike') { ?>
            <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'_price') ?>" class="btn btn-success btn-xs" id="price_navigation">
                <i class="fa fa-calendar fa-fw"></i>Price
            </a>
            <?php if($this->header['page_name'] == 'package') { ?>
                <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'_inclusion_exclusion') ?>" class="btn btn-success btn-xs" id="inclusion_exclusion_navigation">
                    <i class="fa fa-th-list fa-fw"></i>Inclusion/Exclusion
                </a>
                <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'_map') ?>" class="btn btn-success btn-xs" id="map_navigation">
                    <i class="fa fa-map-marker fa-fw"></i>Map
                </a>
                <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'_departure') ?>" class="btn btn-success btn-xs" id="departure_navigation">
                    <i class="fa fa-plane fa-fw"></i>Departure
                </a>
            <?php } else { ?>
                <a href="javascript:void(0);" rel="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'_rent') ?>" class="btn btn-success btn-xs" id="departure_navigation">
                    <i class="fa fa-bicycle fa-fw"></i>Rent
                </a>
            <?php } ?>
        <?php } ?>
    <?php }
}
?>
<?php if($show_sort_link) {
    if($activeModulePermission['edit']) { ?>
        <a href="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'/sort') ?>" data-toggle="ajaxModal" class="btn btn-info btn-xs">
            <i class="fa fa-sort fa-fw"></i> Sort
        </a>
    <?php }
}
?>
<?php
if(isset($this->header['page_name']) && $this->header['page_name'] == 'import_form_data') {
  $backUrl = base_url(BACKENDFOLDER.'/'.segment(4));
}
?>
<?php if(!empty($backUrl)) { ?>
    <a href="<?php echo $backUrl ?>" class="btn btn-success btn-xs">
        <i class="fa fa-backward fa-fw"></i>
    </a>
<?php } ?>
<?php } ?>
<?php if(isset($this->header['page_name']) && $this->header['page_name'] == 'role') { ?>
    <a title="Permission" href="<?php echo base_url(BACKENDFOLDER . '/rolemodule/') ?>" class="btn btn-primary btn-xs permissionIcon"><i class="fa fa-user-plus fa-fw"></i> </a>
<?php } ?>
