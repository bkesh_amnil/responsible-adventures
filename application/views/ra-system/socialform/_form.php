<div class="alert alert-info alert-dismissable fade in" id="social-result" style="display: none">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <p></p>
</div>
<form action="<?php echo base_url(BACKENDFOLDER.'/social') ?>" method="post">
    <input type="hidden" id="moduleId" value="<?php echo $active_module_id ?>" name="activeModuleId"/>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="selectData">Select Data</label>
                <select id="selectData" name="dataId" class="form-control">
                    <option value="">Select Data</option>
                    <?php foreach($socialFormAllData as $rowData) { ?>
                        <option value="<?php echo $rowData->id ?>">
                            <?php echo $rowData->name ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="facebook_title">Facebook Title</label>
                <input type="text" name="facebook_title" value="<?php //echo $social['facebook']->facebook_title ?>" class="form-control" id="facebook_title" placeholder="Facebook Title">
            </div>
            <div class="form-group">
                <label for="facebook_image">Facebook Image(1200X630 or 600X315 or 200X200)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="true" name="facebook_image" value="<?php //echo $social['facebook']->facebook_image ?>" class="form-control" id="facebook_image" placeholder="Facebook Image">
            </div>
            <div class="form-group">
                <label for="facebook_link">Facebook Link</label>
                <input type="text" name="facebook_link" value="<?php //echo $social['facebook']->link ?>" class="form-control" id="facebook_link" placeholder="Facebook Link">
            </div>
            <div class="form-group">
                <label for="facebook_description">Facebook Description</label>
                <textarea rows="10" name="facebook_description" class="form-control" id="facebook_description" placeholder="Facebook Description"><?php //echo $social['facebook']->facebook_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="twitter_title">Twitter Title</label>
                <input type="text" name="twitter_title" value="<?php //echo $social['twitter']->title ?>" class="form-control" id="twitter_title" placeholder="Twitter Title">
            </div>
            <div class="form-group">
                <label for="twitter_image">Twitter Image</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="true" name="twitter_image" value="<?php //echo $social['twitter']->image ?>" class="form-control" id="twitter_image" placeholder="Twitter Image">
            </div>
            <div class="form-group">
                <label for="twitter_link">Twitter Link</label>
                <input type="text" name="twitter_link" value="<?php //echo $social['twitter']->link ?>" class="form-control" id="twitter_link" placeholder="Twitter Link">
            </div>
            <div class="form-group">
                <label for="twitter_description">Twitter Description</label>
                <textarea rows="10" name="twitter_description" class="form-control" id="twitter_description" placeholder="Twitter Description"><?php //echo $social['twitter']->description ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="button" id="submitSocialData" class="btn btn-info">Save Social Tags</button>
            </div>
        </div>
    </div>
</form>