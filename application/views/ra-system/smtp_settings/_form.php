<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="smtp_server">SMTP Server</label>
                <input type="text" name="smtp_server" value="<?php echo isset($smtp_settings->smtp_server) ? $smtp_settings->smtp_server : '' ?>"
                       class="form-control" id="smtp_server" placeholder="SMTP Server">
            </div>
            <div class="form-group">
                <label for="smtp_port">SMTP Port</label>
                <input type="text" name="smtp_port" value="<?php echo isset($smtp_settings->smtp_port) ? $smtp_settings->smtp_port : '' ?>" class="form-control"
                       id="smtp_port" placeholder="SMTP Port">
            </div>
            <div class="form-group">
                <label for="smtp_email">SMTP Email</label>
                <input type="text" name="smtp_email" value="<?php echo isset($smtp_settings->smtp_email) ? $smtp_settings->smtp_email : '' ?>"
                       class="form-control"
                       id="smtp_email" placeholder="SMTP Email">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="admin_email">Admin Email</label>
                <input type="text" name="admin_email" value="<?php echo isset($smtp_settings->admin_email) ? $smtp_settings->admin_email : '' ?>"
                       class="form-control" id="admin_email" placeholder="Admin Email">
            </div>
            <div class="form-group">
                <label for="ssl_enabled">SSL Enabled: </label>
                <input type="radio" name="ssl_enabled" value="1"
                       id="ssl_enabled" <?php if (isset($smtp_settings->ssl_enabled) && $smtp_settings->ssl_enabled == '1') echo 'checked' ?>>Yes
                <input type="radio" name="ssl_enabled" value="0"
                       id="ssl_enabled" <?php if (isset($smtp_settings->ssl_enabled) && $smtp_settings->ssl_enabled == '0') echo 'checked' ?>>No
            </div>
            <div class="form-group">
                <label for="is_authenticated">Is Authenticated</label>
                <input type="radio" name="is_authenticated" value="1"
                       id="is_authenticated" <?php if (isset($smtp_settings->is_authenticated) && $smtp_settings->is_authenticated == '1') echo 'checked' ?>>Yes
                <input type="radio" name="is_authenticated" value="0"
                       id="is_authenticated" <?php if (isset($smtp_settings->is_authenticated) && $smtp_settings->is_authenticated == '0') echo 'checked' ?>>No
            </div>
            <div class="form-group">
                <label for="smtp_email_password">SMTP Email Password</label>
                <input type="password" name="smtp_email_password" value="<?php echo isset($smtp_settings->smtp_email_password) ? $smtp_settings->smtp_email_password : '' ?>"
                       class="form-control" id="smtp_email_password" placeholder="SMTP Email Password">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <!--<a class="btn btn-warning" href="<?php /*echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) */?>"><span>Cancel</span></a>-->
            </div>
        </div>
    </div>
</form>