<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="field_label">Field Label *</label>
                <input type="text" name="field_label" value="<?php echo (isset($form_fields->field_label)) ? $form_fields->field_label : '' ?>" class="form-control" id="field_label" placeholder="Field Label">

            </div>
            <div class="form-group">
                <label for="field_type">Field Type </label>
                <select name="field_type" id="field_type" class="form-control">
                    <?php
                    foreach($field_types as $field_type) {
                        $selected = '';
                        if($field_type->field_type == $form_fields->field_type) {
                            $selected = ' selected="selected"';
                        }
                        ?>
                    <option value="<?php echo $field_type->field_type ?>" multiple_values="<?php echo $field_type->multiple_values ?>"<?php echo $selected ?>><?php echo $field_type->display_text ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="field_name">Field Name *</label>
                <input type="text" name="field_name" value="<?php echo (isset($form_fields->field_name)) ? $form_fields->field_name : '' ?>" class="form-control" placeholder="Field Name">
            </div>
            <div class="form-group">
                <label for="field_placeholder">Field Placeholder</label>
                <input type="text" name="field_placeholder" value="<?php echo (isset($form_fields->field_placeholder)) ? $form_fields->field_placeholder : '' ?>" class="form-control field_placeholder" id="field_placeholder" placeholder="Field Placeholder">
            </div>
            <div class="form-group">
                <label for="default_value">Default Value</label>
                <input type="text" name="default_value" value="<?php echo (isset($form_fields->default_value)) ? $form_fields->default_value : '' ?>" class="form-control" id="default_value" placeholder="Default Value">
            </div>
            <div class="form-group">
                <label for="front_display">Front Display</label>
                <select name="front_display" id="front_display" class="form-control">
                    <option value="1" <?php (isset($form_fields->front_display) && $form_fields->front_display == '1') ? 'selected="selected"' : '' ?>>Yes</option>
                    <option value="0" <?php (isset($form_fields->front_display) && $form_fields->front_display == '0') ? 'selected="selected"' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="show_in_grid">Show in Grid</label>
                <select name="show_in_grid" id="show_in_grid" class="form-control">
                    <option value="1" <?php (isset($form_fields->show_in_grid) && $form_fields->show_in_grid == '1') ? 'selected="selected"' : '' ?>>Yes</option>
                    <option value="0" <?php (isset($form_fields->show_in_grid) && $form_fields->show_in_grid == '0') ? 'selected="selected"' : '' ?>>No</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="position">Field Order</label>
                <input type="text" name="position" value="<?php echo (isset($form_fields->position)) ? $form_fields->position : '' ?>" id="position" class="form-control" placeholder="Position">
            </div>
            <div class="form-group">
                <label for="validation_rule">Validation Rules</label>
                <?php
                if(isset($form_validations) && !empty($form_validations)) {
                    foreach($form_validations as $validation){
                        $checked = '';
                        if(isset($form_fields->validation_rule)) {
                            $fields = explode('|', $form_fields->validation_rule);
                            foreach($fields as $val) {
                                $value = explode('[', $val);

                                if(isset($value[1])){
                                    $arr[] = $value;
                                }

                                if($value[0] == $validation->validation_rule){
                                    $checked = 'checked = "checked"';
                                }
                            }
                        }
                        ?>
                        <br/><input type="checkbox" name="validation_rule[]" value="<?php echo $validation->validation_rule; ?>" <?php echo $checked; ?>>
                        <span><?php echo ucfirst($validation->description); ?></span>
                        <?php
                        if($validation->validation_rule == 'minSize' || $validation->validation_rule == 'maxSize'){
                            $value = '';
                            if(isset($arr) && !empty($arr)) {
                                foreach($arr as $val){
                                    if($val[0] == $validation->validation_rule){
                                        $value = str_replace(']', '', $val[1]);
                                    }
                                }
                            }
                            ?>
                            <input type="text" name="<?php echo $validation->validation_rule; ?>" value="<?php echo $value; ?>" id="<?php echo $validation->validation_rule; ?>" class="form-control">
                            <?php
                        } elseif($validation->validation_rule == 'date') {
                            if(isset($arr) && !empty($arr)) {
                                foreach($arr as $val){
                                    if($val[0] == 'date'){
                                        $value = str_replace(']', '', $val[1]);
                                    }
                                }
                            }
                            ?>
                            <select name="date" id="date" class="form-control">
                                <option value="Y-m-d">Y-m-d</option>
                                <option value="m-d-Y">m-d-Y</option>
                                <option value="d-m-Y">d-m-Y</option>
                            </select>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/' . $form_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>