<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th></th>
            <th>Label</th>
            <th>Type</th>
            <th></th>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th>SN</th>
            <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
            <th>Lable</th>
            <th>Type</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($rows) : $serial_number = 1; ?>
            <?php foreach ($rows as $row) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" data="<?php echo $row->id ?>" /></td>
                    <td><?php echo $row->field_label ?></td>
                    <td><?php echo $row->field_type ?></td>
                    <td>
                        <?php
                        $this->data['actionBtnData'] = [
                            'module' => 'form_fields',
                            'moduleData' => $row,
                            'options' => 'ED'
                        ];
                        $ci = &get_instance();
                        $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                        ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>