<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <input type="hidden" name="email_address" value="<?php echo $package_enquire->email_address ?>"/>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="package_category_id">Package Category *</label>
                <select name="package_category_id" id="package_category_id" class="form-control" disabled>
                    <?php
                    if(isset($package_categories) && !empty($package_categories)) {
                        foreach($package_categories as $package_category) {
                            $selected = '';
                            if($package_category->id == $package_enquire->package_category_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $package_category->id ?>"<?php echo $selected ?>><?php echo $package_category->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="package_id">Package Name *</label>
                <select name="package_id" id="package_id" class="form-control" disabled>
                    <?php
                    if(isset($packages) && !empty($packages)) {
                        foreach($packages as $val) {
                            $selected = '';
                            if($val->id == $package_enquire->package_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="departure_date_id">Departure Date *</label>
                <input type="text" name="departure_date" value="<?php echo $package_enquire->departure_date ?>" class="form-control" disabled>
            </div>
            <div class="form-group">
                <label for="number_of_pax">Number of pax *</label>
                <input type="text" name="number_of_pax" value="<?php echo $package_enquire->number_of_pax ?>" class="form-control" disabled>
            </div>
            <div class="form-group">
                <label for="full_name">Full Name *</label>
                <input type="text" name="full_name" value="<?php echo $package_enquire->full_name ?>" class="form-control" id="full_name" placeholder="Full Name" disabled>
            </div>
            <div class="form-group">
                <label for="note">Note from DTD</label>
                <textarea rows="7" name="note" class="form-control" id="note" placeholder="Note from DTD"><?php echo $package_enquire->note ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('note', true)
                    };
                </script>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="email_address">Email Address *</label>
                <input type="text" name="email_address" value="<?php echo $package_enquire->email_address ?>" class="form-control" id="email_address" placeholder="Email Address" disabled>
            </div>
            <div class="form-group">
                <label for="contact_address">Contact Address *</label>
                <input type="text" name="contact_address" value="<?php echo $package_enquire->contact_address ?>" class="form-control" id="contact_address" placeholder="Contact Address" disabled>
            </div>
            <div class="form-group">
                <label for="contact_number">Contact Nos *</label>
                <input type="text" name="contact_number" value="<?php echo $package_enquire->contact_number ?>" class="form-control" id="contact_number" placeholder="Contact Nos" disabled>
            </div>
            <div class="form-group">
                <label for="enquiry">Customer Enquiry *</label>
                <textarea rows="7" name="enquiry" class="form-control" id="enquiry" placeholder="Customer Enquiry" disabled><?php echo $package_enquire->enquiry ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <?php if(isset($package_enquire->note) && empty($package_enquire->note)) {?>
                    <button type="submit" class="btn btn-primary">Save</button>
                <?php } ?>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>