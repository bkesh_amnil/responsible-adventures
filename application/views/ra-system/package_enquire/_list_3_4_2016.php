<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th>Experience Name</th>
            <th>Package Name</th>
            <th>Customer Name</th>
            <th>Enquire Date</th>
            <th></th>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th>SN</th>
            <th>Experience Name</th>
            <th>Package Name</th>
            <th>Customer Name</th>
            <th>Enquire Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($rows) : $serial_number = 1; ?>
            <?php foreach ($rows as $row) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><?php echo $row->experience_name ?></td>
                    <td><?php echo $row->package_name ?></td>
                    <td><?php echo $row->full_name ?></td>
                    <td><?php echo date('Y-m-d', $row->created_date) ?></td>
                    <td>
                        <?php
                        $this->data['actionBtnData'] = [
                            'module' => 'package_enquire',
                            'moduleData' => $row,
                            'options' => 'ED'
                        ];
                        $ci = &get_instance();
                        $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                        ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>