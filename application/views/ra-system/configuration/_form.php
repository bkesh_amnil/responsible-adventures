<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="site_title">Site Title</label>
                <input type="text" name="site_title" value="<?php echo $configuration->site_title ?>"
                       class="form-control" id="site_title" placeholder="Site Title">
            </div>
            <div class="form-group">
                <label for="site_name">Site Name</label>
                <input type="text" name="site_name" value="<?php echo $configuration->site_name ?>" class="form-control"
                       id="site_name" placeholder="Site Name">
            </div>
            <div class="form-group">
                <label for="sub_domain">Sub Domain</label>
                <input type="text" name="sub_domain" value="<?php echo $configuration->site_name ?>"
                       class="form-control"
                       id="sub_domain" placeholder="Sub Domain">
            </div>
            <div class="form-group">
                <label for="site_from_email">Site Email</label>
                <input type="text" name="site_from_email" value="<?php echo $configuration->site_from_email ?>"
                       class="form-control" id="site_from_email" placeholder="Site Email">
            </div>
            <div class="form-group">
                <label for="site_author">Site Author</label>
                <input type="text" name="site_author" value="<?php echo $configuration->site_author ?>"
                        class="form-control" id="site_author" placeholder="Site Author">
            </div>
            <div class="form-group">
                <label for="site_offline">Site Status: </label>
                <input type="radio" name="site_offline" value="yes"
                       id="site_offline" <?php if ($configuration->site_offline == 'no') echo 'checked' ?>>Online
                <input type="radio" name="site_offline" value="no"
                       id="site_offline" <?php if ($configuration->site_offline == 'yes') echo 'checked' ?>>Offline
            </div>
            <div class="form-group">
                <label for="site_offline_msg">Site Offline Message</label>
                <textarea rows="7" name="site_offline_msg" class="form-control" id="site_offline_msg"
                <textarea rows="7" name="site_offline_msg" class="form-control" id="site_offline_msg"
                          placeholder="Site Offline Msg"><?php echo $configuration->site_offline_msg ?></textarea>
                <script>
                    window.onload = function () {
                        load_ckeditor('site_offline_msg', true)
                    };
                </script>
            </div>
            <div class="form-group">
                <label for="site_logo">Site Logo</label>
                <input type="text" name="site_logo" onclick="BrowseServer(this)" data-resource-type="image"
                       data-multiple="false" value="<?php echo $configuration->site_logo ?>" class="form-control"
                       id="site_logo" placeholder="Site Logo">
                <?php if ($configuration->site_logo != '') { ?>
                    <img src="<?php echo base_url($configuration->site_logo) ?>" class="img-responsive" width="20%"/>
                <?php } ?>
            </div>
            <div class="form-group">
                <label class="banner_size">Site Banner Size :<span
                        class="required">*</span></label>
                <small>Provide Banner Size in the site if you have enabled. (width,height)
                    in
                    pixels
                </small>
                <input type="text" name="siteBanner" value="<?php echo $configuration->siteBanner ?>"
                       class="form-control" id="siteBanner" placeholder="Site Banner Size">
            </div>
            <div class="form-group">
                <label class="image_size">Site Images Sizes :<span
                        class="required">*</span></label>
                <small>Provide Different Sizes of image that site will contain.
                    format(width,height|width,height..) in pixels
                </small>
                <input type="text" name="imageSize" value="<?php echo $configuration->imageSize ?>"
                       class="form-control" id="imageSize" placeholder="Site Image Size">
            </div>
            <div class="form-group">
                <label class="control-label">Google Analytics Code :</label>
                <textarea rows="7" name="google_analytics_code" class="form-control" id="google_analytics_code"
                          placeholder="Google Analytics Code"><?php echo $configuration->google_analytics_code?></textarea>
                <script>
                    window.onload = function () {
                        load_ckeditor('google_analytics_code', true)
                    };
                </script>
                <?php //echo form_input('google_analytics_code', $configuration->google_analytics_code, 'class="col-lg-12 form-control"') ?>
            </div>
            <div class="form-group">
                <label for="phone">Phone(Can add multiple phone numbers using comma. EG: 1 4217455, 9849151515)</label>
                <input type="text" name="phone" value="<?php echo $configuration->phone ?>" class="form-control"
                       id="phone" placeholder="Phone">
            </div>
            <div class="form-group">
                <label for="facebook">Facebook Link</label>
                <input type="text" name="facebook" value="<?php echo $configuration->facebook ?>" class="form-control"
                       id="facebook" placeholder="Facebook">
            </div>
            <div class="form-group">
                <label for="skype">Skype Link</label>
                <input type="text" name="skype" value="<?php echo $configuration->skype ?>" class="form-control"
                       id="skype" placeholder="Skype">
            </div>
            <div class="form-group">
                <label for="twitter">Twitter Link</label>
                <input type="text" name="twitter" value="<?php echo $configuration->twitter ?>" class="form-control"
                       id="twitter" placeholder="Twitter">
            </div>
            <div class="form-group">
                <label for="gplus">Google Plus Link</label>
                <input type="text" name="gplus" value="<?php echo $configuration->gplus ?>" class="form-control"
                       id="gplus" placeholder="Google Plus">
            </div>
            <div class="form-group">
                <label for="youtube">Youtube Link</label>
                <input type="text" name="youtube" value="<?php echo $configuration->youtube ?>" class="form-control"
                       id="youtube" placeholder="Youtube">
            </div>
            <div class="form-group">
                <label for="instagram">Instagram</label>
                <input type="text" name="instagram" value="<?php echo $configuration->instagram ?>" class="form-control"
                       id="instagram" placeholder="Instagram">
            </div>
            <div class="form-group">
                <label for="pininterest">Pinterest</label>
                <input type="text" name="pininterest" value="<?php echo $configuration->pininterest ?>" class="form-control"
                       id="pininterest" placeholder="Pinterest">
            </div>
            <div class="form-group">
                <label for="linkedin">Linkedin</label>
                <input type="text" name="linkedin" value="<?php echo $configuration->linkedin ?>" class="form-control"
                       id="linkedin" placeholder="Linkedin">
            </div>
            <div class="form-group">
                <label for="vimeo">Vimeo</label>
                <input type="text" name="vimeo" value="<?php echo $configuration->vimeo ?>" class="form-control"
                       id="vimeo" placeholder="Vimeo">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="site_offline">Module Enabled: </label>
                <?php echo form_checkbox('enableAllModules', 'true', '', 'class="enableAllModules"') ?>
                Enable All Modules
                <?php
                $enabledMods = explode(",", $configuration->modules_enabled);
                if (isset($modules) && is_array($modules)) {
                    ?>
                    <ul class="listOfModules">
                        <?php
                        foreach ($modules as $ind => $val) {
                            ?>
                            <li>
                                <input type="checkbox"
                                       name="modulesEnabled[<?php echo $val->id ?>]"
                                       class="modulesEnabled"
                                       value="<?php echo $val->id; ?>" <?php if (in_array($val->id, $enabledMods) || isset($modulesEnabled[$val->id])) {
                                    echo 'checked="checked"';
                                } ?> />
                                <?php echo $val->name; ?></li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                }
                ?>
            </div>
            <div class="form-group site-menu">
                <label class="control-label">Number of Menus :</label>
                <?php
                echo form_input('no_of_menus', $no_of_menus, 'class="form-control"" id="noofmenus" style="width:50px; float:left; margin-right:10px;"');
                ?>
                <a class="btn btn-default" id="btn_nomOk"
                   style="margin-top:0px !important;"><span>Ok</span></a>

                <div class="typemenudetails"></div>
            </div>
            <?php if (is_array($menuName)) {
                foreach ($menuName as $ind => $val) { ?>
                    <div class="form-group addedMenus row">
                        <div class="col-lg-6">
                            <span
                                class="site-menu-block">Type Name : <?php echo form_input('menuName[' . $ind . ']', $val, 'class="form-control"') ?></span>
                        </div>
                        <div class="col-lg-6">
                        <span class="site-menu-block">&nbsp;&nbsp;Sub Menus :
                            <?php
                            if (empty($submenus[$ind]) || $submenus[$ind] == "yes") {
                                $yes = TRUE;
                                $no = FALSE;
                            } else {
                                $yes = FALSE;
                                $no = TRUE;
                            }
                            echo form_radio("submenus[{$ind}]", 'yes', $yes) . "Yes ";
                            echo form_radio("submenus[{$ind}]", 'no', $no) . "No ";
                            ?>
                        </span>
                        </div>
                        <div class="col-lg-6">
                            <span
                                class="site-menu-block">&nbsp;&nbsp;Depth: <?php echo form_input('depth[' . $ind . ']', $depth[$ind], 'class="form-control"') ?> </span>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
            <div class="form-group">
                <label for="address">Address</label>
                <textarea rows="7" name="address" class="form-control" id="address"
                          placeholder="Address"><?php echo $configuration->address ?></textarea>
                <script>
                    window.onload = function () {
                        load_ckeditor('address')
                    };
                </script>
            </div>
            <div class="form-group">
                <label for="site-keyword">Meta Keyword</label>
                <input type="text" name="site_keyword" value="<?php echo $configuration->site_keyword ?>"
                       class="form-control" id="site-keyword" placeholder="Meta Keyword">
            </div>
            <div class="form-group">
                <label for="site-description">Meta Description</label>
                <textarea name="site_description" rows="7" class="form-control" id="site-description"
                          placeholder="Meta Description"><?php echo $configuration->site_description ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <!--<a class="btn btn-warning" href="<?php /*echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) */?>"><span>Cancel</span></a>-->
            </div>
        </div>
    </div>
</form>