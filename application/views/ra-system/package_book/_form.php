<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <?php
    $disable = '';
    if($id  != '') {
        $disable = ' disabled="disabled"';
        ?>
        <input type="hidden" name="package_destination_id" value="<?php echo $package_book->package_destination_id ?>">
        <input type="hidden" name="package_experience_id" value="<?php echo $package_book->package_experience_id ?>">
        <input type="hidden" name="package_id" value="<?php echo $package_book->package_id ?>">
        <input type="hidden" name="previous_departure_date_id" value="<?php echo $package_book_status->departure_date_id ?>">
        <input type="hidden" name="previous_book_status" value="<?php echo $package_book_status->booking_status_id ?>">
        <?php
    }
    ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="package_destination_id">Destination *</label>
                <select name="package_destination_id" id="package_destination_id" class="form-control" <?php echo $disable ?>>
                    <?php
                    if(isset($destinations) && !empty($destinations)) {
                        foreach($destinations as $destination) {
                            $selected = '';
                            if($destination->id == $package_book->package_destination_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $destination->id ?>"<?php echo $selected?>><?php echo $destination->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="package_experience_id">Experience *</label>
                <select name="package_experience_id" id="package_experience_id" class="form-control" <?php echo $disable ?>>
                    <?php
                    if(isset($experiences) && !empty($experiences)) {
                        foreach($experiences as $experience) {
                            $selected = '';
                            if($experience->id == $package_book->package_experience_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $experience->id ?>"<?php echo $selected ?>><?php echo $experience->name/* . '|' . $experience->destination_name*/ ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="package_id">Package Name *</label>
                <select name="package_id" id="package_id" class="form-control"<?php echo $disable ?>>
                    <?php
                    if(isset($packages) && !empty($packages)) {
                        foreach($packages as $val) {
                            $selected = '';
                            if($val->id == $package_book->package_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    } else {
                        ?>
                        <option>No Package Available</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="departure_date_id">Departure Date *</label>
                <select name="departure_date_id" id="departure_date_id" class="form-control">
                    <?php
                    if(isset($departure_dates) && !empty($departure_dates)) {
                        foreach($departure_dates as $val) {
                            $selected = '';
                            $disable = '';
                            if($val->id == $package_book_status->departure_date_id) {
                                $selected = ' selected="selected"';
                            }
                            if($val->all_allocation == $val->booked) {
                                $disable = ' disabled="disabled"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>"<?php echo $selected ?><?php echo $disable ?>><?php echo $val->departure_date ?></option>
                            <?php
                        }
                    } else {
                        if($id != '') {

                        } else {
                            $date = 0;
                            ?>
                            <option>No Date/Price Available</option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="number_of_pax">Number of pax *</label>
                <select name="number_of_pax" id="number_of_pax" class="form-control">
                    <option value="Select">Select</option>
                    <?php
                    $range = range(1, 20);
                    foreach($range as $val) {
                        $selected = '';
                        if($val == $package_book_status->number_of_pax) {
                            $selected = ' selected="selected"';
                        }
                        ?>
                        <option value="<?php echo $val ?>"<?php echo $selected ?>><?php echo $val ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="price_per_person">Price per Person(in $) *</label>
                <input type="hidden" name="price_per_person" value="<?php echo $package_book_status->price_per_person ?>">
                <input type="text" name="price_per_person" value="<?php echo $package_book_status->price_per_person ?>" class="form-control" id="price_per_person" placeholder="Price per Person(in $)" disabled>
            </div>
            <div class="form-group">
                <label for="total_price">Total Cost(in $) *</label>
                <input type="hidden" name="total_price" value="<?php echo $package_book_status->total_price ?>">
                <input type="text" name="total_price" value="<?php echo $package_book_status->total_price ?>" class="form-control" id="total_price" placeholder="Total Cost(in $)" disabled>
            </div>
            <div class="form-group">
                <label for="full_name">Full Name *</label>
                <input type="text" name="full_name" value="<?php echo $package_book_status->full_name ?>" class="form-control" id="full_name" placeholder="Full Name">
            </div>
            <div class="form-group">
                <label for="email_address">Email Address *</label>
                <input type="text" name="email_address" value="<?php echo $package_book_status->email_address ?>" class="form-control" id="email_address" placeholder="Email Address">
            </div>
            <div class="form-group">
                <label for="contact_address">Contact Address *</label>
                <input type="text" name="contact_address" value="<?php echo $package_book_status->contact_address ?>" class="form-control" id="contact_address" placeholder="Contact Address">
            </div>
            <div class="form-group">
                <label for="contact_number">Contact Nos *</label>
                <input type="text" name="contact_number" value="<?php echo $package_book_status->contact_number ?>" class="form-control" id="contact_number" placeholder="Contact Nos">
            </div>
            <div class="form-group">
                <label for="booking_status_id">Booking Status *</label>
                <select name="booking_status_id" id="booking_status_id" class="form-control">
                    <option value="1" <?php echo (isset($package_book_status->booking_status_id) && $package_book_status->booking_status_id == '1') ? 'selected="selected"' : '' ?>>Pending</option>
                    <option value="2" <?php echo (isset($package_book_status->booking_status_id) && $package_book_status->booking_status_id == '2') ? 'selected="selected"' : '' ?>>Confirmed</option>
                    <?php if($id != '') { ?>
                    <option value="3" <?php echo (isset($package_book_status->booking_status_id) && $package_book_status->booking_status_id == '3') ? 'selected="selected"' : '' ?>>Cancelled</option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="note">Note from DTD</label>
                <textarea rows="7" name="note" class="form-control" id="note" placeholder="Note from DTD"><?php //echo $package_book_status->note ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('note', true)
                    };
                </script>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <h3>Booking Status</h3>
            <?php
            $package_name = (isset($packages) && !empty($packages)) ? $packages[0]->name : '';
            $departure_date = (isset($departure_dates) && !empty($departure_dates)) ? $departure_dates[0]->departure_date : '';
            $allocated_booking = (isset($departure_dates) && !empty($departure_dates)) ? $departure_dates[0]->all_allocation : '';
            $total_booked = (isset($departure_dates) && !empty($departure_dates)) ? $departure_dates[0]->booked : '';
            $available_booking = (isset($departure_dates) && !empty($departure_dates)) ? $departure_dates[0]->available : '';
            if($id != '') {
                $package_name = $package_book_status->package_name;
                $departure_date = $package_book_status->departure_date;
                $allocated_booking = $package_book_status->allocation;
                $total_booked = $package_book_status->booked;
                $available_booking = $package_book_status->available;
            }
            ?>
            <div class="form-group">
                <label for="package_name">Package Name</label>
                <span id="selected_package_name"><?php echo $package_name ?></span>
            </div>
            <div class="form-group">
                <label for="departure_date">Departure Date</label>
                <span id="selected_departure_date"><?php echo $departure_date ?></span>
            </div>
            <div class="form-group">
                <label for="total_booking_available">Total Booking Available</label>
                <span id="selected_booking_available"><?php echo $allocated_booking ?></span>
            </div>
            <div class="form-group">
                <label for="total_booked">Total Booked</label>
                <span id="selected_total_booked"><?php echo $total_booked ?></span>
            </div>
            <div class="form-group">
                <label for="total_available">Total Available</label>
                <span id="selected_total_available"><?php echo $available_booking ?></span>
            </div>
            <?php if($id != '') { ?>
                <h3>Booking Logs</h3>
                <?php if (isset($logs) && !empty($logs)) { $i = 1; ?>
                    <div class="panel-group" id="accordion">
                        <?php foreach ($logs as $log) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="profie-history-title" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $i ?>">
                                            <div class="history-title"><?php echo '<span>Book/Update Date : </span>' . '<span>' . date('Y-m-d H:i:s', $log->created_date) . '</span>' ?></div>
                                            <div class="history-title"><?php echo '<span>Departure Date : </span>' . '<span>' . $log->departure_date . '</span>' ?></span></div>
                                            <div class="history-title"><?php echo '<sapn>Booking Status : </span>' . '<span>' . $log->booking_status . '</span>' ?></div>
                                        </a>
                                    </h4>
                                </div>
                                <div id="<?php echo $i ?>" class="panel-collapse collapse <?php echo $i == 1 ? 'in' : '' ?>">
                                    <div class="panel-body">
                                        <?php echo (!empty($log->note)) ? $log->note : 'No Note'; ?>
                                    </div>
                                </div>
                            </div>
                            <?php $i++; } ?>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>