<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th></th>
            <th>Package Name</th>
            <th></th>
            <th>Customer Name</th>
            <th>Departure Date</th>
            <th>Booked Date</th>
            <th>Number of Pax</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            <th>Booking Status</th>
            <th></th>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th>SN</th>
            <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
            <th>Package Name</th>
            <th>Booked By</th>
            <th>Customer Name</th>
            <th>Departure Date</th>
            <th>Booked Date</th>
            <th>Number of Pax</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            <th>Booking Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($rows) : $serial_number = 1; ?>
            <?php foreach ($rows as $row) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                    <td><?php echo $row->package_name ?></td>
                    <td><?php echo ($row->created_by != '0') ? get_userdata('name') : $row->full_name  ?></td>
                    <td><?php echo $row->full_name ?></td>
                    <td><?php echo $row->departure_date ?></td>
                    <td><?php echo date('Y-m-d', $row->created_date) ?></td>
                    <td><?php echo $row->number_of_pax ?></td>
                    <td><?php echo $row->price_per_person ?></td>
                    <td><?php echo $row->total_price ?></td>
                    <td><?php echo $row->booking_status ?></td>
                    <td>
                        <?php
                        $this->data['actionBtnData'] = [
                            'module' => 'package_book',
                            'moduleData' => $row,
                            'options' => 'E'
                        ];
                        $ci = &get_instance();
                        $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                        ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>