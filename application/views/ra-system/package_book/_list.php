<form action="<?php echo base_url(BACKENDFOLDER . '/' . $this->header['page_name'] . '/export') ?>" method="post">
    <div class="searchBySite" style="width:auto;display:inline-block">
        <span style="width:75px;">Export Filter in Field</span>
        <select name="export_filter_id" id="export_filter_id">
            <option value="0">All</option>
            <option value="package_destination" data-type="select">Package Destination</option>
            <option value="package_experience" data-type="select">Package Experience</option>
            <option value="package" data-type="select">Package</option>
            <option value="customer_name" data-type="select">Customer Name</option>
            <option value="booking_status" data-type="select">Booking Status</option>
            <option value="booking_date" data-type="input">Booking Date</option>
            <option value="departure_date" data-type="input">Departure Date</option>
        </select>
        <div class="searchFields" style="float: right;">
            <select name="export_filter_package_destination" id="export_filter_package_destination">
                <?php
                if(isset($package_destinations) && !empty($package_destinations)) {
                    foreach($package_destinations as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_package_experience" id="export_filter_package_experience">
                <?php
                if(isset($package_experiences) && !empty($package_experiences)) {
                    foreach($package_experiences as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_package" id="export_filter_package">
                <?php
                if(isset($packages) && !empty($packages)) {
                    foreach($packages as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_customer_name" id="export_filter_customer_name">
                <?php
                if(isset($customers) && !empty($customers)) {
                    foreach($customers as $val) {
                        ?>
                        <option value="<?php echo $val->full_name ?>"><?php echo $val->full_name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <select name="export_filter_booking_status" id="export_filter_booking_status">
                <?php
                if(isset($booking_status) && !empty($booking_status)) {
                    foreach($booking_status as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <input name="export_filter_booking_date" id="export_filter_booking_date" placeholder="Y-m-d">
            <input name="export_filter_departure_date" id="export_filter_departure_date" placeholder="Y-m-d">
        </div>
    </div>
    <input type="submit" name="submit_button" value="Export">
</form>
<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th></th>
            <th>Package Name</th>
            <th></th>
            <th>Customer Name</th>
            <th>Departure Date</th>
            <th>Booked Date</th>
            <th>Number of Pax</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            <th>Booking Status</th>
            <th></th>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th>SN</th>
            <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
            <th>Package Name</th>
            <th>Booked By</th>
            <th>Customer Name</th>
            <th>Departure Date</th>
            <th>Booked Date</th>
            <th>Number of Pax</th>
            <th>Unit Price</th>
            <th>Total Price</th>
            <th>Booking Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($rows) : $serial_number = 1; ?>
            <?php foreach ($rows as $row) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                    <td><?php echo $row->package_name ?></td>
                    <td><?php echo ($row->created_by != '0') ? get_userdata('name') : $row->full_name  ?></td>
                    <td><?php echo $row->full_name ?></td>
                    <td><?php echo $row->departure_date ?></td>
                    <td><?php echo date('Y-m-d', $row->created_date) ?></td>
                    <td><?php echo $row->number_of_pax ?></td>
                    <td><?php echo $row->price_per_person ?></td>
                    <td><?php echo $row->total_price ?></td>
                    <td><?php echo $row->booking_status ?></td>
                    <td>
                        <?php
                        $this->data['actionBtnData'] = [
                            'module' => 'package_book',
                            'moduleData' => $row,
                            'options' => 'E'
                        ];
                        $ci = &get_instance();
                        $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                        ?>
                    </td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>