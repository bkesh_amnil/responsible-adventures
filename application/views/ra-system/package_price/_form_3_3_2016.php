<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="valid_from">Valid From *</label>
                <input type="text" name="valid_from" value="<?php echo isset($package_price->valid_from) ? $package_price->valid_from : '' ?>" class="form-control" id="valid_from" placeholder="Valid From">
            </div>
            <div class="form-group">
                <label for="valid_to">Valid To *</label>
                <input type="text" name="valid_to" value="<?php echo isset($package_price->valid_to) ? $package_price->valid_to : '' ?>" class="form-control" id="valid_to" placeholder="Valid To">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="number_of_pax">Number of Pax (format 1 - 10)*</label>
                <input type="text" name="number_of_pax" value="<?php echo isset($package_price->number_of_pax) ? $package_price->number_of_pax : '' ?>" class="form-control" id="number_of_pax" placeholder="Number of pax">
            </div>
            <div class="form-group">
                <label for="unit_price">Unit Price (in Rs) *</label>
                <input type="text" name="unit_price" value="<?php echo isset($package_price->unit_price) ? $package_price->unit_price : '' ?>" class="form-control" id="unit_price" placeholder="Unit Price">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo (isset($package_price->status) && $package_price->status == '1') ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo (isset($package_price->status) && $package_price->status == '0') ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <?php if(empty($package_price->id)) { ?>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group"><span class="add-info"> [ + Add ]</span></div>
            </div>
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    <table class="table table-bordered multi-table" id="dynamic_table" width="100%">
                        <thead>
                        <th>From</th>
                        <th>To</th>
                        <th>Number of Pax</th>
                        <th>Unit Price(in Rs)</th>
                        <th>Action</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary" id="action">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/'. $package_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>