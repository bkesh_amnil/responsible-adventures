<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
    <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th></th>
            <th>Video Name</th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
    <thead>
        <tr>
            <th>SN</th>
            <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
            <th>Video Name</th>
            <th>Video Image</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php if ($rows) : $serial_number = 1; ?>
        <?php foreach ($rows as $row) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                <td><?php echo $row->name ?></td>
                <td><img src="http://i4.ytimg.com/vi/<?php echo $row->video_id; ?>/default.jpg"/></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'video',
                        'moduleData' => $row,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
</form>