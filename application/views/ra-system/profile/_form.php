<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="full_name">Full Name : </label>
                <span><?php echo $profile->name ?></span>
            </div>
            <div class="form-group">
                <label for="email">Email Address : </label>
                <span><?php echo $profile->email ?></span>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="role_id">User Role : </label>
                <span>
                    <?php
                    foreach($profile_roles as $profile_role) {
                        if($profile->role_id == $profile_role->id) {
                            echo $profile_role->name;
                        }
                    }
                    ?>
                </span>
            </div>
            <div class="form-group">
                <label for="username">Username : </label>
                <span><?php echo $profile->username ?></span>
            </div>
        </div>
    </div>
    <!--<div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>-->
</form>