<div class="searchBySite" style="width:250px;">
    <span style="width:75px;">Menu Type</span>
    <select name="menu_type_id">
        <option value="0">All Menus</option>
    <?php
    if(!empty($menu_types)) {
        foreach($menu_types as $menu_type) {
        ?>
        <option value="<?php echo $menu_type->id ?>"><?php echo $menu_type->name ?></option>
        <?php
        }
    }
    ?>
    </select>
</div>
<form action="" method="post" id="gridForm" autocomplete="off">
<table class="table table-bordered table-hover list-datatable">
    <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th></th>
            <th>Menu Title</th>
            <th>Link Type</th>
            <th></th>
        </tr>
    </tfoot>
    <thead>
        <tr>
            <th>SN</th>
            <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
            <th>Menu Title</th>
            <th>Link Type</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php if ($menus) : $serial_number = 1;?>
        <?php foreach ($menus as $row) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><input type="checkbox" name="selected[]" value="<?php echo $row['id']; ?>" class="rowCheckBox" /></td>
                <td><?php echo $row['name']; ?></td>
                <td><?php echo $row['menu_link_type']; ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'menu',
                        'moduleData' => (object) $row,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');

                    if(isset($row['childList']) && !empty($row['childList']) && count($row['childList'] > 1)){
                        ?>
                        <a href="<?php echo base_url(BACKENDFOLDER.'/'.segment(2).'/sort/' . $row['menu_type_id'] . '/'. $row['id']) ?>" data-toggle="ajaxModal" class="btn btn-info btn-xs sortMenu">
                            <i class="fa fa-sort fa-fw"></i> Sort
                        </a>
                        <?php
                    }
                    ?>
                </td>
            </tr>
            <?php
            if(isset($row['childList']) && !empty($row['childList'])) :
                foreach($row['childList'] as $ind => $child) :
                    ?>
                    <tr>
                        <td><?php echo $serial_number; $serial_number++; ?></td>
                        <td><input type="checkbox" name="selected[]" value="<?php echo $child['id']; ?>" class="rowCheckBox" /></td>
                        <td><?php echo '--'.$child['name'] ?></td>
                        <td><?php echo $child['menu_link_type']; ?></td>
                        <td>
                            <?php
                            $this->data['actionBtnData'] = [
                                'module' => 'menu',
                                'moduleData' => (object)$row['childList'][$ind],
                                'options' => 'EDS'
                            ];
                            $ci = &get_instance();
                            $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                            ?>
                        </td>
                    </tr>
                    <?php
                endforeach;
            endif;
            ?>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
</form>