<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="menu_type">Menu Type *</label>
                <select name="menu_type" class="form-control" id="menu_type">
                    <option>Select</option>
                    <?php if ($menu_type) : ?>
                        <?php foreach ($menu_type as $type) : ?>
                            <option value="<?php echo $type->id ?>" <?php echo (isset($menu->menu_type_id) && $menu->menu_type_id == $type->id) ? 'selected' : '' ?>><?php echo $type->name; ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Title *</label>
                <?php if($menu->relate_destination == '1' && $menu->destination_id != '0') { ?>
                    <input type="hidden" name="name" value="<?php echo $menu->name ?>" class="form-control" id="title" placeholder="Title">
                    <input type="text" name="name" value="<?php echo $menu->name; ?>" class="form-control title" id="title" placeholder="Title" disabled>
                <?php } else { ?>
                    <input type="text" name="name" value="<?php echo $menu->name; ?>" class="form-control title" id="title" placeholder="Title">
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="link">Alias *</label>
                <?php if($menu->relate_destination == '1' && $menu->destination_id != '0') { ?>
                    <input type="hidden" name="slug" value="<?php echo $menu->slug; ?>" class="form-control slug" id="slug" placeholder="Alias">
                    <input type="text" name="slug" value="<?php echo $menu->slug; ?>" class="form-control slug" id="slug" placeholder="Alias" disabled>
                <?php } else { ?>
                    <input type="text" name="slug" value="<?php echo $menu->slug; ?>" class="form-control slug" id="slug" placeholder="Alias">
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="menu_parent">Menu Parent : *</label>
                <?php
                if (isset($menu->menu_type_id) && $menu->menu_type_id > 0) {
                    $availableMenus = $this->administrator_model->getAvailableMenus($menu->menu_type_id, $menu->menu_parent);
                } else {
                    $availableMenus = '<option value="0">Parent</option>';
                }
                ?>
                <select name="menu_parent" class="form-control" id="parentList">
                    <?php echo $availableMenus; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status </label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $menu->status == '1' || $menu->status == '' ? 'selected' : '' ?>>
                        Active
                    </option>
                    <option value="0" <?php echo $menu->status == '0' ? 'selected' : '' ?>>
                        InActive
                    </option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="page_title">Page Title for Menu *</label>
                <input type="text" name="page_title" value="<?php echo $menu->page_title; ?>" class="form-control" id="page_title" placeholder="Page Title">
            </div>
            <div class="form-group">
                <label for="menu_heading">Menu Heading for Menu *</label>
                <input type="text" name="menu_heading" value="<?php echo $menu->menu_heading; ?>" class="form-control" id="menu_heading" placeholder="Menu Heading">
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords for Menu</label>
                <input type="text" name="meta_keywords" value="<?php echo $menu->meta_keywords; ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description for Menu</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $menu->meta_description ?></textarea>
                <!--<script>
                    window.onload = function() {
                        load_ckeditor('meta_description', true)
                    };
                </script>-->
            </div>
        </div>
        <div class="col-lg-6 col-md-6"><h3>Menu Page Settings</h3></div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="menu_link_type" class="control-label">Menu Link Type : *</label>
                <?php
                $menu_link_types['none'] = "None";
                $menu_link_types['url'] = "URL";
                $menu_link_types['page'] = "Page";
                /*$menu_link_types['site'] = "Site";*/
                if (empty($menu_link_type) || $menu_link_type == "none") {
                    $pageDisplay = 'display:none;';
                    $urlDisplay = 'display:none;';
                    $siteDisplay = 'display:none;';
                } else if ($menu_link_type == "url") {
                    $pageDisplay = 'display:none;';
                    $urlDisplay = 'display:table-row;';
                    $siteDisplay = 'display:none;';
                } else if ($menu_link_type == "page") {
                    $pageDisplay = 'display:table-row;';
                    $urlDisplay = 'display:none;';
                    $siteDisplay = 'display:none;';
                } else if ($menu_link_type == "site") {
                    $pageDisplay = 'display:none;';
                    $urlDisplay = 'display:none;';
                    $siteDisplay = 'display:table-row;';
                }
                ?>
                <select name="menu_link_type" class="form-control" id="menu_link_type">
                    <?php
                    foreach($menu_link_types as $ind => $val) {
                        $selected = '';
                        if($ind == $menu->menu_link_type) {
                            $selected = ' selected="selected"';
                        }
                        ?>
                        <option value="<?php echo $ind ?>"<?php echo $selected ?>><?php echo $val ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="menu_opens">Menu Opens</label>
                <select name="menu_opens" class="form-control">
                    <?php foreach($menu_opens as $ind => $menu_open) { ?>
                        <option value="<?php echo $ind ?>"><?php echo $menu_open ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <?php
        if (empty($menu->menu_link_type) || $menu->menu_link_type == "none") {
            $pageDisplay = 'display:none;';
            $urlDisplay = 'display:none;';
            $siteDisplay = 'display:none;';
        } else if ($menu->menu_link_type == "url") {
            $pageDisplay = 'display:none;';
            $urlDisplay = 'display:table-row;';
            $siteDisplay = 'display:none;';
        } else if ($menu->menu_link_type == "page") {
            $pageDisplay = 'display:table-row;';
            $urlDisplay = 'display:none;';
            $siteDisplay = 'display:none;';
        } else if ($menu->menu_link_type == "site") {
            $pageDisplay = 'display:none;';
            $urlDisplay = 'display:none;';
            $siteDisplay = 'display:table-row;';
        }
        ?>
        <div class="col-lg-12 col-md-12">
            <table width="100%">
                <tbody>
                <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                    <th scope="row">Main Content Modules Setting:</th>
                    <td><span class="add_module" length="<?php echo count($settings); ?>">[Add]</span>
                    </td>
                </tr>
                <tr class="setingsRow page" style="<?php echo $pageDisplay; ?>">
                    <td colspan="2" id="moduleSettings"><?php
                        if (isset($settings) && is_array($settings) && count($settings) > 0) {
                            $i = 0;
                            foreach ($settings as $setting) {
                                if ($i > 0) {
                                    $removeCls = " remove_module";
                                    $removeText = "Remove";
                                } else {
                                    $removeCls = "";
                                    $removeText = "";
                                }
                                ?>
                                <div class="div_main_content" position="<?php echo $i ?>">
                                    <?php
                                    /*if (isset($setting->category_id) && intval($setting->category_id) > 0) {
                                        $categoryName = $this->administrator_model->getCategoryAlias($setting->category_id);
                                    } else {
                                        $categoryName = "";
                                    }*/
                                    ?>
                                    <div class="selectModule"><span>Module Title</span><br/>
                                        <span>
                                            <input type="text" class="col-lg-12 form-control" name="module_title[<?php echo $i; ?>]" value="<?php echo isset($setting->module_title) ? $setting->module_title : (isset($setting["module_title"]) ? $setting["module_title"] : ""); ?>"/>
                                        </span>
                                    </div>
                                    <div class="selectModule">
                                        <span>Select Module</span><br/>
                                        <span>
                                            <?php
                                            $moduleId = isset($setting->module_id) ? $setting->module_id : (isset($setting["module_id"]) ? $setting["module_id"] : "");
                                            echo form_dropdown("module_id[$i]", $modules, $moduleId, 'class="col-lg-12 form-control module_id"')
                                            ?>
                                        </span>
                                    </div>
                                    <div class="selectCategory"><span>Select Category</span><br/>
                                        <span>
                                            <select name="category_id[]" class="form-control category_id">
                                                <option value="0">All</option>
                                                <?php
                                                foreach($categories as $category) {
                                                    $selected = '';
                                                    if($category->id == $setting->category_id) {
                                                        $selected = ' selected="selected"';
                                                    }
                                                    ?>
                                                    <option value="<?php echo $category->id ?>"<?php echo $selected ?>><?php echo $category->name ?></option>
                                                <?php } ?>
                                            </select>
                                        </span>
                                    </div>
                                    <input type="hidden"
                                           class="selectedValue selectedModulesIds_<?php echo $i; ?>"
                                           name="selectedModulesIds[<?php echo $i; ?>]"
                                           value="<?php echo(isset($setting->content_ids) ? $setting->content_ids : ""); ?>"/>
                                    <?php if (!empty($removeCls)) { ?>
                                        <span class="addRemove<?php echo $removeCls; ?>"><?php echo $removeText; ?></span>
                                    <?php } ?>
                                    <div style="clear:both"></div>
                                    <div class="div_module_detail">
                                        <?php
                                        if (isset($setting->module_id) && isset ($setting->content_ids) && isset ($setting->category_id)) {
                                            $this->administrator_model->get_module_contents(1, $setting->module_id, $i, $setting->content_ids, $setting->category_id);
                                        } else if (isset($setting->module_id) && isset ($setting->content_ids) && isset ($setting->category_id)) {
                                            $this->administrator_model->get_module_contents(1, $setting->module_id, $i, $setting->content_ids, $setting->category_id);
                                        }
                                        ?>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                                <?php $i++;
                            }
                        } else {
                            ?>
                            <div class="div_main_content" position="0">
                                <div class="selectModule"><span>Module Title</span><br/>
                            <span>
                            <input type="text" class="col-lg-12 form-control" name="module_title[0]" value=""/>
                            </span></div>
                                <div class="selectModule"><span>Select Module</span><br/>
                                    <span>
                                        <select name="module_id[0]" class="form-control module_id">
                                            <?php foreach($modules as $ind => $module) { echo '<pre>'; print_r($module); ?>
                                                <option value="<?php echo $ind ?>"><?php echo $module ?></option>
                                            <?php } ?>
                                        </select>
                                    </span>
                                </div>
                                <div class="selectCategory">
                                    <span>Select Category</span><br/>
                                    <span>
                                        <select name="category_id[0]" class="form-control category_id">
                                            <option value="0">All</option>
                                            <?php foreach($categories as $category) { ?>
                                                <option value="<?php echo $category->id ?>"><?php echo $category->name ?></option>
                                            <?php } ?>
                                        </select>
                                    </span>
                                </div>
                                <input type="hidden"
                                       class="selectedValue selectedModulesIds_0"
                                       name="selectedModulesIds[0]" value=""/>
                                <span class="addRemove"></span>

                                <div style="clear:both"></div>
                                <div class="div_module_detail"></div>
                                <div style="clear:both"></div>
                            </div>
                            <?php
                        }
                        ?></td>
                </tr>
                <tr class="setingsRow url" style="<?php echo $urlDisplay; ?>">
                    <td>
                        <div class="form-group">
                            <label for="menu_url">Url For The Menu:</label>
                            <?php echo form_input('menu_url', $menu->menu_url, 'class="form-control"') ?>
                        </div>
                    </td>
                </tr>
                <!--<tr class="setingsRow site" style="<?php /*echo $siteDisplay; */?>">
                    <td>
                        <div class="col-lg-6">
                            <label for="menu_site">Site Link For The Menu:</label>
                            <?php /*echo form_dropdown('menu_site', $site_names, $menu_site, 'class="col-lg-12"') */?>
                        </div>
                    </td>
                </tr>-->
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>