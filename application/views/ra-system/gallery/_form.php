<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $gallery->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="name">Alias *</label>
                <input type="text" name="slug" value="<?php echo $gallery->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $gallery->status == '1' || $gallery->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $gallery->status == '0' ? 'selected' : '' ?>>InActive</option>
                </select>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $gallery->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $gallery->meta_description ?></textarea>
            </div>
            <!--<div class="form-group">
                <label for="name">Media Type</label>
                <?php /*if(isset($savedMedia)) { */?>
                    <input type="hidden" value="<?php /*echo strtolower($savedMedia[0]->type) */?>" name="mediaTypeSelect" />
                <?php /*} */?>
                <label>
                    <input type="radio" <?php /*echo (segment(4) != '') ? 'disabled' : '' */?> name="mediaTypeSelect" class="mediaTypeSelect" checked value="image"> Image
                </label>
                <label>
                    <input type="radio" <?php /*echo (segment(4) != '') ? 'disabled' : '' */?> name="mediaTypeSelect" class="mediaTypeSelect" value="video"> Video
                </label>
            </div>
            <div class="form-group" id="selectVideos" <?php /*echo (isset($savedMedia) && $savedMedia[0]->type != 'Video') ? 'style="display:none;"' : (!isset($savedMedia)) ? 'style="display:none;"' : '' */?>>
                <label for="video">Videos</label>
                <input type="text" name="video" class="form-control" id="video" placeholder="Videos">
            </div>-->
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group" id="selectImages">
                <label for="cover">Cover Image *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="cover" value="<?php echo $gallery->cover ?>" class="form-control" id="cover" placeholder="Cover Image">
            </div>
            <?php if($gallery->cover != '') { ?>
                <img src="<?php echo base_url($gallery->cover) ?>" width="20%"/>
            <?php } ?>
            <div class="form-group" id="selectImages" <?php echo (isset($savedMedia) && !empty($savedMedia) && $savedMedia[0]->type != 'Image') ? 'style="display:none;"' : '' ?>>
                <label for="image">Images *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="true"
                       name="image" data-show-detail="true" class="form-control" id="image" placeholder="Images">
            </div>
            <?php
            if(isset($savedMedia) && !empty($savedMedia)) { ?>
                <?php foreach($savedMedia as $media) { ?>
                    <div class="mediaWrapper">
                        <?php if($media->type == 'Image') { ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="img-responsive" src="<?php echo base_url($media->media) ?>"/>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control" readonly="readonly" value="<?php echo $media->media ?>" name="media[]"/>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Title" value="<?php echo $media->title ?>" name="title[]"/>
                                    </div>
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Description" name="description[]"><?php echo $media->caption ?></textarea>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <input type="hidden" value="<?php echo $media->media ?>" name="media[]"/>
                            <div class="image-wrapper">
                                <img class="img-responsive" src="http://img.youtube.com/vi/<?php echo $media->media ?>/3.jpg"/>
                            </div>
                        <?php } ?>
                        <a href="javascript:void(0);" class="deleteMedia"
                           data-url="<?php echo base_url(BACKENDFOLDER . '/gallery/deleteMedia/' . $media->id) ?>">
                            Delete
                        </a>
                    </div>
                <?php } ?>
            <?php }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>

<!-- Modal -->
<div class="modal fade" id="galleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Media Details</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url(BACKENDFOLDER . '/gallery/saveMediaDetail') ?>" method="post">
                    <div class="form-group">
                        <label for="mediaTitle">Title</label>
                        <input class="form-control" type="text" name="mediaTitle" id="mediaTitle" placeholder="Media Title"/>
                    </div>
                    <div class="form-group">
                        <label for="mediaCaption">Caption</label>
                        <textarea rows="8" class="form-control" name="mediaCaption" id="mediaCaption" placeholder="Media Caption"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="mediaDescription">Description</label>
                        <textarea rows="8" class="form-control" name="mediaDescription" id="mediaDescription" placeholder="Media Description"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Save" class="btn btn-primary" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>