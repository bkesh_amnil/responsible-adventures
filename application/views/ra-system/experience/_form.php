<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="destination_id">Destination *</label>
                <select name="destination_id" id="destination_id" class="form-control">
                    <?php
                    if(isset($destinations) && !empty($destinations)) {
                        foreach($destinations as $destination) {
                            $selected = '';
                            if($destination->id == $experience->destination_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $destination->id ?>"<?php echo $selected ?>><?php echo $destination->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Title *</label>
                <input type="text" name="name" value="<?php echo $experience->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="name">Alias *</label>
                <input type="text" name="slug" value="<?php echo $experience->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="sub_title">Sub Title *</label>
                <input type="text" name="sub_title" value="<?php echo $experience->sub_title ?>" class="form-control" id="sub_title" placeholder="Sub Title">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $experience->status == '1' || $experience->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $experience->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $experience->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $experience->meta_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="image">Image(488 X 350) *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $experience->image ?>" class="form-control" id="image" placeholder="Image">
                <?php if($experience->image != '') { ?>
                    <img src="<?php echo base_url($experience->image) ?>" width="25%"/>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="image">Icon Image(69 X 44) *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="icon" value="<?php echo $experience->icon ?>" class="form-control" id="icon" placeholder="Icon Image">
                <?php if($experience->icon != '') { ?>
                    <img src="<?php echo base_url($experience->icon) ?>" width="25%"/>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="description">Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Banner Description"><?php echo $experience->description ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('description');
                    };
                </script>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>