<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $news->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="slug">Alias *</label>
                <input type="text" name="slug" value="<?php echo $news->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="publish_date">News Publish Date *</label>
                <input class="form-control" type="text" id="valid_from" name="publish_date" value="<?php echo $news->publish_date ?>" placeholder="News Publish Date"/>
            </div>
            <div class="form-group">
                <label for="unpublish_date">News UnPublish Date *</label>
                <input class="form-control" type="text" id="valid_to" name="unpublish_date" value="<?php echo $news->unpublish_date ?>" placeholder="News UnPublish Date"/>
            </div>
            <div class="form-group">
                <label for="posted_by">Posted By *</label>
                <input class="form-control" type="text" id="posted_by" name="posted_by" value="<?php echo $news->posted_by ?>" placeholder="Posted By">
            </div>
            <div class="form-group">
                <label for="is_highlighted">Highlight News:</label>
                <input type="radio" name="is_highlighted" value="0" <?php echo $news->is_highlighted == '0' || $news->is_highlighted == '' ? 'checked' : '' ?>>No
                <input type="radio" name="is_highlighted" value="1" <?php echo $news->is_highlighted == '1' ? 'checked' : '' ?>>Yes
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $news->status == '1' || $news->status == '' ? 'selected' : '' ?>>
                        Publish
                    </option>
                    <option value="0" <?php echo $news->status == '0' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="image">Image (1920 X 416)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false"
                       name="image" value="<?php echo $news->image ?>" class="form-control"
                       id="image"
                       placeholder="Image">
                <?php if ($news->image != '') { ?>
                    <div class="image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($news->image) ?>"/>
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="short_description">News Short Description</label>
                <textarea rows="7" name="short_description" class="form-control" id="short_description"
                          placeholder="News Short Description"><?php echo $news->short_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $news->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $news->meta_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="description">News Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description"
                          placeholder="news Description"><?php echo $news->description ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
            <label for="news_category_id">News Category *</label>
            <br/>
            <select class="multi-select" multiple="multiple" name="news_category_id[]" id="news_category_id">
                <?php
                if(isset($news_categories) && !empty($news_categories)) {
                    foreach($news_categories as $news_category) {
                        $selected = '';
                        if(in_array($news_category->id, explode(',', $news->news_category_id))) {
                            $selected = ' selected="selected"';
                        }
                        ?>
                        <option value="<?php echo $news_category->id ?>"<?php echo $selected ?>><?php echo $news_category->name ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        load_ckeditor('description');
    };
</script>