<form action="<?php echo base_url(BACKENDFOLDER . '/import_form_data/import') ?>" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="type">Type</label>
                <select name="type" id="type" class="form-control">
                    <option value="event">Event</option>
                    <option value="race">Race</option>
                </select>
            </div>
            <div class="form-group">
                <label for="id">Name</label>
                <select name="id" id="id" class="form-control">
                    <?php
                    if(isset($rows) && !empty($rows)) {
                        foreach($rows as $val) {
                            ?>
                            <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="excel">Import File</label>
                <input type="file" name="excel" id="excel" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Import</button>
            </div>
        </div>
    </div>
</form>