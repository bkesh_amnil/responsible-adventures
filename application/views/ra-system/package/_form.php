<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="destination_id">Destination *</label>
                <select name="destination_id" id="destination_id" class="form-control">
                    <?php
                    if(isset($destinations) && !empty($destinations)) {
                        foreach($destinations as $destination) {
                            $selected = '';
                            if($destination->id == $package->destination_id) {
                                $selected = ' selected="selected"';
                            }
                        ?>
                        <option value="<?php echo $destination->id ?>"<?php echo $selected ?>><?php echo $destination->name ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="experience_id">Experience *</label>
                <select name="experience_id" id="experience_id" class="form-control">
                    <option>Select</option>
                    <?php
                    if(!empty($experiences)) {
                      foreach($experiences as $experience) {
                          $selected = '';
                          if($experience->id == $package->experience_id) {
                              $selected = ' selected="selected"';
                          }
                        ?>
                          <option value="<?php echo $experience->id ?>"<?php echo $selected ?>><?php echo $experience->name ?></option>
                        <?php
                      }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="area_id">Area *</label>
                <select name="area_id" id="area_id" class="form-control">
                    <option>Select</option>
                    <?php
                    if(!empty($areas)) {
                        foreach($areas as $area) {
                            $selected = '';
                            if($area->id == $package->area_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $area->id ?>"<?php echo $selected ?>><?php echo $area->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="code">Code *</label>
                <input type="text" name="code" value="<?php echo $package->code ?>" class="form-control" id="name" placeholder="Code">
            </div>
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $package->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="slug">Slug *</label>
                <input type="text" name="slug" value="<?php echo $package->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="duration">Duration *</label>
                <input type="text" name="duration" value="<?php echo $package->duration ?>" class="form-control" id="slug" placeholder="Duration">
            </div>
            <div class="form-group">
                <label for="minimum_group_size">Minimum Group Size *</label>
                <input type="text" name="minimum_group_size" value="<?php echo $package->minimum_group_size ?>" class="form-control" id="slug" placeholder="Minimum Group Size">
            </div>
            <div class="form-group">
                <label for="best_season">Best Season *</label>
                <input type="text" name="best_season" value="<?php echo $package->best_season ?>" class="form-control" id="slug" placeholder="Best Season">
            </div>
            <div class="form-group">
                <label for="altitude">Altitude (in mts.) *</label>
                <input type="text" name="altitude" value="<?php echo $package->altitude ?>" class="form-control" id="slug" placeholder="Altitude (in mts.)">
            </div>
            <div class="form-group">
                <label for="difficulty_id">Difficulty *</label>
                <select name="difficulty_id" id="difficulty_id" class="form-control">
                    <option>Select</option>
                    <?php
                    if(!empty($package_difficulty)) {
                        foreach($package_difficulty as $val) {
                            $selected = '';
                            if($val->id == $package->difficulty_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->id ?>" <?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="is_highlighted">Highlight Package</label>
                <select name="is_highlighted" id="is_highlighted" class="form-control">
                    <option value="1" <?php echo $package->is_highlighted == '1'? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo $package->is_highlighted == '0' || $package->is_highlighted == '' ? 'selected' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="support">Support *</label>
                <input type="radio" value="1"  name="support" <?php echo isset($package->support) && $package->support == '1' ? 'checked="checked"' : '' ?>>Yes
                <input type="radio" value="0"  name="support" <?php echo isset($package->support) && $package->support == '0' ? 'checked="checked"' : '' ?>>No
            </div>
            <div class="form-group">
                <label for="is_recommended">Is Recommended *</label>
                <input type="radio" value="1"  name="is_recommended" <?php echo (isset($package->is_recommended) && $package->is_recommended == 1) ? 'checked' : '' ?>>Yes
                <input type="radio" value="0"  name="is_recommended" <?php echo (isset($package->is_recommended) && $package->is_recommended == 0) ? 'checked' : '' ?>>No
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $package->status == '1' || $package->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $package->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group" id="selectImages">
                <label for="cover_image">Cover Image (570 X 350)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="cover_image" value="<?php echo $package->cover_image ?>" class="form-control" id="cover_image" placeholder="Cover Image">
            </div>
            <?php if($package->cover_image != '') { ?>
                <img src="<?php echo base_url($package->cover_image) ?>" width="20%"/>
            <?php } ?>
            <div class="form-group">
                <label for="file">File (pdf only)*</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="all" data-multiple="false" name="file" value="<?php echo $package->file ?>" class="form-control" id="files" placeholder="File">
            </div>
            <div class="form-group">
                <label for="map_file">Map File (10MB size kml only)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="all" data-multiple="false" name="map_file" value="<?php echo $package->map_file ?>" class="form-control" id="map_files" placeholder="Map File">
            </div>
            <div class="form-group">
                <label for="departure_type_id">Departure Type *</label>
                <?php
                $disable = '';
                if(isset($package->departure_type) && !empty($package->departure_type) && $package->departure_type != 'Select') {
                    $disable = ' disabled="disabled"';
                    ?>
                    <input type="hidden" name="departure_type" value="<?php echo $package->departure_type ?>">
                    <?php
                }
                ?>
                <select name="departure_type" id="departure_type" class="form-control"<?php echo $disable ?>>
                   <?php
                    if(!empty($package_departure)) {
                        foreach($package_departure as $val) {
                            $selected = '';
                            if($val->code == $package->departure_type) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $val->code ?>"<?php echo $selected ?>><?php echo $val->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="departure_year">Departure Year *</label>
                <?php
                if(isset($package->departure_year) && !empty($package->departure_year) && !is_array($package->departure_year)) {
                    $saved_years = explode(',', $package->departure_year);
                    //if($id != 0) {
                        ?>
                        <input type="hidden" name="saved_departure_year" value="<?php echo $package->departure_year ?>">
                        <?php
                    //}
                }
                $years = range(2015, 2035);
                if(isset($years) && !empty($years)) {
                    ?>
                    <div>
                    <?php
                    foreach($years as $year) {
                        $disable = '';
                        if(isset($saved_years) && in_array($year, $saved_years)) {
                            $disable = ' disabled="disabled"';
                        }
                        ?>
                        <span>
                        <input type="checkbox" name="departure_year[]" value="<?php echo $year ?>"<?php echo $disable ?>>
                        <?php echo $year; ?>
                        </span>
                        <?php
                    }
                    ?>
                    </div>
                <?php
                }
                ?>
            </div>
            <div class="form-group">
                <label for="short_description">Short Description *</label>
                <textarea rows="7" name="short_description" class="form-control" id="short_description" placeholder="Short Description"><?php echo $package->short_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $package->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $package->meta_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="description">Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Description"><?php echo $package->description ?></textarea>
            </div>
            <div class="form-group">
                <label for="itinerary">Itinerary *</label>
                <textarea rows="7" name="itinerary" class="form-control" id="itinerary" placeholder="Itinerary"><?php echo $package->itinerary ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description');
        load_ckeditor('itinerary');
    };
</script>