<form action="<?php echo base_url(BACKENDFOLDER . '/' . $this->header['page_name'] . '/export') ?>" method="post">
    <div class="searchBySite" style="width:auto;display:inline-block">
        <span style="width:75px;">Export Filter in Field</span>
        <select name="export_filter_id" id="export_filter_id">
            <option value="0">All</option>
            <option value="package" data-type="select">Package</option>
            <option value="download_date" data-type="input">Departure Date</option>
        </select>
        <div class="searchFields" style="float: right;">
            <select name="export_filter_package" id="export_filter_package">
                <?php
                if(isset($packages) && !empty($packages)) {
                    foreach($packages as $val) {
                        ?>
                        <option value="<?php echo $val->id ?>"><?php echo $val->name ?></option>
                        <?php
                    }
                } else { ?>
                    <option value="0">No Data</option>
                <?php } ?>
            </select>
            <input name="export_filter_download_date" id="export_filter_download_date" placeholder="Y-m-d">
        </div>
    </div>
    <input type="submit" name="submit_button" value="Export">
</form>
<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
        <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th>Package Name</th>
            <th>Customer Name</th>
            <th>Customer Email</th>
            <th>Download Date</th>
        </tr>
        </tfoot>
        <thead>
        <tr>
            <th>SN</th>
            <th>Package Name</th>
            <th>Customer Name</th>
            <th>Customer Email</th>
            <th>Download Date</th>
        </tr>
        </thead>
        <tbody>
        <?php if ($rows) : $serial_number = 1; ?>
            <?php foreach ($rows as $row) : ?>
                <tr>
                    <td><?php echo $serial_number; $serial_number++; ?></td>
                    <td><?php echo $row->package_name ?></td>
                    <td><?php echo $row->full_name ?></td>
                    <td><?php echo $row->email ?></td>
                    <td><?php echo date('Y-m-d H:i:s', $row->date) ?></td>
                </tr>
            <?php endforeach ?>
        <?php else : ?>
            <tr>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
                <td>No Data</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</form>