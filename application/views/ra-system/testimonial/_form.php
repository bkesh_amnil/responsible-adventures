<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="customer_full_name">Full Name *</label>
                <input type="text" name="customer_full_name" value="<?php echo $testimonial->customer_full_name ?>" class="form-control title"
                       id="customer_full_name" placeholder="Full Name">
            </div>
            <div class="form-group">
                <label for="slug">Alias *</label>
                <input type="text" name="slug" value="<?php echo $testimonial->slug ?>" class="form-control slug"
                       id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="customer_email_address">Email Address *</label>
                <input type="text" name="customer_email_address" value="<?php echo $testimonial->customer_email_address ?>"
                       class="form-control" id="customer_email_address" placeholder="Email Address">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $testimonial->status == '1' || $testimonial->status == '' ? 'selected' : '' ?>>
                        Publish
                    </option>
                    <option value="0" <?php echo $testimonial->status == '0' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="customer_country_id">Nationality *</label>
                <select name="customer_country_id" class="form-control">
                    <?php
                    foreach($countries as $country) {
                        $selected = '';
                        if($country->id == $testimonial->customer_country_id) {
                            $selected = ' selected="selected"';
                        }
                        ?>
                        <option value="<?php echo $country->id ?>"><?php echo $country->printable_name ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-group">
                <label for="customer_image">Image (120 X 120)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false"
                       name="customer_image" value="<?php echo $testimonial->customer_image ?>" class="form-control"
                       id="customer_image"
                       placeholder="Image">
                <?php if ($testimonial->customer_image != '') { ?>
                    <div class="customer_image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($testimonial->customer_image) ?>"/>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="customer_testimonial">Testimonial</label>
                <textarea rows="7" name="customer_testimonial" class="form-control" id="customer_testimonial"
                          placeholder="Testimonial"><?php echo $testimonial->customer_testimonial ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        load_ckeditor('customer_testimonial');
    };
</script>