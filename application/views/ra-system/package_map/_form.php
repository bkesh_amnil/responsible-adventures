<script src="https://maps.googleapis.com/maps/api/js" type="text/javascript"></script>
<div id="map" style="width: 100%; height: 500px;"></div>
<br>

    <form action="" method="post" autocomplete="off" id="form1" enctype="multipart/form-data" class="horizontal-form">
        <div class="row">
        <table class="table table-bordered multi-table" width="100%" id="HtmlTable">
            <thead>
            <tr>
                <th>S.No.</th>
                <th>Title</th>
                <th>Address</th>
                <th>Description</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Order</th>
                <th>Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if(isset($rows) && !empty($rows)) {
                $locations = array();
                foreach($rows as $row) {
                    $Tlat = (string)$row->latitude;
                    $Tlng = (string)$row->longitude;
                    $Tid = (string)$row->id;

                    $locations[] = array('id'=>$Tid,'label'=>$row->marker_label,'lat'=>$Tlat,'long'=>$Tlng);
                    ?>
                    <tr id="<?php echo $row->id ?>" class='trdesc'>
                        <td>
                            <input type='text' class="form-control" name='sort_order[]' value='<?php echo $row->marker_label ?>' disabled />
                        </td>
                        <td>
                            <input type='text' class="form-control" name='title[]' value='<?php echo $row->marker_title ?>' />
                        </td>
                        <td>
                            <input type='text' class="form-control" name='address[]' value='<?php echo $row->marker_address ?>' />
                        </td>
                        <td>
                            <input type='text' class="form-control" name='description[]' value='<?php echo $row->marker_description ?>' />
                        </td>
                        <td>
                            <input type='text' value='<?php echo $row->latitude ?>' disabled />
                            <input type='hidden' name='latitude[]' value='<?php echo $row->latitude ?>' class='google_latitude form-control' />
                        </td>
                        <td>
                            <input type='text' value='<?php echo $row->longitude ?>' disabled />
                            <input type='hidden' name='longitude[]' value='<?php echo $row->longitude ?>' class='google_longitude form-control' />
                        </td>
                        <td>
                            <input type='text' class="form-control" name='order[]' value='<?php echo $row->marker_sort_order ?>' class="sort_order_value" />
                        </td>
                        <td>
                            <span class='removeinfo btn btn-danger'>Delete</span>
                        </td>
                    </tr>
                    <?php
                }
                $locations = json_encode($locations);
            } else {
                $locations = '';
            }
            ?>
            </tbody>
        </table>
        <input type="hidden" id="hidden_lat_long" value='<?php echo $locations ?>' />
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/' . $package_id) ?>"><span>Cancel</span></a>
                </div>
            </div>
        </div>
        </div>
    </form>
