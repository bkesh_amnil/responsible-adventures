<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="type">Data Type</label>
                <select name="data_type" class="form-control">
                    <option value="inclusion" <?php echo (isset($package_inclusion_exclusion->data_type) && $package_inclusion_exclusion->data_type == 'inclusion') ? 'selected="selected"' : '' ?>>Inclusion</option>
                    <option value="exclusion" <?php echo (isset($package_inclusion_exclusion->data_type) && $package_inclusion_exclusion->data_type == 'exclusion') ? 'selected="selected"' : '' ?>>Exclusion</option>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Package Data *</label>
                <input type="text" name="data" value="<?php echo isset($package_inclusion_exclusion->data) ? $package_inclusion_exclusion->data : '' ?>" class="form-control" id="data" placeholder="Package Data">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo (isset($package_inclusion_exclusion->status) && $package_inclusion_exclusion->status == '1') ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo (isset($package_inclusion_exclusion->status) && $package_inclusion_exclusion->status == '0') ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <?php if(empty($package_inclusion_exclusion->id)) { ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group"><span class="add-info"> [ + Add ]</span></div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <table class="table table-bordered multi-table" id="dynamic_table" width="100%">
                    <thead>
                        <th>Data Type</th>
                        <th>Data</th>
                        <th>Action</th>
                        </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary" id="action">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/'. $package_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>