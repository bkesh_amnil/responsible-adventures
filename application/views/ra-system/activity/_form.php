<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="destination_id">Destination *</label>
                <select name="destination_id" id="destination_id" class="form-control">
                <?php
                if(isset($destinations) && !empty($destinations)) {
                    foreach($destinations as $destination) {
                        $selected = '';
                        if($destination->id == $activity->destination_id) {
                            $selected = ' selected="selected"';
                        }
                        ?>
                        <option value="<?php echo $destination->id ?>"<?php echo $selected ?>><?php echo $destination->name ?></option>
                        <?php
                    }
                }
                ?>
                </select>
            </div>
            <div class="form-group">
                <label for="title">Title *</label>
                <input type="text" name="name" value="<?php echo $activity->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="name">Alias *</label>
                <input type="text" name="slug" value="<?php echo $activity->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="show_in_front">Show in Front ?</label>
                <select name="show_in_front" id="show_in_front" class="form-control">
                    <option value="1" <?php echo $activity->show_in_front == '1' || $activity->show_in_front == '' ? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo $activity->show_in_front == '0' ? 'selected' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="link_type">Link Type</label>
                <select name="link_type" id="link_type" class="form-control">
                    <option value="none" <?php echo $activity->link_type == 'none' ? 'selected="selected"' : '' ?>>None</option>
                    <option value="experience" <?php echo $activity->link_type == 'experience' ? 'selected="selected"' : '' ?>>Experience</option>
                    <option value="url" <?php echo $activity->link_type == 'url' ? 'selected="selected"' : '' ?>>Url</option>
                </select>
            </div>
            <?php
            $experienceDisplay = $urlDisplay = $linkOpens = 'display:none;';
            if($activity->link_type == 'experience') {
                $experienceDisplay = $linkOpens = 'display:block;';
            } else if($activity->link_type == 'url') {
                $urlDisplay = $linkOpens = 'display:block;';
            }
            ?>
            <div class="form-group" id="experienceId" style="<?php echo $experienceDisplay ?>">
                <label class="form-label">Experiences</label>
                <select name="experience_link" class="form-control">
                    <option value="0">Select Experience</option>
                    <?php
                    if(isset($experiences) && !empty($experiences)) {
                        foreach ($experiences as $experience) {
                            $selected = '';
                            if(isset($activity->link) && !empty($activity->link) && $experience->id == $activity->link) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $experience->id ?>"<?php echo $selected ?>><?php echo $experience->name . '|' . $experience->destination_name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="urlId" style="<?php echo $urlDisplay ?>">
                <label class="form-label">Url</label>
                <input type="text" name="url_link" value="<?php echo (isset($activity->link) && !empty($activity->link)) ? $activity->link : '' ?>" class="form-control" id="link_url" placeholder="Url">
            </div>
            <div class="form-group" id="linkOpens" style="<?php echo $linkOpens ?>">
                <label class="form-label">Link Opens</label>
                <select name="opens" id="opens" class="form-control">
                    <option value="same" <?php echo $activity->opens == 'same' ? 'selected="selected"' : '' ?>>Same Window</option>
                    <option value="new" <?php echo $activity->opens == 'new' ? 'selected="selected"' : '' ?>>New Tab</option>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $activity->status == '1' || $activity->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $activity->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="image">Image(570 X 570) *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $activity->image ?>" class="form-control" id="image" placeholder="Image">
                <?php if($activity->image != '') { ?>
                    <img src="<?php echo base_url($activity->image) ?>" width="25%"/>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="description">Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Banner Description"><?php echo $activity->description ?></textarea>
                <script>
                    window.onload = function() {
                        load_ckeditor('description')
                    };
                </script>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>