<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
    <tfoot id="table-search-row">
    <tr>
        <th></th>
        <th></th>
        <th>Package Departure Date</th>
        <th>Package All Allocation</th>
        <th>Package Available</th>
        <th>Package Booked</th>
        <th>Is Fixed Departure</th>
        <th></th>
    </tr>
    </tfoot>
    <thead>
    <tr>
        <th>SN</th>
        <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
        <th>Package Departure Date</th>
        <th>Package All Allocation</th>
        <th>Package Available</th>
        <th>Package Booked</th>
        <th>Is Fixed Departure</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($rows) : $serial_number = 1; ?>
        <?php foreach ($rows as $row) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                <td><?php echo $row->departure_date ?></td>
                <td><?php echo $row->all_allocation ?></td>
                <td><?php echo $row->available ?></td>
                <td><?php echo $row->booked ?></td>
                <td><?php echo $row->is_fixed_departure == '1' ? 'Yes' : 'No' ?></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'package_departure',
                        'moduleData' => $row,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
</form>