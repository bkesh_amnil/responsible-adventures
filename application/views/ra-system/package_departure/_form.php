<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="package_departure_date">Departure Date *</label>
                <input type="text" name="package_departure_date" value="<?php echo isset($package_departure->departure_date) ? $package_departure->departure_date : '' ?>" class="form-control" id="departure_date" placeholder="Departure Date">
            </div>
            <div class="form-group">
                <label for="package_all_allocation">All Allocation *</label>
                <input type="text" name="package_all_allocation" value="<?php echo isset($package_departure->all_allocation) ? $package_departure->all_allocation : '' ?>" class="form-control" id="all_allocation" placeholder="Package All Allocation">
            </div>
            <div class="form-group">
                <label for="package_available">Available *</label>
                <input type="text" name="package_available" value="<?php echo isset($package_departure->available) ? $package_departure->available : '' ?>" class="form-control" id="available" placeholder="Available">
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="package_booked">Booked *</label>
                <input type="text" name="package_booked" value="<?php echo isset($package_departure->booked) ? $package_departure->booked : '' ?>" class="form-control" id="booked" placeholder="Booked">
            </div>
            <div class="form-group">
                <label for="is_fixed_departure">Is Fixed Departure *</label>
                <select name="is_fixed_departure" id="is_fixed_departure" class="form-control">
                    <option value="1" <?php echo isset($package_departure->is_fixed_departure) && ($package_departure->is_fixed_departure == '1' || $package_departure->is_fixed_departure == '') ? 'selected' : '' ?>>Yes</option>
                    <option value="0" <?php echo isset($package_departure->is_fixed_departure) && $package_departure->is_fixed_departure == '0'? 'selected' : '' ?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo isset($package_departure->status) && ($package_departure->status == '1' || $package_departure->status == '') ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo isset($package_departure->status) && $package_departure->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name'] . '/' . $package_id) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>