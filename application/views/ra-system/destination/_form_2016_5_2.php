<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="title">Title *</label>
                <input type="text" name="name" value="<?php echo $destination->name ?>" class="form-control title" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="name">Alias *</label>
                <input type="text" name="slug" value="<?php echo $destination->slug ?>" class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="long_description">Description *</label>
                <textarea rows="7" name="long_description" class="form-control" id="long_description" placeholder="Description"><?php echo $destination->long_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $destination->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $destination->meta_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $destination->status == '1' || $destination->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $destination->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
            <div class="form-group">
                <label for="image">Cover Image(285 X 285) *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $destination->image ?>" class="form-control" id="image" placeholder="Cover Image">
                <?php if($destination->image != '') { ?>
                    <img src="<?php echo base_url($destination->image) ?>" width="25%"/>
                <?php } ?>
            </div>
            <div class="form-group" id="selectImages">
                <label for="image">Images(1920 X 653) *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="true"
                       name="images" data-show-detail="true" class="form-control" id="images" placeholder="Images">
            </div>
            <?php
            if(isset($savedMedia) && !empty($savedMedia)) { ?>
                <?php foreach($savedMedia as $media) { ?>
                    <div class="mediaWrapper">
                        <div class="row">
                            <div class="col-md-4">
                                <img class="img-responsive" src="<?php echo base_url($media->media) ?>"/>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly="readonly" value="<?php echo $media->media ?>" name="media[]"/>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Title" value="<?php echo $media->title ?>" name="title[]"/>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Description" name="description[]"><?php echo $media->caption ?></textarea>
                                </div>
                            </div>
                        </div>
                        <a href="javascript:void(0);" class="deleteMedia"
                           data-url="<?php echo base_url(BACKENDFOLDER . '/gallery/deleteMedia/' . $media->id) ?>">
                            Delete
                        </a>
                    </div>
                <?php } ?>
            <?php }
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('long_description')
    };
</script>
