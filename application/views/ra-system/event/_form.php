<?php if (validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="name">Title *</label>
                <input type="text" name="name" value="<?php echo $event->name ?>" class="form-control title"
                       id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="slug">Alias *</label>
                <input type="text" name="slug" value="<?php echo $event->slug ?>"
                       class="form-control slug" id="slug" placeholder="Alias">
            </div>
            <div class="form-group">
                <label for="event_date">Event Start Date *</label>
                <input class="form-control" type="text" id="event_date" name="event_date"
                       value="<?php echo $event->event_date ?>" placeholder="Event Date"/>
            </div>
            <div class="form-group">
                <label for="publish_date">Event Publish Date *</label>
                <input class="form-control" type="text" id="valid_from" name="publish_date"
                       value="<?php echo $event->publish_date ?>" placeholder="Event Publish Date"/>
            </div>
            <div class="form-group">
                <label for="unpublish_date">Event UnPublish Date *</label>
                <input class="form-control" type="text" id="valid_to" name="unpublish_date"
                       value="<?php echo $event->unpublish_date ?>" placeholder="Event UnPublish Date"/>
            </div>
            <div class="form-group">
                <label for="status">Highlight Event:</label>
                <input type="radio" name="is_highlighted" value="0" <?php echo $event->is_highlighted == '0' || $event->is_highlighted == '' ? 'checked' : '' ?>>No
                <input type="radio" name="is_highlighted" value="1" <?php echo $event->is_highlighted == '1' ? 'checked' : '' ?>>Yes
            </div>
            <div class="form-group">
                <label for="google_form_link">Google Form Link</label>
                <input type="text" name="google_form_link" value="<?php echo $event->google_form_link ?>" class="form-control" id="google_form_link" placeholder="Google Form Link" <?php echo (/*!empty($event->google_form_link*/$id ? 'disabled="disabled"' : '') ?>>
            </div>
            <?php if(/*!empty($event->google_form_link)*/ $id != '') { ?>
                <input type="hidden" name="google_form_link_edit" value="<?php echo $event->google_form_link ?>" class="form-control">
                <input type="checkbox" name="edit_google_form" id="edit_google_form" value="1">Edit Google Form
            <?php } ?>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $event->status == '1' || $event->status == '' ? 'selected' : '' ?>>
                        Publish
                    </option>
                    <option value="0" <?php echo $event->status == '0' ? 'selected' : '' ?>>UnPublish</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="image">Image (573 X 416)</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false"
                       name="image" value="<?php echo $event->image ?>" class="form-control"
                       id="image"
                       placeholder="Event Image">
                <?php if ($event->image != '') { ?>
                    <div class="image-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($event->image) ?>"/>
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="short_description">Event Short Description *</label>
                <textarea rows="7" name="short_description" class="form-control" id="short_description"
                          placeholder="Event Short Description"><?php echo $event->short_description ?></textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">Meta Keywords</label>
                <input type="text" name="meta_keywords" value="<?php echo $event->meta_keywords ?>" class="form-control" id="meta_keywords" placeholder="Meta Keywords">
            </div>
            <div class="form-group">
                <label for="meta_description">Meta Description</label>
                <textarea rows="7" name="meta_description" class="form-control" id="meta_description" placeholder="Meta Description"><?php echo $event->meta_description ?></textarea>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="description">Event Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description"
                          placeholder="Event Description"><?php echo $event->description ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function () {
        load_ckeditor('description');
    };
</script>