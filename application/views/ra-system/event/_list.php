<form action="" method="post" id="gridForm" autocomplete="off">
    <table class="table table-bordered table-hover list-datatable">
    <tfoot id="table-search-row">
        <tr>
            <th></th>
            <th></th>
            <th>Event Title</th>
            <th></th>
            <th></th>
        </tr>
    </tfoot>
    <thead>
        <tr>
            <th>SN</th>
            <th><input type="checkbox" name="selectAll" value="selectAll" class="selectAll" /></th>
            <th>Event Title</th>
            <th>Event Image</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php if ($events) : $serial_number = 1; ?>
        <?php foreach ($events as $row) : ?>
            <tr>
                <td><?php echo $serial_number; $serial_number++; ?></td>
                <td><input type="checkbox" name="selected[]" value="<?php echo $row->id; ?>" class="rowCheckBox" /></td>
                <td><?php echo $row->name ?></td>
                <td><img src ="<?php echo base_url().$row->image ?>" style="width:150px;height:100px;"></td>
                <td>
                    <?php
                    $this->data['actionBtnData'] = [
                        'module' => 'event',
                        'moduleData' => $row,
                        'options' => 'EDS'
                    ];
                    $ci = &get_instance();
                    $ci->partialRender(BACKENDFOLDER.'/include/actionButton');
                    ?>
                </td>
            </tr>
        <?php endforeach ?>
    <?php else : ?>
        <tr>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
            <td>No Data</td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>
</form>