<?php if(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable fade in">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?php echo validation_errors() ?>
    </div>
<?php endif ?>
<form action="" method="post">
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="title">Title *</label>
                <input type="text" name="name" value="<?php echo $banner->name ?>" class="form-control" id="name" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="image">Image(1920 X 794) *</label>
                <input type="text" onclick="BrowseServer(this)" data-resource-type="image" data-multiple="false" name="image" value="<?php echo $banner->image ?>" class="form-control" id="image" placeholder="Banner Image">
                <?php if($banner->image != '') { ?>
                    <img src="<?php echo base_url($banner->image) ?>" width="25%"/>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select name="status" id="status" class="form-control">
                    <option value="1" <?php echo $banner->status == '1' || $banner->status == '' ? 'selected' : '' ?>>Active</option>
                    <option value="0" <?php echo $banner->status == '0' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="link_type">Link Type</label>
                <select name="link_type" id="link_type" class="form-control">
                    <option value="none" <?php echo $banner->link_type == 'none' ? 'selected="selected"' : '' ?>>None</option>
                    <option value="menu" <?php echo $banner->link_type == 'menu' ? 'selected="selected"' : '' ?>>Menu</option>
                    <option value="destination" <?php echo $banner->link_type == 'destination' ? 'selected="selected"' : ''?>>Destination</option>
                    <option value="area" <?php echo $banner->link_type == 'area' ? 'selected="selected"' : ''?>>Area</option>
                    <option value="experience" <?php echo $banner->link_type == 'experience' ? 'selected="selected"' : ''?>>Experience</option>
                    <option value="package" <?php echo $banner->link_type == 'package' ? 'selected="selected"' : ''?>>Package</option>
                    <option value="url" <?php echo $banner->link_type == 'url' ? 'selected="selected"' : '' ?>>Url</option>
                </select>
            </div>
            <?php
            $menuDisplay = $urlDisplaby = $destinationDisplay = $areaDisplay = $experienceDisplay = $packageDisplay = $linkOpens = 'display:none;';
            if($banner->link_type == 'menu') {
                $menuDisplay = $linkOpens = 'display:block;';
            } else if($banner->link_type == 'url') {
                $urlDisplaby = $linkOpens = 'display:block;';
            } else if($banner->link_type == 'destination') {
                $destinationDisplay = $linkOpens = 'display:block';
            } else if($banner->link_type == 'area') {
                $areaDisplay = $linkOpens = 'display:block';
            } else if($banner->link_type == 'experience') {
                $experienceDisplay = $linkOpens = 'display:block';
            } else if($banner->link_type == 'package') {
                $packageDisplay = $linkOpens = 'display:block';
            }
            ?>
            <div class="form-group" id="menuId" style="<?php echo $menuDisplay ?>">
                <label class="form-label">Menus</label>
                <select name="menu_link_id" class="form-control">
                    <option value="0">Select Menu</option>
                    <?php
                    if(isset($menus) && !empty($menus)) {
                        foreach ($menus as $menu) {
                            $selected = '';
                            if($menu->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                         ?>
                            <option value="<?php echo $menu->id ?>"<?php echo $selected ?>><?php echo $menu->name ?></option>
                         <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="destinationId" style="<?php echo $destinationDisplay ?>">
                <label class="form-label">Destinations</label>
                <select name="destination_link_id" class="form-control">
                    <option value="0">Select Destination</option>
                    <?php
                    if(isset($destinations) && !empty($destinations)) {
                        foreach ($destinations as $destination) {
                            $selected = '';
                            if($destination->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $destination->id ?>"<?php echo $selected ?>><?php echo $destination->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="experienceId" style="<?php echo $experienceDisplay ?>">
                <label class="form-label">Experiences</label>
                <select name="experience_link_id" class="form-control">
                    <option value="0">Select Experience</option>
                    <?php
                    if(isset($experiences) && !empty($experiences)) {
                        foreach ($experiences as $experience) {
                            $selected = '';
                            if($experience->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $experience->id ?>"<?php echo $selected ?>><?php echo $experience->name . ' | ' . $experience->destination_name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="areaId" style="<?php echo $areaDisplay ?>">
                <label class="form-label">Areas</label>
                <select name="area_link_id" class="form-control">
                    <option value="0">Select Area</option>
                    <?php
                    if(isset($areas) && !empty($areas)) {
                        foreach ($areas as $area) {
                            $selected = '';
                            if($area->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $area->id ?>"<?php echo $selected ?>><?php echo $area->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="packageId" style="<?php echo $packageDisplay ?>">
                <label class="form-label">Packages</label>
                <select name="package_link_id" class="form-control">
                    <option value="0">Select Package</option>
                    <?php
                    if(isset($packages) && !empty($packages)) {
                        foreach ($packages as $package) {
                            $selected = '';
                            if($package->id == $banner->link_id) {
                                $selected = ' selected="selected"';
                            }
                            ?>
                            <option value="<?php echo $package->id ?>"<?php echo $selected ?>><?php echo $package->name ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group" id="urlId" style="<?php echo $urlDisplaby ?>">
                <label class="form-label">Url</label>
                <input type="text" name="link_url" value="<?php echo $banner->link_url ?>" class="form-control" id="link_url" placeholder="Url">
            </div>
            <div class="form-group" id="linkOpens" style="<?php echo $linkOpens ?>">
                <label class="form-label">Link Opens</label>
                <select name="opens" id="opens" class="form-control">
                    <option value="same">Same Window</option>
                    <option value="new">New Tab</option>
                </select>
            </div>
            <div class="form-group">
                <label for="description">Description *</label>
                <textarea rows="7" name="description" class="form-control" id="description" placeholder="Banner Description"><?php echo $banner->description ?></textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Save</button>
                <a class="btn btn-warning" href="<?php echo base_url(BACKENDFOLDER . '/'. $this->header['page_name']) ?>"><span>Cancel</span></a>
            </div>
        </div>
    </div>
</form>
<script>
    window.onload = function() {
        load_ckeditor('description', true)
    };
</script>