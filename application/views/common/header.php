<!DOCTYPE html>
<html>
    <head>
        <title><?php echo (!empty($page_title) ? $page_title : $site_title) ?></title>
        <meta charset="UTF-8">
        <?php if($count > 0) { ?>
            <meta name="keywords" content="<?php echo isset($meta_keywords) && !empty($meta_keywords) ? $meta_keywords : '' ?>">
            <meta name="description" content="<?php echo isset($meta_description) && !empty($meta_description) ? $meta_description : '' ?>">
        <?php } else { ?>
            <meta name="keywords" content="<?php echo $site_keywords ?>">
            <meta name="description" content="<?php echo $site_description ?>">
        <?php } ?>
        <?php if($module_name == 'book' || $module_name == 'enquire') { ?>
            <meta name="robots" content="noindex,nofollow"/>
        <?php } ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0,  target-densitydpi=device-dpi">
        <meta name="author" content="<?php echo $site_author ?>">
        <link rel="icon" type="image/x-icon"  href="<?php echo base_url('img/RA-favicon.ico') ?>">
        <link rel="canonical" href="<?php echo current_url() ?>" />
        <link rel="alternate" hreflang="en-us" href="<?php echo current_url() ?>" />
        <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url('css/bootstrap.css') ?>" media="all" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('js/vendor/select2-4.0.1/select2.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url('js/vendor/owl-carasouel/owl.carousel.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('js/vendor/owl-carasouel/owl.theme.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/animate.css') ?>" media="all" rel="stylesheet" type="text/css">
        <?php if(($active_menu == 'destination' && $count == 3) || $active_menu == 'package') { ?>
        <link href="<?php echo base_url('css/fullcalendar.css') ?>" media="all" rel="stylesheet" type="text/css">
        <?php } ?>
        <?php if($active_menu == 'packages' || ($module_name == 'destination' && $count == '4') || $active_menu == 'news') { ?>
        <link href="<?php echo base_url('css/paginate.css') ?>" media="all" rel="stylesheet" type="text/css">
        <?php } ?>
        <?php //if($module_name == 'book' || $module_name == 'enquire') { ?>
        <link href="<?php echo base_url('assets/datepicker/datepicker3.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('css/book.css') ?>" rel="stylesheet" type="text/css">
        <?php //} ?>
        <?php if($active_menu == 'gallery' || ($active_menu == 'photo' && $count > 1)) { ?>
        <link href="<?php echo base_url('css/jquery.fancybox.css') ?>" media="all" rel="stylesheet" type="text/css">
        <?php } ?>
        <?php if($active_menu == 'contacts' || $active_menu == 'packages' || ($active_menu == 'package' && $count == 5) || $module_name == 'book' || $module_name == 'enquire') { ?>
        <link href="<?php echo base_url('css/error.css') ?>" media="all" rel="stylesheet" type="text/css">
        <?php } ?>
        <link href="<?php echo base_url('css/jquery.autocomplete.css') ?>" media="all" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('css/main.css') ?>" media="all" rel="stylesheet" type="text/css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--Adobe Edge Runtime-->
        <!-- facebook & twitter meta tags -->
        <?php
        if(isset($social_datas) && !empty($social_datas)) {
            foreach($social_datas as $ind => $social_data) {
                if($ind == 'Facebook') {
                ?>
                    <meta property="og:url" content="<?php echo (isset($social_data->link) && !empty($social_data->link)) ? $social_data->link : current_url(); ?>" />
                    <meta property="og:type" content="website" />
                    <meta property="og:title" content="<?php echo $social_data->title  ?>" />
                    <meta property="og:description" content="<?php echo strip_tags($social_data->description) ?>" />
                    <meta property="og:image" content="<?php echo base_url($social_data->image) ?>">
                    <meta property="fb:app_id" content="1689241494642495" />
                <?php } else { ?>
                    <meta name="twitter:card" content="summary">
                    <meta name="twitter:url" content="<?php echo (isset($social_data->link) && !empty($social_data->link)) ? $social_data->link : current_url(); ?>">
                    <meta name="twitter:title" content="<?php echo $social_data->title ?>">
                    <meta name="twitter:description" content="<?php echo strip_tags($social_data->description) ?>">
                    <meta name="twitter:image" content="<?php echo base_url($social_data->image) ?>">
                <?php
                }
            }
            ?>
            <!-- add this share -->
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56cff803a894bdcd"></script>
            <script type="text/javascript">
                if (typeof addthis_config !== "undefined") {
                    addthis_config.ui_click = true
                } else {
                    var addthis_config = {
                        ui_click: true
                    };
                }
            </script>
            <?php
        }
        ?>
        <script src="<?php echo base_url('js/jquery-1.11.3.min.js') ?>" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<script>
    var recaptcha1;
    var recaptcha2;
    var recaptcha3;
    var CaptchaCallback = function() {
        //Render the recaptcha1 on the element with ID "recaptcha1"
        recaptcha1 = grecaptcha.render('recaptcha1', {
            'sitekey': '6Lcq6QsUAAAAACVSs6LNApv8jSuSmy2OjV4RCPUr', //Replace this with your Site key
            'theme': 'light'
        });

        //Render the recaptcha2 on the element with ID "recaptcha2"
        recaptcha2 = grecaptcha.render('recaptcha2', {
            'sitekey': '6Lcq6QsUAAAAACVSs6LNApv8jSuSmy2OjV4RCPUr', //Replace this with your Site key
            'theme': 'light'
        });
        //Render the recaptcha on the element with ID "recaptcha-contact"
        recaptcha3 = grecaptcha.render('recaptcha3', {
            'sitekey': '6Lcq6QsUAAAAACVSs6LNApv8jSuSmy2OjV4RCPUr', //Replace this with your Site key
            'theme': 'light'
        });
    };
    </script>
    </head>