<body>
<header>
    <nav class="navbar header-nav-wrap">
        <div class="container">
            <div class="search-wrap">
                <form>
                    <button class="glyphicon glyphicon-search search-btn"></button>
                    <input class="form-control" id="search" type="text" placeholder="SEARCH">
                </form>
            </div>

            <div class="navbar-header">
                <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?php echo site_url() ?>" class="navbar-brand header-logo"><img class="img-responsive" src="<?php echo base_url($site_logo) ?>" alt="<?php echo $site_name ?>" /></a>
            </div>

            <div class="navbar-collapse collapse navbar-right header-nav" id="navbar">
                <?php if(isset($main_menus) && !empty($main_menus)) { ?>
                <ul class="nav navbar-nav">
                    <?php
                    foreach($main_menus as $main_menu) {
                        if(isset($main_menu['childs']) && !empty($main_menu['childs'])) { ?>
                            <li class="dropdown">
                                <a class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-expanded="false" href="javascript:void(0);"><?php echo strtoupper($main_menu['name']) ?></a>
                                <ul class="dropdown-menu">
                                    <?php foreach($main_menu['childs'] as $ind => $child) { ?>
                                        <li><a href="<?php echo site_url($child['slug']) ?>"><?php echo strtoupper($child['name']) ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php } else { ?>
                            <li><a href="<?php echo site_url($main_menu['slug']) ?>"><?php echo strtoupper($main_menu['name']) ?></a></li>
                        <?php }
                    }
                    ?>
                </ul>
                <?php } ?>
            </div>
        </div>
    </nav>
</header>