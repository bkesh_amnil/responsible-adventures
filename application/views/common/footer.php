        <?php if($active_menu != 'contacts' && $module_name != 'content' && $module_name != 'book' && $module_name != 'team') { ?>
        <section class="featured-experiences">
            <?php if(isset($featured_packages) && !empty($featured_packages)) { ?>
                <?php $this->load->view('package/featured', $featured_packages); ?>
            <?php } ?>
        </section>
        <?php } ?>
        <section class="testimonial-wrap">
            <?php if(isset($testimonials) && !empty($testimonials) && $module_name != 'testimonial') { ?>
                <?php $this->load->view('testimonial', $testimonials); ?>
            <?php } ?>
        </section>

        <?php if(isset($footer_static_content) && !empty($footer_static_content)) { ?>
        <section class="why-us-wrap">
            <div class="container">
                <h3><?php echo $footer_static_content[0]->name ?></h3>
                <?php echo $footer_static_content[0]->long_description ?>
            </div>
        </section>
        <?php } ?>

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-lg-4">
                        <div class="footer-content-wrap">
                            <?php if(isset($footer_content) && !empty($footer_content)) { ?>
                                <h5><?php echo $footer_content[0]->name ?></h5>
                                <?php echo $footer_content[0]->long_description ?>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-lg-4">
                        <div class="footer-content-wrap">
                            <h5>Important Links</h5>
                            <nav class="footer-nav">
                                <?php if(isset($bottom_menus) && !empty($bottom_menus)) { ?>
                                <ul>
                                    <?php foreach($bottom_menus as $bottom_menu) { ?>
                                    <li><a href="<?php echo site_url($bottom_menu['slug']) ?>"><?php echo $bottom_menu['name'] ?></a></li>
                                    <?php } ?>
                                </ul>
                                <?php } ?>
                            </nav>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-lg-4">
                        <div class="footer-content-wrap">
                            <?php if(isset($footer_contact) && !empty($footer_contact)) { ?>
                            <h5><?php echo $footer_contact[0]->name ?></h5>
                            <?php echo $footer_contact[0]->long_description ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-7 col-sm-6">
                        <?php if(isset($footer_logos) && !empty($footer_logos)) { ?>
                            <div class="footer-top">
                                <?php
                                foreach($footer_logos as $footer_logo) {
                                    if(!empty($footer_logo->url)) {
                                        ?>
                                        <a href="<?php echo $footer_logo->url ?>" target="_blank">
                                    <?php } ?>
                                    <img src="<?php echo base_url($footer_logo->image) ?>" alt="<?php echo $footer_logo->name ?>" />
                                    <?php if(!empty($footer_logo->url)) { ?>
                                        </a>
                                    <?php } ?>
                                <?php } ?>
                                <span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=7XI5pzP3jJ1n95fUhrJamm2PQOsb0r5TY4A95shu7xiyHwbEM9KnbvoLnZk2"></script></span>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-lg-4 col-md-5 col-sm-6">
                        <div class="social-wrap">
                            <h5>Follow Us on</h5>
                            <?php if(isset($facebook) && !empty($facebook)) { ?>
                                <a class="facebook" href="<?php echo $facebook ?>" target="_blank">
                                    <img src="<?php echo base_url('img/icons/icon-facebook.png') ?>" alt="Facebook"/>
                                </a>
                            <?php } ?>
                            <?php if(isset($gplus) && !empty($gplus)) { ?>
                                <a class="gmail" href="<?php echo $gplus ?>" target="_blank">
                                    <img src="<?php echo base_url('img/icons/icon-googleplus.png') ?>" alt="Gmail"/>
                                </a>
                            <?php } ?>
                            <?php if(isset($youtube) && !empty($youtube)) { ?>
                                <a class="youtube" href="<?php echo $youtube ?>" target="_blank">
                                    <img src="<?php echo base_url('img/icons/icon-youtube.png') ?>" alt="Youtube"/>
                                </a>
                            <?php } ?>
                            <?php if(isset($twitter) && !empty($twitter)) { ?>
                                <a class="twitter" href="<?php echo $twitter ?>" target="_blank">
                                    <img src="<?php echo base_url('img/icons/icon-twitter.png') ?>" alt="Twitter"/>
                                </a>
                            <?php } ?>
                            <?php if(isset($linkedin) && !empty($linkedin)) { ?>
                                <a class="linkedin" href="<?php echo $linkedin ?>" target="_blank">
                                    <img src="<?php echo base_url('img/icons/icon-linkedin.png') ?>" alt="Linkedin"/>
                                </a>
                            <?php } ?>
                            <?php if(isset($pinterest) && !empty($pinterest)) { ?>
                                <a class="pintrest" href="<?php echo $pinterest ?>" target="_blank">
                                    <img src="<?php echo base_url('img/icons/icon-pintrest.png') ?>" alt="Pintrest"/>
                                </a>
                            <?php } ?>
                            <?php if(isset($instagram) && !empty($instagram)) { ?>
                                <a class="instagram" href="<?php echo $instagram ?>" target="_blank">
                                    <img src="<?php echo base_url('img/icons/icon-instagram.png') ?>" alt="Instagram"/>
                                </a>
                            <?php } ?>
                            <?php if(isset($vimeo) && !empty($vimeo)) { ?>
                                <a class="vimeo" href="<?php echo $vimeo ?>" target="_blank">
                                    <img src="<?php echo base_url('img/icons/icon-vimeo.png') ?>" alt="Vimeo"/>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <strong>Copyright &copy; <?php echo date('Y') ?> <a href="<?php echo base_url() ?>"><?php echo $site_title ?></a>.</strong> All rights reserved. Powered by <a class="amnil" href="http://www.candidservice.com/" target="_blank">Candid Service</a>
            </div>
        </footer>

        <a href="#0" class="cd-top">Top</a>
        <!-- popup for download pdf -->
        <?php $this->load->view('form/download_pdf.php') ?>
        <!-- popup for pdf download -->
        <!-- for form message popup -->
        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel"></h4>
                    </div>
                    <div class="modal-body">
                        <img class="messageModal-image tick-icon" src="<?php echo base_url('img/icons/tick.svg') ?>" alt="tick-icon" />
                        <img class="messageModal-image cross-icon" src="<?php echo base_url('img/icons/cross.svg') ?>" alt="cross-icon" />
                        <div class="success-msg"></div>
                        <button class="btn-ok" type="submit" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if(isset($analytics) && !empty($analytics)) { ?>
            <script><?php echo $analytics; ?></script>
        <?php } ?>
        <!-- for form message popup -->
        <input type="hidden" name="baseUrl" id="baseUrl" value="<?php echo base_url() ?>">
        <input type="hidden" id="hidden_current_url" value="<?php echo current_url() ?>">
        <input type="hidden" id="hidden_count" value="<?php echo isset($count) ? $count : '' ?>">
        <input type="hidden" id="hidden_total_pages" value="<?php echo (isset($total_pages) && !empty($total_pages)) ? $total_pages : 0 ?>">
        <input type="hidden" id="hidden_type" value="<?php echo (isset($type) && !empty($type)) ? $type : '' ?>">
        <input type="hidden" id="hidden_item_per_page" value="<?php echo (isset($item_per_page) && !empty($item_per_page)) ? $item_per_page : 0 ?>">


        <script src="<?php echo base_url('js/vendor/smart-menu/jquery.smartmenus.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/vendor/smart-menu/jquery.smartmenus.bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/vendor/select2-4.0.1/select2.full.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/jquery.autocomplete.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/vendor/owl-carasouel/owl.carousel.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/owl-custom.js') ?>" type="text/javascript"></script>
        
        
        <!--form pop up build-->
        <!--<script src="<?php echo base_url('js/package-detail.js') ?>" type="text/javascript"></script>-->
        <script src="<?php echo base_url('js/datepicker.js') ?>" type="text/javascript"></script>
        <!--<script src="<?php echo base_url('js/package_calendar.js') ?>" type="text/javascript"></script>-->
        <script src="<?php echo base_url('js/jquery.validate.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/form.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/book.js') ?>" type="text/javascript"></script>
        
        <script src="<?php echo base_url('js/vendor/fullcalender/moment.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/vendor/fullcalender/fullcalendar.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/package-booking-enquiry.js') ?>" type="text/javascript"></script>
        <!--form pop up build-->
        
        <?php if($active_menu == 'home') { ?>
        <script src="<?php echo base_url('js/home.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'home' || $active_menu == 'destination' || $active_menu == 'package' || $module_name == 'team') { ?>
        <script src="<?php echo base_url('js/filter_experience.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'package' || $module_name == 'team') { ?>
        <script src="<?php echo base_url('js/bootstrap-tabcollapse.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'package' || $active_menu == 'packages') { ?>
<!--        <script src="<?php echo base_url('js/vendor/fullcalender/moment.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/vendor/fullcalender/fullcalendar.min.js') ?>" type="text/javascript"></script>-->
        <script src="<?php echo base_url('js/package.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'package' || $active_menu == 'packages' || ($module_name == 'destination' && $count == 4)) { ?>
        <script src="<?php echo base_url('js/package-filter.js') ?>" type="text/javascript"></script>
        <!--<script src="<?php echo base_url('js/package-detail.js') ?>" type="text/javascript"></script>-->
        <?php } ?>
        <?php if($active_menu == 'packages' || ($module_name == 'destination' && $count == 4) || $active_menu == 'news') { ?>
        <script src="<?php echo base_url('js/jquery.paginate.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/paginate.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'package' || $active_menu == 'contacts') { ?>
        <!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
        <?php } ?>
        <?php if($active_menu == 'package') { ?>
        <script src="<?php echo base_url('js/maps.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($module_name == 'book' || $module_name == 'enquire') { ?>
        <!--<script src="<?php echo base_url('js/datepicker.js') ?>" type="text/javascript"></script>-->
        <!--<script src="<?php echo base_url('js/package_calendar.js') ?>" type="text/javascript"></script>-->
        <!--<script src="<?php echo base_url('js/jquery.validate.min.js') ?>" type="text/javascript"></script>-->
        <!--<script src="<?php echo base_url('js/form.js') ?>" type="text/javascript"></script>-->
        <?php } ?>
        <?php if($module_name == 'book') { ?>
        <!--<script src="<?php echo base_url('js/book.js') ?>" type="text/javascript"></script>-->
        <?php } ?>
        <?php if($active_menu == 'package' || $module_name == 'book' || $module_name == 'enquire') { ?>
        <!--<script src="<?php echo base_url('js/jquery.validate.min.js') ?>" type="text/javascript"></script>-->
        <script src="<?php echo base_url('js/download.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($module_name == 'team') { ?>
        <script src="<?php echo base_url('js/custom-floatin-tab.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'photo') { ?>
        <script src="<?php echo base_url('js/jquery.fancybox.pack.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/gallery.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'gallery') { ?>
        <script src="<?php echo base_url('js/jquery.fancybox.pack.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/jquery.fancybox-media.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/gallery.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <?php if($active_menu == 'contacts') { ?>
        <!--<script src="<?php echo base_url('js/jquery.validate.min.js') ?>" type="text/javascript"></script>-->
        <script src="<?php echo base_url('js/contact-us.js') ?>" type="text/javascript"></script>
        <!--<script src="<?php echo base_url('js/form.js') ?>" type="text/javascript"></script>-->
        <?php } ?>
        <?php if($active_menu == 'testimonial' && $count > 1) { ?>
        <script src="<?php echo base_url('js/testimonial.js') ?>" type="text/javascript"></script>
        <?php } ?>
        <script src="<?php echo base_url('js/load-more.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('js/main.js') ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).on("change", "select#all_packages_filter", function() {
                var url = $(this).find("option:selected").attr("rel");
                window.location.href = url;
            })
        </script>
        <script src="<?php echo base_url('js/vendor/jQuery-slimScroll-1.3.7/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
        <script type="text/javascript">
            $(".desc-scroll").slimScroll({
                height: '156px',
                color: '#B263AB'
            });
             $(".moun-scroll").slimScroll({
                height: '160px',
                color: '#aaa'
            });
        </script>
        <?php if($active_menu == 'package') { ?>
            <script type="text/javascript">
            $(document).on("click", "a.view-detail-map", function() {

                $("a.view-detail-map").text('View Lat-Long Map');
                $("a.view-detail-map").addClass("view-latlong-map");
                $("a.view-detail-map").removeClass("view-detail-map");

                $("div#map").css("display", "block");
                $("div#map").css("height", "800px");
                $("div#map-packages").css("display", "none");
                $("div#map-packages").css("height", "0px");
                initMap();
            })

            $(document).on("click", "a.view-latlong-map", function() {

                $("a.view-latlong-map").text('View Detail Map');
                $("a.view-latlong-map").addClass("view-detail-map");
                $("a.view-latlong-map").removeClass("view-latlong-map");

                $("div#map").css("display", "none");
                $("div#map").css("height", "0px");
                $("div#map-packages").css("display", "block");
                $("div#map-packages").css("height", "800px");
            })
        </script>
        <?php } ?>
        <?php $this->load->view('form/book_package_modal.php'); ?>
        <?php $this->load->view('form/enquire_package_modal.php'); ?>
    </body>
</html>