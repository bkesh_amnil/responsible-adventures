<section id="main-banner" class="owl-carousel">
    <?php foreach($banners as $banner) { ?>
        <div class="banner-content">
            <img src="<?php echo base_url($banner['info']->image) ?>" alt="<?php echo $banner['info']->name ?>" />
            <div class="banner-desc">
                <h1><?php echo $banner['info']->name ?></h1>
                <div class="hidden-xs"><p><?php echo $banner['info']->description ?></p></div>
                <?php if(isset($banner['url']) && !empty($banner['url'])) { ?>
                <a class="view-more" href="<?php echo $banner['url'] ?>">View more</a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</section>