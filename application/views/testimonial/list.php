<section class="testimonials-listing-wrap">
    <div class="container">
        <div class="row">
            <?php
            if(isset($content) && !empty($content)) {
                foreach($content as $val) { ?>
                    <div class="col-xs-12 col-sm-4 col-lg-4">
                        <div class="testimonials clearfix">
                            <div class="comments-wrap">
                                <span class="comment-user-img">
                                    <img class="img-responsive img-circle" src="<?php echo base_url($val->customer_image) ?>" alt="<?php echo $val->customer_full_name ?>"/>
                                </span>
                                <span class="comment-user-name"><?php echo $val->customer_full_name ?></span>
                                <p><?php echo (strlen($val->customer_testimonial) > 223) ? substr(strip_tags($val->customer_testimonial), 0, 223) . ' ...' : strip_tags($val->customer_testimonial) ?></p>
                                <a href="<?php echo site_url('testimonial/' . $val->slug) ?>"><span>Read More</span></a>
                            </div>
                        </div>
                    </div>
                <?php }
                ?>
            <?php } else { ?>
            <p>No Testimonial Added as of Yet.</p>
            <?php } ?>
        </div>
    </div>
</section>