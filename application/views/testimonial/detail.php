<section class="testimonial-detail-wrap">
    <div class="container">
        <?php foreach($content as $val) { ?>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-lg-2">
                <div class="user-info-wrap" id="user-info-wrap-div">
                    <span class="comment-user-img">
                        <img class="img-responsive img-circle" src="<?php echo base_url($val->customer_image) ?>" alt="<?php echo $val->customer_full_name ?>"/>
                        <span class="comment-country-name"><?php echo $val->printable_name ?></span>
                    </span>
                    <span class="comment-user-name"><?php echo $val->customer_full_name ?></span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-9 col-lg-10">
                <div class="comments">
                    <?php echo $val->customer_testimonial ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</section>