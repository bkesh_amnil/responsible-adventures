<section class="team-wrapper">
    <div class="container">
        <div class="custom-tab">
            <div class="row">
                <div class="col-sm-4 col-lg-4">
                    <div id="<?php echo ($module_name == 'content') ? 'about' : 'team' ?>" class="custom-aside-tab-head">
                        <h3><?php echo str_replace('-', ' ', $module_name); ?></h3>
                        <ul id="myTab" class="nav nav-tabs">
                            <?php
                            foreach($content as $ind => $val) {
                                $class = '';
                                if($ind == 0) {
                                    $class = 'active';
                                }
                                ?>
                                <li class="team-<?php echo $ind . ' ' . $class ?>" id="<?php echo $val->slug ?>">
                                    <a href="#tab-<?php echo $ind ?>" data-toggle="tab" rel="<?php echo $val->slug ?>" data-img="<?php echo (isset($val->image) && !empty($val->image)) ? base_url($val->image) : base_url('img/logo.png') ?>" data-background-img="<?php echo (isset($val->background_image) && !empty($val->background_image)) ? base_url($val->background_image) : 'img/inner-page-banner-pattern-bg.jpg' ?>">
                                        <?php echo ($module_name == 'content' || $module_name == 'team') ? $val->name : $val->question; ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-8 col-lg-8">
                    <div id="myTabContent" class="tab-content" >
                        <?php
                        foreach($content as $ind => $val) {
                            $social_data_individual = $this->public_model->getSocialData($modules[0]['module_id'], $val->id);
                            $class = '';
                            if($ind == 0){
                                $class = ' active in';
                            }
                            if($module_name == 'content') {
                                $desc = $val->long_description;
                            } else if($module_name == 'faq') {
                                $desc = $val->answer;
                            } else {
                                $desc = $val->team_description;
                            }
                            ?>

                            <div class="tab-pane fade<?php echo $class ?>" id="tab-<?php echo $ind ?>">
                                <input type="hidden" id="tabid" value="<?php echo $ind ?>">
                                <h3><?php echo ($module_name == 'content' || $module_name == 'team') ? $val->name : $val->question ?></h3>
                                <div class="owl-carousel team-image-slider" id="team-image-slider-<?php echo $ind ?>">
                                    <?php
                                    if(isset($val->image) && !empty($val->image)) {
                                        $medias = $this->db->select('media, title, caption')->where('team_id', $val->id)->get('tbl_team_media')->result();
                                        if(isset($medias) && !empty($medias)) {
                                            foreach($medias as $media) {
                                                ?>
                                                <img src="<?php echo base_url($media->media) ?>" alt="<?php echo $media->title ?>" />
                                                <?php
                                            }
                                        }
                                    } ?>
                                </div>
                                <?php if($module_name == 'team') { ?>
                                    <div class="from-ck">
                                        <span id="team-post"><?php echo $val->post; ?></span>
                                        <?php echo $desc ?>
                                    </div>
                                <?php } else { ?>
                                    <?php echo $desc ?>
                                <?php } ?>
                                <?php if(isset($social_data_individual) && !empty($social_data_individual)) { ?>
                                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                    <a class="addthis_button btn-share" addthis:url="<?php echo !empty($social_data_individual['Facebook']->link) ? $social_data_individual['Facebook']->link : current_url() . '/' . $val->slug ?>" addthis:title="<?php echo $social_data_individual['Facebook']->title ?>" addthis:description="<?php echo $social_data_individual['Facebook']->description ?>" adthis:image="<?php echo base_url($social_data_individual['Facebook']->image) ?>" href="javascript:void(0)"><img src="<?php echo base_url('img/icons/icon-addthis-share.png') ?>" alt=""/></a>
                                    
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>