<div class="trip-detail">
    <h4>Trip Details</h4>
    <div class="details clearfix">
        <span class="details-title">Code</span><span><?php echo $package_description['info']->code ?></span>
        <span class="details-title">Frequency</span><span><?php echo $package_description['info']->departure_name ?></span>
        <span class="details-title">Duration</span><span><?php echo $package_description['info']->duration ?></span>
        <span class="details-title">Group Size</span><span><?php echo $package_description['info']->minimum_group_size ?></span>
        <span class="details-title">Best Season</span><span><?php echo $package_description['info']->best_season ?></span>
        <span class="details-title">Altitude</span><span><?php echo $package_description['info']->altitude . ' mts.' ?></span>
        <span class="details-title">Support</span><span><?php echo ($package_description['info']->support == '1') ? 'Yes' : 'No' ?></span>
        <?php if(!empty($package_description['info']->unit_price)) { ?><span class="details-title">Trip Cost</span><span>Rs <?php echo $package_description['info']->unit_price ?></span><?php } ?>
        <span class="details-title">Difficulty</span><span><?php echo $package_description['info']->difficulty_name ?></span>
    </div>
</div>

<div class="download-share-wrap">
    <?php /*if(isset($social_datas) && !empty($social_datas)) { ?>
    <div class="share">
        <img src="<?php echo base_url('img/icons/icon-share.png') ?>" alt="Share"/>
        <a class="addthis_button" addthis:url="<?php echo !empty($social_datas['Facebook']->link) ? $social_datas['Facebook']->link : current_url() ?>" addthis:title="<?php echo (isset($social_datas['Facebook']->title) && !empty($social_datas['Facebook']->title)) ? $social_datas['Facebook']->title : $package_description['info']->name ?>" addthis:description="<?php echo (isset($social_datas['Facebook']->description) && !empty($social_datas['Facebook']->description)) ? strip_tags($social_datas['Facebook']->description) : strip_tags($package_description['info']->description) ?>" addthis:image="<?php echo base_url($social_datas['Facebook']->image) ?>" href="javascript:void(0)">Share</a>
    </div>
    <?php }*/ ?>
    <div class="download">
        <img src="<?php echo base_url('img/icons/icon-pdf.png') ?>" alt="Download"/>
        <a class="btn-download-pdf" href="javascript:void(0);" file="<?php echo $package_description['info']->file ?>">
            Download Pdf
        </a>
    </div>
    <!--<form id="download_pdf_file" method="post" action="<?php echo base_url('download/index') ?>">
        <input type="hidden" id="file" name="file" value="">
    </form>-->
</div>