<?php if(isset($package_description) && !empty($package_description)) { ?>
<input type="hidden" id="hidden_package_id" value="<?php echo $package_description['info']->id ?>">
<section class="package-details-wrap">
    <div class="container">
        <h2><?php echo $package_description['info']->name ?></h2>
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-9">
                <div class="package-status-wrap">
                    <?php if(isset($package_description['gallery']) && !empty($package_description['gallery'])) { ?>
                    <div id="package-detail-slide" class="owl-carousel">
                        <?php foreach($package_description['gallery'] as $img) { ?>
                            <div><img class="img-responsive" src="<?php echo base_url($img->image) ?>" alt="<?php echo $img->title ?>"></div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <div class="project-desc">
                        <?php echo $package_description['info']->description ?>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="download-share-wrap">
                    <?php if(isset($social_datas) && !empty($social_datas)) { ?>
                        <div class="share">
                            <img src="<?php echo base_url('img/icons/icon-share.png') ?>" alt="Share"/>
                            <a class="addthis_button" addthis:url="<?php echo !empty($social_datas['Facebook']->link) ? $social_datas['Facebook']->link : current_url() ?>" addthis:title="<?php echo (isset($social_datas['Facebook']->title) && !empty($social_datas['Facebook']->title)) ? $social_datas['Facebook']->title : $package_description['info']->name ?>" addthis:description="<?php echo (isset($social_datas['Facebook']->description) && !empty($social_datas['Facebook']->description)) ? strip_tags($social_datas['Facebook']->description) : strip_tags($package_description['info']->description) ?>" addthis:image="<?php echo base_url($social_datas['Facebook']->image) ?>" href="javascript:void(0)">Share</a>
                        </div>
                    <?php } ?>
                </div>
                
                <div class="package-availability-calendar clearfix">
                    <h4>AVAILABILITY</h4>
                    <div id="package-calendar"></div>
                    <!--<a class="btn-enquire btn-all" href="<?php echo current_url() . '/enquire' ?>">Enquire now</a>-->
                    <!--<a class="btn-all btn-book-now" href="<?php echo current_url() . '/book' ?>">Book now</a>-->
                    <!--<a class="btn-enquire btn-all" rel href="<?php echo site_url('enquire/'. $package_slug) ?>">Enquire now</a>-->
                    <a class="btn-enquire btn-all" rel="<?php echo $package_slug?>" href="javascript:void(0);">Enquire now</a>
                    <!--<a class="btn-all btn-book-now" href="<?php echo site_url('booking/'. $package_slug) ?>">Book now</a>-->
                    <a class="btn-all btn-book-package-now" data-date="" rel="<?php echo $package_slug?>" href="javascript:void(0);">Book now</a>
                </div>

                <?php $this->load->view('package/aside_detail.php') ?>
            </div>
        </div>
    </div>
</section>

<section class="package-misc-detail-wrap">
    <div class="container">
        <ul id="package-detail-tab" class="nav nav-tabs">
            <li class="active"><a class="itinerary" href="#itineray" data-toggle="tab">ITINERARY</a></li>
            <li><a class="costing" href="#costing" data-toggle="tab">COSTING</a></li>
            <li><a class="datesavailable" href="#datesavailable" data-toggle="tab">AVAILABLE DATES</a></li>
            <?php
            $onclick = '';
            if(!empty($package_description['info']->map_file)) {
                $map_type = 'file';
                $onclick = ' onclick="initMap()"';
            }
            if(!empty($package_description['package_map'])) {
                $map_type = 'lat_long';
                $onclick = '';
            }
            if(!empty($package_description['info']->map_file) && !empty($package_description['package_map'])) {
                $map_type = 'both';
                $onclick = '';
            }
            ?>
            <!--<li><a class="tab-map" href="#map1" data-toggle="tab" onclick="initMap()">Location MAP</a></li>-->
            <li><a class="tab-map" href="#map1" data-toggle="tab"<?php echo $onclick ?>>LOCATION MAP</a></li>
        </ul>

        <div id="package-detail-tab-content" class="tab-content" >
            <div class="tab-pane fade active in" id="itineray">
                <?php echo $package_description['info']->itinerary ?>
                 <?php if(isset($package_description['inexclusions']) && !empty($package_description['inexclusions'])) { ?>
                <div class="row">
                    <?php if(isset($package_description['inexclusions']['inclusion']) && !empty($package_description['inexclusions']['inclusion'])) { ?>
                        <div class="col-lg-6">
                            <div class="table-responsive">
                                <table class="table table-bordered custom-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                Cost Includes
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <?php foreach($package_description['inexclusions']['inclusion'] as $ind => $val) { ?>
                                            <td><?php echo $val ?></td>
                                            <?php 
                                            if($ind % 2 != 0)  {
                                                echo '</tr><tr>';
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if(isset($package_description['inexclusions']['exclusion']) && !empty($package_description['inexclusions']['exclusion'])) { ?>
                        <div class="col-lg-6">
                            <div class="table-responsive">
                                <table class="table table-bordered custom-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                Cost Excludes
                                            </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <?php foreach($package_description['inexclusions']['exclusion'] as $ind => $val) { ?>
                                            <td><?php echo $val ?></td>
                                            <?php 
                                            if($ind % 2 != 0)  {
                                                echo '</tr><tr>';
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>

            <div class="tab-pane fade" id="costing">
                
                <?php if(isset($package_description['package_price']) && !empty($package_description['package_price'])) { ?>
                        <table class="table table-bordered custom-table">
                            <thead>
                            <tr>
                                <th>From</th>
                                <th>To</th>
                                <th>Number of Pax</th>
                                <th>Unit Price ($)</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $current_date = date('Y-m-d');
                            foreach($package_description['package_price'] as $price) {
                                $query = "SELECT departure_date, all_allocation, available, booked
                                            FROM `tbl_package_departure_detail`
                                            JOIN `tbl_package_price` ON tbl_package_departure_detail.`departure_date` BETWEEN tbl_package_price.`valid_from` AND tbl_package_price.`valid_to`
                                            WHERE valid_from = '".$price->valid_from."'
                                            AND valid_to = '".$price->valid_to."'
                                            AND departure_date >= '".$current_date."'
                                            AND tbl_package_departure_detail.`package_id` = " . $package_description['info']->id;
                                $result = $this->db->query($query)->result();
                                ?>
                                <tr>
                                    <td><?php echo date('d F Y', strtotime($price->valid_from)) ?></td>
                                    <td><?php echo date('d F Y', strtotime($price->valid_to)) ?></td>
                                    <td><?php echo $price->number_of_pax ?></td>
                                    <td><?php echo $price->unit_price ?></td>
                                    <?php
                                    if(strtotime($price->valid_to) > strtotime($current_date) && !empty($result)) { ?>
                                    <td><a href="javascript:void(0);" class="book-now" rel="<?php echo $result[0]->departure_date ?>" data-allocation="<?php echo $result[0]->all_allocation ?>" data-booked="<?php echo $result[0]->booked ?>" data-available="<?php echo $result[0]->available ?>">BOOK NOW</a></td>
                                    <?php } ?>
                                    <!--<td><a href="">BOOK NOW</a></td>-->
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                <?php } else { ?>
                    <tr><td>No Pricing Available as of Yet.</td></tr>
                <?php } ?>
                <?php /*if(isset($package_description['inexclusions']['inclusion']) && !empty($package_description['inexclusions']['inclusion'])) { ?><h4>COST INCLUDE(S)</h4>
                <ul>
                    <?php foreach($package_description['inexclusions']['inclusion'] as $inclusion) { ?>
                    <li><?php echo $inclusion ?></li>
                    <?php } ?>
                </ul>
                <?php } else { ?>
                    <p>No Data Added as of Yet.</p>
                <?php } ?>

                <h4>COST EXCLUDE(S)</h4>
                <?php if(isset($package_description['inexclusions']['exclusion']) && !empty($package_description['inexclusions']['exclusion'])) { ?>
                <ul>
                    <?php foreach($package_description['inexclusions']['exclusion'] as $exclusion) { ?>
                    <li><?php echo $exclusion ?></li>
                    <?php } ?>
                </ul>
                <?php } else { ?>
                    <p>No Data Added as of Yet.</p>
                <?php }*/ ?>
            </div>

            <div class="tab-pane fade" id="datesavailable">
                <div class="table-responsive">
                    <?php if(isset($package_description['departure_dates']) && !empty($package_description['departure_dates'])) { ?>
                        <table class="table table-bordered custom-table">
                            <thead>
                            <tr>
                                <th>Departure Date</th>
                                <th>Allocation</th>
                                <th>Available</th>
                                <th>Booked</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($package_description['departure_dates'] as $dept_date) { ?>
                                <tr>
                                    <td><?php echo $dept_date->departure_date ?></td>
                                    <td><?php echo $dept_date->all_allocation ?></td>
                                    <td><?php echo $dept_date->available ?></td>
                                    <td><?php echo $dept_date->booked ?></td>
                                    <?php
                                    if(strtotime($dept_date->departure_date) > strtotime(date('Y-m-d')) && !empty($result)) { ?>
                                    <td><a href="javascript:void(0);" class="book-now" rel="<?php echo $dept_date->departure_date ?>" data-allocation="<?php echo $dept_date->all_allocation ?>" data-booked="<?php echo $dept_date->booked ?>" data-available="<?php echo $dept_date->available ?>">BOOK NOW</a></td>
                                    <?php } ?>
                                    <!--<td><a href="">BOOK NOW</a></td>-->
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                    <p>No Departure Date with Pricing Available as of Yet.</p>
                    <?php } ?>
                </div>
            </div>

            <div class="tab-pane fade" id="map1">
                <?php
                //if((isset($package_description['info']->map_file) && !empty($package_description['info']->map_file)) || (isset($package_description['package_map']) && !empty($packaage_description['package_map']))) {
                if((isset($package_description['package_map']) && !empty($package_description['package_map'])) || (isset($package_description['info']->map_file) && !empty($package_description['info']->map_file))) {
                    if(!empty($package_description['info']->map_file)) {
                        $map_type = 'file';
                    }
                    if(!empty($package_description['package_map'])) {
                        $map_type = 'lat_long';
                    }
                    if(!empty($package_description['info']->map_file) && !empty($package_description['package_map'])) {
                        $map_type = 'both';
                    }
                    if($map_type == 'file') {
                    ?>
                        <div id="map" style="height: 800px;"></div>
                        <input type="hidden" id="hidden_lat_long" value='0' />
                        <input type="hidden" id="hidden_map_file" value="<?php echo $package_description['info']->map_file ?>">
                        <script>
                            function initMap() {
                                setTimeout(function(){
                                    var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 11,
                                        center: {lat: 27.740132, lng: 85.331406}
                                    });

                                    var kmlUrl = $("#baseUrl").val() + $("#hidden_map_file").val();

                                    var kmlOptions = {
                                        suppressInfoWindows: true,
                                        preserveViewport: false,
                                        map: map
                                    };
                                    var kmlLayer = new google.maps.KmlLayer(kmlUrl, kmlOptions);
                                }, 200);
                            }
                        </script>
                        <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYG6L43xnsrQrd7XOTfqsqx6f94_qal24&signed_in=true">
                        </script>
                        <input type="hidden" id="hidden_map_type" value="mapfile">
                    <?php } else if($map_type == 'lat_long') { ?>
                        <script async defer
                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYG6L43xnsrQrd7XOTfqsqx6f94_qal24&signed_in=true">
                        </script>
                        <input type="hidden" id="hidden_lat_long" value='<?php echo $package_description['package_map'] ?>' />
                        <div id="map-packages"></div>
                        <input type="hidden" id="hidden_map_type" value="maplatlong">
                    <?php } else { ?>
                        <a href="javascript:void(0);" class="view-detail-map">View Detail Map</a>
                        <div id="map" style="display:none"></div>
                        <input type="hidden" id="hidden_map_file" value="<?php echo $package_description['info']->map_file ?>">
                        <script>
                            function initMap() {
                                setTimeout(function(){
                                    var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 11,
                                        center: {lat: 27.740132, lng: 85.331406}
                                    });

                                    var kmlUrl = $("#baseUrl").val() + $("#hidden_map_file").val();

                                    var kmlOptions = {
                                        suppressInfoWindows: true,
                                        preserveViewport: false,
                                        map: map
                                    };
                                    var kmlLayer = new google.maps.KmlLayer(kmlUrl, kmlOptions);
                                }, 200);



                            }
                        </script>
                        <script async defer
                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYG6L43xnsrQrd7XOTfqsqx6f94_qal24&signed_in=true">
                        </script>
                        <input type="hidden" id="hidden_lat_long" value='<?php echo $package_description['package_map'] ?>' />
                        <div id="map-packages" style="height:800px"></div>
                        <input type="hidden" id="hidden_map_type" value="maplatlong">
                    <?php } ?>
                <?php } else { ?>
                <input type="hidden" id="hidden_lat_long" value='0' />
                <p>No Map Available as of Now</p>
                <?php } ?>
                <?php /*if($package_description['package_map'] && !empty($packaage_description['package_map'])) { */?><!--
                    <input type="hidden" id="hidden_lat_long" value='<?php /*echo $package_description['package_map'] */?>' />
                    <div id="map-packages"></div>
                <?php /*} else { */?>
                    <input type="hidden" id="hidden_lat_long" value='0' />
                    <p>No Map Available as of Now</p>
                --><?php /*}*/ ?>
            </div>
        </div>
    </div>
</section>
<form id="book_now_date" method="post" action="">
    <input type="hidden" id="date" name="date" value="">
    <input type="hidden" id="allocation" name="allocation" value="">
    <input type="hidden" id="booked" name="booked" value="">
    <input type="hidden" id="available" name="available" value="">
    <input type="hidden" id="url" name="url" value="">
</form>
<?php } else { ?>
    <p>No Description</p>
<?php } ?>