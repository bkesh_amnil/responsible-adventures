<div class="container">
    <div class="featured-header clearfix">
        <h2>FEATURED EXPERIENCES</h2>
        <select class="form-control js-example-basic-single" id="all_packages_filter">
            <option>All</option>
            <?php foreach($featured_packages_search as $featured_package) { ?>
            <option rel="<?php echo site_url('destination/' . $featured_package->destination_slug . '/' . $featured_package->experience_slug . '/' .  $featured_package->area_slug . '/' . $featured_package->slug) ?>"><?php echo $featured_package->name ?></option>
            <?php } ?>
        </select>
    </div>

    <?php
    $count_data = count($featured_packages);
    $total_pages = ceil($count_data/$item_per_page);
    $data['total_pages'] = $total_pages;

    foreach($featured_packages as $ind => $featured_package) {
        $cur_date = date('Y-m-d');
        /*$query = "SELECT unit_price FROM tbl_package_price
                    WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $featured_package->id .")
                    AND CURDATE() BETWEEN valid_from AND valid_to
                    AND package_id =" . $featured_package->id;*/
        $query = "SELECT unit_price FROM tbl_package_price
                    WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $featured_package->id .")
                    AND '".$cur_date."' BETWEEN valid_from AND valid_to
                    AND package_id =" . $featured_package->id;
        $row = $this->db->query($query)->row();
        //if($ind == $item_per_page) {break;}
        $ids[] = $featured_package->id;
        $divClass = '';
        if($ind % 2 != 0) {
            $divClass = ' pull-right';
        }
        ?>
        <div class="tour-package-wrap">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6<?php echo $divClass ?>">
                    <div class="featured-experiences-img-wrapper">
                        <img class="img-responsive" src="<?php echo base_url($featured_package->cover_image) ?>" alt="<?php echo $featured_package->name ?>"/>
                        <div class="trek-short-desc">
                            <p><?php echo $featured_package->short_description ?></p>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <div class="featured-experiences-detail">
                        <div class="align-middle">
                            <h3><?php echo $featured_package->name ?></h3>
                            <span>Duration: <?php echo $featured_package->duration . ' days' ?></span>
                            <span>Minimum Pax: <?php echo $featured_package->minimum_group_size . ' people' ?></span>
                            <?php if(!empty($row->unit_price)) { ?><span>Price Starting from: US$ <?php echo $row->unit_price ?></span><?php } ?>
                            <span class="trek-difficulty-level">Difficulty: <?php echo $featured_package->difficulty_name ?></span>
                            <a class="btn-all" href="<?php echo site_url('destination/' . $featured_package->destination_slug . '/' . $featured_package->experience_slug . '/' . $featured_package->area_slug . '/' . $featured_package->slug . '/book') ?>">Book Now</a>
                            <a class="btn-all" href="<?php echo site_url('destination/' . $featured_package->destination_slug . '/' . $featured_package->experience_slug . '/' .  $featured_package->area_slug . '/' . $featured_package->slug) ?>">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?php //if($count_data > $item_per_page) { ?>
    <a class="btn-all" id="load-more" href="javascript:void(0);">Load More</a>
    <div class="animation_image" style="display:none;"><img src="<?php echo base_url('img/load.gif') ?>" alt="loader"> Loading...</div>
<?php //} ?>
<input type="hidden" id="hidden_package_ids" value="<?php echo implode(',', $ids) ?>">
<input type="hidden" id="hidden_search_type" value="default">