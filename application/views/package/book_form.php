<section class="booking-form-wrapper"> <div class="container"><div class="row">
            <div class="col-sm-12 col-md-9 col-lg-9">
                <div class="tours-status-wrap">
                    <div class="bookingform-wrap">
                        <div style="display: none;" class="processing similar-processing"></div>
                        <div class="row">
                            <?php
                            if($module_name == 'book') {
                                $this->load->view('form/book_package.php');
                            } else {
                                $this->load->view('form/enquire_package.php');
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3">
                <div class="about-tour">
                    <div class="aside-tour-about">
                        <?php $this->load->view('package/aside_detail.php') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>