<section class="media-wrap">
    <div class="container">
        <h3 class="sections-title"><?php echo ucfirst($module_name) ?></h3>
        <div class="row" id="news-events-listing">
        <?php
        foreach($content as $media) {
            if(isset($modules)) {
                $social_data_individual = $this->public_model->getSocialData($modules[0]['module_id'], $media->id);
            }
            ?>
            <div class="col-xs-6 col-sm-3 col-lg-3">
                <?php
                if($count == 1) {
                    $module_name = $module_name == 'gallery' ? 'photo' : $module_name;
                    $aClass = '';
                    $attr = '';
                    $aUrl = site_url($module_name . '/' . $media->slug);
                    $target = '';
                    if($module_name == 'video') {
                        $imgUrl = 'http://i4.ytimg.com/vi/'.$media->video_id.'/0.jpg';
                        $aUrl = $media->youtube_link;
                        $aClass = ' fancybox-media';
                        $attr = ' rel="media-gallery"';
                        $target = ' target="_blank"';
                    } else {
                        $imgUrl = image_thumb($media->cover, 270, 270, '', true);
                    }
                    ?>
                    <div class="media-content-wrap">
                        <?php if(isset($social_data_individual) && !empty($social_data_individual)) { ?>
                            <!-- Go to www.addthis.com/dashboard to customize your tools -->
                        <span class="addthis_button btn-share" addthis:url="<?php echo !empty($social_data_individual['Facebook']->link) ? $social_data_individual['Facebook']->link : current_url() . '/' . $media->slug ?>" addthis:title="<?php echo (isset($social_data_individual['Facebook']->title) && !empty($social_data_individual['Facebook']->title)) ? $social_data_individual['Facebook']->title : $media->name ?>" addthis:description="<?php echo (isset($social_data_individual['Facebook']->description) && !empty($social_data_individual['Facebook']->description)) ? $social_data_individual['Facebook']->description : '' ?>"href="javascript:void(0)"><img src="<?php echo base_url('img/icons/icon-addthis-share.png') ?>" alt=""/></span>
                        <?php } ?>
                        <a href="<?php echo $aUrl ?>" class="<?php echo $aClass ?>"<?php echo $attr ?> data-caption="<?php echo (isset($media->description) && !empty($media->description)) ? '<h2>'.$media->name . '</h2><p>' . strip_tags($media->description) .'</p>' : '<h2>'.$media->name.'</h2>' ?>"<?php echo $target ?>>
                            <div class="media-content-img-wrap">
                                <img class="img-responsive" src="<?php echo $imgUrl ?>" alt="<?php echo $media->name ?>" />
                            </div>
                            <?php //} ?>
                            <span class="media-contents-title" title="<?php echo $media->name ?>"><?php echo (strlen($media->name) > 52) ? substr($media->name, 0, 52) . ' ...' : $media->name ?></span>
                        </a>
                    </div>
                <?php } else { ?>
                    <?php if(!empty($media->media)) { ?>
                        <a class="media-content-wrap gallery-link fancybox" href="<?php echo base_url($media->media) ?>" data-fancybox-group="gallery" title="<?php echo $media->title ?>" data-caption="<?php echo '<h2>'.$media->title . '</h2><p>' . $media->caption .'</p>' ?>">
                            <div class="trip-img-wrap">
                                <img class="img-responsive" src="<?php echo image_thumb($media->media, 270, 270, '', true) ?>" alt="<?php echo $media->title ?>" />
                            </div>
                            <?php if(!empty($media->title)) { ?>
                                <span class="media-contents-title" title="<?php echo $media->title ?>"><?php echo (strlen($media->title) > 52) ? substr($media->title, 0, 52) . ' ...' : $media->title ?></span>
                            <?php } ?>
                        </a>
                    <?php } ?>

                <?php } ?>
            </div>
        <?php } ?>
    </div>
    </div>
</section>