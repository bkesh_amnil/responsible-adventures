<div class="asideContent">
    <h5>Recent Posts</h5>
    <ul>
        <?php foreach($recent_news as $recent_new) { ?>
            <li><a href="<?php echo site_url('news/' . $recent_new->slug) ?>"><?php echo $recent_new->name ?></a></li>
        <?php } ?>
    </ul>
</div>
<div class="asideContent">
    <h5>Archives</h5>
    <ul>
        <?php foreach($archive_news as $archive_new) { ?>
            <li><a href="<?php echo site_url('news/' . $archive_new->date_url) ?>"><?php echo $archive_new->archive_date ?></a></li>
        <?php } ?>
    </ul>
</div>
<div class="asideContent">
    <h5>Categories</h5>
    <ul>
        <?php foreach($category_news as $category_new) { ?>
            <li><a href="<?php echo site_url('news/category/' . $category_new->slug) ?>"><?php echo $category_new->name ?></a></li>
        <?php } ?>
    </ul>
</div>