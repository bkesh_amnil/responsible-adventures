<?php $img = base_url('img/home-office-optimized.jpg') ?>
<section class="banner-news" style="background-image: url(<?php echo $img ?>);">
    <h2>NEWS</h2>
</section>

<section class="news-wrap" >
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div id="paging_container2" class="container_srch">
                    <ul class="content">
                    <?php
                    if(isset($content) && !empty($content)) {
                        foreach($content as $val) {
                            $news_cat_arr = explode(',', $val->news_category_id);
                            ?>
                            <div class="contents">
                                <div class="contents-head">
                                    <div class="date">
                                        <span><?php echo date('M', strtotime($val->publish_date)) ?></span>
                                        <span><?php echo date('d', strtotime($val->publish_date)) ?></span>
                                    </div>

                                    <div class="contentTitlewrap">
                                        <h3><?php echo $val->name ?></h3>

                                        <div class="postedBy"><span>Posted By</span>: <span><?php echo $val->posted_by ?></span></div>
                                        <div class="tagiingWrap">
                                            <span>UNDER</span>:
                                            <?php
                                            $count = count($news_cat_arr);
                                            foreach($news_cat_arr as $ind => $news_cat) {
                                                $cat_det = $this->db->select('name, slug')->where('id', $news_cat)->get('tbl_news_category')->row();
                                                if($ind > 0) {
                                                    echo ', ';
                                                }
                                            ?>
                                            <a href="<?php echo site_url('news/category/' . $cat_det->slug) ?>"><?php echo $cat_det->name ?></a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>

                                <?php if(!empty($val->image)) { ?>
                                    <div class="contents-img-wrap">
                                        <img class="img-responsive" src="<?php echo base_url($val->image) ?>" alt="<?php echo $val->name ?>" />
                                        <span><?php echo $val->name ?></span>
                                    </div>
                                <?php } ?>

                                <p><?php echo $val->short_description ?></p>

                                <a class="btnReadmore" href="<?php echo site_url('news/' . $val->slug) ?>">Read More</a>
                            </div>
                            <?php
                        }
                    } else { ?>
                        <p>No News Added as of Yet.</p>
                    <?php } ?>
                    </ul>
                    <div class="page_navigation pagination-wrap"></div>
                </div>
            </div>
            
            <div class="col-lg-3">
                <?php $this->load->view('news/aside_list.php') ?>
            </div>
        </div>
    </div>
</section>
