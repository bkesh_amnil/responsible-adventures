<section class="news-wrap" >
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <?php if(isset($content) && !empty($content)) {
                    $news_cat_arr = explode(',', $content->news_category_id);
                    $tags_arr = explode(',', $content->tags);
                    ?>
                <div class="contents">
                    <div class="contents-head">
                        <div class="date">
                            <span><?php echo date('M', strtotime($content->publish_date)) ?></span>
                            <span><?php echo date('d', strtotime($content->publish_date)) ?></span>
                        </div>
                        
                        <div class="contentTitlewrap">
                            <h3><?php echo $content->name ?></h3>
                            <div class="postedBy"><span>Posted By</span>: <span><?php echo $content->posted_by ?></span></div>
                        </div>
                    </div>
                    <?php if(!empty($content->image)) { ?>
                    <div class="contents-img-wrap">
                        <img class="img-responsive" src="<?php echo base_url($content->image) ?>" alt="<?php echo $content->name ?>" />
                        <span><?php echo $content->name ?></span>
                    </div>
                    <?php } ?>
                    <?php echo $content->description ?>
                    <div class="tagiingWrap"><span>UNDER</span>:
                        <?php
                        foreach($news_cat_arr as $ind => $news_cat) {
                            $cat_det = $this->db->select('name, slug')->where('id', $news_cat)->get('tbl_news_category')->row();
                            if($ind > 0) {
                                echo ', ';
                            }
                        ?>
                        <a href="<?php echo site_url('news/category/' . $cat_det->slug) ?>"><?php echo $cat_det->name ?></a>
                        <?php } ?>
                    </div>
                    <div class="tagiingWrap"><span>TAGS</span>:
                        <?php
                        foreach($tags_arr as $ind => $tags) {
                            if($ind > 0) {
                                echo ', ';
                            }
                            /*echo $tags;*/
                            ?>
                            <a href="<?php echo site_url('news/tags/' . $tags) ?>"><?php echo $tags ?></a>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <?php } ?>
                <?php if(isset($social_datas) && !empty($social_datas)) { ?>
                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_sharing_toolbox"></div>
                <?php } ?>
            </div>

            <div class="col-lg-3">
                <?php $this->load->view('news/aside_list.php') ?>
            </div>
        </div>
    </div>
</section>
