<section class="packages-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="package-filter">
                    <?php $this->load->view('form/filter_package.php'); ?>
                </div>
            </div>

            <div class="col-sm-12 col-md-8 col-lg-9">
                <div class="processing" style="display: none;"></div>
                <?php if(isset($content) && !empty($content)) { ?>
                    <div class="packages">
                        <div id="paging_container2" class="container_srch">
                            <ul class="content">
                                <?php
                                $cur_date = date('Y-m-d');
                                foreach($content as $val) {
                                    /*$query = "SELECT unit_price FROM tbl_package_price
                                                WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $val->id .")
                                                AND CURDATE() BETWEEN valid_from AND valid_to
                                                AND package_id =" . $val->id;*/
                                    $query = "SELECT unit_price FROM tbl_package_price
                                                WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $val->id .")
                                                AND '".$cur_date."' BETWEEN valid_from AND valid_to
                                                AND package_id =" . $val->id;
                                    $row = $this->db->query($query)->row();
                                    ?>
                                <div class="tour-package-wrap">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="featured-experiences-img-wrapper">
                                                <img src="<?php echo base_url($val->cover_image) ?>" alt="<?php echo $val->name ?>"/>
                                                <div class="trek-short-desc">
                                                    <p><?php echo $val->short_description ?></p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="featured-experiences-detail">
                                                <div class="align-middle">
                                                    <h2 title="<?php echo $val->name ?>"><?php echo $val->name ?></h2>
                                                    <span>Duration: <?php echo $val->duration . ' days' ?></span>
                                                    <span>Minimum Pax: <?php echo $val->minimum_group_size . ' people' ?></span>
                                                    <?php if(!empty($row)) { ?><span>Price Starting from: US$ <?php echo $row->unit_price ?></span><?php } ?>
                                                    <span class="trek-difficulty-level">Difficulty: <?php echo $val->difficulty_name ?></span>
                                                    <!--<a class="btn-all" href="<?php echo site_url('destination/'. $val->destination_slug . '/' . $val->experience_slug . '/' . $val->area_slug . '/' . $val->slug . '/book') ?>">Book Now</a>-->
                                                    <a class="btn-all btn-book-now" rel="<?php echo $val->slug ?>" href="javascript:void(0);">Book Now</a>
                                                    <!--<a class="btn-all" href="<?php echo site_url('destination/'. $val->destination_slug . '/' . $val->experience_slug . '/' . $val->area_slug . '/' . $val->slug) ?>">Read More</a>-->
                                                    <a class="btn-all" href="<?php echo site_url('package/'. $val->slug) ?>">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </ul>
                            <div class="paging-information">
                                <?php echo '<p class="total-count">Total Packages : ' . count($content) . ' , </p>'; ?>
                                <p class="page">
                                    <span class="current_page_number">1</span>
                                    <?php echo ' of total ' ?>
                                    <span class="total_page_number"></span>
                                    <?php echo ' pages' ?>
                                </p>
                            </div>
                            <div class="page_navigation pagination-wrap"></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
