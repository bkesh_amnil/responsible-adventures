<?php if(isset($destination_sliders) && !empty($destination_sliders)) { ?>
<section id="destinaiton" class="owl-carousel">
    <?php foreach($destination_sliders as $destination_slider) { ?>
    <div class="banner-content">
        <img src="<?php echo base_url($destination_slider->media) ?>" alt="<?php echo $destination_slider->title ?>"/>
        <div class="banner-desc">
            <h1><?php echo $destination_slider->title ?></h1>
            <?php if(!empty($destination_slider->caption)) { ?>
                <p><?php echo $destination_slider->caption ?></p>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</section>
<?php } ?>

<section class="search-filter-wrap">
    <?php $this->load->view('form/filter'); ?>
</section>

<section class="destination-content-wrap">
    <div class="container">
        <div style="margin-bottom: 30px;">
            <?php if(isset($destination_detail) && !empty($destination_detail)) { ?>
                <h2><?php echo $destination_detail->name ?></h2>
                <?php echo $destination_detail->long_description ?>
            <?php } ?>
        </div>
        <?php
        if(isset($experiences) && !empty($experiences)) {
            foreach($experiences as $ind => $experience) {
            $divClass = '';
            if($ind % 2 != 0) {
                $divClass = ' pull-right';
            }
            ?>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-lg-5<?php echo $divClass ?>">
                    <!--<a href="<?php echo site_url('destination/' . $destination_detail->slug . '/' . $experience->slug) ?>">-->
                      <a href="<?php echo site_url($destination_detail->slug . '/' . $experience->slug) ?>">
                        <div class="destination-content-img">
                            <h2>EXP : <?php echo strtoupper($experience->name) ?></h2>
                            <img class="img-responsive" src="<?php echo base_url($experience->image) ?>" alt="<?php echo $experience->name ?>"/>
                        </div>
                    </a>
                </div>

                <div class="col-xs-12 col-sm-2 col-lg-2<?php echo $divClass ?>">
                    <div class="destination-content-icon-wrap">
                        <img src="<?php echo base_url($experience->icon) ?>" alt="<?php echo $experience->name ?>"/>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-5 col-lg-5">
                    <div class="destination-content-desc clearfix">
                        <h3><?php echo $experience->sub_title ?></h3>
                        <div class="desc-scroll"><div class="scroll-description"><?php echo $experience->description ?></div></div>
                    </div>
                    <!--<a class="btn-all" href="<?php echo site_url('destination/' . $destination_detail->slug . '/' . $experience->slug) ?>" style="background-color: rgb(179, 100, 172)">READ MORE</a>-->
                    <a class="btn-all" href="<?php echo site_url($destination_detail->slug . '/' . $experience->slug) ?>" style="background-color: rgb(179, 100, 172)">READ MORE</a>
                </div>
            </div>
        <?php } ?>
    </div>
    <?php } else { ?>
        <p>No Experience Added as of Yet for <?php echo $destination_detail->name ?></p>
    <?php } ?>
</section>