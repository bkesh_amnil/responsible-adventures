<div class="container">
    <?php
    $count = count($destinations);
    $half_count = $count / 2;

    if (strpos($half_count,'.') !== false) {
        $half_count = floor($half_count);
        $half_count = $half_count + 1;
    }
    $i = 0;
    //$j = 0;
    for($i = 0; $i < $half_count; $i++) {
        ?>
        <div class="tour-highlighted-wraps clearfix">
            <div class="tour-location-wrap">
                <?php
                $j = $i;
                foreach($destinations as $ind => $destination) {
                    $j = $i+$i;
                    if($ind == $j || $ind == $j+1) {

                    ?>
                    <div class="tours clearfix" data-country="<?php echo $destination->id ?>">
                        <div class="location-img-wrapper" data-country="<?php echo $destination->id ?>">
                            <img class="img-responsive" src="<?php echo base_url($destination->image) ?>" alt="<?php echo $destination->name ?>"/>
                        </div>
                        <h2 class="destintaion_name" data-country="<?php echo $destination->id ?>"><?php echo $destination->name ?></h2>
                    </div>
                    <?php
                    }
                    $j++;
                    ?>
                <?php } ?>
            </div>
            <div class="tour-detail-slide-in-wrap">
                <?php
                $j = $i;
                foreach($destinations as $ind => $destination) {
                    $j = $i + $i;
                    if ($ind == $j || $ind == $j + 1) {
                    ?>
                        <a class="country" data-country="<?php echo $destination->id ?>" href="javascript:void(0);"></a>
                        <?php if($ind % 2 == 0) { ?>
                            <div class="carousel tour-detail-slide-in" style="height:570px; width: 570px"></div>
                            <?php
                        }
                    }
                    $j++;
                }
                ?>
                <!--
                <?php /*/*foreach($activities as $activity) { */?>
                <img class="img-responsive" src="<?php /*echo base_url('img/tour-2.jpg') */?>" alt=""/>
                <div class="trek-short-desc">
                    <h3>TREKKING</h3>
                    <p>
                        Neque porro quisquam est qui dolorem ipsum quia dolor sit amet,Consectetur,
                        adipisci velit est qui dolorem ipsum quia dolor sit amet,Consectetur, adipisci velit
                        qui dolorem ipsum quia dolor sit amet,Consectetur, adipisci velit
                    </p>
                </div>-->
               <?php /*} */?>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<input type="hidden" id="hidden_json_data" value='<?php echo $activity_sliders ?>'>
