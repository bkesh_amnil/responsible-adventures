<?php if(isset($destination_sliders) && !empty($destination_sliders)) { ?>
    <section id="destinaiton" class="owl-carousel">
        <?php foreach($destination_sliders as $destination_slider) { ?>
            <div class="banner-content">
                <img src="<?php echo base_url($destination_slider->media) ?>" alt="<?php echo $destination_slider->title ?>"/>
                <div class="banner-desc">
                    <h1><?php echo $destination_slider->title ?></h1>
                    <?php if(!empty($destination_slider->caption)) { ?>
                        <p><?php echo $destination_slider->caption ?></p>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </section>
<?php } ?>

<section class="search-filter-wrap">
    <?php $this->load->view('form/filter'); ?>
</section>

<section class="destination-areas">
    <?php if(isset($areas) && !empty($areas)) { ?>
        <div class="container">
            <div class="featured-header clearfix">
                <h2><?php echo $areas[0]->experience_name ?></h2>
            </div>
            <?php
            foreach($areas as $ind => $area) {
                $divClass = '';
                if($ind % 2 != 0) {
                    $divClass = ' pull-right';
                }
                ?>
                <div class="tour-package-wrap">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6<?php echo $divClass ?>">
                            <a href="<?php echo current_url() . '/' . $area->slug ?>">
                                <div class="featured-experiences-img-wrapper">
                                    <img class="img-responsive" src="<?php echo base_url($area->image) ?>" alt="<?php echo $area->name ?>"/>
                                </div>
                            </a>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="featured-experiences-detail">
                                <div class="align-middle">
                                    <h3><?php echo $area->name ?></h3>
                                    <div class="moun-scroll">
                                    <?php echo $area->description ?>
                                    </div>
                                    <a class="btn-all" href="<?php echo current_url() . '/' . $area->slug ?>">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <p>No Package Related to Area added as of Yet.</p>
    <?php } ?>
</section>