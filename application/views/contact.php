<section class="breadcrumb-wrap">
    <?php if(isset($breadcrumb) && !empty($breadcrumb)) { ?>
        <?php $this->load->view('common/breadcrumb', $breadcrumb) ?>
    <?php } ?>
</section>
<section id="map-canvas"></section>
<section class="contactus-wrap">
    <div class="container">
        <h2>Contact Us</h2>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <?php $this->load->view('form/inquiry.php'); ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <?php foreach($content as $val) { ?>
                    <div class="contacts">
                        <h3><?php echo $val->name ?></h3>
                        <?php echo $val->long_description ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="connectus-wrap">
            <h2>Connect with us</h2>
            <?php if(isset($facebook) && !empty($facebook)) { ?>
                <a class="facebook" href="<?php echo $facebook ?>" target="_blank">
                    <img src="<?php echo base_url('img/icons/icon-facebook.png') ?>" alt="Facebook"/>
                </a>
            <?php } ?>
            <?php if(isset($gplus) && !empty($gplus)) { ?>
                <a class="gmail" href="<?php echo $gplus ?>" target="_blank">
                    <img src="<?php echo base_url('img/icons/icon-googleplus.png') ?>" alt="Gmail"/>
                </a>
            <?php } ?>
            <?php if(isset($youtube) && !empty($youtube)) { ?>
                <a class="youtube" href="<?php echo $youtube ?>" target="_blank">
                    <img src="<?php echo base_url('img/icons/icon-youtube.png') ?>" alt="Youtube"/>
                </a>
            <?php } ?>
            <?php if(isset($twitter) && !empty($twitter)) { ?>
                <a class="twitter" href="<?php echo $twitter ?>" target="_blank">
                    <img src="<?php echo base_url('img/icons/icon-twitter.png') ?>" alt="Twitter"/>
                </a>
            <?php } ?>
            <?php if(isset($linkedin) && !empty($linkedin)) { ?>
                <a class="linkedin" href="<?php echo $linkedin ?>" target="_blank">
                    <img src="<?php echo base_url('img/icons/icon-linkedin.png') ?>" alt="Linkedin"/>
                </a>
            <?php } ?>
            <?php if(isset($pinterest) && !empty($pinterest)) { ?>
                <a class="pintrest" href="<?php echo $pinterest ?>" target="_blank">
                    <img src="<?php echo base_url('img/icons/icon-pintrest.png') ?>" alt="Pintrest"/>
                </a>
            <?php } ?>
            <?php if(isset($instagram) && !empty($instagram)) { ?>
                <a class="instagram" href="<?php echo $instagram ?>" target="_blank">
                    <img src="<?php echo base_url('img/icons/icon-instagram.png') ?>" alt="Instagram"/>
                </a>
            <?php } ?>
            <?php if(isset($vimeo) && !empty($vimeo)) { ?>
                <a class="vimeo" href="<?php echo $vimeo ?>" target="_blank">
                    <img src="<?php echo base_url('img/icons/icon-vimeo.png') ?>" alt="Vimeo"/>
                </a>
            <?php } ?>
        </div>
    </div>
    </div>
</section>