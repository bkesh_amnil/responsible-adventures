<section class="breadcrumb-wrap">
    <?php if(isset($breadcrumb) && !empty($breadcrumb)) { ?>
        <?php $this->load->view('common/breadcrumb', $breadcrumb) ?>
    <?php } ?>
</section>
<?php
//echo $module_name;echo $count;die;
switch($module_name) {
    case "destination":
        switch($count) {
            case "3":
                $this->load->view('destination/area.php');
                break;
            case "4":
                $this->load->view('destination/package.php');
                break;
            default:
                $this->load->view('destination/experience.php');
                break;
        }
        break;
    case "package":
        $this->load->view('destination/package.php');
        break;
    case "content":
        $this->load->view('content.php');
        break;
    case "book":
        $this->load->view('package/book_form.php');
        break;
    case "enquire":
        $this->load->view('package/book_form.php');
        break;
    case "team":
        $this->load->view('team.php');
        break;
    case "photo":
    case "gallery":
    case "video":
        $this->load->view('media/list.php');
        break;
    case "menu":
        $this->load->view('site_map.php');
        break;
    case "testimonial":
        switch($count) {
            case "2":
                $this->load->view('testimonial/detail.php');
                break;
            case "1":
                $this->load->view('testimonial/list.php');
                break;
        }
        break;
    case "news":
        switch($count) {
            case "2":
                $this->load->view('news/detail.php');
                break;
            case "1":
            case "3":
                $this->load->view('news/list.php');
                break;
        }
        break;
    default:
        $this->load->view('package/detail.php');
        break;
}
?>