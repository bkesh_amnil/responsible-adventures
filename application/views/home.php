<?php if(isset($banners) && !empty($banners)) { ?>
    <?php $this->load->view('banner', $banners); ?>
<?php } ?>
<section class="search-filter-wrap">
    <?php $this->load->view('form/filter'); ?>
</section>
<section>
<?php if(isset($destinations) && !empty($destinations)) { ?>
    <?php $this->load->view('destination/home', $destinations); ?>
<?php } ?>
</section>
