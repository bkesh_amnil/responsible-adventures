<section class="inner-content">
    <?php if(isset($content) && !empty($content)) { ?>
    <img class="img-responsive inner-banner" src="<?php echo base_url($content[0]->background_image) ?>" alt="<?php echo $content[0]->name ?>"/>
    <div class="container">
        <h2><?php echo $content[0]->name ?></h2>
        <?php echo $content[0]->long_description ?>
        <?php if(isset($social_datas) && !empty($social_datas)) { ?>
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <div class="addthis_sharing_toolbox"></div>
        <?php } ?>
    </div>
    <?php } else { ?>
    <div class="container">
        <p>No Content Added as of Yet.</p>
    </div>
    <?php } ?>
</section>