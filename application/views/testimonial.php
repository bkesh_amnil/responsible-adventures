<div class="container">
    <div class="row">
        <div class="col-xs-2 col-sm-2 col-lg-2">
            <img class="img-responsive" src="<?php echo base_url('img/icons/icon-blockquote-left.png') ?>" alt="left-arrow"/>
        </div>
        <div class="col-xs-8 col-sm-8 col-lg-8">
            <div id="testimonial" class="owl-carousel">
                <?php foreach($testimonials as $testimonial) { ?>
                    <div class="comments-wrap">
                        <?php echo (strlen(strip_tags($testimonial->customer_testimonial)) > 100) ? substr(strip_tags($testimonial->customer_testimonial), 0, 100) . ' ...' : $testimonial->customer_testimonial ?>
                        <span class="comment-user-name">-<?php echo $testimonial->customer_full_name ?></span>
                        <span class="comment-user-img">
                            <img class="img-responsive img-circle" src="<?php echo base_url($testimonial->customer_image) ?>" alt="<?php echo $testimonial->customer_full_name ?>"/>
                        </span>
                        <a href="<?php echo site_url('testimonial/' . $testimonial->slug) ?>"><span>Read More</span></a>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-xs-2 col-sm-2 col-lg-2">
            <img class="img-responsive" src="<?php echo base_url('img/icons/icon-blockquote-right.png') ?>" alt="right-arrow"/>
        </div>
    </div>
</div>