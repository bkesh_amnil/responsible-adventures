<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div style="display: none;" class="processing popup-processing"></div>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Please let us know where to email you the package pdf</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo base_url('dynamic_form/mail_pdf') ?>" method="post" id="comit_form" name="send-pdf-form">
                    <input type="hidden" name="fileid" value="" id="fileid"/>
                    <input type="hidden" name="filename" value="" id="filename"/>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control required" id="name" name="name" placeholder="Full Name *">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control required email" id="email" name="email" placeholder="Email Address *">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary modal-send" id="send-pdf">Send me the pdf</button>
            </div>
        </div>
    </div>
</div>