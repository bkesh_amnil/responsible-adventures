<form>
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-lg-12">
            <div class="form-group">
                <label>Location</label>
                <select class="form-control js-example-basic-single" id="destination">
                    <option value="0">Select Location</option>
                    <?php
                    if(isset($destinations) && !empty($destinations)) {
                        foreach($destinations as $destination) {
                            $selected = '';
                            if(isset($destination_id) && $destination->id == $destination_id) {
                                $selected = ' selected="selected"';
                            }
                        ?>
                        <option value="<?php echo $destination->id ?>" <?php echo $selected ?>><?php echo $destination->name ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4 col-lg-12">
            <div class="form-group">
                <label>Experience</label>
                <select class="form-control js-example-basic-single" id="experience">
                    <option value="0">Select Experience</option>
                    <?php
                    if(isset($experiences) && !empty($experiences)) {
                        foreach($experiences as $experience) {
                            $selected = '';
                            if(isset($experience_slug) && $experience->slug == $experience_slug) {
                                $selected = ' selected="selelcted"';
                            }
                        ?>
                        <option value="<?php echo $experience->slug ?>"<?php echo $selected ?>><?php echo $experience->name ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-4 col-lg-12">
            <div class="form-group">
                <label>Prices</label>
                <select class="form-control js-example-basic-single" id="price">
                    <option value="ASC">Low to High</option>
                    <option value="DESC">High to Low</option>
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-12">
            <div class="form-group">
                <label>Year</label>
                <select class="form-control js-example-basic-single" id="years">
                    <option value="0">Select Year</option>
                    <?php
                    if(isset($years) && !empty($years)) {
                        foreach($years as $year) {
                        ?>
                        <option value="<?php echo $year->years ?>"><?php echo $year->years ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-12">
            <div class="form-group">
                <label>Month</label>
                <select class="form-control js-example-basic-single" id="months">
                    <option value="0">Select Month</option>
                    <?php
                    if(isset($months) && !empty($months)) {
                        foreach($months as $month) {
                        ?>
                        <option value="<?php echo $month->months ?>"><?php echo $month->months ?></option>
                        <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>

    </div>

    <input class="btn-all btn-reset-package-search" type="reset" value="Reset Filters"/>
</form>