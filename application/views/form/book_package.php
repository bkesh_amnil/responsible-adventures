<input type="hidden" id="hidden_package_id" value="<?php echo $package_description['info']->id ?>">
<div class="col-sm-8 col-lg-8">
    <h3>BOOKING FORM</h3>
    <!--<div style="display: none;" class="processing similar-processing"></div>-->
    <?php if(isset($package_description['departure_dates']) && !empty($package_description['departure_dates'])) { ?>
        <form name="package_booking" method="POST" action="<?php echo base_url('dynamic_form/package_booking') ?>" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-6 col-lg-6">
                    <div class="form-group">
                        <label>Departure Date</label>
                        <?php
                        foreach($package_description['departure_dates'] as $dval) {
                            $array[] = $dval->departure_date;
                        }

                        $commaList = implode(',', $array);
                        ?>
                        <input type="hidden" id="hidden_selected_dates" value="<?php echo $commaList ?>">
                        <?php if(isset($selected_date) && !empty($selected_date)) { ?>
                        <input type="text" name="departure_date" id="departure_date" class="form-control required" placeholder="Departure Date" value="<?php echo $selected_date ?>">
                        <?php } else { ?>
                        <input type="text" name="departure_date" id="departure_date" class="form-control required" placeholder="Departure Date">
                        <?php } ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Price Per Person(in $)</label>
                        <input class="form-control price_per_person" type="hidden" name="price_per_person" value="" />
                        <input class="form-control price_per_person required" type="text" name="price_per_person" placeholder="Price Per Person" value="" disabled />
                    </div>
                    
                    <div class="form-group">
                        <label>Full Name *</label>
                        <input class="form-control required" type="text" name="full_name" placeholder="Full Name" />
                    </div>
                    
                    <div class="form-group">
                        <label>Contact Address *</label>
                        <input class="form-control required" type="text" name="contact_address" placeholder="Contact Address" />
                    </div>
                    
                    <div class="form-group">
                        <label>Remarks</label>
                        <textarea class="form-control" placeholder="Remarks" name="remarks"></textarea>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-6">
                    <div class="form-group">
                        <?php
                        $range = range(1, 20);
                        ?>
                        <label>Number of Pax</label>
                        <select class="form-control js-example-basic-single select2-hidden-accessible required" id="nos_of_pax" name="nos_of_pax">
                            <option value="0">Select</option>
                            <?php foreach($range as $val) { ?>
                                <option value="<?php echo $val ?>"><?php echo $val ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Total Cost(in $)</label>
                        <input class="form-control total_cost" type="hidden" name="total_cost" value="" />
                        <input class="form-control total_cost required" type="text" name="total_cost" id="" placeholder="Total Cost" value="" disabled />
                    </div>
                    
                    <div class="form-group">
                        <label>Email Address *</label>
                        <input class="form-control required email" type="text" name="email_address" placeholder="Email Address" />
                    </div>
                    
                    <div class="form-group">
                        <label>Contact Number *</label>
                        <input class="form-control number required" type="text" name="contact_nos" placeholder="Contact Number" />
                    </div>
                    <div class="form-group">
                        <label>Captcha *</label>
                        <div class="g-recaptcha" data-sitekey="6Lcq6QsUAAAAACVSs6LNApv8jSuSmy2OjV4RCPUr"></div>
                    </div>
                    
<!--                    <div class="form-group">
                            <div class="validation-sum">
                            <?php
                            // init variables
                            $min_number = 1;
                            $max_number = 15;
                            // generating random numbers
                            $random_number1 = mt_rand($min_number, $max_number);
                            $random_number2 = mt_rand($min_number, $max_number);
                            ?>
                            <input type="text" value="<?php echo $random_number1 . ' + ' . $random_number2 . ' = ';?>" disabled="disabled" />
                            <input name="captchaResult" class="required number math" type="text" size="2" placeholder="sum" />
                            <input name="firstNumber" type="hidden" value="<?php echo $random_number1; ?>" />
                            <input name="secondNumber" type="hidden" value="<?php echo $random_number2; ?>" />
                        </div>
                    </div>-->
                </div>

                <div class="col-sm-12 col-lg-12">
                    <input class="btn-booking-submit pull-right" id="btn-send" type="submit" name="submit_button" value="Book" style="display:none;" />
                </div>
            </div>
            <input type="hidden" name="hidden_package_name" value="<?php echo $package_description['info']->name ?>" />
            <input type="hidden" name="hidden_package_destination_id" value="<?php echo $package_description['info']->destination_id ?>">
            <input type="hidden" name="hidden_package_experience_id" value="<?php echo $package_description['info']->experience_id ?>" />
            <input type="hidden" name="hidden_package_id" value="<?php echo $package_description['info']->id ?>" />
            <input type="hidden" name="hidden_package_code" value="<?php echo $package_description['info']->code ?>" />
        </form>
    <?php } else { ?>
        <input type="hidden" id="hidden_selected_dates" value="0">
    <?php } ?>
</div>
<?php if(isset($package_description['departure_dates']) && !empty($package_description['departure_dates'])) { ?>
    <div class="col-sm-4 col-lg-4">
        <div class="booking-status-wrap">
            <h3>BOOKING STATUS</h3>
            <ul>
                <li><div>Departure date</div><span id="selected_departure_date"><?php echo (isset($selected_date) && !empty($selected_date)) ? $selected_date : $package_description['departure_dates'][0]->departure_date ?></span></li>
                <li><div>Total Booking Available</div><span id="total_booking_available"><?php echo (isset($selected_all_allocation) && !empty($selected_all_allocation)) ? $selected_all_allocation : $package_description['departure_dates'][0]->all_allocation ?></span></li>
                <li><div>Total Booked</div><span id="total_booked"><?php echo (isset($selected_booked) && !empty($selected_booked)) ? $selected_booked : $package_description['departure_dates'][0]->booked ?></span></li>
                <li><div>Total Available</div><span id="total_available"><?php echo (isset($selected_available) && !empty($selected_available)) ? $selected_available : $package_description['departure_dates'][0]->available ?></span></li>
            </ul>
        </div>
    </div>
<?php }  else {
    $enquire_url = str_replace('book', 'enquire', current_url());
    ?>
    <p>No Booking Available as of Now.</p>
    <p class="enquire-link col-md-12 text-center">But you can send in your enquiry <a href="<?php echo $enquire_url ?>">here</a>.</p>
<?php } ?>
<a href="javascript:void(0)" class="close-btn"></a>
