<!-- Modal -->
<div class="modal fade modal-default" id="enquireModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">ENQUIRE FORM</h4>
            </div>
            <div style="display: none;" class="processing similar-processing"></div>
            <form name="package_enquire" method="POST" action="<?php echo base_url('dynamic_form/package_enquiry') ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <h3>ENQUIRE FORM</h3>

                    <div class="row">
                        <div class="col-sm-6 col-lg-6">
                            <div class="form-group">
                                <input type="text" name="departure_date" id="departure_date_enquire" class="form-control required" placeholder="Departure Date">
                            </div>

                            <div class="form-group">
                                <input class="form-control required" type="text" name="full_name" placeholder="Full Name" />
                            </div>

                            <div class="form-group">
                                <input class="form-control required" type="text" name="contact_address" placeholder="Contact Address" />
                            </div>
                        </div>

                        <div class="col-sm-6 col-lg-6">
                            <div class="form-group">
                                <?php
                                $range = range(1, 20);
                                ?>
                                <select class="form-control js-example-basic-single " id="nos_of_pax_enquire" name="nos_of_pax">
                                    <option value="0">Number of Pax</option>
                                    <?php foreach ($range as $val) { ?>
                                        <option value="<?php echo $val ?>"><?php echo $val ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="form-control required email" type="text" name="email_address" placeholder="Email Address" />
                            </div>

                            <div class="form-group">
                                <input class="form-control number required" type="text" name="contact_number" placeholder="Contact Number" />
                            </div>    


                            <!--                <div class="form-group">
                                                    <div class="validation-sum">
                            <?php
                            // init variables
                            $min_number = 1;
                            $max_number = 15;
                            // generating random numbers
                            $random_number1 = mt_rand($min_number, $max_number);
                            $random_number2 = mt_rand($min_number, $max_number);
                            ?>
                                                    <input type="text" value="<?php echo $random_number1 . ' + ' . $random_number2 . ' = '; ?>" disabled="disabled" />
                                                    <input name="captchaResult" class="required number math" type="text" size="2" placeholder="sum" />
                                                    <input name="firstNumber" type="hidden" value="<?php echo $random_number1; ?>" />
                                                    <input name="secondNumber" type="hidden" value="<?php echo $random_number2; ?>" />
                                                </div>
                                            </div>-->
                        </div>

                        <div class="col-sm-12 col-lg-12">
                            <div class="form-group">
                                <textarea class="form-control required" placeholder="Enquiry" name="enquiry"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-12">
                            <div class="form-group">
                                <!--<div class="g-recaptcha" data-sitekey="6Lcq6QsUAAAAACVSs6LNApv8jSuSmy2OjV4RCPUr"></div>-->
                                <div id="recaptcha1"></div>
                            </div>
                        </div>
                        <input type="hidden" name="hidden_package_name" value="" />
                        <input type="hidden" name="hidden_package_destination_id" value="">
                        <input type="hidden" name="hidden_package_experience_id" value="" />
                        <input type="hidden" name="hidden_package_id" value="" />
                        <input type="hidden" name="hidden_package_code" value="" />
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="loader"></div>
                    <input class="btn-all" id="btn-send" type="submit" name="submit_button" value="Enquire"/>
                </div>
            </form>
        </div>
    </div>
</div>