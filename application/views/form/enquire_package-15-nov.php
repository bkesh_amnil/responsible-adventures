<input type="hidden" id="hidden_package_id" value="<?php echo $package_description['info']->id ?>">
<div class="col-sm-12 col-lg-12">
    <h3>ENQUIRE FORM</h3>
    <div style="display: none;" class="processing similar-processing"></div>
    <form name="package_booking" method="POST" action="<?php echo base_url('dynamic_form/package_enquiry') ?>" enctype="multipart/form-data">
        <div class="row">
            <div class="col-sm-6 col-lg-6">
                <div class="form-group">
                    <label>Departure Date</label>
                    <input type="text" name="departure_date" id="departure_date" class="form-control required" placeholder="Departure Date">
                </div>
                
                <div class="form-group">
                    <label>Full Name *</label>
                    <input class="form-control required" type="text" name="full_name" placeholder="Full Name" />
                </div>
                
                <div class="form-group">
                    <label>Contact Address *</label>
                    <input class="form-control required" type="text" name="contact_address" placeholder="Contact Address" />
                </div>
                
                <div class="form-group">
                    <label>Enquiry *</label>
                    <textarea class="form-control required" placeholder="Enquiry" name="enquiry"></textarea>
                </div>
            </div>

            <div class="col-sm-6 col-lg-6">
                <div class="form-group">
                    <?php
                    $range = range(1, 20);
                    ?>
                    <label>Number of Pax</label>
                    <select class="form-control js-example-basic-single select2-hidden-accessible" id="nos_of_pax" name="nos_of_pax">
                        <option value="0">Select</option>
                        <?php foreach($range as $val) { ?>
                            <option value="<?php echo $val ?>"><?php echo $val ?></option>
                        <?php } ?>
                    </select>
                </div>
                
                <div class="form-group">
                    <label>Email Address *</label>
                    <input class="form-control required email" type="text" name="email_address" placeholder="Email Address" />
                </div>
                
                <div class="form-group">
                    <label>Contact Number *</label>
                    <input class="form-control number required" type="text" name="contact_number" placeholder="Contact Number" />
                </div>                
                
                <div class="form-group">
                        <div class="validation-sum">
                        <?php
                        // init variables
                        $min_number = 1;
                        $max_number = 15;
                        // generating random numbers
                        $random_number1 = mt_rand($min_number, $max_number);
                        $random_number2 = mt_rand($min_number, $max_number);
                        ?>
                        <input type="text" value="<?php echo $random_number1 . ' + ' . $random_number2 . ' = ';?>" disabled="disabled" />
                        <input name="captchaResult" class="required number math" type="text" size="2" placeholder="sum" />
                        <input name="firstNumber" type="hidden" value="<?php echo $random_number1; ?>" />
                        <input name="secondNumber" type="hidden" value="<?php echo $random_number2; ?>" />
                    </div>
                </div>
            </div>

            <input type="hidden" name="hidden_package_name" value="<?php echo $package_description['info']->name ?>" />
            <input type="hidden" name="hidden_package_destination_id" value="<?php echo $package_description['info']->destination_id ?>" />
            <input type="hidden" name="hidden_package_experience_id" value="<?php echo $package_description['info']->experience_id ?>" />
            <input type="hidden" name="hidden_package_id" value="<?php echo $package_description['info']->id ?>" />
            <input type="hidden" name="hidden_package_code" value="<?php echo $package_description['info']->code ?>" />

            <div class="col-sm-12 col-lg-12">
                <input class="btn-booking-submit pull-right" id="btn-send" type="submit" name="submit_button" value="Enquire" style="display:none;" />
            </div>
        </div>

    </form>
</div>
<a href="javascript:void(0)" class="close-btn"></a>