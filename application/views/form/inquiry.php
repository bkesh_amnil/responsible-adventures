<?php $fields = $this->public_model->getFormFields($formdata->id); ?>
<div style="display: none;" class="processing similar-processing"></div>
<form name="<?php echo $formdata->form_name ?>" class="feedback-form" method="POST" action="<?php echo base_url('dynamic_form/submit') ?>" enctype="multipart/form-data">
    <?php
    foreach ($fields as $field) {
        $validation_class = '';
        $field_name = 'field_name[' . $field->id . ']';
        $validataion_rules = explode('|', $field->validation_rule);
        foreach ($validataion_rules as $validation_rule) {
            $validation_class .= $validation_rule . ' ';
        }
        $value = '';
        ?>
        <div class="form-group">
            <?php
            switch ($field->field_type) {
                case 'textarea':
                    $attribute = 'class="form-control ' . $validation_class . '" name="' . $field->field_name . '" placeholder="' . $field->field_placeholder . '" rows="12"';
                    echo form_textarea($field_name, $value, $attribute);
                    break;
                default:
                    $attribute = 'class="form-control ' . $validation_class . '" type="text" name="' . $field->field_label . '" placeholder="' . $field->field_placeholder . '"';
                    echo form_input($field_name, $value, $attribute);
                    break;
            }
            ?>
        </div>
        <?php
    }
    ?>

<!--    <div class="form-group">
        <label>Captcha *</label>
        <div class="g-recaptcha" data-sitekey="6Lcq6QsUAAAAACVSs6LNApv8jSuSmy2OjV4RCPUr"></div>
    </div>-->
    <!--    <div class="form-group">
    <?php
    // init variables
    $min_number = 1;
    $max_number = 15;
    // generating random numbers
    $random_number1 = mt_rand($min_number, $max_number);
    $random_number2 = mt_rand($min_number, $max_number);
    ?>
            <input class="random-number" id="text" value="<?php echo $random_number1 . ' + ' . $random_number2 . ' = '; ?>" disabled="disabled" />
            <input name="captchaResult" class="required number math" placeholder="sum" type="text" size="2" />
            <input name="firstNumber" type="hidden" value="<?php echo $random_number1; ?>" />
            <input name="secondNumber" type="hidden" value="<?php echo $random_number2; ?>" />
        </div>-->
    
    <div class="col-sm-12 col-lg-12">
                            <div class="form-group">
                                <!--<div class="g-recaptcha" data-sitekey="6Lcq6QsUAAAAACVSs6LNApv8jSuSmy2OjV4RCPUr"></div>-->
                                <div id="recaptcha3"></div>
                            </div>
                        </div>
    <input class="btn" id="btn-send" type="submit" value="SUBMIT" style="display:none;" />
    <input type="hidden" name="form_id" value="<?php echo $formdata->id ?>" />
</form>


