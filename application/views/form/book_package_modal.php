<!-- Modal -->
<div class="modal fade modal-default" id="bookingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">BOOKING FORM</h4>
            </div>
            <div style="display: none;" class="processing similar-processing"></div>
            <form name="package_booking" method="POST" action="<?php echo base_url('dynamic_form/package_booking') ?>" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row"  id="current-booking-staus">
                        <div class="col-sm-8 col-lg-8">
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <input type="hidden" id="hidden_selected_dates" value="">
                                        <input type="text" name="departure_date" id="departure_date" class="form-control required" placeholder="Departure Date">
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control price_per_person" type="hidden" name="price_per_person" value="" />
                                        <input class="form-control price_per_person required" type="text" name="price_per_person" placeholder="Price Per Person(in $)" value="" disabled />
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control required" type="text" name="full_name" placeholder="Full Name" />
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control required" type="text" name="contact_address" placeholder="Contact Address" />
                                    </div>
                                </div>

                                <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                        <?php
                                        $range = range(1, 20);
                                        ?>
                                        <select class="form-control js-example-basic-single  required" id="nos_of_pax" name="nos_of_pax">
                                            <option value="0">Number of Pax</option>
                                            <?php foreach ($range as $val) { ?>
                                                <option value="<?php echo $val ?>"><?php echo $val ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control total_cost" type="hidden" name="total_cost" value="" />
                                        <input class="form-control total_cost required" type="text" name="total_cost" id="" placeholder="Total Cost(in $)" value="" disabled />
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control required email" type="text" name="email_address" placeholder="Email Address" />
                                    </div>

                                    <div class="form-group">
                                        <input class="form-control number required" type="text" name="contact_nos" placeholder="Contact Number" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Remarks" name="remarks"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-12">
                                    <div class="form-group">
                                        <!--<div class="g-recaptcha" data-sitekey="6Lcq6QsUAAAAACVSs6LNApv8jSuSmy2OjV4RCPUr"></div>-->
                                        <div id="recaptcha2"></div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="hidden_package_name" value="" />
                            <input type="hidden" name="hidden_package_destination_id" value="">
                            <input type="hidden" name="hidden_package_experience_id" value="" />
                            <input type="hidden" name="hidden_package_id" value="" />
                            <input type="hidden" name="hidden_package_code" value="" />

<!--<input type="hidden" id="hidden_selected_dates" value="0">-->
                        </div>
                        <div class="col-sm-4 col-lg-4">
                            <div class="booking-status-wrap">
                                <h3>BOOKING STATUS</h3>
                                <ul>
                                    <li><label>Departure date</label><span id="selected_departure_date"></span></li>
                                    <li>
                                        <label>Total Booking Available</label><span id="total_booking_available"></span>
                                    </li>
                                    <li>
                                        <label>Total Booked</label><span id="total_booked"></span></li>
                                    <li>
                                        <label>Total Available</label><span id="total_available"></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="current-booking-staus-no">
                        <p>No Booking Available as of Now.</p>
                        <p class="enquire-link text-center">But you can send in your enquiry <a href="javascript:void(0);" rel="" class="btn-enquire">here</a>.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="loader showloader"></div>
                    <input class="btn-booking-submit btn-all pull-right" id="btn-send" type="submit" name="submit_button" value="Book" />
                </div>
            </form>
        </div>
    </div>
</div>