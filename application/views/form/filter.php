<div class="container">
    <form>
        <div class="row">
            <div class="col-xs-12 col-sm-5 col-lg-5">
                <select class="form-control js-example-basic-single" id="destination">
                    <option value="0">Choose your destination</option>
                    <?php foreach($destinations as $destination) { ?>
                    <option value="<?php echo $destination->id ?>"><?php echo $destination->name ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-xs-12 col-sm-5 col-lg-5">
                <select class="form-control js-example-basic-single" id="experience">
                    <option value="0">Choose your experience</option>
                    <?php foreach($search_experiences as $experience) { ?>
                    <option value="<?php echo $experience->slug ?>"><?php echo $experience->name ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-xs-12 col-sm-2 col-lg-2">
                <input class="form-control go" type="submit" value="Search" />
            </div>
        </div>
    </form>
</div>