<?php

class My_Controller extends CI_Controller
{

    public $template = '';
    public $data = array();
    public $global_config;
    public $all_child_modules = array();
    public $all_parent_modules = array();

    public function __construct()
    {
        parent::__construct();
        $this->template = BACKENDFOLDER.'/layout/default';

        $this->load->model('configuration_model', 'configuration');
        $this->load->model('module_model', 'module');

        $this->global_config = $this->configuration->get(1);
        $current_user_role = get_userdata('role_id');
        if($current_user_role && $current_user_role != 1)
            $sql = "SELECT * FROM tbl_module m WHERE m.id IN(SELECT module_id FROM `tbl_role_module` rm WHERE rm.role_id = {$current_user_role} ORDER BY priority)";
        else
            $sql = "SELECT * FROM tbl_module ORDER BY priority";
        $current_user_modules = $this->module->query($sql);

        $this->data['active_module_id'] = 0;
        foreach($current_user_modules as $parent_module) {
            if($parent_module->parent_id == 0) {
                if(segment(2) == $parent_module->slug) {
                    $this->data['active_module_id'] = $parent_module->id;
                    $this->data['active_module_name'] = $parent_module->slug;
                }
                $allowed_module_slugs[] = $parent_module->slug;
                if(isset($parent_module->show_in_navigation)) {
                    $parent_modules[] = $parent_module;
                }
            }
            foreach($current_user_modules as $child_module) {
                if($parent_module->id == $child_module->parent_id) {
                    if(segment(2) == $child_module->slug) {
                        $this->data['active_module_id'] = $child_module->id;
                        $this->data['active_module_name'] = $child_module->slug;
                    }
                    $allowed_module_slugs[] = $child_module->slug;
                    if(isset($child_module->show_in_navigation)) {
                        $child_modules[$parent_module->id][] = $child_module;
                    }
                }
            }
        }
        // checking for social integration settings
        $isSocialIntegrationOn = $this->checkSocialIntegrationStatus($this->data['active_module_id']);
        if($isSocialIntegrationOn && segment(3) == '') {
            $moduleName = $this->data['active_module_name'];
            $this->load->model($this->data['active_module_name'].'_model', $this->data['active_module_name']);
            $this->data['socialFormAllData'] = $this->$moduleName->get();
            $this->data['socialForm'] = BACKENDFOLDER.'/socialform/_form';
        }

        $this->data['activeModulePermission'] = $this->checkModulePermission($this->data['active_module_id']);

        $this->all_parent_modules = $parent_modules;
        $this->all_child_modules = $child_modules;

        define('SITENAME', $this->global_config->site_title);
        define('SITEMAIL', $this->global_config->site_from_email);

        $this->data['meta_title'] = SITENAME;
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;

        $this->data['categories'] = $this->configuration->activeCategories();

        // Login check
        $exception_uris = array(
            'login',
            'logout',
            'retrieve-password',
            'social'
        );
        if (!in_array(segment(2), $exception_uris)) {
            if(!in_array(segment(2), $allowed_module_slugs) && segment(2) != 'dashboard') {
                set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
                redirect(BACKENDFOLDER.'/dashboard');
            }
            if (!logged_in('user_id')) {
                redirect(BACKENDFOLDER.'/login');
            }
        } else {
            if(segment(2) != 'social') {
                if (logged_in('user_id') && segment(2) != 'logout') {
                    redirect(BACKENDFOLDER.'/dashboard');
                }
            }
        }
    }

    public function render()
    {
        $this->load->view($this->template, $this->data);
    }

    public function partialRender($view)
    {
        $this->load->view($view, $this->data);
    }

    public function form($id, $module, $extra_params = NULL)
    {
        $this->$module->__construct();
        $this->data['body'] = BACKENDFOLDER.'/'.$module.'/_form';

        if($_POST) {
            $this->data[$module] = (object) $_POST;
        } elseif($id != '') {
            $this->data[$module] = $this->$module->get(1, array('id' => $id));
        } else {
            $this->data[$module] = $this->$module;
        }

        if(isset($extra_params) && !empty($extra_params)) {
            if($id == 0)
                $this->data['sub_module_name'] = ucwords($module) . ' Add for <b>' . $extra_params . '</b> (All fileds marked * are compulsory)';
            else
                $this->data['sub_module_name'] = ucwords($module) . ' Edit for <b>' . $extra_params . '</b> (All fileds marked * are compulsory)';
        } else {
            if ($id == '')
                $this->data['sub_module_name'] = ucwords($module) . ' Add (All fileds marked * are compulsory)';
            else
                $this->data['sub_module_name'] = ucwords($module) . ' Edit (All fileds marked * are compulsory)';
        }

        $this->render();
    }

    /*public function changeStatus($module)
    {
        $status = segment(4) == '0' ? '1' : '0';
        $id = segment(5);
        $data = array('status' => $status);
        $condition = array('id' => $id);

        return $this->$module->save($data, $condition);
    }*/

    public function checkModulePermission($module)
    {
        $roleId = get_userdata('role_id');
        // all access to superadmin
        if($roleId == '1') {
            $permission = [
                'add' => true,
                'edit' => true,
                'view' => true,
                'delete' => true
            ];
            return $permission;
        }

        $this->load->model('rolemodule_model', 'rolemodule');

        $permission = [
            'add' => false,
            'edit' => false,
            'view' => false,
            'delete' => false
        ];
        $permissions = $this->rolemodule->get('1', ['module_id' => $module, 'role_id' => $roleId]);

        if($permissions) {
            $permission = [
                'view' => substr($permissions->permission, 0, 1) ? true : false,
                'add' => substr($permissions->permission, 1, 1) ? true : false,
                'edit' => substr($permissions->permission, 2, 1) ? true : false,
                'delete' => substr($permissions->permission, 3, 1) ? true : false,
            ];
        }

        return $permission;
    }

    function checkSocialIntegrationStatus($module)
    {
        $moduleData = $this->module->get('1', array('id' => $module));
        $status = ($moduleData && $moduleData->social == 'On') ? true : false;

        return $status;
    }

    function sort()
    {
        $module = segment(2);
        $count = count($this->uri->segment_array());
        if($module == 'menu') {
            $menu_type_id = segment(4);
            $parent_id = 0;
            if($count > 4) {
                $parent_id = segment(5);
            }

        }
        $this->load->model($module . '_model', $module);
        $orderData = $this->input->post();

        if(!empty($orderData)) {
            foreach($orderData['id'] as $order => $id) {
                $order = $order + 1;
                $this->$module->save(array('position' => $order), array('id' => $id));
            }
        } else {
            if(isset($menu_type_id) && !empty($menu_type_id)) {
                $this->data['allDataSort'] = $this->$module->get('', array('menu_type_id' => $menu_type_id, 'menu_parent' => $parent_id), 'position ASC');
            } else {
                $this->data['allDataSort'] = $this->$module->get('', '', 'position ASC');
            }
            $this->data['modal_title'] = 'Sort ' . ucwords(str_replace('_', ' ', $module)) . ' Data';
            $this->data['modal_body'] = $this->load->view(BACKENDFOLDER . '/sort/_index', $this->data, true);
            echo $this->load->view(BACKENDFOLDER . '/layout/bootstrap_modal', $this->data, true);
        }
    }

}