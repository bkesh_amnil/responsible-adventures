<?php

class BASE_Controller extends CI_Controller {

    protected $template = array();
    protected $base_template = "layout/layout";
    protected $current_user = array();

    function __construct() {
        parent::__construct();
        //track the IP if unique store in the database.
        $newip = $this->input->ip_address();
        //check if the IP is unique
        $this->db->select('id')
                ->from('tbl_ip_track')
                ->where('ip_address',$newip)
                ->limit(1);
        $query = $this->db->get();
        if($query->num_rows() < 1)
        {
            $this->db->insert('tbl_ip_track',array('ip_address'=>$newip));
        }

        $item_per_page = 2;
        $url = $this->uri->segment_array();
        $count = count($url);

        if($count > 1) {
            /*$alias_count = $count - 2;
            $table_count = $count - 1;
            $alias = $this->uri->segment($count - $alias_count);
            $table = $this->uri->segment($count - $table_count);*/

            $alias = $this->uri->segment($count);
            $table = $this->uri->segment(1);
            switch($table) {
                case "destination":
                    switch($count) {
                        case "3":
                            $table = 'experience';
                            break;
                        case "4":
                            $table = 'area';
                            break;
                        case "5":
                            $table = 'package';
                            break;
                        case "6":
                            $table = 'package';
                            $alias = $this->uri->segment($count - 1);
                            break;
                        default:
                            break;
                    }
                    break;
                case "news":
                    switch($count) {
                        case "3":
                            $alias = $this->uri->segment(1);
                            $table = 'menu';
                            break;
                    }
            }
            $table_count = $count - 1;
            $table_for_title = $this->uri->segment($count - $table_count);
        } else {
            $alias = $this->uri->segment($count);
            $table = $this->uri->segment($count - 1);
        }

        $row = $this->db->order_by('id', 'ASC')->get('tbl_site')->row();
        
        if($count > 1) {
            $meta_contents = $this->getMetaKeywordsDescription($alias, $table);
            if($table == 'news') {
                $page_title = ucwords(str_replace('-', ' ', $alias));
            } else {
                $page_title = $this->getMetaKeywordsDescription($table_for_title, 'menu');
            }
        } else {
            $meta_contents = $this->getMetaKeywordsDescription($alias, 'menu');
        }

        $this->template['item_per_page'] = $item_per_page;
        $this->template['site_name'] = $row->site_name;
        $this->template['site_title'] = $row->site_title;
        $this->template['site_logo'] = $row->site_logo;
        $this->template['site_keywords'] = $row->site_keyword;
        $this->template['site_description'] = $row->site_description;
        $this->template['site_author'] = $row->site_author;

        $this->template['main_menus'] = $this->public_model->getMenu(2);
        $this->template['bottom_menus'] = $this->public_model->getMenu(3);
        $this->template['footer_logos'] = $this->public_model->getLogos();

        $this->template['facebook'] = $row->facebook;
        $this->template['twitter'] = $row->twitter;
        $this->template['gplus'] = $row->gplus;
        $this->template['skype'] = $row->skype;
        $this->template['youtube'] = $row->youtube;
        $this->template['instagram'] = $row->instagram;
        $this->template['pinterest'] = $row->pininterest;
        $this->template['linkedin'] = $row->linkedin;
        $this->template['vimeo'] = $row->vimeo;

        $this->template['analytics'] = $row->google_analytics_code;

        if(isset($meta_contents) && !empty($meta_contents)) {
            $this->template['meta_keywords'] = $meta_contents->meta_keywords;
            $this->template['meta_description'] = $meta_contents->meta_description;
            if(isset($meta_contents->page_title)) {
                $this->template['page_title'] = $meta_contents->page_title;
                $this->template['menu_heading'] = $meta_contents->menu_heading;
            }
        }

        if(isset($page_title) && !empty($page_title)) {
            if($count > 1) {
                //$this->template['page_title'] = $page_title->page_title . ' | ' . ucwords(str_replace('-', ' ', $alias));
                if($count > 2) {
                    /*$loops = '';
                    $loop = $count - 2;
                    for ($i = $loop-1; $i >= 0; $i--) {
                        $this->template['page_title'] .= ' | ' . ucwords(str_replace('-', ' ', $this->uri->segment($count - $i)));
                    }*/
                    if($this->uri->segment($count) != 'book' &&  $this->uri->segment($count) != 'enquire') {
                        $this->template['page_title'] = ucwords(str_replace('-', ' ', $this->uri->segment($count)));
                    } else {
                        $this->template['page_title'] = ucwords(str_replace('-', ' ', $this->uri->segment($count - 1)));
                    }
                } else {
                    $this->template['page_title'] = ucwords(str_replace('-', ' ', $this->uri->segment($count)));
                }

            } else {
echo $alias;
                //$this->template['page_title'] = $page_title->page_title . ' | ' . ucwords(str_replace('-', ' ', $alias));
                $this->template['page_title'] = ucwords(str_replace('-', ' ', $alias));
            }
            if($table != 'news') {
                $this->template['menu_heading'] = $page_title->menu_heading;
            }
        }
        // Assign current_user to the instance so controllers can use it
        /*if ($this->check_login()) {
            $this->template['current_user'] = $this->session->all_userdata();
            $this->current_user = $this->session->all_userdata();
        } else {
            $this->template['current_user'] = null;
            $this->current_user = null;
        }*/

        //$this->template['countries'] = $this->public_model->getCountries();
        $this->template['featured_packages'] = $this->public_model->getFeaturedPackages('', '', 'list');
        $this->template['featured_packages_search'] = $this->public_model->getFeaturedPackages('', '', 'search');
        $this->template['testimonials'] = $this->public_model->getTestimonials();
        $this->template['footer_static_content'] = $this->public_model->getContent('2');
        $this->template['footer_content'] = $this->public_model->getContent('4');
        $this->template['footer_contact'] = $this->public_model->getContent('5');

    }

    /*public function check_login() {
        return ($this->session->userdata('logged_in'));
    }*/

    public function _output($output) {
        echo $this->load->view($this->base_template, $this->template, TRUE);
    }

    public function getMetaKeywordsDescription($alias, $table) {
        if($table != 'search' && $table != 'booking' && $table != 'about-us' && $table != 'faq' && $table != 'team' && $table != 'getPackageBookingDate' && $table != 'testimonial') {
            if($table == 'photo') {
                $table = 'gallery';
            }
            if($this->db->table_exists('tbl_' . $table)) {
                if($table == 'menu') {
                    $this->db->select('page_title, menu_heading, meta_keywords, meta_description');
                } else {
                    $this->db->select('tbl_' . $table .'.meta_keywords, tbl_' . $table . '.meta_description');
                }
                if($table == 'experience') {
                    $this->db->join('tbl_destination', 'tbl_destination.id = tbl_experience.destination_id');
                    $this->db->where('tbl_destination.name', $this->uri->segment(2));
                }
                if (!empty($alias)) {
                    $this->db->where('tbl_' . $table . '.slug', $alias);
                } else {
                    $this->db->where('slug', 'home');
                }
                $row = $this->db->get('tbl_' . $table)->row();

                return (isset($row) && !empty($row)) ? $row : '';
            } else {
                show_404();
            }
        }
    }

}