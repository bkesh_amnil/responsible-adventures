<?php

class Smtp_Model extends My_Model
{

    protected $table = 'tbl_smtp_settings';

    public $id = '',
        $smtp_server ='',
        $smtp_port ='',
        $smtp_email ='',
        $admin_email ='',
        $ssl_enabled ='',
        $is_authenticated = '',
        $smtp_email_password = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'smtp_server',
                'label' => 'SMTP Server',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'smtp_port',
                'label' => 'SMTP Port',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'smtp_email',
                'label' => 'SMTP Email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'admin_email',
                'label' => 'Admin Email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'ssl_enabled',
                'label' => 'Is SSL Enabled',
                'rules' => 'required'
            ),
            array(
                'field' => 'is_authenticated',
                'label' => 'Is Authenticated',
                'rules' => 'required',
            ),
            array(
                'field' => 'smtp_email_password',
                'label' => 'SMTP Email Password',
                'rules' => 'trim|required'
            )
        );

        return $array;
    }
}