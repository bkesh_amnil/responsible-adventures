<?php

class Departure_Type_Model extends My_Model
{

    protected $table = 'tbl_departure_type';

    public $id = '',
        $name = '',
        $code = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique[' . $this->table . '.name.' . $id . ']',
            ),
            array(
                'field' => 'code',
                'label' => 'Code',
                'rules' => 'trim|required|unique[' . $this->table . '.code.' . $id . ']',
            )
        );

        return $array;
    }

    public function getPackageDepartures() {
        $this->db->select('name, code');
        $this->db->where('status', 1);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
}