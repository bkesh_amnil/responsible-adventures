<?php

class News_Category_Model extends My_Model
{

    protected $table = 'tbl_news_category';

    public $id = '',
        $name = '',
        $slug = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique[' . $this->table . '.name.' . $id . ']',
            ),
        );

        return $array;
    }

    function get_all_categories(){
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}