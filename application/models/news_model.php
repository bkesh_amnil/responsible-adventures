<?php
	class News_model extends My_Model {

		protected $table = 'tbl_news';

		public $id = '',
				$name = '',
				$slug = '',
				$image = '',
				$short_description = '',
				$description= '',
				$publish_date = '',
				$unpublish_date= '',
				$posted_by = '',
				$is_highlighted = '',
				$news_category_id = '',
                $tags = '',
				$meta_keywords = '',
				$meta_description = '',
				$status = '';

		public function __construct()
		{
			parent::__construct();
			$this->created_timestamp = true;
			$this->updated_timestamp = true;
			$this->created_by = true;
			$this->updated_by = true;
		}

		public function rules($id)
		{
			$array = array(
                array(
                    'field' => 'name',
                    'label' => 'Ttile',
                    'rules' => 'trim|required|xss_clean|unique[tbl_news.name.'.$id.']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|xss_clean|unique[tbl_news.slug.'.$id.']',
                ),
                array(
                    'field' => 'image',
                    'label' => 'Image',
                    'rules' => 'valid_size['.$this->table.'.image]',
                ),
                array(
                    'field' => 'publish_date',
                    'label' => 'News Publish Date',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'description',
                    'label' => 'News Description',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'posted_by',
                    'label' => 'Posted By',
                    'rules' => 'required',
                ),
                array(
                    'field' => 'news_category_id[]',
                    'label' => 'News Category',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'tags',
                    'label' => 'News Tags',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required',
                )
			);

			return $array;
		}

               public function getAllNews() {
                    $this->db->order_by('id', 'DESC');
                    $result = $this->db->get($this->table)->result();
                    
                    return (isset($result) && !empty($result)) ? $result : array();
               }
	}
?>