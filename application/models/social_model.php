<?php

class Social_Model extends My_Model
{

    protected $table = 'tbl_social_data';

    public $id = '',
            $title = '',
            $description = '',
            $image = '',
            $link = '',
            $module_id = '',
            $data_id = '',
            $social_site = '';

    public $rules = '';

    public function __construct()
    {
        parent::__construct();
    }

}