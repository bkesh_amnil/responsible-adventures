<?php

class Difficulty_Model extends My_Model
{

    protected $table = 'tbl_difficulty';

    public $id = '',
        $name = '',
        $code = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique[' . $this->table . '.name.' . $id . ']',
            ),
            array(
                'field' => 'code',
                'label' => 'Code',
                'rules' => 'trim|required|unique[' . $this->table . '.code.' . $id . ']',
            )
        );

        return $array;
    }

    public function getPackageDifficulty() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
}