<?php

class Package_download_Model extends My_Model
{

    protected $table = 'tbl_package_download_detail';
    var $package = 'tbl_package';

    public $id = '',
        $package_id = '',
        $full_name = '',
        $email = '',
        $date = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = false;
        $this->updated_timestamp = false;
        $this->created_by = false;
        $this->updated_by = false;
    }

    public function getPackageDownloads() {
        $this->db->select($this->table . '.*, ' . $this->package . '.name as package_name');
        $this->db->join($this->package, $this->package . '.id = ' . $this->table . '.package_id');
        $this->db->order_by($this->table . '.id', 'DESC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    
    public function getExportData($field_type, $field_value) {
        $query = "SELECT
                      *
                    FROM
                      (SELECT
                        `tbl_package`.`id` AS package_id,
                        `tbl_package`.`name` AS package_name,
                        `tbl_package_download_detail`.`id` AS package_download_id,
                        `full_name`,
                        `email`,
                        DATE_FORMAT(FROM_UNIXTIME(`tbl_package_download_detail`.`date`),'%Y-%m-%d') AS download_date
                      FROM
                        (`tbl_package_download_detail`)
                        JOIN `tbl_package`
                          ON `tbl_package`.`id` = `tbl_package_download_detail`.`package_id`
                      ORDER BY tbl_package_download_detail.id DESC) AS t";

        if($field_type == 'package') {
            $query .= " WHERE t.`package_id` = " . $field_value;
        } else if($field_type == 'download_date' && !empty($field_value)) {
            $query .= " WHERE t.`download_date` = '".$field_value."'";
        }

        $query .= " ORDER BY t.package_download_id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}