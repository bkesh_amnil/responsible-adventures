<?php

class Public_model extends CI_Model {
    
    var $site_menus = 'tbl_site_menus';
    var $profile = 'tbl_profile';
    var $profile_detail = 'tbl_profile_detail';
    var $module = 'tbl_module';
    var $site = 'tbl_site';
    var $categories = 'tbl_category';
    var $menu = 'tbl_menu';
    var $banner = 'tbl_banner';
    var $content = 'tbl_content';
    var $menu_page_settings = 'tbl_menu_page_settings';
    var $popup_image = 'tbl_popup_image';
    var $social_media = 'tbl_social_media';
    var $form = 'tbl_form';
    var $form_field = 'tbl_form_field';
    var $form_submission = 'tbl_form_submission';
    var $form_submission_fields = 'tbl_form_submission_fields';
    var $package = 'tbl_package';
    var $package_gallery = 'tbl_package_gallery';
    var $package_inclusion_exclusion = 'tbl_package_inclusion_exclusion';
    var $package_map = 'tbl_package_map';
    var $package_price = 'tbl_package_price';
    var $departure_type = 'tbl_departure_type';
    var $package_departure_detail = 'tbl_package_departure_detail';
    var $difficulty_manager = 'tbl_difficulty';
    var $news = 'tbl_news';
    var $event = 'tbl_event';
    var $testimonial = 'tbl_testimonial';
    var $countries = 'tbl_countries';
    var $video = 'tbl_video';
    var $faq = 'tbl_faq';
    var $gallery = 'tbl_gallery';
    var $gallery_media = 'tbl_gallery_media';
    var $social_data = 'tbl_social_data';
    var $destination = 'tbl_destination';
    var $destination_media = 'tbl_destination_media';
    var $activity = 'tbl_activity';
    var $experience = 'tbl_experience';
    var $area = 'tbl_area';
    var $team = 'tbl_team';
    var $news_category = 'tbl_news_category';
    var $package_download_detail = 'tbl_package_download_detail';
    var $logos = 'tbl_logos';

    function getMenu($id, $parent = 0) {
        $this->db->where($this->site_menus . '.id', $id);
        $this->db->where($this->menu . '.menu_parent', $parent);
        $this->db->where($this->menu . '.status', 1);
        $this->db->order_by($this->menu . '.position', 'ASC');
        $this->db->select($this->menu .' .id, ' . $this->menu . '.name, slug');
        $this->db->from($this->menu);
        $this->db->join($this->site_menus, $this->site_menus . '.id = ' .$this->menu. '.menu_type_id');
        $result = $this->db->get()->result();
        $list = "";

        if (!empty($result) && is_array($result)) {
          foreach ($result as $ind => $menu) {
            $menuListArr = "";
            foreach ($menu as $mind => $mval) {
              $menuListArr[$mind] = $mval;
            }
            $menuListArr['childs'] = $this->getMenu($id, $menu->id);
            $list[] = $menuListArr;
          }
        }
        if (empty($list)) {
          return array();
        }
        return $list;
    }

    function getActiveMenu($slug) {
        $this->db->select('menu_parent, slug');
        $this->db->where('slug', $slug);
        $row = $this->db->get($this->menu)->row();

        if(isset($row) && !empty($row)) {
            if(isset($row) && $row->menu_parent != '0') {
                $this->db->select('slug');
                $this->db->where('id', $row->menu_parent);
                $row1 = $this->db->get($this->menu)->row();

                return $row1->slug;
            } else {
                return $row->slug;
            }
        }

        return '';
    }

    function getBanner() {
        $this->db->select('name, description, image, link_type, link_id, link_url, opens');
        $this->db->where('status', '1');
        $this->db->order_by('position');
        $result = $this->db->get($this->banner)->result();

        $list = "";
        if(isset($result) && !empty($result) && is_array($result)) {
            foreach($result as $row) {
                $data['info'] = $row;
                switch($row->link_type) {
                    case "menu":
                        $data['url'] = $this->Alias($row->link_id, 'menu');
                        break;
                    case "destination":
                        $data['url'] = $this->Alias($row->link_id, 'destination');
                        break;
                    case "experience":
                        $data['url'] = $this->Alias($row->link_id, 'experience');
                        break;
                    case "area":
                        $data['url'] = $this->Alias($row->link_id, 'area');
                        break;
                    case "package":
                        $data['url'] = $this->Alias($row->link_id, 'package');
                        break;
                    case "url":
                        $data['url'] = $row->external_url;
                        break;
                    default:
                        $data['url'] = '';
                        break;
                }
                $list[] = $data;
            }
            return $list;
        }

        return array();
    }

    function Alias($id, $table){
        if($table == 'menu' || $table == 'destination') {
            $this->db->select('slug');
            $this->db->where('id', $id);
            $row = $this->db->get($this->$table)->row();
        } else if($table == 'experience'){
            $this->db->select($this->experience . '.slug, ' . $this->destination . '.slug as destination_slug');
            $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
            $this->db->where($this->$table . '.id', $id);
            $row = $this->db->get($this->$table)->row();
        } else {
            $this->db->select($this->package . '.slug as package_slug, ' . $this->area . '.slug as area_slug, ' . $this->experience . '.slug as experience_slug, ' . $this->destination . '.slug as destination_slug');
            $this->db->join($this->destination, $this->destination . '.id = ' . $this->package . '.destination_id');
            $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
            $this->db->join($this->area, $this->area . '.id = ' . $this->package . '.area_id');
            $this->db->where($this->$table . '.id', $id);
            $row = $this->db->get($this->$table)->row();
        }

        if(isset($row) && !empty($row)) {
            switch ($table) {
                case "menu":
                case "destination":
                    $url = site_url($row->slug);
                    break;
                case "experience":
                    $url = site_url($row->destination_slug . '/' . $row->slug);
                    break;
                case "area":
                    $url = site_url($row->destination_slug . '/' . $row->experience_slug . '/' . $row->area_slug);
                    break;
                case "package":
                    $url = site_url('package/' . $row->package_slug);
                    break;
                default:
                    break;
            }
        }

        return (isset($row) && !empty($row)) ? $url : '';
    }

    function getBreadCrumb($url) {
        $count = count($url);
        $alias = $this->uri->segment($count);
        $html = '';

        if($count > 1) {
            $type = $this->uri->segment(1);

            if($count > 2) {
                if($count > 3){
                    $html .= '<li>' . ucfirst($type) . '</li>';
                    $cur_url = current_url();
                    $parts = explode('/', $cur_url);

                    for($i = 2; $i<=$count; $i++) {
                        $url = site_url() . $parts[3];
                        for($j = 1; $j < $i; $j++) {
                            if($i == 2) {
                                $url1 = site_url() . $parts[4];
                            } else {
                                $url .= '/' . $parts[$j + 3];
                            }
                        }
                        $name = ucfirst($this->uri->segment($i));
                        if (strpos($this->uri->segment($i),'-') !== false) {
                            $name = ucfirst(str_replace('-', ' ', $this->uri->segment($i)));
                        }
                        if($i == 2) {
                            $html .= '<li><a href="' . $url1 . '">' . $name . '</a></li>';
                        } else {
                            $html .= '<li><a href="' . $url . '">' . $name . '</a></li>';
                        }
                    }

                } else {
                    $data = $this->getNameforBreadCrumbByAlias($this->uri->segment($count - 1), $type, $count);
                    if($type == 'news') {
                        $html .= '<li><a href="'.site_url($type).'">' . ucfirst($type) . '</a></li>';
                        $html .= '<li>' . $this->uri->segment($count - 1) . '</li>';
                    } else if($type == 'destination') {
                        $html .= '<li>' . ucfirst($type) . '</li>';
                        $html .= '<li><a href="' . site_url($this->uri->segment($count - 1)) . '">' . $data . '</a></li>';
                    } else {
                        $html .= '<li><a href="' . site_url($this->uri->segment($count - 2)) . '">' . ucfirst($type) . '</a></li>';
                        $html .= '<li><a href="' . site_url($this->uri->segment($count - 2) . '/' . $this->uri->segment($count - 1)) . '">' . $data . '</a></li>';
                    }
                    $html .= '<li><a class="active">'.ucfirst($this->uri->segment($count)).'</a> </li>';
                }

            } else {
                if ($type != 'team' && $type != 'about-us') {
                    $data = $this->getNameforBreadCrumbByAlias($alias, $type, $count);
                    $html .= '<li><a href="' . site_url($type) . '">' . ucfirst($type) . '</a></li>';
                    $html .= '<li><a class="active">' . $data . '</a></li>';
                } else {
                    $menu_name = $this->getNameforBreadCrumb($this->uri->segment(1), 'content');
                    if (isset($menu_name) && !empty($menu_name)) {
                        $html .= $menu_name;
                    } else {
                        $html .= '<li><a class="active" href="javascript:void(0);">' . str_replace('-', ' ', ucwords($this->uri->segment(1))) . '</a></li>';
                    }
                }
            }
        } else {
            $url = site_url($this->uri->segment(1));
            $menu_name = $this->getNameforBreadCrumb($this->uri->segment(1), 'content');
            if(isset($menu_name) && !empty($menu_name)) {
                $html .= $menu_name;
            } else {
                $html .= '<li><a class="active" href="javascript:void(0);">'.str_replace('-', ' ', ucwords($this->uri->segment(1))).'</a></li>';
            }
        }

        return $html;
    }

    function getNameforBreadCrumb($alias, $type) {
        if($type == 'content') {
            $this->db->select('menu_parent, name');
            $this->db->where('slug', $alias);
            $row = $this->db->get($this->menu)->row();

            if(isset($row) && !empty($row)) {
                if($row->menu_parent != '0') {
                    $this->db->select('name');
                    $this->db->where('id', $row->menu_parent);
                    $row1 = $this->db->get($this->menu)->row();

                    return '<li>'.ucwords($row1->name).'</li><li class="active"><a href="'.base_url($alias).'">'.ucwords($row->name).'</a></li>';
                } else {
                    return '<li><a  class="active" href="'.base_url($alias).'">'.ucwords($row->name).'</a></li>';
                }
            }
        }

        return NULL;
    }

    function getNameforBreadCrumbByAlias($alias, $type, $count) {
        if($count > 3) {

        } if($count == '3') {
            $this->db->select('name');
            $this->db->where('slug', $alias);
            if($type == 'destination') {
                $row = $this->db->get($this->destination)->row();
            } else {
                $row = $this->db->get($this->experience)->row();
            }
        } else {
            if($type != 'testimonial') {
                $type = ($type == 'photo') ? 'gallery' : $type;
                $this->db->select('name');
            } else {
                $this->db->select('customer_full_name as name');
            }
            $this->db->where('slug', $alias);
            $row = $this->db->get($this->$type)->row();
        }

        return (isset($row) && !empty($row)) ? $row->name : '';

        /*if (isset($row) && !empty($row)) {
            $data['name'] = $row->name;
            return $data;
        }*/
    }
    /* cms */
    function getModuleId($slug) {
        $slug = ($slug == 'photo') ? 'gallery' : $slug;
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $row = $this->db->get($this->module)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getDataId($slug, $table) {
        $table = ($table == 'photo') ? 'gallery' : $table;
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $row = $this->db->get('tbl_' . $table)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getMenuId($slug) {
        $this->db->select('id');
        $this->db->where('slug', $slug);
        $this->db->where('status', 1);
        $row = $this->db->get($this->menu)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getModules($menuId, $contentAlias = false, $paginate = false, $start = 0, $per_page = 10) {
        $this->db->select($this->menu_page_settings . '.module_id, ' . $this->menu_page_settings . '.content_ids, ' . $this->menu_page_settings . '.module_title, category_id, ' . $this->module . '.slug as module_name');
        $this->db->where($this->menu_page_settings . '.menu_id', $menuId);
        $this->db->order_by("position", "asc");
        $this->db->from($this->menu_page_settings);
        $this->db->join($this->module, $this->module . '.id = ' . $this->menu_page_settings . '.module_id');
        $result = $this->db->get()->result();

        $modules = array();
        if (!empty($result) && is_array($result)) {
            foreach ($result as $ind => $data) {
                $tblName = $this->getTableName($data->module_name);
                if (!empty($tblName)) {
                    $modules[$ind] = array("module_id" => $data->module_id, "module" => $data->module_name, "module_title" => $data->module_title, "data" => $this->getData($tblName, $data->content_ids, $data->category_id, $paginate, $start, $per_page));
                }
            }
        }

        return $modules;
    }

    function getTableName($moduleName = "") {
        switch ($moduleName) {
            case "menu":
                $tblName = "tbl_menu";
                $fld = "name";
                break;
            case "content":
                $tblName = "tbl_content";
                break;
            case "dynamic_form":
                $tblName = "tbl_form";
                break;
            case "form_fields":
                $tblName = "tbl_form";
                break;
            /*case "experience":
                $tblName = "tbl_experience";
                break;*/
            case 'package':
                $tblName = 'tbl_package';
                break;
            case 'gallery';
                $tblName = 'tbl_gallery';
                break;
            case 'video':
                $tblName = 'tbl_video';
                break;
            case 'team':
                $tblName = 'tbl_team';
                break;
            case 'testimonial':
                $tblName = 'tbl_testimonial';
                break;
            case 'news':
                $tblName = 'tbl_news';
                break;
            default:
                $tblName = "";
        }
        return $tblName;
    }

    function getData($tblName, $ids, $category_id, $paginate = false, $start = 0, $per_page = 10) {
        if($tblName == 'tbl_package') {
            $data = $this->getPackages();
        } elseif($tblName == 'tbl_menu') {
            $data['main_menus'] = $this->public_model->getMenu(2);
            //$data['footer_menus'] = $this->public_model->getMenu(3);
        } else {
            if ($category_id == '1' || ($category_id == '0' && $tblName == 'tbl_content')) {
                $ids = explode(',', $ids);
                $this->db->where_in($tblName . ".id", $ids);
            }
            if ($paginate)
                $this->db->limit($per_page, $start);

            if ($tblName == 'tbl_content') {
                $this->db->select($tblName . '.*');
                $this->db->join($this->categories, $this->categories . '.id = ' . $tblName . ' .category_id');
                if ($category_id != '1' && $category_id != '0') {
                    $this->db->where($tblName . '.category_id', $category_id);
                }

            }

            if ($tblName != 'tbl_form' && $tblName != 'tbl_news') {
                $this->db->where($tblName . '.status', 1);
                $this->db->order_by($tblName . '.position');
            }
            
            if($tblName == 'tbl_news') {
                $this->db->order_by('id', 'DESC');
                $this->db->order_by('position', 'ASC');
            }

            $data = $this->db->get($tblName)->result();
        }

        return $data;
    }

    function getSocialData($module_id, $data_id) {
        $this->db->select('title, description, image, link, social_site');
        $this->db->where('module_id', $module_id);
        $this->db->where('data_id', $data_id);
        $result = $this->db->get($this->social_data)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $array[$val->social_site] = $val;
            }
        }

        return (isset($array) && !empty($array)) ? $array : array();
    }

    function getGalleryImages($alias) {
        $this->db->select('media, title, caption');
        $this->db->join($this->gallery_media, $this->gallery_media . '.gallery_id = ' . $this->gallery . '.id');
        $this->db->where('type', 'Image');
        $this->db->where($this->gallery . '.slug', $alias);
        $result = $this->db->get($this->gallery)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getContent($category_id) {
        $this->db->select('name, long_description');
        $this->db->where('category_id', $category_id);
        $this->db->where('status', 1);
        $result = $this->db->get($this->content)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getLogos() {
        $this->db->select('name, image, url');
        $this->db->where('status', 1);
        $this->db->order_by('position');
        $result = $this->db->get($this->logos)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* cms */
    function getPopupImage() {
        $cur_date = strtotime(date('Y-m-d'));
        $query = "SELECT popup_image FROM tbl_popup_image WHERE " . $cur_date . " BETWEEN publish_date AND unpublish_date";
        $row = $this->db->query($query)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }
   /* destination, activity, experiences */
    //function getDestinationwithActivity() {
    function getDestinations() {
        $this->db->select('id, name, slug, sub_title, image');
        $this->db->where('status', 1);
        $this->db->order_by('position');
        $result = $this->db->get($this->destination)->result();

        return (isset($result) && !empty($result)) ? $result : array();

        /*if(isset($result) && !empty($result)) {
            foreach($result as $row) {
                $data[$row->name]['info'] = $row;
                //$data['destination'][] = $row;
                $data[$row->name]['activity'] = $this->getActivity($row->id);
            }
        }

        return (isset($data) && !empty($data)) ? $data : array();*/
    }

    //function getActivity($destination_id) {
    function getActivities() {
        $this->db->select('destination_id, name, image, description, link_type, link, opens');
        $this->db->where('status', 1);
        $this->db->where('show_in_front', 1);
        //$this->db->where('destination_id', $destination_id);
        //$this->db->order_by('destination_id');
        $this->db->order_by('position');
        $result = $this->db->get($this->activity)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $row) {
                $data[] = array('info' => $row, 'link' => $this->getActivityLink($row->link_type, $row->link));
            }
        }

        return (isset($data) && !empty($data)) ? $data : array();
    }

    function getActivityLink($link_type, $link) {
        if($link_type != 'url' && $link_type != 'none') {
            $this->db->select($this->$link_type . '.slug as experience_slug, ' . $this->destination . '.slug as destination_slug');
            $this->db->join($this->destination, $this->destination . '.id = ' . $this->$link_type . '.destination_id');
            $this->db->where($this->$link_type . '.id', $link);
            $row = $this->db->get($this->$link_type)->row();

//            return (isset($row) && !empty($row)) ? site_url('destination/' . $row->destination_slug. '/'. $row->experience_slug) : ''; //#bkesh commented
            return (isset($row) && !empty($row)) ? site_url($row->destination_slug. '/'. $row->experience_slug) : '';
        } else {
            return $link;
        }
    }

    function getDetinationDetail($menu_id) {
        $this->db->select($this->destination . '.name, ' . $this->destination . '.slug, long_description');
        $this->db->join($this->menu, $this->menu . '.destination_id = ' . $this->destination . '.id');
        $this->db->where($this->menu . '.id', $menu_id);
        $result = $this->db->get($this->destination)->row();

        return (isset($result) && !empty($result)) ? $result : '';
    }

    function getDestinationSliders($menu_id) {
        $this->db->select('media, title, caption, ' . $this->destination . '.slug');
        $this->db->join($this->menu, $this->menu . '.destination_id = ' . $this->destination_media . '.destination_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->destination_media . '.destination_id');
        $this->db->where($this->menu . '.id', $menu_id);
        $result = $this->db->get($this->destination_media)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getDestinationRelatedExperiences($menu_id) {
        $this->db->select($this->experience . '.name, ' . $this->experience . '.slug, sub_title, image, icon, description');
        $this->db->join($this->menu, $this->menu . '.destination_id = ' . $this->experience . '.destination_id');
        $this->db->where($this->menu . '.id', $menu_id);
        $this->db->where($this->experience . '.status', 1);
        $result = $this->db->get($this->experience)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getExperiences($destination_id = NULL) {
        if(!empty($destination_id) && $destination_id != 0) {
            $this->db->select('id, name, slug');
            $this->db->where('destination_id', $destination_id);
        } else {
            $this->db->select('DISTINCT(name), slug');
        }

        $this->db->where('status', 1);
        $result = $this->db->get($this->experience)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* destination, activity, experiences */
    /* packages */
    function getFeaturedPackages($destination_id, $experience_id, $type) {
        $this->db->select($this->package . '.id, '. $this->package . '.name, ' . $this->package . '.slug, cover_image, duration, minimum_group_size, short_description, ' . $this->difficulty_manager . '.name as difficulty_name, ' . $this->experience . ' .slug as experience_slug, ' . $this->destination . '.slug as destination_slug, ' . $this->area . '.slug as area_slug, ' . $this->package_price . '.unit_price');
        $this->db->join($this->difficulty_manager, $this->difficulty_manager . '.id = ' . $this->package . '.difficulty_id');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
        $this->db->join($this->area, $this->area . '.id = ' . $this->package . '.area_id');
        $this->db->join($this->package_price, $this->package_price . '.package_id = ' . $this->package . '.id AND min_pax = (SELECT MIN(min_pax) FROM tbl_package_price)', 'LEFT');
        $this->db->where($this->package . '.status', 1);
        $this->db->where($this->experience . '.status', 1);
        $this->db->where($this->destination . '.status', 1);
        $this->db->where($this->area . '.status', 1);
        $this->db->where('is_highlighted', 1);
        $this->db->order_by($this->package . '.position');

        if(!empty($destination_id) && $destination_id != '0') {
            $this->db->where($this->package . '.destination_id', $destination_id);
        }

        if(!empty($experience_id) && $experience_id != '0') {
            if(is_numeric($experience_id)) {
                $this->db->where($this->package . '.experience_id', $experience_id);
            } else {
                $this->db->where($this->experience . '.slug', $experience_id);
            }
        }

        if($type != 'search') {
            $this->db->limit(2);
        }

        $result = $this->db->get($this->package)->result();
//echo $this->db->last_query();
        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getFeaturedPackagesAll($destination_id, $experience_id) {
            $this->db->select($this->package . '.id, '. $this->package . '.name, ' . $this->package . '.slug, cover_image, duration, minimum_group_size, short_description, ' . $this->difficulty_manager . '.name as difficulty_name, ' . $this->experience . ' .slug as experience_slug, ' . $this->destination . '.slug as destination_slug, ' . $this->area . '.slug as area_slug, ' . $this->package_price . '.unit_price');
            $this->db->join($this->difficulty_manager, $this->difficulty_manager . '.id = ' . $this->package . '.difficulty_id');
            $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
            $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
            $this->db->join($this->area, $this->area . '.id = ' . $this->package . '.area_id');
            $this->db->join($this->package_price, $this->package_price . '.package_id = ' . $this->package . '.id AND min_pax = (SELECT MIN(min_pax) FROM tbl_package_price)', 'LEFT');
            $this->db->where($this->package . '.status', 1);
            $this->db->where($this->experience . '.status', 1);
            $this->db->where($this->destination . '.status', 1);
            $this->db->where($this->area . '.status', 1);
            $this->db->where('is_highlighted', 1);
            $this->db->order_by($this->package . '.position');

            if(!empty($destination_id) && $destination_id != '0') {
                $this->db->where($this->destination . '.id', $destination_id);
            }

            if(!empty($experience_id) && $experience_id != '0') {
                if(is_numeric($experience_id)) {
                    $this->db->where($this->package . '.experience_id', $experience_id);
                } else {
                    $this->db->where($this->experience . '.slug', $experience_id);
                }
            }

            $this->db->limit(2);

            $result = $this->db->get($this->package)->result();

            return (isset($result) && !empty($result)) ? $result : array();
    }

function getPackageData($package_slug){
        $query = $this->db->select('*,tbl_package.slug as package_slug,tbl_destination.slug as destination_slug,tbl_area.slug as area_slug,tbl_experience.slug as experience_slug,tbl_package.name as package_name,tbl_destination.name as destination_name,tbl_area.name as area_name,tbl_experience.name as experience_name')
                ->from($this->package)
                ->where($this->package.'.slug',$package_slug)
                ->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id')
                ->join($this->destination, $this->destination . '.id = ' . $this->package . '.destination_id')
                ->join($this->area, $this->area . '.id = ' . $this->package . '.area_id')
                ->get();
        if($query->num_rows()){
            $return = $query->row();
        }
        else{
            $return = '';
        }
        return $return;
    }


    function getPackages($destination_slug = '', $experience_slug = '', $area_slug = '') {
        $current_date = date('Y-m-d');
        $this->db->select($this->package . '.id, '. $this->package . '.name, ' . $this->package . '.slug, ' . $this->package . '.code, cover_image, duration, minimum_group_size, short_description, ' . $this->difficulty_manager . '.name as difficulty_name, ' . $this->destination . '.slug as destination_slug, ' . $this->experience . '.slug as experience_slug, ' . $this->area . '.slug as area_slug, '. $this->package_price . '.unit_price');
        $this->db->join($this->difficulty_manager, $this->package . '.difficulty_id = ' . $this->difficulty_manager . '.id');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
        $this->db->join($this->area, $this->area . '.id = ' . $this->package . '.area_id');
        $this->db->join($this->package_price, $this->package_price . '.package_id = ' . $this->package . '.id AND min_pax = (SELECT MIN(min_pax) FROM tbl_package_price) AND valid_from >= ' . $current_date . ' AND valid_to <=' . $current_date, 'LEFT');
        $this->db->where($this->difficulty_manager . '.status', 1);
        $this->db->where($this->package . '.status', 1);

        if(!empty($destination_slug) && !empty($experience_slug) && !empty($area_slug)) {
            $this->db->where($this->destination . '.slug', $destination_slug);
            $this->db->where($this->experience . '.slug', $experience_slug);
            $this->db->where($this->area . '.slug', $area_slug);
        }

        $this->db->order_by($this->package . '.position');
        $this->db->order_by($this->package . '.id', 'DESC');
        $result = $this->db->get($this->package)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getPackageAreas($destination_slug = '', $experience_slug = '') {
        $this->db->select('DISTINCT('. $this->area . '.name), ' . $this->area . '.slug, ' . $this->area . '.image, ' . $this->area . '.description, ' . $this->experience . '.name as experience_name');
        $this->db->join($this->package, $this->package . '.area_id = ' . $this->area . '.id');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
        $this->db->where($this->area . '.status', 1);
        $this->db->where($this->destination . '.slug', $destination_slug);
        $this->db->where($this->experience . '.slug', $experience_slug);
        $result = $this->db->get($this->area)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getPackageDescription($destination_slug, $experience_slug, $area_slug, $package_slug) {
        $this->db->select($this->destination . '.id as destination_id, ' . $this->destination . '.name as destination_name, '. $this->package . '.id, ' . $this->package . '.code, ' . $this->package . '.name, experience_id, cover_image, file, map_file, duration, minimum_group_size, best_season, altitude, difficulty_id, is_highlighted, support, ' . $this->package . '.description, itinerary, '. $this->difficulty_manager . '.name as difficulty_name, ' . $this->package_price . '.unit_price, ' . $this->departure_type . '.name as departure_name');
        $this->db->join($this->difficulty_manager, $this->difficulty_manager . '.id = ' . $this->package . '.difficulty_id');
        $this->db->join($this->departure_type, $this->departure_type . '.code = ' . $this->package . '.departure_type');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
        $this->db->join($this->area, $this->area . '.id = ' . $this->package . '.area_id');
        $this->db->join($this->package_price, $this->package_price . '.package_id = ' . $this->package . '.id AND min_pax = (SELECT MIN(min_pax) FROM tbl_package_price)', 'LEFT');
        $this->db->where($this->destination . '.slug', $destination_slug);
        $this->db->where($this->experience . '.slug', $experience_slug);
        $this->db->where($this->area . '.slug', $area_slug);
        $this->db->where($this->package . '.slug', $package_slug);
        $row = $this->db->get($this->package)->row();

        if(isset($row) && !empty($row)) {
            $data['info'] = $row;
            $data['gallery'] = $this->getPackageGallery($row->id);
            $data['inexclusions'] = $this->getPackageInclusionExclusion($row->id);
            $data['package_map'] = $this->getPackageMaps($row->id);
            $data['package_price'] = $this->getPackagePrice($row->id);
            $data['departure_dates'] = $this->getPackageDepartureDates($row->id);
        }

        return (isset($data) && !empty($data)) ? $data : array();
    }

    function getPackageGallery($package_id) {
        $this->db->select('image, title, caption');
        $this->db->where('package_id', $package_id);
        $this->db->where('status', 1);
        $result = $this->db->get($this->package_gallery)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getPackageInclusionExclusion($package_id) {
        $this->db->select('data_type, data');
        $this->db->where('package_id', $package_id);
        $this->db->where('status', 1);
        $result = $this->db->get($this->package_inclusion_exclusion)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $data[$val->data_type][] = $val->data;
            }
        }
        return (isset($data) && !empty($data)) ? $data : array();
    }

    function getPackageMaps($package_id) {
        $this->db->select('latitude, longitude, marker_title, marker_address, marker_description');
        $this->db->where('package_id', $package_id);
        $this->db->order_by('marker_sort_order');
        $result = $this->db->get($this->package_map)->result();

        if(isset($result) && !empty($result)) {
            $locations = array();
            $i = 1;
            foreach($result as $val) {
                $Tlat = (string)$val->latitude;
                $Tlng = (string)$val->longitude;
                $Tid = (string)$i;
                $Tname = (string)$val->marker_title;
                $Taddr = (string)$val->marker_address;
                $Tdesc = (string)$val->marker_description;

                $locations[] = array('label'=>$Tid,'lat'=>$Tlat,'long'=>$Tlng,'titl'=>$Tname,'addr'=>$Taddr,'desc'=>$Tdesc);
                $i++;
            }
            $locations = json_encode($locations);
        }

        return (isset($locations) && !empty($locations)) ? $locations : array();
    }

    function getPackagePrice($package_id) {
        $this->db->select('valid_from, valid_to, number_of_pax, unit_price');
        $this->db->where('package_id', $package_id);
        $this->db->where('status', 1);
        $result = $this->db->get($this->package_price)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getPackageDepartureDates($package_id)
    {
        $current_date = date('Y-m-d');
        $query = "SELECT DISTINCT(tbl_package_departure_detail.`id`),departure_date, all_allocation,available,booked,is_fixed_departure/*, number_of_pax, unit_price*/
                    FROM `tbl_package_departure_detail`
                    JOIN `tbl_package_price` ON tbl_package_departure_detail.`departure_date` BETWEEN tbl_package_price.`valid_from` AND tbl_package_price.`valid_to`
                    WHERE tbl_package_departure_detail.`status` = 1
                    AND tbl_package_price.`status` = 1
                    AND tbl_package_departure_detail.`departure_date` >= '" . $current_date . "'
                    AND tbl_package_departure_detail.`package_id` = " . $package_id . "
                    AND tbl_package_price.`package_id` = " . $package_id;

        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getDateDetail($date, $id) {
        $this->db->select('all_allocation, available, booked, is_fixed_departure');
        $this->db->where('package_id', $id);
        $this->db->where('departure_date', $date);
        $row = $this->db->get($this->package_departure_detail)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function getPriceDetail($nos_of_pax, $sel_date, $id) {
        $query = "SELECT unit_price
                    FROM " . $this->package_price . "
                    WHERE package_id = " . $id . "
                    AND ('" . $sel_date . "' BETWEEN valid_from AND valid_to)
                    AND ('" . $nos_of_pax . "' BETWEEN min_pax AND max_pax
                    OR min_pax = " . $nos_of_pax . "
                    OR max_pax = " . $nos_of_pax . ")";
        $row = $this->db->query($query)->row();

        return(isset($row) && !empty($row)) ? $row : '';
    }

    function getPackageDepartureYear() {
        $query = "SELECT DISTINCT(YEAR(STR_TO_DATE(departure_date, '%Y-%m-%d')))  AS years
                        FROM " . $this->package_departure_detail . "
                        ORDER BY years DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getPackageDepartureMonth() {
        $query = "SELECT DISTINCT(MONTHNAME(STR_TO_DATE(departure_date, '%Y-%m-%d')))  AS months, MONTH(departure_date) as month_names
                        FROM " . $this->package_departure_detail . "
                        ORDER BY month_names";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getPackageDepartureNosofDays() {
        $this->db->select('DISTINCT(duration)');
        $this->db->where('status', 1);
        $result = $this->db->get($this->package)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getPackageDeparturebyFilter($year, $month, $days) {
        $query = "SELECT DISTINCT( " . $this->package . ".name), " . $this->package . ".code, cover_image, duration, " . $this->package . ".slug, " . $this->departure_type . " .name as departure_name, departure_date, " . $this->package_departure_detail . ".id/*, number_of_pax*/
                    FROM " . $this->package . "
                    JOIN " . $this->package_departure_detail . "  ON " . $this->package_departure_detail . ".`package_id` = " . $this->package . " .id
                    JOIN " . $this->departure_type . " ON " . $this->departure_type . ".`code` = " . $this->package . ".`departure_type`
                    WHERE " . $this->package . ".`status` =  1
                    AND " . $this->package_departure_detail . ".`status` =  1
                    AND YEAR(departure_date) =  " . $year . "
                    AND MONTH(departure_date) =  " . $month;
        if($days != 0) {
            $query .= " AND duration = '".$days."'";
        }
        $result = $this->db->query($query)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $data[$val->name][] = $val;
            }
        }
        
        return (isset($data) && !empty($data)) ? $data : array();
    }

    function getSearchPackage($location_id, $experience_id, $order_id, $year_id, $month_id) {
        $current_date = date('Y-m-d');
        $this->db->distinct($this->package . '.name');
        $this->db->select($this->package . '.id, ' . $this->package . '.name, ' . $this->package . '.slug, cover_image, duration, minimum_group_size, short_description, ' . $this->difficulty_manager . '.name as difficulty_name, ' . $this->destination . '.slug as destination_slug, ' . $this->experience . '.slug as experience_slug, ' . $this->area . '.slug as area_slug, ' . $this->package_price . '.unit_price');
        $this->db->join($this->difficulty_manager, $this->package . '.difficulty_id = ' . $this->difficulty_manager . '.id');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
        $this->db->join($this->area , $this->area . '.id = ' . $this->package . '.area_id');
        $this->db->join($this->package_departure_detail, $this->package_departure_detail . '.package_id = ' . $this->package . '.id', 'LEFT');
        $this->db->join($this->package_price, $this->package_price . '.package_id = ' . $this->package . '.id AND min_pax = (SELECT MIN(min_pax) FROM tbl_package_price) AND valid_from >= ' . $current_date . ' AND valid_to <= ' . $current_date, 'LEFT');
        $this->db->where($this->difficulty_manager . '.status', 1);
        $this->db->where($this->package . '.status', 1);
        if($location_id != 0) {
            $this->db->where($this->package . '.destination_id', $location_id);
        }
        if($experience_id != 0 || !empty($experience_id)) {
            if(is_numeric($experience_id)) {
                $this->db->where($this->package . '.experience_id', $experience_id);
            } else {
                $this->db->where($this->experience . '.slug', $experience_id);
            }
        }
        if($order_id != 0 || !empty($order_id)) {
            $this->db->order_by('unit_price', $order_id);
        }
        if($year_id != 0) {
            $this->db->where('YEAR(departure_date)', $year_id);
        }
        if($month_id != 0) {
            $this->db->where('MONTH(departure_date)', $month_id);
        }
        $result = $this->db->get($this->package)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* packages */
    /* news */
    function getNewsListbyDate($month, $year) {
        $this->db->select('name, slug, image, short_description, publish_date, posted_by, news_category_id');
        $this->db->where('status', 1);
        $this->db->where('YEAR(publish_date)', $year);
        $this->db->where('MONTH(publish_date)', $month);
        $result = $this->db->get($this->news)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getRecentNews($limit) {
        $this->db->select('name, slug');
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        $result = $this->db->get($this->news)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getArchives() {
        $query = "SELECT DISTINCT CONCAT(MONTHNAME(publish_date), ' ', YEAR(publish_date)) AS archive_date,
                         DATE_FORMAT(publish_date,'%m/%Y') AS date_url
                  FROM " . $this->news . "
                  WHERE status = 1";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getNewsCategories() {
        $query = "SELECT DISTINCT ( " . $this->news_category .".name), " . $this->news_category . ".slug
                    FROM " . $this->news_category . "
                    JOIN " . $this->news . " ON FIND_IN_SET( " . $this->news_category . " .id, " . $this->news . ".news_category_id) > 0
                    AND " . $this->news . ".status = 1
                    AND " . $this->news_category . ".status = 1";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getNewsListbyCategory($alias, $type) {
        if($type == 'category') {
            $query = "SELECT " . $this->news . ".name, " . $this->news . ".slug, image, short_description, posted_by, publish_date, news_category_id
                        FROM " . $this->news . "
                        JOIN " . $this->news_category . " ON FIND_IN_SET ( " . $this->news_category . " .id, " . $this->news . " .news_category_id) > 0
                        WHERE " . $this->news_category . " .slug = '" . $alias . "'
                        AND " . $this->news . ".status = 1";
        } else {
            $query = "SELECT " . $this->news . ".name, " . $this->news . ".slug, image, short_description, posted_by, publish_date, news_category_id
                        FROM " . $this->news . "
                        WHERE FIND_IN_SET ('" . $alias . "'," . $this->news . ".tags) > 0
                        AND " . $this->news . ".status = 1";
        }
        $result = $this->db->query($query)->result();

        return (isset($result) & !empty($result)) ? $result : array();
    }

    function getNewsEventsDesc($alias, $type) {
        if($type == 'event') {
            $this->db->select('name, image, short_description, description, publish_date, unpublish_date, google_form_link');
        } else {
            $this->db->select('name, image, description, publish_date, posted_by, news_category_id, tags');
        }
        $this->db->where('slug', $alias);
        $result = $this->db->get($this->$type)->row();

        return (isset($result) && !empty($result)) ? $result : '';
    }
    /* news */
    /* events */
    function getEvents($type, $limit) {
        $this->db->select('name, slug, image, short_description, event_date');
        $this->db->where('status', 1);
        if($type == 'highlight') {
            $this->db->where('is_highlighted', 1);
            $this->db->order_by('position');
        } else {
            $this->db->order_by('id', 'DESC');
        }
        $this->db->order_by('position');
        $this->db->limit($limit);
        $result = $this->db->get($this->event)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* events */
    /* form */
    function getFormFields($form_id) {
        $this->db->order_by('position', 'ASC');
        $this->db->where('form_id', $form_id);
        $result = $this->db->get($this->form_field)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function get_form_details($form_id){
        $this->db->where('form_status', 'Published');
        $this->db->where('id', $form_id);
        $row = $this->db->get($this->form)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }

    function get_formElemOrData($formId){
        $array = '';
        $this->db->select('form_field_value, field_label, form_submission_id');
        $this->db->where($this->form_submission . '.form_id', $formId);
        $this->db->where($this->form_field . '.form_id', $formId);
        $this->db->order_by($this->form_field . '.field_order', 'ASC');
        $this->db->join($this->form_field, $this->form_field . '.id = ' .$this->form_submission_fields . '.form_field_id');
        $this->db->join($this->form_submission, $this->form_submission . '.id = ' . $this->form_submission_fields . '.form_submission_id');
        $result = $this->db->get($this->form_submission_fields)->result();
        if(isset($result) && !empty($result)){
            foreach($result as $ind=>$res){
                $array[$res->form_submission_id][$res->field_label] = $res->form_field_value;
            }
        }

        return $array;
    }

    function get_form_data_details($formsubmission_id){
        $this->db->select('form_field_value, field_label');
        $this->db->where($this->form_submission_fields . '.form_submission_id', $formsubmission_id);
        $this->db->join($this->form_field, $this->form_field . '.id = ' . $this->form_submission_fields . '.form_field_id');
        $this->db->join($this->form_submission, $this->form_submission . '.id = ' . $this->form_submission_fields . '.form_submission_id');
        $this->db->order_by($this->form_field . '.field_order', 'ASC');
        $result = $this->db->get($this->form_submission_fields)->result();
        if(isset($result) && !empty($result)){
            foreach($result as $ind=>$res){
                $array[$res->field_label] = $res->form_field_value;
            }
        }
        return $array;
    }
    /* form */
    /* testimonials */
    function getTestimonials($slug = NULL) {
        $this->db->select('customer_full_name, slug, customer_testimonial, customer_image, created_date, iso, printable_name');
        $this->db->join($this->countries, $this->testimonial . '.customer_country_id = ' . $this->countries . '.id');
        $this->db->where('status', 1);
        if(!empty($slug)) {
            $this->db->where('slug', $slug);
        } else {
            $this->db->order_by('position');
        }
        $result = $this->db->get($this->testimonial)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* testimonials */
    /* search */
    function getSearchResult($search_value) {
        $this->db->select('name, slug');
        $this->db->where('status', 1);
        $this->db->like('name', $search_value, 'both');
        $destination_result = $this->db->get($this->destination)->result();

        if(isset($destination_result) && !empty($destination_result)) {
            $result['destination'] = $destination_result;
        }

        $this->db->select($this->experience . '.name, ' . $this->experience . '.slug, ' . $this->destination . '.slug as destination_slug');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
        $this->db->where($this->experience . '.status', 1);
        $this->db->where($this->destination . '.status', 1);
        $this->db->like($this->experience . '.name', $search_value, 'both');
        $experience_result = $this->db->get($this->experience)->result();

        if(isset($experience_result) && !empty($experience_result)) {
            $result['experience'] = $experience_result;
        }

        $this->db->select($this->area . '.name, ' . $this->area . '.slug, ' . $this->experience . '.slug as experience_slug, ' . $this->destination . '.slug as destination_slug');
        $this->db->join($this->package, $this->package . '.area_id = ' . $this->area . '.id');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->package . '.destination_id');
        $this->db->where($this->area . '.status', 1);
        $this->db->where($this->package . '.status', 1);
        $this->db->where($this->experience . '.status', 1);
        $this->db->where($this->destination . '.status', 1);
        $this->db->like($this->area . '.name', $search_value, 'both');
        $area_result = $this->db->get($this->area)->result();

        if(isset($area_result) && !empty($area_result)) {
            $result['area'] = $area_result;
        }

        $this->db->select($this->package . '.name, ' . $this->package . '.slug, ' . $this->area . '.slug as area_slug, ' . $this->experience . '.slug as experience_slug, ' . $this->destination . '.slug as destination_slug');
        $this->db->join($this->area, $this->area . '.id = ' . $this->package . '.area_id');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->package . '.destination_id');
        $this->db->where($this->area . '.status', 1);
        $this->db->where($this->package . '.status', 1);
        $this->db->where($this->experience . '.status', 1);
        $this->db->where($this->destination . '.status', 1);
        $this->db->like($this->package . '.name', $search_value, 'both');
        $package_result = $this->db->get($this->package)->result();

        if(isset($package_result) && !empty($package_result)) {
            $result['package'] = $package_result;
        }

        $this->db->select('name, slug');
        $this->db->where('status', 1);
        $this->db->where('category_id', '1');
        $this->db->like('name', $search_value, 'both');
        $content_result = $this->db->get($this->content)->result();

        if(isset($content_result) && !empty($content_result)) {
            $result['content'] = $content_result;
        }
        //>>
        $query = "SELECT " . $this->news . ".name, " . $this->news . ".slug
                        FROM " . $this->news . "
                        WHERE FIND_IN_SET ('" . $search_value . "'," . $this->news . ".tags) > 0
                        AND " . $this->news . ".status = 1";
        $tag_result = $this->db->query($query)->result();
        if(isset($tag_result) && !empty($tag_result)) {
            $result['news'] = $tag_result;
        }
        
        //<<

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* search */
    /* nationality */
    function getCountries() {
        $this->db->select('id, name, printable_name');
        $result = $this->db->get($this->countries)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* nationality */
    /* get more data */
    function getMoreData($destination_id, $experience_id, $ids) {
        $this->db->select($this->package . '.id, '. $this->package . '.name, ' . $this->package . '.slug, cover_image, duration, minimum_group_size, short_description, ' . $this->difficulty_manager . '.name as difficulty_name, ' . $this->experience . ' .slug as experience_slug, ' . $this->destination . '.slug as destination_slug, ' . $this->area . '.slug as area_slug, ' . $this->package_price . '.unit_price');
        $this->db->join($this->difficulty_manager, $this->difficulty_manager . '.id = ' . $this->package . '.difficulty_id');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->package . '.experience_id');
        $this->db->join($this->destination, $this->destination . '.id = ' . $this->experience . '.destination_id');
        $this->db->join($this->area, $this->area . '.id = ' . $this->package . '.area_id');
        $this->db->join($this->package_price, $this->package_price . '.package_id = ' . $this->package . '.id AND min_pax = (SELECT MIN(min_pax) FROM tbl_package_price)', 'LEFT');
        $this->db->where($this->package . '.status', 1);
        $this->db->where($this->experience . '.status', 1);
        $this->db->where($this->destination . '.status', 1);
        $this->db->where($this->area . '.status', 1);
        $this->db->where('is_highlighted', 1);
        $this->db->order_by($this->package . '.position');

        if(!empty($destination_id) && $destination_id != '0') {
            $this->db->where($this->package . '.destination_id', $destination_id);
        }

        if(!empty($experience_id) && $experience_id != '0') {
            if(is_numeric($experience_id)) {
                $this->db->where($this->package . '.experience_id', $experience_id);
            } else {
                $this->db->where($this->experience . '.slug', $experience_id);
            }
        }

        if(!empty($ids)) {
            $this->db->where_not_in($this->package . '.id', explode(',', $ids));
        }

        $this->db->limit(2);

        $result = $this->db->get($this->package)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    /* get more data */
   function insert_data() {
        $insert_data['full_name']   = $this->input->post('name');
        $insert_data['email']       = $this->input->post('email');
        $insert_data['package_id']  = $this->input->post('fileid');
        $insert_data['date']   	    = time();
        $this->db->insert($this->package_download_detail, $insert_data);

        if($this->db->affected_rows() == 1) {
            return 'success';
        }
        return 'error';
    }
}
?>