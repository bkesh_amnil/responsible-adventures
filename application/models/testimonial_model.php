<?php
class Testimonial_Model extends My_Model {

	protected $table = 'tbl_testimonial';
	protected $countries = 'tbl_countries';
	
	public $id = '',
			$customer_full_name = '',
			$slug = '',
			$customer_email_address = '',
			$customer_country_id = '',
			$customer_image = '',
			$customer_testimonial = '',
			$status = '';


	public function __construct()
	{
		parent::__construct();
		$this->created_timestamp = true;
		$this->updated_timestamp = true;
		$this->created_by = true;
		$this->updated_by = true;
	}

	public function rules($id)
	{
		$array = array(
				array(
						'field' => 'customer_full_name',
						'label' => 'Full Name',
						'rules' => 'trim|required|xss_clean|unique[tbl_testimonial.customer_full_name.'.$id.']',
				),
				array(
						'field' => 'slug',
						'label' => 'Alias',
						'rules' => 'trim|required|xss_clean|unique[tbl_testimonial.slug.'.$id.']',
				),
				array(
						'field' => 'customer_email_address',
						'label' => 'Email Address',
						'rules' => 'trim|required|valid_email',
				),
				array(
						'field' => 'customer_country_id',
						'label' => 'Customer Nationality',
						'rules' => 'required|integer',
				),
				array(
						'field' => 'customer_testimonial',
						'label' => 'Testimonial',
						'rules' => 'trim|required',
				),
				array(
					'field' => 'customer_image',
					'label' => 'Image',
					'rules' => 'valid_size['.$this->table.'.customer_image]',
				)
		);

		return $array;
	}

	function get_testimonials(){
		$this->db->select($this->table . '.*, ' . $this->countries . '.printable_name');
		$this->db->join($this->countries, $this->countries . '.id = ' . $this->table . '.customer_country_id');
		$this->db->order_by('position', 'ASC');
                $this->db->order_by('id', 'DESC');

		$result = $this->db->get($this->table)->result();

		return (isset($result) && !empty($result)) ? $result : array();
	}
}
?>
