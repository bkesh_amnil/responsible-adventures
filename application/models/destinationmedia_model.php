<?php

class Destinationmedia_Model extends My_Model
{

    protected $table = 'tbl_destination_media';

    public $id = '',
        $destination_id = '',
        $media = '',
        $title = '',
        $caption = '';

    public function __construct()
    {
        parent::__construct();
    }

}