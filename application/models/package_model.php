<?php

class Package_Model extends My_Model
{

    protected $table = 'tbl_package';
    var $package_booking = 'tbl_package_booking';
    var $package_enquire = 'tbl_package_enquire';
    var $package_download_detail  = 'tbl_package_download_detail';

    public $id = '',
        $destination_id = '',
        $experience_id = '',
        $area_id = '',
        $code = '',
        $name = '',
        $slug = '',
        $cover_image = '',
        $file = '',
        $map_file = '',
        $duration = '',
        $minimum_group_size = '',
        $best_season= '',
        $altitude = '',
        $difficulty_id = '',
        $support = '',
        $is_recommended = '',
        $is_highlighted = '',
        $short_description = '',
        $description = '',
        $itinerary = '',
        $departure_type = '',
        $departure_year = '',
        $meta_keywords = '',
        $meta_description = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if(empty($id)) {
            $array = array(
                array(
                    'field' => 'destination_id',
                    'label' => 'Destination',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'experience_id',
                    'label' => 'Experience',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'area_id',
                    'label' => 'Area',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'code',
                    'label' => 'Code',
                    'rules' => 'trim|required|unique[' . $this->table . ' .code.' . $id . ']',
                ),
                array(
                    'field' => 'name',
                    'label' => 'Title',
                    'rules' => 'trim|required|unique[' . $this->table . ' .name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[' . $this->table . ' .slug.' . $id . ']',
                ),
                array(
                    'field' => 'duration',
                    'label' => 'Duration',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'minimum_group_size',
                    'label' => 'Minimum Group Size',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'best_season',
                    'label' => 'Best Season',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'altitude',
                    'label' => 'Altitude (in mts.)',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'difficulty_id',
                    'label' => 'Difficulty',
                    'rules' => 'trim|required|integer',
                ),
                array(
                    'field' => 'support',
                    'label' => 'Support',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'is_recommended',
                    'label' => 'Is Recommended',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'cover_image',
                    'label' => 'Cover Image',
                    'rules' => 'trim|required|valid_size['.$this->table.'.cover_image]',
                ),
                array(
                    'field' => 'file',
                    'label' => 'File',
                    'rules' => 'trim|required|valid_file['.$this->table.'.file]',
                ),
                array(
                    'field' => 'map_file',
                    'label' => 'Map File',
                    'rules' => 'trim|valid_file['.$this->table.'.map_file]|valid_file_size['.$this->table.'.map_file]',
                ),
                array(
                    'field' => 'departure_type',
                    'label' => 'Departure Type',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'departure_year[]',
                    'label' => 'Departure Year',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'short_description',
                    'label' => 'Short Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'itinerary',
                    'label' => 'Itinerary',
                    'rules' => 'trim|required',
                ),
            );
        } else {
            $array = array(
                array(
                    'field' => 'destination_id',
                    'label' => 'Destination',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'experience_id',
                    'label' => 'Experience',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'area_id',
                    'label' => 'Area',
                    'rules' => 'required|integer',
                ),
                array(
                    'field' => 'code',
                    'label' => 'Code',
                    'rules' => 'trim|required|unique[' . $this->table . ' .code.' . $id . ']',
                ),
                array(
                    'field' => 'name',
                    'label' => 'Title',
                    'rules' => 'trim|required|unique[' . $this->table . ' .name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[' . $this->table . ' .slug.' . $id . ']',
                ),
                array(
                    'field' => 'duration',
                    'label' => 'Duration',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'minimum_group_size',
                    'label' => 'Minimum Group Size',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'best_season',
                    'label' => 'Best Season',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'altitude',
                    'label' => 'Altitude (in mts.)',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'difficulty_id',
                    'label' => 'Difficulty',
                    'rules' => 'trim|required|integer',
                ),
                array(
                    'field' => 'support',
                    'label' => 'Support',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'is_recommended',
                    'label' => 'Is Recommended',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'short_description',
                    'label' => 'Short Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'itinerary',
                    'label' => 'Itinerary',
                    'rules' => 'trim|required',
                ),
                /*array(
                    'field' => 'cover_image',
                    'label' => 'Cover Image',
                    'rules' => 'valid_size['.$this->table.'.cover_image]',
                ),*/
                array(
                    'field' => 'file',
                    'label' => 'File',
                    'rules' => 'trim|required|valid_file['.$this->table.'.file]',
                ),
                array(
                    'field' => 'map_file',
                    'label' => 'Map File',
                    'rules' => 'trim|valid_file['.$this->table.'.map_file]|valid_file_size['.$this->table.'.map_file]',
                )
            );
        }

        return $array;
    }

    public function getPackages($experience_id = NULL) {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $this->db->order_by('id', 'ASC');
        if(!empty($experience_id)) {
            $this->db->where('experience_id', $experience_id);
        }
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getPackageBookingNames() {
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->package_booking, $this->package_booking . '.package_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getPackageEnquireNames() {
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->package_enquire, $this->package_enquire . '.package_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
    
    public function getPackageDownloadNames() {
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->package_download_detail, $this->package_download_detail . '.package_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}