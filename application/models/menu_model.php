<?php
class Menu_Model extends My_Model
{

    protected $table = 'tbl_menu';
    var $site_menus = 'tbl_site_menus';

    public $id = '',
            $menu_type_id = '',
            $name = '',
            $slug='',
            $menu_type = '',
            $menu_link_type = '',
            $menu_parent = '',
            $page_title = '',
            $menu_heading = '',
            $meta_keywords = '',
            $meta_description = '',
            $position = '',
            $link = '',
            $link_target = '',
            $menu_opens = '',
            $menu_url = '',
            $menu_site = '',
            $status = '',
            $relate_destination = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id) {
        $array = array(
            array(
                'field' => 'menu_type',
                'label' => 'Menu Type',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique[tbl_menu.name.'.$id.']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique[tbl_menu.slug.'.$id.']',
            ),
            array(
                'field' => 'page_title',
                'label' => 'Page Title for Menu',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'menu_heading',
                'label' => 'Menu Heading for Menu',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function get_menutypes()
    {
        $sql = "SELECT * FROM `tbl_site_menus`";

        $result = $this->query($sql);

        return $result;
    }

    public function get_menulinktype()
    {
        $sql = "SELECT linktype, title  FROM `tbl_menu_link_types`";

        $results = $this->query($sql);

        if($results){
            foreach($results as $key=>$data){
                $result[$data->linktype]= $data->title;
            }
        }
        return $result;
    }

    public function get_menulinks($key)
    {
        if($key == 'content'){
            $sql = "SELECT c.id, c.name FROM tbl_content c JOIN tbl_category ca ON c.category_id = ca.id";
            $results = $this->query($sql);
            if(empty($results)) {
                return [];
            }
            foreach($results as $data) {
                $result[$data->id] = $data->name;
            }
        } else if($key == "module"){
            $sql = "SELECT m.id, m.name FROM tbl_module m WHERE m.parent_id = '0'";
            $results = $this->query($sql);
            if(empty($results)) {
                return [];
            }
            foreach($results as $data) {
                $result[$data->id] = $data->name;
            }
        } elseif($key == 'url') {
            $result = 'text';
        } else {
            $result = 'none';
        }
        return $result;
    }

    public function getAllData($parent = 0, $padding = 0) {

        $query = "SELECT *
                  FROM tbl_menu
                  WHERE menu_parent='" . $parent . "'
                  ORDER BY position";

        $result = $this->query($query);;

        $finList = array();

        if (isset($result) && !empty($result)) {

            foreach ($result as $ind => $resultval) {
                $menuListGen = "";

                if (!empty($resultval)) {
                    foreach ($resultval as $resultind => $val) {
                        $menuListGen[$resultind] = $val;
                    }

                    $menuListGen["cellpadding"] = $padding;
                    $menuListGen["childList"] = $this->getAllData($resultval->id, ($padding + 10));

                    array_push($finList, $menuListGen);

                }
            }
        }

        return $finList;
    }

    public function getMenus() {
        $this->db->select('id, name, menu_parent');
        $this->db->where('status', 1);
        $this->db->order_by('name');
        $result = $this->db->get($this->table)->result();

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                if($val->menu_parent == '0') {
                    $this->db->select('id');
                    $this->db->where('menu_parent', $val->id);
                    $result1 = $this->db->get($this->table)->result();

                    if(isset($result1) && !empty($result1)) {

                    } else {
                        $data[] = $val;
                    }
                } else {
                    $data[] = $val;
                }
            }
        }
        return (isset($data) && !empty($data)) ? $data : array();
    }

    public function get_menu_page_settings($id = 0)
    {
        if($id == 0){
            return false;
        }
        $this->db->where('menu_id', $id);
        $this->db->order_by('position', 'asc');
        $result = $this->db->get('tbl_menu_page_settings')->result();
        if($result)
        {
            return $result;
        }
        return false;
    }

    function get_enabled_module($site_id)
    {
        $row = $this->db->select('modules_enabled')->where('id', $site_id)->get('tbl_site')->row();
        if($row)
            $modules = $row->modules_enabled;
        else
            $modules = '';

        $modules =  explode(',', $modules);

        $this->db->where_in('id', $modules);
        $this->db->where('public_module', 'yes');
        $result = $this->db->get('tbl_module')->result();

        $tmp['']  = 'Select Module';
        foreach($result as $row)
        {
            $tmp[$row->id] =  $row->name;
        }
        return $tmp;
    }

    function getMenuTypes()
    {
        $this->db->select('id, name');
        $result = $this->db->get($this->site_menus)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}