<?php

class Banner_Model extends My_Model
{

    protected $table = 'tbl_banner';

    public $id = '',
            $name = '',
            $description = '',
            $image = '',
            $category_id = '',
            $link_type = '',
            $link_id = '',
            $link_url = '',
            $opens = '',
            $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique['.$this->table.'.name.'.$id.']',
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                'rules' => 'trim|required|valid_size['.$this->table.'.image]',
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

}