<?php

class Booking_Status_Model extends My_Model
{

    protected $table = 'tbl_booking_status';
    var $package_booking = 'tbl_package_booking';

    public $id = '',
        $name = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique[' . $this->table . '.name.' . $id . ']',
            )
        );

        return $array;
    }

    public function getBookStatus() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getBookingStatus($type) {
        $this->db->select('DISTINCT('. $this->table .'.id), name');
        $this->db->join($this->$type, $this->$type . '.last_booking_status_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}