<?php

class Package_Price_Model extends My_Model
{

    protected $table = 'tbl_package_price';

    public $id = '',
        $package_id = '',
        $description = '',
        $valid_from = '',
        $valid_to = '',
        $number_of_pax = '',
        $min_pax = '',
        $max_pax = '',
        $unit_price = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if($id == 0) {
            $array = array(
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required',
                ),
            );
        } else {
            $array = array(
                array(
                    'field' => 'valid_from',
                    'label' => 'Valid From',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'valid_to',
                    'label' => 'Valid To',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'number_of_pax',
                    'label' => 'Number of Pax',
                    'rules' => 'trim|required',
                ),
                array(
                    'field' => 'unit_price',
                    'label' => 'Unit Price',
                    'rules' => 'trim|required',
                ),
            );
        }
        return $array;
    }

    public function getPackagePrice($package_id) {
        $this->db->select('id, package_id, description, valid_from, valid_to, number_of_pax, unit_price, status');
        $this->db->where('package_id', $package_id);
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}