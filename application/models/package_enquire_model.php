<?php

class Package_enquire_Model extends My_Model
{

    protected $table = 'tbl_package_enquire';
    var $package = 'tbl_package';
    var $experience = 'tbl_experience';

    public $id = '',
        $package_destination_id = '',
        $package_experience_id = '',
        $package_id = '',
        $departure_date = '',
        $number_of_pax = '',
        $full_name = '',
        $email_address = '',
        $contact_number = '',
        $contact_address = '',
        $enquiry = '',
        $note = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            /*array(
                'field' => 'note',
                'label' => 'Enquire Note',
                'rules' => 'trim|required',
            )*/
        );

        return $array;
    }

    public function getPackageEnquiries() {
        $this->db->select($this->table . '.*, ' . $this->package . '.name as package_name, ' . $this->experience . '.name as experience_name');
        $this->db->join($this->package, $this->package . '.id = ' . $this->table . '.package_id');
        $this->db->join($this->experience, $this->experience . '.id = ' . $this->table . '.package_experience_id');
        $this->db->order_by($this->table . '.id', 'DESC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getPackageEnquireCustomers() {
        $this->db->select('DISTINCT(full_name)');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getExportData($field_type, $field_value) {
        $query = "SELECT
                    DATE_FORMAT(FROM_UNIXTIME(`tbl_package_enquire`.`created_date`), '%Y-%m-%d') AS enquire_date,
                    `tbl_destination`.`name` as destination_name,
                    `tbl_experience`.`name` as experience_name,
                    `tbl_package`.`name` as package_name,
                    `tbl_package`.`code`,
                    `departure_date`,
                    `number_of_pax`,
                    `full_name`,
                    `email_address`,
                    `contact_number`,
                    `contact_address`,
                    `enquiry`,
                    `note`,
                    `tbl_package_enquire`.`created_date`
                    FROM `tbl_package_enquire`
                    JOIN `tbl_destination` ON `tbl_destination`.`id` = `tbl_package_enquire`.`package_destination_id`
                    JOIN `tbl_experience` ON `tbl_experience`.`id` = `tbl_package_enquire`.`package_experience_id`
                    JOIN `tbl_package` ON `tbl_package`.`id` = `tbl_package_enquire`.`package_id`";

        if($field_type == 'package_destination') {
            $query .= " WHERE package_destination_id = " . $field_value;
        } else if($field_type == 'package_experience') {
            $query .= " WHERE package_experience_id = " . $field_value;
        } else if($field_type == 'package') {
            $query .= " WHERE package_id  =" . $field_value;
        } else if($field_type == 'customer_name') {
            $query .= " WHERE full_name = '".$field_value."'";
        } else if($field_type == 'enquire_date' && !empty($field_value)) {
            $query .= " WHERE DATE_FORMAT(FROM_UNIXTIME(`tbl_package_enquire`.`created_date`), '%Y-%m-%d') = '".$field_value."'";
        }

        $query .= " ORDER BY `tbl_package_enquire`.`id` DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();

    }


}