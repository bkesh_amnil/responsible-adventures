<?php

class Gallery_Model extends My_Model
{

    protected $table = 'tbl_gallery';

    public $id = '',
            $name = '',
            $slug = '',
            $category_id = '',
            $cover = '',
            $meta_keywords = '',
            $meta_description = '',
            $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if(empty($id)) {
            $array = array(
                array(
                    'field' => 'name',
                    'label' => 'Title',
                    'rules' => 'trim|required|unique[tbl_gallery.name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[tbl_gallery.slug.' . $id . ']',
                ),
                /*array(
                    'field' => 'category_id',
                    'label' => 'Category',
                    'rules' => 'trim|required|integer',
                ),*/
                array(
                    'field' => 'cover',
                    'label' => 'Cover Image',
                    //'rules' => 'required|valid_size['.$this->table.'.cover]',
                    'rules' => 'required',
                ),
                array(
                'field' => 'image',
                'label' => 'Images',
                'rules' => 'required',
                )
            );
        } else {
            $array = array(
                array(
                    'field' => 'name',
                    'label' => 'Gallery Name',
                    'rules' => 'trim|required|unique[tbl_gallery.name.' . $id . ']',
                ),
                array(
                    'field' => 'slug',
                    'label' => 'Alias',
                    'rules' => 'trim|required|unique[tbl_gallery.slug.' . $id . ']',
                )/*,
                array(
                    'field' => 'category_id',
                    'label' => 'Category',
                    'rules' => 'trim|required|integer',
                ),
                array(
                    'field' => 'cover',
                    'label' => 'Cover Image',
                    'rules' => 'valid_size['.$this->table.'.cover]',
                )*/
            );
        }

        return $array;
    }

    public function getData() {
        $query = "SELECT g.*,c.name as category_name, gm.type as mediaType
                  FROM `tbl_gallery` g
                  JOIN `tbl_category` c ON g.`category_id` = c.`id`
                  LEFT JOIN `tbl_gallery_media` gm ON gm.`gallery_id` = g.`id`
                  GROUP BY g.`id`
                  ORDER BY g.`position` ASC,g.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getSavedMedia($id)
    {
        $query = "SELECT *
                    FROM `tbl_gallery_media` gm
                    WHERE gm.`gallery_id` = $id";

        return $this->query($query);
    }

}