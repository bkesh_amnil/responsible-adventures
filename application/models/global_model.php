<?php

class Global_model extends My_Model
{
    public function get_single_data($table_name, $select_field, $value, $field = 'id')
    {
        $this->db->where($field, $value);
        $row = $this->db->get($table_name)->row();

        if(count($row) > 0)
        {
            return $row->$select_field;
        }
        else
        {
            return '';
        }
    }
}
?>