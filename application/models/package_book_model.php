<?php

class Package_book_Model extends My_Model
{

    protected $table = 'tbl_package_booking';
    var $package = 'tbl_package';
    var $package_departure_detail = 'tbl_package_departure_detail';
    var $booking_status = 'tbl_booking_status';
    var $package_booking_status = 'tbl_package_booking_status';

    public $id = '',
        $package_destination_id = '',
        $package_experience_id = '',
        $package_id = '',
        $remarks = '',
        $last_booking_status_id = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
           array(
                'field' => 'package_id',
                'label' => 'Package',
                'rules' => 'trim|required|integer',
            )
        );

        return $array;
    }

    public function getPackageBookings() {
        $query = "SELECT * FROM
                    (SELECT
                      `tbl_package_booking`.`id`,
                      `tbl_package_booking`.`created_date`,
                      `tbl_package_booking`.`created_by`,
                      `tbl_package`.`name` AS package_name,
                      `full_name`,
                      `departure_date`,
                      `number_of_pax`,
                      `price_per_person`,
                      `total_price`,
                      `tbl_booking_status`.`name` AS booking_status,
                      tbl_package_booking_status.id AS booking_id,
                      package_booking_id
                    FROM (`tbl_package_booking`)
                      JOIN `tbl_package_booking_status`
                        ON `tbl_package_booking_status`.`package_booking_id` = `tbl_package_booking`.`id`
                      JOIN `tbl_package`
                        ON `tbl_package`.`id` = `tbl_package_booking`.`package_id`
                      JOIN `tbl_package_departure_detail`
                        ON `tbl_package_departure_detail`.`id` = `tbl_package_booking_status`.`departure_date_id`
                      JOIN `tbl_booking_status`
                        ON `tbl_booking_status`.`id` = `tbl_package_booking_status`.`booking_status_id`
                        ORDER BY tbl_package_booking_status.id DESC)AS t
                    GROUP BY t.package_booking_id
                    ORDER BY t.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getExportData($field_type, $field_value) {
        $query = "SELECT * FROM
                    (SELECT
                      `tbl_package_booking`.`id`,
                      `tbl_package_booking`.`package_destination_id`,
                      `tbl_package_booking`.`package_experience_id`,
                      `tbl_package_booking`.`package_id`,
                      `tbl_package_booking`.`last_booking_status_id`,
                      `tbl_package_booking`.`created_date`,
                      `tbl_package_booking`.`created_by`,
                      `tbl_package`.`name` AS package_name,
                      `tbl_package`.`code`,
                      `full_name`,
                      `email_address`,
                      `contact_number`,
                      `contact_address`,
                      `number_of_pax`,
                      `price_per_person`,
                      `total_price`,
                      `departure_date`,
                      `tbl_booking_status`.`name` AS booking_status,
                      tbl_package_booking_status.id AS booking_id,
                      package_booking_id,
                      DATE_FORMAT(FROM_UNIXTIME(`tbl_package_booking`.`created_date`), '%Y-%m-%d') AS booking_date,
                      `tbl_destination`.`name` AS destination_name,
                      `tbl_experience`.`name` AS experience_name
                    FROM (`tbl_package_booking`)
                      JOIN `tbl_package_booking_status`
                        ON `tbl_package_booking_status`.`package_booking_id` = `tbl_package_booking`.`id`
                      JOIN `tbl_package`
                        ON `tbl_package`.`id` = `tbl_package_booking`.`package_id`
                      JOIN `tbl_destination`
                        ON `tbl_destination`.`id` = `tbl_package_booking`.`package_destination_id`
                      JOIN `tbl_experience`
                        ON `tbl_experience`.`id` = `tbl_package_booking`.`package_experience_id`
                      JOIN `tbl_package_departure_detail`
                        ON `tbl_package_departure_detail`.`id` = `tbl_package_booking_status`.`departure_date_id`
                      JOIN `tbl_booking_status`
                        ON `tbl_booking_status`.`id` = `tbl_package_booking_status`.`booking_status_id`
                        ORDER BY tbl_package_booking_status.id DESC)AS t";

        if($field_type == 'package_destination') {
            $query .= " WHERE t.`package_destination_id` = " . $field_value;
        } else if($field_type == 'package_experience') {
            $query .= " WHERE t.`package_experience_id` = " . $field_value;
        } else if($field_type == 'package') {
            $query .= " WHERE t.`package_id` = " . $field_value;
        } else if($field_type == 'customer_name') {
            $query .= " WHERE t.`full_name` = '".$field_value."'";
        } else if($field_type == 'booking_status') {
            $query .= " WHERE t.`last_booking_status_id` = ". $field_value;
        } else if($field_type == 'booking_date' && !empty($field_value)) {
            $query .= " WHERE t.booking_date = '".$field_value."'";
        } else if($field_type == 'departure_date' && !empty($field_value)) {
            $query .= " WHERE t.`departure_date` = '".$field_value."'";
        }

        $query .= " GROUP BY t.package_booking_id
                    ORDER BY t.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}