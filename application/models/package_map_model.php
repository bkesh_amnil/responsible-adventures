<?php

class Package_Map_Model extends My_Model
{

    protected $table = 'tbl_package_map';

    public $id = '',
        $package_id = '',
        $package_latitude = '',
        $package_longitude = '',
        $package_marker_title = '',
        $package_marker_address = '',
        $package_marker_description = '',
        $package_marker_label = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function getPackageMaps($package_id) {
        $this->db->select('id, package_id, latitude, longitude, marker_title, marker_address, marker_description, marker_label, marker_sort_order');
        $this->db->where('package_id', $package_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}