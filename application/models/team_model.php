<?php
class Team_Model extends My_Model
{

	protected $table = 'tbl_team';

	public $id = '',
			$name = '',
			$slug = '',
			$post = '',
			$team_description = '',
			$image = '',
			$status = '';

	public function __construct()
	{
		parent::__construct();
		$this->created_timestamp = true;
		$this->updated_timestamp = true;
		$this->created_by = true;
		$this->updated_by = true;
	}

	public function rules($id)
	{
		$array = array(
				array(
						'field' => 'name',
						'label' => 'Team Name',
						'rules' => 'trim|required|xss_clean|unique[tbl_team.name.' . $id . ']',
				),
				array(
						'field' => 'slug',
						'label' => 'Slug',
						'rules' => 'trim|required|xss_clean|unique[tbl_team.slug.' . $id . ']',
				),
				array(
						'field' => 'team_description',
						'label' => 'Team Description',
						'rules' => 'required',
				),
				array(
						'field' => 'image',
						'label' => 'Image',
						'rules' => 'required',
				)
		);

		return $array;
	}

	public function getSavedMedia($id)
	{
		$query = "select *
                    from `tbl_team_media` gm
                    where gm.`team_id` = $id";

		return $this->query($query);
	}

}
?>
