<?php

class Experience_Model extends My_Model
{

    protected $table = 'tbl_experience';
    var $destination = 'tbl_destination';
    var $package_booking = 'tbl_package_booking';
    var $package_enquire = 'tbl_package_enquire';

    public $id = '',
        $destination_id = '',
        $name = '',
        $slug = '',
        $sub_title = '',
        $image = '',
        $icon = '',
        $description = '',
        $meta_keywords = '',
        $meta_description = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id, $destination_id)
    {
        $array = array(
            array(
                'field' => 'destination_id',
                'label' => 'Destination',
                'rules' => 'required|integer',
            ),
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique_multi['.$this->table.'.name.'.$destination_id.'.'.$id.']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique_multi['.$this->table.'.slug.'.$destination_id.'.'.$id.']',
            ),
            array(
                'field' => 'sub_title',
                'label' => 'Sub Title',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                'rules' => 'trim|required|valid_size['.$this->table.'.image]',
            ),
            array(
                'field' => 'icon',
                'label' => 'Icon Image',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'required'
            )
        );

        return $array;
    }

    public function getData() {
        $query = "SELECT e.*,d.name as destination_name
                  FROM " . $this->table . " AS e
                  JOIN `tbl_destination` d ON e.`destination_id` = d.`id`
                  ORDER BY e.position ASC, e.id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getExperiences($destination_id = NULL) {
        if(!empty($destination_id)) {
            $this->db->select('id, name');
            $this->db->where('destination_id', $destination_id);
            $this->db->where('status', 1);
            $result = $this->db->get($this->table)->result();
        } else {
            $query = "SELECT e.id, e.name, d.name as destination_name
                        FROM " . $this->table . " AS e
                        JOIN " . $this->destination . " d ON d.id = e.destination_id
                        WHERE e.status = 1
                        ORDER BY e.position";
            $result = $this->db->query($query)->result();
        }

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getPackageBookingExperiences() {
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->package_booking, $this->package_booking . '.package_experience_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getPackageEnquireExperiences() {
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->package_enquire, $this->package_enquire . '.package_experience_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}