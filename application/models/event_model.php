<?php
	class Event_Model extends My_Model {

		protected $table = 'tbl_event';

		public $id = '',
				$name = '',
				$slug = '',
				$image = '',
				$short_description = '',
				$description ='',
				$event_date = '',
				$publish_date = '',
				$unpublish_date = '',
				$is_highlighted = '',
				$google_form_link = '',
				$position = '',
				$meta_keywords = '',
				$meta_description = '',
				$status = '';

		public function __construct()
		{
			parent::__construct();
			$this->created_timestamp = true;
			$this->updated_timestamp = true;
			$this->created_by = true;
			$this->updated_by = true;
		}
		public function rules($id)
		{
			$array = array(
				array(
						'field' => 'name',
						'label' => 'Title',
						'rules' => 'trim|required|xss_clean|unique[tbl_event.name.'.$id.']',
				),
				array(
						'field' => 'slug',
						'label' => 'Alias',
						'rules' => 'trim|required|xss_clean|unique[tbl_event.slug.'.$id.']',
				),
				array(
						'field' => 'image',
						'label' => 'Image',
						'rules' => 'valid_size['.$this->table.'.image]',
				),
				array(
						'field' => 'event_date',
						'label' => 'Event Start Date',
						'rules' => 'required',
				),
				array(
						'field' => 'publish_date',
						'label' => 'Event Publish Date',
						'rules' => 'required',
				),
				array(
						'field' => 'unpublish_date',
						'label' => 'Event Unpublish Date',
						'rules' => 'required',
				),
				array(
						'field' => 'short_description',
						'label' => 'Event Short Description',
						'rules' => 'required',
				),
				array(
						'field' => 'description',
						'label' => 'Event Description',
						'rules' => 'required',
				),
				array(
						'field' => 'is_highlighted',
						'label' => 'Event Is Highlighted',
						'rules' => 'trim|required',
				),
				array(
					'field' => 'google_form_link',
					'label' => 'Google Form Link',
					'rules' => 'prep_url|valid_google_url[google_form_link]',
				),
				array(
						'field' => 'status',
						'label' => 'Status',
						'rules' => 'trim|required',
				)
			);

			return $array;
		}
	}
?>