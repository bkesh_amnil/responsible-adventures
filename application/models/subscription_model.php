<?php

class Subscription_Model extends My_Model
{

    protected $table = 'tbl_subscription';

    public $id = '',
        $subscription_email = '',
        $subscription_date = '';

    public function __construct()
    {
        parent::__construct();
    }

    public function getExcelData() {
        $this->db->select('id as `ID`');
        $this->db->select('subscription_email AS `Email`');
        $this->db->select('subscription_date AS `Date`');
        $result = $this->db->get('tbl_subscription')->result_array();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}