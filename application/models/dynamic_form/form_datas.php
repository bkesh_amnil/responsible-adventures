<?php

class Form_datas extends My_Model
{

    protected $table = 'tbl_form_submission_fields';
    var $form_submissions = 'tbl_form_submissions';

    public $id = '',
        $form_submission_id = '',
        $form_field_id = '',
        $form_field_value = '',
        $form_display_text = '';


    public function __construct()
    {
        parent::__construct();
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'form_submission_id',
                'label' => 'Form Submission Id',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'form_field_id',
                'label' => 'Form Field id',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'form_field_value',
                'label' => 'Form Field Value',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'form_display_text',
                'label' => 'Form Display Text',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    function getSubmittedForms($form_id) {
        $this->db->where('form_id', $form_id);
        $this->db->order_by('order', 'ASC');
        $result = $this->db->get($this->form_submissions)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    function getSubmittedFormData($form_submission_id, $form_field_id) {
        $this->db->select('form_field_value');
        $this->db->where('form_submission_id', $form_submission_id);
        $this->db->where('form_field_id', $form_field_id);
        $row = $this->db->get($this->table)->row();

        return (isset($row) && !empty($row)) ? $row->form_field_value : '';
    }

    public function get_submitted_data($form_submission_id, $form_field_id) {
        $this->db->select('form_field_value');
        $this->db->where('form_submission_id', $form_submission_id);
        $this->db->where('form_field_id', $form_field_id);
        $row = $this->db->get($this->table)->row();

        return (isset($row) && !empty($row)) ? $row->form_field_value : '';
    }
}