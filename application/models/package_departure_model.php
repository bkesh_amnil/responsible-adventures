<?php

class Package_Departure_Model extends My_Model
{

    protected $table = 'tbl_package_departure_detail';

    public $id = '',
        $package_id = '',
        $package_departure_date = '',
        $package_all_allocation = '',
        $package_available = '',
        $package_booked = '',
        $is_fixed_departure = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'package_departure_date',
                'label' => 'Departure Date',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'package_all_allocation',
                'label' => 'All Allocation',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'package_available',
                'label' => 'Available',
                'rules' => 'trim|required|integer'
            ),
            array(
                'field' => 'package_booked',
                'label' => 'Booked',
                'rules' => 'trim|required|integer'
            )
        );

        return $array;
    }

    public function getPackageDeparture($package_id) {
        $this->db->select('id, package_id, departure_date, all_allocation, available, booked, is_fixed_departure, status');
        $this->db->where('package_id', $package_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}