<?php

class Destination_Model extends My_Model
{

    protected $table = 'tbl_destination';
    var $desination_media = 'tbl_destination_media';
    var $package_booking = 'tbl_package_booking';
    var $package_enquire = 'tbl_package_enquire';

    public $id = '',
        $name = '',
        $slug = '',
        $sub_title='',
        $image = '',
        $long_description = '',
        $meta_keywords = '',
        $meta_description = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique['.$this->table.'.name.'.$id.']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique['.$this->table.'.slug.'.$id.']',
            ),
            array(
                'field' => 'sub_title',
                'label' => 'Sub Title',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                'rules' => 'trim|required|valid_size['.$this->table.'.image]',
            ),
            array(
                'field' => 'long_description',
                'label' => 'Description',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function getAllDestinations() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $this->db->order_by('position', 'ASC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getSavedMedia($id)
    {
        $query = "SELECT *
                    FROM " . $this->desination_media . "
                    WHERE destination_id = " . $id;

        return $this->query($query);
    }

    public function getPackageBookingDestination() {
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->package_booking, $this->package_booking. '.package_destination_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

    public function getPackageEnquireDestination() {
        $this->db->select('DISTINCT('. $this->table.'.id), name');
        $this->db->join($this->package_enquire, $this->package_enquire. '.package_destination_id = ' . $this->table . '.id');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
}