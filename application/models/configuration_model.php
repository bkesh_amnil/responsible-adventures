<?php

class Configuration_Model extends My_Model
{

    protected $table = 'tbl_site';

    public $id = '',
            $sub_domain='',
            $site_status='',
            $site_offline='',
            $site_offline_msg='',
            $google_analytics_code='',
            $site_title = '',
            $site_name='',
            $address = '',
            $phone = '',
            $facebook = '',
            $twitter = '',
            $gplus = '',
            $pininterest = '',
            $instagram = '',
            $skype = '',
            $linkedin = '',
            $vimeo = '',
            $site_keyword = '',
            $site_description = '',
            $site_from_email = '',
            $site_author = '',
            $site_logo = '',
            $modules_enabled='',
            $siteBanner='',
            $imageSize='';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public $rules =
        array(
            array(
                'field' => 'site_title',
                'label' => 'Site Title',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'site_name',
                'label' => 'Site Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'site_offline',
                'label' => 'Site Offline',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'site_from_email',
                'label' => 'Site Email',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'phone',
                'label' => 'Phone',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'facebook',
                'label' => 'Facebook Link',
                'rules' => 'trim'
            ),
            array(
                'field' => 'twitter',
                'label' => 'Twitter Link',
                'rules' => 'trim'
            ),
            array(
                'field' => 'gplus',
                'label' => 'Google Plus Link',
                'rules' => 'trim'
            ),
            array(
                'field' => 'skype',
                'label' => 'Skype Link',
                'rules' => 'trim'
            ),
            array(
                'field' => 'linkedin',
                'label' => 'Linkedin',
                'rules' => 'trim'
            ),
            array(
                'field' => 'vimeo',
                'label' => 'Vimeo',
                'rules' => 'trim'
            ),
            array(
                'field' => 'modules_enabled',
                'label' => 'Modules Enabled',
                'rules' => 'trim'
            ),
            array(
                'field' => 'imageSize',
                'label' => 'Image Size',
                'rules' => 'trim'
            ),
            array(
                'field' => 'modules_enabled',
                'label' => 'Modules Enabled',
                'rules' => 'trim'
            )
        );
}