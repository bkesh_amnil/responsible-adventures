<?php
class Email_send_Model extends My_Model {

    var $smtp_settings = 'tbl_smtp_settings';
    var $site = 'tbl_site';

    function sendMail($subject, $user_email = NULL, $body, $file = NULL) {
        $smtp_settings = $this->getSmtpSettings();
        $site = $this->siteDetails();

        $smtp_server = $smtp_settings->smtp_server;
        $smtp_port = $smtp_settings->smtp_port;
        $username = $smtp_settings->smtp_email;
        $password = $smtp_settings->smtp_email_password;
        $ssl_enabled = ($smtp_settings->ssl_enabled == '1') ? 'true' : 'false';
        $smtp_secure = ($smtp_port == '587') ? 'tls' : 'ssl';

        $to_email = $smtp_settings->admin_email;
        if(!empty($user_email)) {
            $to_email = $user_email;
        }
        $to_email = 'bikesh@amniltech.com';
        /*phpmailer*/
        require_once(APPPATH . 'third_party/PHPMailer/PHPMailerAutoload.php');
        require_once(APPPATH . 'third_party/PHPMailer/class.phpmailer.php');
        require_once(APPPATH . 'third_party/PHPMailer/class.smtp.php');

        $mail = new PHPMailer();

        $mail->SMTPDebug = 0;                       // 0 = no output, 1 = errors and messages, 2 = messages only.

        $mail->IsSMTP();
        $mail->Mailer = 'smtp';
        $mail->SMTPAuth = $ssl_enabled;
        $mail->SMTPSecure = $smtp_secure;           // sets the prefix to the servier
        $mail->Priority    = 1;

        $mail->Host = $smtp_server;                 // sets Gmail as the SMTP server
        $mail->Port = $smtp_port;                   // set the SMTP port for the GMAIL
        $mail->Username = $username;                // Gmail username
        $mail->Password = $password;                // Gmail password

        $mail->CharSet = 'UTF-8';
        $mail->SetFrom ($username, $site->site_title);
        $mail->Subject = $subject;
        /*$mail->ContentType = 'text/html';*/
        if(!empty($file)) {
            $mail->addAttachment(urldecode($file));
        }
        $mail->IsHTML(true);

        $body .= '<tr><td style="padding: 30px;  font-size: 12px; font-weight: 400; color: #777; background-color:#fff; padding-left: 30px;">';
        $body .= 'Visit us: <a href="'.site_url().'" style="color: #FFA200; text-decoration: none;">Responsible Adventures</a>';
        $body .= '</td></tr>';
        $body .= '</tbody></table>';

        $mail->Body = $body;


        if (strpos($to_email,',') !== false) {
            $addr = explode(',',$to_email);
            foreach($addr as $ad) {
                $mail->AddAddress(trim($ad));
            }
        } else {
            $mail->AddAddress($to_email);
        }

        if(!$mail->Send()){
            $data = 'error';
           // $error_message = "Mailer Error: " . $mail->ErrorInfo;
        } else {
            $data = 'success';
            //$error_message = "Successfully sent!";
        }

        return ($data);

    }

    function getSmtpSettings() {
        return $this->db->get($this->smtp_settings)->row();
    }

    function siteDetails() {
        $this->db->select('site_title');
        $row = $this->db->get($this->site)->row();

        return (isset($row) && !empty($row)) ? $row : '';
    }
}
?>