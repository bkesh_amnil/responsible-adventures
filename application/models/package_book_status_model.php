<?php

class Package_book_status_Model extends My_Model
{

    protected $table = 'tbl_package_booking_status';
    var $package = 'tbl_package';
    var $package_departure_detail = 'tbl_package_departure_detail';
    var $booking_status = 'tbl_booking_status';
    var $package_booking = 'tbl_package_booking';

    public $id = '',
        $package_booking_id = '',
        $departure_date_id = '',
        $number_of_pax = '',
        $price_per_person = '',
        $total_price = '',
        $full_name = '',
        $email_address = '',
        $contact_number = '',
        $contact_address = '',
        $booking_status_id = '',
        $note = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->created_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'departure_date_id',
                'label' => 'Departure Date',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'number_of_pax',
                'label' => 'Number of pax',
                'rules' => 'trim|required|integer',
            ),
            array(
                'field' => 'price_per_person',
                'label' => 'Price per Person',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'total_price',
                'label' => 'Total Cost',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'full_name',
                'label' => 'Full Name',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'email_address',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email',
            ),
            array(
                'field' => 'contact_number',
                'label' => 'Contact Address',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'contact_address',
                'label' => 'Contact Nos',
                'rules' => 'trim|required',
            )
        );

        return $array;
    }

    public function getSavedData($package_booking_id, $type) {
        $this->db->select($this->table . '.*, ' . $this->package . '.id as package_id, ' . $this->package . '.name as package_name, ' . $this->package_departure_detail . '.departure_date, all_allocation, available, booked, ' . $this->booking_status . '.name as booking_status');
        $this->db->join($this->package_booking, $this->package_booking. '.id = ' . $this->table . '.package_booking_id');
        $this->db->join($this->package, $this->package . '.id = '. $this->package_booking . '.package_id');
        $this->db->join($this->package_departure_detail, $this->package_departure_detail . '.id = ' . $this->table . '.departure_date_id');
        $this->db->join($this->booking_status, $this->booking_status . '.id = ' . $this->table . '.booking_status_id');
        $this->db->where('package_booking_id', $package_booking_id);
        $this->db->order_by('id', 'DESC');
        if($type == 'logs') {
            $result = $this->db->get($this->table)->result();

            return (isset($result) && !empty($result)) ? $result : array();
        } else {
            $this->db->limit(1);
            $row = $this->db->get($this->table)->row();

            return (isset($row) && !empty($row)) ? $row : '';
        }
    }

    public function getPackageBookingCustomers() {
        $this->db->select('DISTINCT(full_name)');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}