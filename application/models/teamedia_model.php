<?php

class Teamedia_Model extends My_Model
{

    protected $table = 'tbl_team_media';

    public $id = '',
        $team_id = '',
        $type = '',
        $title = '',
        $caption = '',
        $description = '';

    public function __construct()
    {
        parent::__construct();
    }

}