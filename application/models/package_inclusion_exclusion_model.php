<?php

class Package_Inclusion_Exclusion_Model extends My_Model
{

    protected $table = 'tbl_package_inclusion_exclusion';

    public $id = '',
        $package_id = '',
        $data_type = '',
        $data = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        if($id == 0) {
            $array = array(
                array(
                    'field' => 'status',
                    'label' => 'Status',
                    'rules' => 'trim|required',
                ),
            );
        } else {
            $array = array(
                array(
                    'field' => 'data',
                    'label' => 'Package Data',
                    'rules' => 'trim|required',
                ),
            );
        }
        return $array;
    }

    public function getPackageInclusionExclustion($package_id) {
        $this->db->select('id, package_id, data_type, data, status');
        $this->db->where('package_id', $package_id);
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }

}