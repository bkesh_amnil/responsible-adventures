<?php

class Area_Model extends My_Model
{

    protected $table = 'tbl_area';

    public $id = '',
        $name = '',
        $slug = '',
        $description = '',
        $image = '',
        $meta_keywords = '',
        $meta_description = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id)
    {
        $array = array(
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique['.$this->table.'.name.'.$id.']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique['.$this->table.'.slug.'.$id.']',
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                'rules' => 'trim|required|valid_size['.$this->table.'.image]',
            )
        );

        return $array;
    }

    public function getAreas() {
        $this->db->select('id, name');
        $this->db->where('status', 1);
        $this->db->order_by('id', 'DESC');
        $result = $this->db->get($this->table)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
}