<?php

class Activity_Model extends My_Model
{

    protected $table = 'tbl_activity';

    public $id = '',
        $destination_id = '',
        $name = '',
        $slug = '',
        $image = '',
        $description = '',
        $show_in_front = '',
        $link_type = '',
        $link = '',
        $opens = '',
        $status = '';

    public function __construct()
    {
        parent::__construct();
        $this->created_timestamp = true;
        $this->updated_timestamp = true;
        $this->created_by = true;
        $this->updated_by = true;
    }

    public function rules($id, $destination_id)
    {
        $array = array(
            array(
                'field' => 'destination_id',
                'label' => 'Destination',
                'rules' => 'required|integer',
            ),
            array(
                'field' => 'name',
                'label' => 'Title',
                'rules' => 'trim|required|unique_multi['.$this->table.'.name.'.$destination_id.'.'.$id.']',
            ),
            array(
                'field' => 'slug',
                'label' => 'Alias',
                'rules' => 'trim|required|unique_multi['.$this->table.'.slug.'.$destination_id.'.'.$id.']',
            ),
            array(
                'field' => 'image',
                'label' => 'Image',
                'rules' => 'trim|required|valid_size['.$this->table.'.image]',
            ),
            array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'required'
            )
        );

        return $array;
    }

    public function getData() {
        $query = "SELECT a.*,d.name as destination_name
                  FROM " . $this->table . " AS a
                  JOIN `tbl_destination` d ON a.`destination_id` = d.`id`
                  ORDER BY a.position ASC, id DESC";
        $result = $this->db->query($query)->result();

        return (isset($result) && !empty($result)) ? $result : array();
    }
}