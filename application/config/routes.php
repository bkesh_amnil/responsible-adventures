<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

/*$route['default_controller'] = 'welcome';
$route['404_override'] = '';*/
/* Front End Routes */
$route['default_controller'] = "home";
$route['download/(:any)'] = "download/index/$1";
$route['booking/package'] = "booking/package/$1";

$route['search/experience'] = "search/experience/$1";
$route['search/packageexperience'] = "search/packageexperience/$1";
$route['search/package'] = "search/package/$1";
$route['search'] = "search/index/$1";

require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();
$query = $db->get('tbl_destination');
$result = $query->result();
if($result) {
    foreach($result as $sites) {
        $route[$sites->slug.'/(:any)'] = 'content/destinationexperience/$1';
        $route[$sites->slug.'/(:any)/(:any)'] = 'content/destinationexperience/$1/$2';
    }
}

$route['package/(:any)'] = "content/package/$1";
$route['booking/(:any)'] = "content/booking/$1";
$route['bookingm/(:any)'] = "content/bookingm/$1";
$route['enquire/(:any)'] = "content/enquire/$1";

$route['dynamic_form/package_booking'] = "dynamic_form/package_booking/$1";
$route['dynamic_form/package_enquiry'] = "dynamic_form/package_enquiry/$1";
$route['dynamic_form/mail_pdf'] = "dynamic_form/mail_pdf/$1";
$route['dynamic_form/(:any)'] = "dynamic_form/form_submit/$1";

$route['getPackageBookingDate/(:any)'] = "content/getPackageBookingDates/$1";
$route['content/load_more'] = "content/load_more/$1";
$route['404_override'] = "content/index/$1";
/* Front End Routes */

/* Admin End Routes */
$route[BACKENDFOLDER] = BACKENDFOLDER.'/user/login';
$route[BACKENDFOLDER.'/login'] = BACKENDFOLDER.'/user/login';
$route[BACKENDFOLDER.'/logout'] = BACKENDFOLDER.'/user/logout';
$route[BACKENDFOLDER.'/retrieve-password'] = BACKENDFOLDER.'/user/getPassword';
/* Admin End Routes */

/* End of file routes.php */
/* Location: ./application/config/routes.php */