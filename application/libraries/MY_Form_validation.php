<?php

class MY_Form_validation extends CI_Form_validation {

	function unique($value, $params) {

		$CI =& get_instance();

		$CI->form_validation->set_message('unique',	'The %s is already being used.');

		list($table, $field, $cur_id) = explode(".", $params, 3);
	
		$cur_id = (int)$cur_id;
		if($cur_id)
			$CI->db->where('id != ', $cur_id);
			
		$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();

		if ($query->row()) {
			return false;
		} else {
			return true;
		}
	}

	function unique_multi($value, $params) {
		$CI =& get_instance();

		$CI->form_validation->set_message('unique_multi', 'The %s is already being used.');

		list($table, $field, $foreign_id, $cur_id) = explode(".", $params, 4);

		$cur_id = (int)$cur_id;
		if($cur_id) {
			$CI->db->where('id != ', $cur_id);
		}
		$CI->db->where('destination_id', $foreign_id);
		$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();

		if ($query->row()) {
			return false;
		} else {
			return true;
		}
	}

	function valid_google_url($value) {
		$CI =& get_instance();

		$CI->form_validation->set_message('valid_google_url',	'The %s is not valid.');

		if (strpos($value,'http://goo.gl/forms/') !== false) {
			return true;
		} else {
			return false;
		}
	}

	function valid_size($value, $params) {
		$CI =& get_instance();

		list($table, $field) = explode(".", $params, 2);

		switch($table) {
			case "tbl_banner":
				$width = '1920';
				$height = '794';
				break;
			case "tbl_destination":
				switch($field) {
					case "image":
						$width = '285';
						$height = '285';
						break;
				}
				break;
			case "tbl_activity":
				$width = '570';
				$height = '570';
				break;
			case "tbl_package":
				$width = '570';
				$height = '350';
				break;
			case "tbl_testimonial":
				$width = '120';
				$height = '120';
				break;
			case "tbl_experience":
				switch($field) {
					case "image":
						$width = '488';
						$height = '350';
						break;
					/*case "icon":
						$width = '69';
						$height = '44';
						break;*/
				}
				break;
			case "tbl_content":
				switch($field){
					case "image":
						$width = '188';
						$height = '188';
						break;
					case "background_image":
						$width = '1920';
						$height = '300';
						break;
				}
				break;
			case "tbl_team":
				$width = '690';
				$height = '350';
				break;
			case "tbl_gallery":
				$width = '1920';
				$height = '1323';
				break;
			case "tbl_area":
				$width = '570';
				$height = '350';
				break;
			case "tbl_news":
				$width = '1024';
				$height = '683';
				break;
		}
		
		$img_size = getimagesize($value);
		$img_width = $img_size[0];
		$img_height = $img_size[1];

		if($img_width > $width || $img_height > $height) {
			$CI->form_validation->set_message('valid_size', 'The %s size is bigger');
			return false;
		} else if($img_width < $width || $img_height < $height) {
			$CI->form_validation->set_message('valid_size', 'The %s size is smaller');
			return false;
		} else {
			return true;
		}
	}

	function valid_file($value, $params) {
		$CI =& get_instance();

		list($table, $field) = explode(".", $params, 2);

		$file_ext = pathinfo(base_url($value), PATHINFO_EXTENSION);

		switch($table) {
			case "tbl_package":
				switch($field) {
					case "file":
						if($file_ext != 'pdf') {
							$CI->form_validation->set_message('valid_file', 'File isn\'t pdf');
							return false;
						} else {
							return true;
						}
					break;
					case "map_file":
						if($file_ext != 'kml') {
							$CI->form_validation->set_message('valid_file', 'Map File isn\'t kml');
							return false;
						} else {
							return true;
						}
					break;
				}
			break;
		}
		die;
	}

	function valid_file_size($value, $params) {
		$CI =& get_instance();

		list($table, $field) = explode(".", $params, 2);

		$file_size = (filesize($_SERVER["DOCUMENT_ROOT"] . '/ra/' . $value) * .0009765625) * .0009765625;

		if($file_size > 10) {
			$CI->form_validation->set_message('valid_file_size', 'File size should be less than or equal to 10MB');
			return false;
		} else {
			return true;
		}
	}
	
	function exists($value, $params) {

		$CI =& get_instance();

		$CI->form_validation->set_message('exists',	'The %s does not exist.');

		list($table, $field) = explode(".", $params, 2);
	
		$query = $CI->db->select($field)->from($table)->where($field, $value)->limit(1)->get();
	
		if ( ! $query->row()) {
			return false;
		} else {
			return true;
		}
	}
	
	//calling:: fixed_values[Active,Inactive]
	function fixed_values($value, $valid_values = '')
	{
		$this->CI->form_validation->set_message('fixed_values',	'The %s is invalid.');
		
		if($valid_values == '') {		
			$array = array('Yes', 'No');
		} else {
			$array = explode(',', $valid_values);
		}
		
		return (in_array(trim($value), $array)) ? TRUE : FALSE;
	}
	
	function url($value)
	{
		
	}
}
?>