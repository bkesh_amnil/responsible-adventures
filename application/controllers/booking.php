<?php

Class Booking extends BASE_Controller{

    function __construct() {
        parent::__construct();
    }

    function package() {
        $post = $_POST;

        if(isset($post) && !empty($post)) {
            $data = '';
            $data['package_number_of_pax'] = '';
            $data['package_unit_price'] = '';
            $data['all_allocation'] = '';
            $data['available'] = '';
            $data['booked'] = '';
            $data['package_is_fixed_departure_id'] = '';
            $data['package_is_fixed_departure'] = '';


            $date_detail = $this->public_model->getDateDetail($post['sel_date'], $post['package_id']);

            if($post['nos_of_pax'] != '0') {
                $price_detail = $this->public_model->getPriceDetail($post['nos_of_pax'], $post['sel_date'], $post['package_id']);
            }

            if(isset($date_detail) && !empty($date_detail)) {
                $data['all_allocation'] = $date_detail->all_allocation;
                $data['available'] = $date_detail->available;
                $data['booked'] = $date_detail->booked;
                $data['package_is_fixed_departure_id'] = $date_detail->is_fixed_departure;
                $data['package_is_fixed_departure'] = ($date_detail->is_fixed_departure == '1') ? 'Book Now' : 'Enquire';
            }

            if(isset($price_detail) && !empty($price_detail)) {
                $data['package_unit_price'] = $price_detail->unit_price;
            }

            echo json_encode($data);
            die;
        }
    }

}
?>