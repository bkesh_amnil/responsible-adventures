<?php

Class Content extends BASE_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $url = $this->uri->segment_array();
        $count = count($url);
        $alias = $this->uri->segment($count);

        $content = 1;
        $contact = $form = 0;
        $item_per_page = 2;

        $data['active_menu'] = $this->public_model->getActiveMenu($this->uri->segment($count));

        if ($count > 2) {
            $data['active_menu'] = $this->uri->segment($count - 2);
        } else if ($count > 1) {
            $data['active_menu'] = $this->uri->segment($count - 1);
        }

        if (empty($data['active_menu'])) {
            show_404();
        }
        if (($data['active_menu'])=='booking') {
            show_404();
        }

        $data['count'] = $count;
        $data['breadcrumb'] = $this->public_model->getBreadCrumb($url);

        $data['item_per_page'] = $item_per_page;

        if ($count > 1) {
            if ($this->uri->segment(1) == 'video') {
                show_404();
            }
            $type = $this->uri->segment($count - 1);
            if ($count > 2) {
                if ($this->uri->segment(1) == 'photo') {
                    show_404();
                }
                if ($count > 3) {
                    show_404();
                } else {
                    $data['module_name'] = $this->uri->segment($count - 2);
                    if ($data['module_name'] != 'news') {
                        show_404();
                    }
                    if (is_numeric($this->uri->segment($count)) && is_numeric($this->uri->segment($count - 1))) {
                        $data['content'] = $this->public_model->getNewsListbyDate($this->uri->segment($count - 1), $this->uri->segment($count));
                    } else {
                        if ($this->uri->segment($count - 1) != 'category' && $this->uri->segment($count - 1) != 'tags') {
                            show_404();
                        }
                        $data['content'] = $this->public_model->getNewsListbyCategory($this->uri->segment($count), $this->uri->segment($count - 1));
                    }
                    $data['recent_news'] = $this->public_model->getRecentNews(5);
                    $data['archive_news'] = $this->public_model->getArchives();
                    $data['category_news'] = $this->public_model->getNewsCategories();
                }
            } else {
                if ($type != 'team') { // gallery / video page 
                    if ($this->uri->segment($count - 1) == 'about-us') {
                        show_404();
                    }
                    $moduleid = $this->public_model->getModuleId($this->uri->segment($count - 1));
                    $data_id = $this->public_model->getDataId($alias, $this->uri->segment($count - 1));
                    if (isset($moduleid) && !empty($moduleid) && isset($data_id) && !empty($data_id)) {
                        $data['social_datas'] = $this->public_model->getSocialData($moduleid->id, $data_id->id);
                    }
                    switch ($type) {
                        case "photo";
                            $data['module_name'] = 'photo';
                            $data['content'] = $this->public_model->getGalleryImages($alias);
                            if (empty($data['content'])) {
                                show_404();
                            }
                            break;
                        case "testimonial":
                            $data['module_name'] = $type;
                            $data['content'] = $this->public_model->getTestimonials($alias);
                            if (empty($data['content'])) {
                                show_404();
                            }
                            break;
                        case "news":
                            $data['module_name'] = $type;
                            $data['content'] = $this->public_model->getNewsEventsDesc($alias, 'news');
                            if (empty($data['content'])) {
                                show_404();
                            }
                            $data['recent_news'] = $this->public_model->getRecentNews(5);
                            $data['archive_news'] = $this->public_model->getArchives();
                            $data['category_news'] = $this->public_model->getNewsCategories();
                            break;
                        default:
                            show_404();
                            break;
                    }
                } else { /* team page */
                    $menuid = $this->public_model->getMenuId($type);
                    $data['modules'] = $modules = $this->public_model->getModules($menuid->id);
                    $data_id = $this->public_model->getDataId($alias, $this->uri->segment($count - 1));
                    if (empty($data_id)) {
                        show_404();
                    }
                    $data['social_datas'] = $this->public_model->getSocialData($modules[0]['module_id'], $data_id->id);

                    $data['module_name'] = $modules[0]['module'];
                    $data['content'] = $modules[0]['data'];
                }
            }
        } else {
            $menuid = $this->public_model->getMenuId($alias);
            $data['modules'] = $modules = $this->public_model->getModules($menuid->id);

            if (!empty($modules) && !empty($modules[0]['data'])) {
                if ($modules[0]['module_id'] != '35') {
                    $data['social_datas'] = $this->public_model->getSocialData($modules[0]['module_id'], $modules[0]['data'][0]->id);
                }
            }

            if (isset($modules) && !empty($modules)) {
                if ($modules[0]['module_title'] == 'contact') {
                    $data['module_name'] = 'contacts';
                    $data['testimonials'] = $this->public_model->getTestimonials();
                    $contact = 1;
                    $data['formdata'] = $modules[0]['data'][0];
                    $data['content'] = $modules[1]['data'];
                } else {
                    $data['content'] = '';
                    $data['module_name'] = $modules[0]['module'];

                    $data['content'] = $modules[0]['data'];
                    $count_data = count($data['content']);
                    /* $total_pages = ceil($count_data/$item_per_page);
                      $data['total_pages'] = $total_pages;
                      $data['item_per_page'] = $item_per_page; */

                    switch ($modules[0]['module']) {
                        case 'package':
                            $this->load->model('destination_model');
                            $data['destinations'] = $this->destination_model->getAllDestinations();
                            $data['experiences'] = $this->public_model->getExperiences();
                            $data['years'] = $this->public_model->getPackageDepartureYear();
                            $data['months'] = $this->public_model->getPackageDepartureMonth();
                            break;
                        case 'news':
                            $data['recent_news'] = $this->public_model->getRecentNews(5);
                            $data['archive_news'] = $this->public_model->getArchives();
                            $data['category_news'] = $this->public_model->getNewsCategories();
                        //echo '<pre>'; print_r($data['archive_news']);die;
                        default:
                            break;
                    }
                }
            } else {
                /* destination page related to experience */
                $data['module_name'] = $data['active_menu'];
                $data['destinations'] = $this->public_model->getDestinations();
                $data['search_experiences'] = $this->public_model->getExperiences();
                $data['destination_detail'] = $this->public_model->getDetinationDetail($menuid->id);
                $data['destination_sliders'] = $this->public_model->getDestinationSliders($menuid->id);
                $data['experiences'] = $this->public_model->getDestinationRelatedExperiences($menuid->id);

                $this->template['content'] = $this->load->view('page', $data, TRUE);
            }
        }

        if ($content) {
            if ($contact) {
                $data['facebook'] = $this->template['facebook'];
                $data['gplus'] = $this->template['gplus'];
                $data['youtube'] = $this->template['youtube'];
                $data['twitter'] = $this->template['twitter'];
                $data['instagram'] = $this->template['instagram'];
                $data['pinterest'] = $this->template['pinterest'];
                $data['linkedin'] = $this->template['linkedin'];
                $data['vimeo'] = $this->template['vimeo'];
                $this->template['content'] = $this->load->view('contact', $data, TRUE);
            } else {
                $this->template['content'] = $this->load->view('page', $data, TRUE);
            }
        } else {
            if ($form) {
                $this->template['content'] = $this->load->view('form', $data, TRUE);
            } else {
                $this->template['content'] = $this->load->view('page', $data, TRUE);
            }
        }
    }

    /* load more */

    function load_more() {
        $post = $_POST;
        $html['data'] = '';
        $html['ids'] = explode(',', $post['ids']);
        $destination_post = isset($post['destination']) ? $post['destination'] : 0;
        $experience_post = isset($post['experience']) ? $post['experience'] : 0;

        $result = $this->public_model->getMoreData($destination_post, $experience_post, $post['ids']);

        if (isset($result) && !empty($result)) {
            $count = count($html['ids']);
            foreach ($result as $ind => $val) {
                $cur_date = date('Y-m-d');
                /* $query = "SELECT unit_price FROM tbl_package_price
                  WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $val->id .")
                  AND CURDATE() BETWEEN valid_from AND valid_to
                  AND package_id =" . $val->id; */
                $query = "SELECT unit_price FROM tbl_package_price
                                WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $val->id . ")
                                AND '" . $cur_date . "' BETWEEN valid_from AND valid_to
                                AND package_id =" . $val->id;
                $row = $this->db->query($query)->row();

                array_push($html['ids'], $val->id);
                $divClass = '';
                if ($count % 2 != 0) {
                    if ($ind % 2 == 0) {
                        $divClass = ' pull-right';
                    }
                } else {
                    if ($ind % 2 != 0) {
                        $divClass = ' pull-right';
                    }
                }
                $html['data'] .= '<div class="tour-package-wrap"><div class="row">';
                $html['data'] .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 ' . $divClass . '">';
                $html['data'] .= '<div class="featured-experiences-img-wrapper">';
                $html['data'] .= '<img src="' . base_url($val->cover_image) . '" alt="' . $val->name . '"/>';
                $html['data'] .= '<div class="trek-short-desc"><p>' . $val->short_description . '</p></div>';
                $html['data'] .= '</div></div>';
                $html['data'] .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><div class="featured-experiences-detail"><div class="align-middle">';
                $html['data'] .= '<h2>' . $val->name . '</h2>';
                $html['data'] .= '<span>Duration: ' . $val->duration . ' days</span>';
                $html['data'] .= '<span>Minimum Pax: ' . $val->minimum_group_size . ' people</span>';
                if (!empty($row->unit_price)) {
                    $html['data'] .= '<span>Price Starting from: US$ ' . $row->unit_price . '</span>';
                }
                $html['data'] .= '<span class="trek-difficulty-level">Difficulty: ' . $val->difficulty_name . '</span>';
                $html['data'] .= '<a class="btn-all btn-book-now" rel="' . $val->slug . '" href="javascript:void();">Book Now</a>';
                $html['data'] .= '<a class="btn-all" href="' . site_url('package/' . $val->slug) . '">Read More</a>';
                $html['data'] .= '</div></div></div></div></div>';
            }
        } else {
            $html['data'] .= 'empty';
        }
        echo json_encode($html);
        die;
    }

    /* load more */

    function getPackageBookingDates($package_id) {
        $result = $this->public_model->getPackageDepartureDates($package_id);
        if (isset($result) && !empty($result)) {
            foreach ($result as $val) {
                $array = array(
                    'id' => $val->id,
                    'title' => $val->all_allocation . ' seats',
                    'allocation' => $val->all_allocation,
                    'booked' => $val->booked,
                    'available' => $val->available,
                    'start' => $val->departure_date
                );

                $output_arrays[] = $array;
            }

            echo json_encode($output_arrays);
            die;
        }
        return array();
    }

    function destinationexperience($experience_category, $experience = '', $extra = '') {
        if ($extra != '') {
            show_404();
        }
        $url = $this->uri->segment_array();
        $count = count($url);
        $item_per_page = 2;
        $data['active_menu'] = 'destination';
        $location = $this->uri->segment(1);
        $data['count'] = $count + 1; //# got to do this as page is rendered on base of count.
        $data['breadcrumb'] = '<li>Destination</li><li><a href="' . site_url() . $location . '">' . ucfirst($location) . '</a></li>';
        $data['item_per_page'] = $item_per_page;
        $data['module_name'] = 'destination';
        $menuid = $this->public_model->getMenuId($this->uri->segment(1));
        $data['destination_sliders'] = $this->public_model->getDestinationSliders($menuid->id);
        if ($experience != '') {
            $this->load->model('destination_model');
            $data['destination_id'] = $this->destination_model->get_single_data('tbl_destination', 'id', $this->uri->segment(1), 'slug');
            $data['destinations'] = $this->destination_model->getAllDestinations();
            $data['experiences'] = $this->public_model->getExperiences($data['destination_id']);
            $data['years'] = $this->public_model->getPackageDepartureYear();
            $data['months'] = $this->public_model->getPackageDepartureMonth();
            $data['content'] = $this->public_model->getPackages($this->uri->segment(1), $this->uri->segment(2), $this->uri->segment(3));
            if (empty($data['content'])) {
//                show_404();
            }
            $data['experience_slug'] = $this->uri->segment(2);
            $data['breadcrumb'] .= '<li><a href="' . site_url() . $location . '/' . $experience_category . '">' . ucfirst($experience_category) . '</a></li><li><a class="active">' . ($this->template['page_title']) . '</a> </li>';
        } else {
            $data['areas'] = $this->public_model->getPackageAreas($this->uri->segment(1), $this->uri->segment(2));
            if (empty($data['areas'])) {
//                show_404();
            }
            $data['destinations'] = $this->public_model->getDestinations();
            $data['destination_detail'] = $this->public_model->getDetinationDetail($menuid->id);
            $data['search_experiences'] = $this->public_model->getExperiences();
            $data['breadcrumb'] .= '<li><a class="active">' . ucfirst($experience_category) . '</a> </li>';
        }
        $this->template['content'] = $this->load->view('page', $data, TRUE);
    }

    function package($package_slug, $extra = '') {
        if (!isset($package_slug)) {
            show_404();
        }
        if ($extra != '') {
            show_404();
        }
        $data['count'] = 5; //# got to do this as page is rendered on base of count.
        $data['module_name'] = $data['package_slug'] = $package_slug;
        $package_row = $this->public_model->getPackageData($package_slug);
        if ($package_row == '') {
            show_404();
        }
        $data['breadcrumb'] = '<li>Destination</li><li><a href="' . site_url() . $package_row->destination_slug . '">' . ($package_row->destination_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a href="' . site_url() . $package_row->destination_slug . '/' . $package_row->experience_slug . '">' . ($package_row->experience_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a href="' . site_url() . $package_row->destination_slug . '/' . $package_row->experience_slug . '/' . $package_row->area_slug . '">' . ($package_row->area_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a class="active">' . ucfirst($package_row->package_name) . '</a> </li>';
        $data['active_menu'] = 'package';
        $data['package_description'] = $this->public_model->getPackageDescription($package_row->destination_slug, $package_row->experience_slug, $package_row->area_slug, $package_slug);
        if (isset($data['package_description']) && !empty($data['package_description'])) {
            $data['social_datas'] = $this->public_model->getSocialData(44, $data['package_description']['info']->id);
        }
//        debug( $this->template);
        $this->template['content'] = $this->load->view('page', $data, TRUE);
    }

    function booking_page($package_slug, $extra = '') {
        if (!isset($package_slug)) {
            show_404();
        }
        if ($extra != '') {
            show_404();
        }
        $data['count'] = 6; //# got to do this as page is rendered on base of count.
        $data['module_name'] = 'book';
        $package_row = $this->public_model->getPackageData($package_slug);
        if ($package_row == '') {
            show_404();
        }
        if (isset($_POST) && !empty($_POST)) {
            $data['selected_date'] = date('Y-m-d', strtotime($_POST['date']));
            $data['selected_all_allocation'] = $_POST['allocation'];
            $data['selected_booked'] = $_POST['booked'];
            $data['selected_available'] = $_POST['available'];
        }
        $data['breadcrumb'] = '<li>Destination</li><li><a href="' . site_url() . $package_row->destination_slug . '">' . ($package_row->destination_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a href="' . site_url() . $package_row->destination_slug . '/' . $package_row->experience_slug . '">' . ($package_row->experience_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a href="' . site_url() . $package_row->destination_slug . '/' . $package_row->experience_slug . '/' . $package_row->area_slug . '">' . ($package_row->area_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a href="' . site_url() . 'package/' . $package_row->package_slug . '">' . ucfirst($package_row->package_name) . '</a> </li>';
        $data['breadcrumb'] .= '<li><a class="active">Book</a> </li>';
        $data['active_menu'] = 'package';
        $data['package_description'] = $this->public_model->getPackageDescription($package_row->destination_slug, $package_row->experience_slug, $package_row->area_slug, $package_slug);
        if (isset($data['package_description']) && !empty($data['package_description'])) {
            $data['social_datas'] = $this->public_model->getSocialData(44, $data['package_description']['info']->id);
        }
        $this->template['content'] = $this->load->view('page', $data, TRUE);
    }

    function bookingm($package_slug, $extra = '') {
        if (!$this->input->is_ajax_request()) {
            redirect(site_url(), 'refresh');
        }
        if (!isset($package_slug)) {
            $return = array(
                'status' => 'error',
                'data' => 'error'
            );
        }
        if ($extra != '') {
            $return = array(
                'status' => 'error',
                'data' => 'error'
            );
        }
        $data['count'] = 6; //# got to do this as page is rendered on base of count.
        $data['module_name'] = 'book';
        $package_row = $this->public_model->getPackageData($package_slug);
        if ($package_row == '') {
            $return = array(
                'status' => 'error',
                'data' => 'error'
            );
        }
//        if (isset($_POST) && !empty($_POST)) {
//            if (isset($_POST['timestamp']) && $_POST['timestamp'] == 'no') {
//                $data['selected_date'] = date('Y-m-d', strtotime($_POST['date']));
//            } else {
//                $data['selected_date'] = date('Y-m-d', ($_POST['date'] / 1000));
//            }
//            $data['selected_all_allocation'] = $_POST['allocation'];
//            $data['selected_booked'] = $_POST['booked'];
//            $data['selected_available'] = $_POST['available'];
//        }


        $data['current_month'] = 'yes';
        $data['package_description'] = $pd = $this->public_model->getPackageDescription($package_row->destination_slug, $package_row->experience_slug, $package_row->area_slug, $package_slug);
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['current_calender_month']) && $_POST['current_calender_month'] != '') {
                $current_month = substr($_POST['current_calender_month'], 0, 7);
                $departure_dates = $pd['departure_dates'];
                $departure_months = array();
                foreach ($departure_dates as $departure_date) {
                    $departure_months[] = substr($departure_date->departure_date, 0, -3);
                }
                if (!in_array($current_month, $departure_months)) {
                    $data['current_month'] = 'no';
                }

                $data['bkesh'] = $departure_months;
                $data['bkesh xxx'] = $current_month;
            } else {
                if (isset($_POST['timestamp']) && $_POST['timestamp'] == 'no') {
                    $data['selected_date'] = date('Y-m-d', strtotime($_POST['date']));
                } else {
                    $data['selected_date'] = date('Y-m-d', ($_POST['date'] / 1000));
                }
                $data['selected_all_allocation'] = $_POST['allocation'];
                $data['selected_booked'] = $_POST['booked'];
                $data['selected_available'] = $_POST['available'];
            }
        }
        $return = array(
            'status' => 'success',
            'data' => $data
        );
        echo json_encode($return);
        exit;
    }

    function enquire_page($package_slug, $extra = '') {
        if (!isset($package_slug)) {
            show_404();
        }
        if ($extra != '') {
            show_404();
        }
        $data['count'] = 6; //# got to do this as page is rendered on base of count.

        $data['module_name'] = 'enquire';

        $package_row = $this->public_model->getPackageData($package_slug);
        if ($package_row == '') {
            show_404();
        }
        $data['breadcrumb'] = '<li>Destination</li><li><a href="' . site_url() . $package_row->destination_slug . '">' . ($package_row->destination_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a href="' . site_url() . $package_row->destination_slug . '/' . $package_row->experience_slug . '">' . ($package_row->experience_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a href="' . site_url() . $package_row->destination_slug . '/' . $package_row->experience_slug . '/' . $package_row->area_slug . '">' . ($package_row->area_name) . '</a></li>';
        $data['breadcrumb'] .= '<li><a href="' . site_url() . 'package/' . $package_row->package_slug . '">' . ucfirst($package_row->package_name) . '</a> </li>';
        $data['breadcrumb'] .= '<li><a class="active">Enquire</a> </li>';
        $data['active_menu'] = 'package';
        $data['package_description'] = $this->public_model->getPackageDescription($package_row->destination_slug, $package_row->experience_slug, $package_row->area_slug, $package_slug);
        if (isset($data['package_description']) && !empty($data['package_description'])) {
            $data['social_datas'] = $this->public_model->getSocialData(44, $data['package_description']['info']->id);
        }
        $this->template['content'] = $this->load->view('page', $data, TRUE);
    }

    function enquire($package_slug, $extra = '') {
        if (!$this->input->is_ajax_request()) {
            redirect(site_url(), 'refresh');
        }
        if (!isset($package_slug)) {
            $return = array(
                'status' => 'error',
                'data' => 'error'
            );
        }
        if ($extra != '') {
            $return = array(
                'status' => 'error',
                'data' => 'error'
            );
        }
        $data['count'] = 6; //# got to do this as page is rendered on base of count.
        $data['module_name'] = 'enquire';
        $package_row = $this->public_model->getPackageData($package_slug);
        if ($package_row == '') {
            $return = array(
                'status' => 'error',
                'data' => 'error'
            );
        }
        $data['package_description'] = $this->public_model->getPackageDescription($package_row->destination_slug, $package_row->experience_slug, $package_row->area_slug, $package_slug);
        $return = array(
            'status' => 'success',
            'data' => $data
        );
        echo json_encode($return);
        exit;
    }

}
