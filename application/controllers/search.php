<?php

Class Search extends BASE_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $q0 = false;
        $q = isset($_GET['query'])?$_GET['query']:'';
        $str['query'] = $q;
        if($q == ''){
            show_404();
        }

        $result = $this->public_model->getSearchResult($q);

        if (!empty($result)) {
            $q0 = true;
            foreach ($result as $ind => $val) {
                foreach ($val as $values) {
                    /* if($ind == 'content') {
                      $key = site_url($ind);
                      } else { */
                    switch ($ind) {
                        case "experience":
                            $key = site_url($values->destination_slug . '/' . $values->slug);
                            break;
                        case "area":
                            $key = site_url($values->destination_slug . '/' . $values->experience_slug . '/' . $values->slug);
                            break;
                        case "package":
                            $key = site_url('package/' . $values->slug);
                            break;
                        case "news"://search by tag
                            $key = site_url('news/' . $values->slug);
                            break;
                        default:
                            $key = site_url($values->slug);
                            break;
                    }
                    // }
                    /* if(isset($values->slug)) {
                      $key = site_url($ind . '/' . $values->slug);
                      } */
                    if ($ind == 'experience') {
                        $value = ucwords($values->name) . '|' . ucfirst($ind) . '|' . ucwords($values->destination_slug);
                    } else {
                        $value = ucwords($values->name) . '|' . ucfirst($ind);
                    }

                    $str['suggestions'][] = $value;
                    $str['data'][] = $key;
                }
            }
        }
        if ($q0 == false) {
            $str['suggestions'][] = "No Search Result Found";
            $str['data'][] = 0;
        }

        echo json_encode($str);
        die;
    }

    function experience() {
        $post = $_POST;
        if (isset($post) && !empty($post)) {
            $html = "";
            $html .= "<option value='0'>Choose your destination</option>";

            $result = $this->public_model->getExperiences($post['destination_id']);

            if (isset($result) && !empty($result)) {
                if ($post['destination_id'] != 0) {
                    foreach ($result as $val) {
                        $html .= "<option value=" . $val->id . ">" . $val->name . "</option>";
                    }
                } else {
                    foreach ($result as $val) {
                        $html .= "<option value" . $val->slug . ">" . $val->name . "</option>";
                    }
                }
            }

            echo json_encode($html);
            die;
        }
    }

    function packageexperience() {
        $post = $_POST;
        if (isset($post) && !empty($post)) {
            $html['data'] = '';
            $html['search_data'] = '<option>All</option>';
            $html['ids'] = array();

            $result = $this->public_model->getMoreData($post['destination_id'], $post['experience_id'], '');
            $result1 = $this->public_model->getFeaturedPackages($post['destination_id'], $post['experience_id'], 'search');

            if (isset($result) && !empty($result)) {
                foreach ($result as $ind => $val) {
                    $cur_date = date('Y-m-d');
                    /* $query = "SELECT unit_price FROM tbl_package_price
                      WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $val->id .")
                      AND CURDATE() BETWEEN valid_from AND valid_to
                      AND package_id =" . $val->id; */
                    $query = "SELECT unit_price FROM tbl_package_price
                                    WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $val->id . ")
                                    AND '" . $cur_date . "' BETWEEN valid_from AND valid_to
                                    AND package_id =" . $val->id;
                    $row = $this->db->query($query)->row();

                    array_push($html['ids'], $val->id);
                    //if($ind == $total_pages - 1) {break;}
                    $divClass = '';
                    if ($ind % 2 != '') {
                        $divClass = ' pull-right';
                    }
                    $html['data'] .= '<div class="tour-package-wrap"><div class="row">';
                    $html['data'] .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6' . $divClass . '">';
                    $html['data'] .= '<div class="featured-experiences-img-wrapper">';
                    $html['data'] .= '<img src="' . base_url($val->cover_image) . '" alt="' . $val->name . '"/>';
                    $html['data'] .= '<div class="trek-short-desc">';
                    $html['data'] .= '<p>' . $val->short_description . '</p>';
                    $html['data'] .= '</div></div></div>';
                    $html['data'] .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">';
                    $html['data'] .= '<div class="featured-experiences-detail">';
                    $html['data'] .= '<div class="align-middle">';
                    $html['data'] .= '<h2>' . $val->name . '</h2>';
                    $html['data'] .= '<span>Duration: ' . $val->duration . ' days</span>';
                    $html['data'] .= '<span>Minimum Pax: ' . $val->minimum_group_size . ' people</span>';
                    if (!empty($row->unit_price)) {
                        $html['data'] .= '<span>Price Starting from: US$ ' . $row->unit_price . '</span>';
                    }
                    $html['data'] .= '<span class="trek-difficulty-level">Difficulty: ' . $val->difficulty_name . '</span>';
                    $html['data'] .= '<a class="btn-all btn-book-now" rel="' . $val->slug . '" href="javascript:void();">Book Now</a>';
                    $html['data'] .= '<a class="btn-all" href="' . site_url('package/' . $val->slug) . '">Read More</a>';
                    $html['data'] .= '</div></div></div></div></div>';
                }
            } else {
                $html['data'] .= 'empty';
            }

            if (isset($result1) && !empty($result1)) {
                foreach ($result1 as $value) {
                    $html['search_data'] .= '<option rel="' . site_url('package/' . $value->slug) . '">' . $value->name . '</option>';
                }
            }

            echo json_encode($html);
            die;
        }
    }

    function package() {
        $post = $_POST;
        $html['data'] = '';
        $html['count'] = 0;

        $result = $this->public_model->getSearchPackage($post['location_id'], $post['experience_id'], $post['order_id'], $post['year_id'], $post['month_id']);
        if (isset($result) && !empty($result)) {
            $html['count'] = count($result);
            foreach ($result as $val) {
                $cur_date = date('Y-m-d');
                /* $query = "SELECT unit_price FROM tbl_package_price
                  WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $val->id .")
                  AND CURDATE() BETWEEN valid_from AND valid_to
                  AND package_id =" . $val->id; */
                $query = "SELECT unit_price FROM tbl_package_price
                                WHERE min_pax = (SELECT MIN(min_pax) FROM tbl_package_price WHERE package_id = " . $val->id . ")
                                AND '" . $cur_date . "' BETWEEN valid_from AND valid_to
                                AND package_id =" . $val->id;
                $row = $this->db->query($query)->row();

                $html['data'] .= '<div class="tour-package-wrap"><div class="row">';
                $html['data'] .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><div class="featured-experiences-img-wrapper">';
                $html['data'] .= '<img src=' . base_url($val->cover_image) . ' alt=' . $val->name . '/>';
                $html['data'] .= '<div class="trek-short-desc"><p>' . $val->short_description . '</p></div>';
                $html['data'] .= '</div></div>';
                $html['data'] .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"><div class="featured-experiences-detail">';
                $html['data'] .= '<div class="align-middle">';
                $html['data'] .= '<h2>' . $val->name . '</h2>';
                $html['data'] .= '<span>Duration: ' . $val->duration . ' days</span>';
                $html['data'] .= '<span>Minimum Pax: ' . $val->minimum_group_size . ' people</span>';
                if (!empty($row->unit_price)) {
                    $html['data'] .= '<span>Price Starting from: US$ ' . $row->unit_price . '</span>';
                }
                $html['data'] .= '<span class="trek-difficulty-level">Difficulty: ' . $val->difficulty_name . '</span>';
                $html['data'] .= '<a class="btn-all btn-book-now" rel="' . $val->slug . '" href="javascript:void();">Book Now</a>';
                $html['data'] .= '<a class="btn-all" href="' . site_url('package/' . $val->slug) . '">Read More</a>';
                $html['data'] .= '</div></div></div></div></div>';
            }
        } else {
            $html['data'] .= '<div class="tour-package-wrap"><p>Search Result not found</p></div>';
        }

        echo json_encode($html);
        die;
    }

}

?>