<?php

class Package_price extends My_Controller
{
    var $table = 'tbl_package_price';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_price_model', 'package_price');
        $this->data['module_name'] = 'Package Price Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {

        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($id = '')
    {
          if($this->data['activeModulePermission']['view']) {
            $package_id = $this->uri->segment(3);
            $package_name = $this->package_price->get_single_data('tbl_package', 'name', $package_id, 'id');
            $this->data['sub_module_name'] = 'Package Price List for <b>' . $package_name .'</b>';
            $this->data['rows'] = $this->package_price->getPackagePrice($package_id);
            $this->data['body'] = BACKENDFOLDER.'/package_price/_list';
            $this->render();
          } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $package_id = segment(4);
        $package_name = $this->package_price->get_single_data('tbl_package', 'name', $package_id, 'id');
        $id = segment(5);
        $this->data['package_id'] = $package_id;
        if($_POST) {
            $post = $_POST;
            $this->package_price->id = $id;
            $this->form_validation->set_rules($this->package_price->rules($id));
            if($this->form_validation->run()) {
                if ($id == 0) {
                    $package_description_arr = $post['hidden_package_description'];
                    $package_valid_from_arr = $post['hidden_package_valid_from'];
                    $package_valid_to_arr = $post['hidden_package_valid_to'];
                    $package_num_of_pax = $post['hidden_package_nos_of_person'];
                    $package_unit_price = $post['hidden_package_unit_price'];

                    foreach ($package_valid_from_arr as $ind => $val) {
                        $num_of_pax = explode('-', $package_num_of_pax[$ind]);
                        $insert_batch[] = array(
                            'package_id' => $package_id,
                            'description' => $package_description_arr[$ind],
                            'valid_from' => $val,
                            'valid_to' => $package_valid_to_arr[$ind],
                            'number_of_pax' => $package_num_of_pax[$ind],
                            'min_pax' => $num_of_pax[0],
                            'max_pax' => !empty($num_of_pax[1]) ? $num_of_pax[1] : 0,
                            'unit_price' => $package_unit_price[$ind],
                            'status' => $this->input->post('status'),
                            'created_by' => get_userdata('user_id'),
                            'created_date' => time()
                        );
                    }

                    if (isset($insert_batch) && !empty($insert_batch)) {
                        if ($this->db->insert_batch($this->table, $insert_batch)) {
                            set_flash('msg', 'Data saved');
                        } else {
                            set_flash('msg', 'Data could not be saved');
                        }
                    }

                } else {
                    $num_of_pax = explode('-', $this->input->post('number_of_pax'));
                    $insert_data['description'] = $this->input->post('description');
                    $insert_data['valid_from'] = $this->input->post('valid_from');
                    $insert_data['valid_to'] = $this->input->post('valid_to');
                    $insert_data['number_of_pax'] = $this->input->post('number_of_pax');
                    $insert_data['min_pax'] = $num_of_pax[0];
                    $insert_data['max_pax'] = $num_of_pax[1];
                    $insert_data['unit_price'] = $this->input->post('unit_price');
                    $insert_data['status'] = $this->input->post('status');
                    $insert_data['updated_by'] = get_userdata('user_id');
                    $insert_data['updated_date'] = time();
                    $this->db->where('id', $id);
                    $this->db->where('package_id', $package_id);
                    if($this->db->update($this->table, $insert_data)) {
                        set_flash('msg', 'Data saved');
                    } else {
                        set_flash('msg', 'Data could not be saved');
                    }
                }
                redirect(BACKENDFOLDER.'/package_price/' . $package_id);
            } else {
                $this->form($id, 'package_price', $package_name);
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js','assets/' . BACKENDFOLDER . '/dist/js/calendar.js', 'assets/' . BACKENDFOLDER . '/dist/js/dynamic_add.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'package_price', $package_name);
        }
    }

    public function delete()
    {
        $package_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->package_price->delete(array('id' => $selected_id, 'package_id' => $package_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->package_price->delete(array('id' => $id, 'package_id' => $package_id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/package_price/' . $package_id);
    }

    public function status()
    {
        $post = $_POST;
        $package_id = segment(5);
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->package_price->changeStatus('package_price', $status, $selected_id, $package_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(6);
            $res = $this->package_price->changeStatus('package_price', $status, $id, $package_id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/package_price/' . $package_id);
    }

}