<?php

class Activity extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('experience_model', 'experience');
        $this->load->model('destination_model', 'destination');
        $this->load->model('activity_model', 'activity');
        $this->data['module_name'] = 'Activity Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Activity List';
            $this->data['rows'] = $this->activity->getData();
            $this->data['body'] = BACKENDFOLDER . '/activity/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['destinations'] = $this->destination->getAllDestinations();
        $this->data['experiences'] = $this->experience->getExperiences();

        if($_POST) {
            $post = $_POST;
            $this->activity->id = $id;

            $this->form_validation->set_rules($this->activity->rules($id, $post['destination_id']));
            if($this->form_validation->run()) {
                $POST['destination_id'] = $post['destination_id'];
                $POST['name'] = $post['name'];
                $POST['slug'] = $post['slug'];
                $POST['image'] = $post['image'];
                $POST['description'] = $post['description'];
                $POST['show_in_front'] = $post['show_in_front'];
                $POST['link_type'] = $post['link_type'];
                $POST['link'] = $_POST['opens'] = '';
                if($post['link_type'] == 'experience') {
                    $POST['link'] = $post['experience_link'];
                    $POST['opens'] = $post['opens'];
                } elseif($post['link_type'] == 'url') {
                    $POST['link'] = $post['url_link'];
                    $POST['opens'] = $post['opens'];
                }
                $POST['status'] = $post['status'];

                if($id == '') {
                    $res = $this->activity->save($POST);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->activity->save($POST, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/activity');
            } else {
                $this->form($id, 'activity');
            }
        } else {
            $this->data['addJs'] = array('assets/'.BACKENDFOLDER.'/dist/js/activity.js');
            $this->form($id, 'activity');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->activity->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->activity->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/activity');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->activity->changeStatus('activity', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->activity->changeStatus('activity', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/activity');
    }

}