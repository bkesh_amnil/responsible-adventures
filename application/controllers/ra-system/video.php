<?php

class Video extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('video_model', 'video');
        $this->data['module_name'] = 'Video Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
         if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Videos List';
            $this->data['rows'] = $this->video->get('', '', 'id DESC');
            $this->data['body'] = BACKENDFOLDER.'/video/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        if($id == '')
            $this->data['video'] = $this->video->get();
        else
            $this->data['video'] = $this->video->get('', array('id !=' => $id));
       if($_POST) {
            $post = $_POST;
            $this->video->id = $id;

            $this->form_validation->set_rules($this->video->rules($id));
            if($this->form_validation->run()) {
                $video_url = $post['youtube_link'];
                $url = parse_url($video_url);
                $video_id = isset($url['query']) ? $url['query'] : "";
                $v_id = isset($video_id) ? explode('&', $video_id) : "";
                $v_id = isset($v_id) ? explode('=', $v_id[0]) : "";
                $v_id = isset($v_id[1]) ? $v_id[1] : false;
                if(empty($v_id)){
                    set_flash('msg', 'No video available. Please provide a valid you tube video link.');
                    $this->form($id, 'video');
                }else{
                    $post['video_id'] = $v_id;
                }
                if($id == '') {
                    $res = $this->video->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->video->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/video');
            } else {
                $this->form($id, 'video');
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js','assets/'.BACKENDFOLDER.'/dist/js/jquery.textarea-counter.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'video');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->video->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->video->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/video');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->video->changeStatus('video', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->video->changeStatus('video', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/video');
    }

}