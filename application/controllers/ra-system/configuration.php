<?php

class Configuration extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['module_name'] = 'Configuration Manager';
        $this->data['show_add_link'] = false;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $id = 1;
            $this->load->model('module_model', 'module');
            $this->load->model('site_menu_model', 'site_menu');
            $this->data['addJs'] = array('assets/'.BACKENDFOLDER.'/dist/js/site_menu.js');
            $this->data['modules'] = $this->module->get();
            $this->data['site_menus'] = $this->site_menu->get();
            $this->data['no_of_menus'] = count($this->data['site_menus']);
            foreach($this->data['site_menus'] as $val){
                $this->data['menuName'][] = $val->name;
                $this->data['submenus'][] = $val->submenus;
                $this->data['depth'][] 	= $val->depth;
            }

            if($_POST) {
                $post = $_POST;

                $this->configuration->id = $id;

                $data['site_title']= $post['site_title'];
                $data['site_name'] = $post['site_name'];
                $data['main_site'] = '1';
                $data['sub_domain'] = $post['sub_domain'];
                $data['site_offline'] = $post['site_offline'];
                $data['site_offline_msg'] =$post['site_offline_msg'];
                $data['site_from_email'] = $post['site_from_email'];
                $data['site_author'] = $post['site_author'];
                $data['site_logo'] = $post['site_logo'];
                $data['address'] = $post['address'];
                $data['phone'] = $post['phone'];
                $data['site_keyword']=$post['site_keyword'];
                $data['site_description'] = $post['site_description'];
                $data['google_analytics_code'] = $post['google_analytics_code'];
                $data['modules_enabled'] = implode(',',$post['modulesEnabled']);
                $data['siteBanner'] = $post['siteBanner'];
                $data['imageSize'] = $post['imageSize'];
                $data['facebook'] = $post['facebook'];
                $data['twitter'] = $post['twitter'];
                $data['gplus'] = $post['gplus'];
                $data['skype'] = $post['skype'];
                $data['youtube'] = $post['youtube'];
                $data['pininterest'] = $post['pininterest'];
                $data['instagram'] = $post['instagram'];
                $data['linkedin'] = $post['linkedin'];
                $data['vimeo'] = $post['vimeo'];

                $this->form_validation->set_rules($this->configuration->rules);
                if($this->form_validation->run()) {
                    if($id == '') {
                        $data['created_date'] = time();
                        $res = $this->configuration->save($data);
                        if($res){
                            $siteId = $this->configuration->get('id',['site_title'=> $data['site_title']]);
                        }
                    } else {
                        $condition = array('id' => $id);
                        $data['updated_date'] = time();
                        $res = $this->configuration->save($data, $condition);
                        if($res){
                            $siteId = $id;
                        }
                    }
                    $insert_data_mt['name']                 = $post['menuName'];
                    $insert_data_mt['submenus']             = $post['submenus'];
                    $insert_data_mt['depth']                = $post['depth'];
                    $insert_data_mt['status']               = "yes";
                    if(is_array($insert_data_mt['name']) && !empty($insert_data_mt['name'])){
                        for($i = 0; $i < count($insert_data_mt['name']); $i++){
                            unset($menu_data);
                            $menu_data['name'] = trim(strip_tags($insert_data_mt['name'][$i]));
                            $menu_data['submenus'] = (($insert_data_mt['submenus'][$i] == "yes") ? "yes" : "no");
                            $menu_data['depth'] = ((intval($insert_data_mt['depth'][$i]) >0) ? intval($insert_data_mt['depth'][$i]) : 0);
                            $menu_data['status'] = "yes";
                            if(!empty($menu_data['name'])){
                                $this->db->where("tbl_site_menus.name", $menu_data['name']);
                                $this->db->where("tbl_site_menus.site_id",1);
                                $result = $this->db->get("tbl_site_menus")->row();
                                if($result->id > 0){
                                    $this->db->where('id', $result->id);
                                    $this->db->update("tbl_site_menus", $menu_data);
                                    $ids[] = $result->id;
                                }else{
                                    $this->db->insert("tbl_site_menus", $menu_data);
                                    $ids[] = $this->db->insert_id();
                                }
                            }
                        }
                        if(is_array($ids) && !empty($ids)){
                            $ids = implode(",",$ids);
                            $this->db->query("DELETE FROM tbl_site_menus WHERE id NOT IN (".$ids.")");
                        }else{
                            $this->db->query("DELETE FROM tbl_site_menus");
                        }
                    }

                    $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                    redirect(BACKENDFOLDER.'/configuration');
                } else {
                    $this->form($id, 'configuration');
                }
            } else {
                $this->form($id, 'configuration');
            }
        }  else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

}