<?php

class Gallery extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('gallery_model', 'gallery');
        $this->data['module_name'] = 'Gallery Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
         if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Gallery List';
            $this->data['galleries'] = $this->gallery->getData();
            $this->data['body'] = BACKENDFOLDER.'/gallery/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;

            $mediaType = 'image';
            if($mediaType == 'image' && isset($post['media'])) {
                $medias = $post['media'];
                $mediaTitles = $post['title'];
                $mediaDescriptions = $post['description'];
                unset($post['media'], $post['title'], $post['description']);
            }
            unset($post['image']);
            $this->gallery->id = $id;

            $this->form_validation->set_rules($this->gallery->rules($id));
            if($this->form_validation->run()) {
                $post['category_id'] = '1'; // by default Uncategorised
                if($id == '') {
                    $res = $this->gallery->save($post, '', true);
                    $id = $res;
                } else {
                    $condition = array('id' => $id);
                    $res = $this->gallery->save($post, $condition);
                }
                // saving gallery media
                $this->load->model('gallerymedia_model', 'gallerymedia');
                $this->gallerymedia->delete(array('gallery_id' => $id));
                foreach($medias as $key => $singleMedia) {
                    $mediaInsertData = array(
                        'gallery_id' => $id,
                        'media' => $singleMedia,
                        'type' => $mediaType,
                        'title' => isset($mediaTitles) ? $mediaTitles[$key] : '',
                        'caption' => isset($mediaDescriptions) ? $mediaDescriptions[$key] : ''
                    );
                    $this->gallerymedia->save($mediaInsertData);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/gallery');
            } else {
                $this->form($id, 'gallery');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/gallery.js');
            if($id != '') {
                $this->data['savedMedia'] = $this->gallery->getSavedMedia($id);
            }
            $this->form($id, 'gallery');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->gallery->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->gallery->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/gallery');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->gallery->changeStatus('gallery', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->gallery->changeStatus('gallery', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/gallery');
    }

    public function deleteMedia()
    {
        $id = segment(4);
        $this->load->model('gallerymedia_model', 'gallerymedia');
        echo $this->gallerymedia->delete(array('id' => $id));
    }

}