<?php

class Team extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('team_model', 'team');
        $this->data['module_name'] = 'Team Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Teams List';
            $this->data['rows'] = $this->team->get('', '', 'id DESC');
            $this->data['body'] = BACKENDFOLDER.'/team/_list';
            $this->render();
         } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        if($id == '')
            $this->data['team'] = $this->team->get();
        else
            $this->data['team'] = $this->team->get('', array('id !=' => $id));
        if($_POST) {
            $post = $_POST;

            if(isset($_POST['media'])) {
                $medias = $post['media'];
                $mediaTitles = $post['title'];
                $mediaDescriptions = $post['description'];
                unset($post['media'], $post['title'], $post['description']);
            }

            unset($post['media'], $post['title'], $post['description']);
            $this->team->id = $id;

            $this->form_validation->set_rules($this->team->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->team->save($post, '', true);
                    $id = $res;
                } else {
                    $condition = array('id' => $id);
                    $res = $this->team->save($post, $condition);
                }
                // saving team media
                $this->load->model('teamedia_model', 'teamedia');
                $this->teamedia->delete(array('team_id' => $id));
                foreach($medias as $key => $singleMedia) {
                    $mediaInsertData = array(
                        'team_id' => $id,
                        'media' => $singleMedia,
                        'title' => isset($mediaTitles) ? $mediaTitles[$key] : '',
                        'caption' => isset($mediaDescriptions) ? $mediaDescriptions[$key] : ''
                    );
                    $this->teamedia->save($mediaInsertData);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/team');
            } else {
                $this->form($id, 'team');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/gallery.js');
            if($id != '') {
                $this->data['savedMedia'] = $this->team->getSavedMedia($id);
            }
            $this->form($id, 'team');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->team->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->team->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/team');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->team->changeStatus('team', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->team->changeStatus('team', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/team');
    }

}