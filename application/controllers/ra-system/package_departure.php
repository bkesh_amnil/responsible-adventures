<?php

class Package_departure extends My_Controller
{
    var $table = 'tbl_package_departure_detail';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_departure_model', 'package_departure');
        $this->data['module_name'] = 'Package Departure Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {

        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($id = '')
    {
            if($this->data['activeModulePermission']['view']) {
           $package_id = $this->uri->segment(3);
           $package_name = $this->package_departure->get_single_data('tbl_package', 'name', $package_id, 'id');
           $this->data['sub_module_name'] = 'Package Departure List for <b>' . $package_name . '</b>';
           $this->data['rows'] = $this->package_departure->getPackageDeparture($package_id);
           $this->data['body'] = BACKENDFOLDER.'/package_departure/_list';
           $this->render();
         } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $package_id = segment(4);
        $package_name = $this->package_departure->get_single_data('tbl_package', 'name', $package_id, 'id');
        $id = segment(5);
        $this->data['package_id'] = $package_id;
        if($_POST) {
            $post = $_POST;

            $this->package_departure->id = $id;
            $this->form_validation->set_rules($this->package_departure->rules($id));
            if($this->form_validation->run()) {
                $insert_data['package_id'] = $package_id;
                $insert_data['departure_date'] = $post['package_departure_date'];
                $insert_data['all_allocation'] = $post['package_all_allocation'];
                $insert_data['available'] = $post['package_available'];
                $insert_data['booked'] = $post['package_booked'];
                $insert_data['is_fixed_departure'] = $post['is_fixed_departure'];
                $insert_data['status'] = $post['status'];
                if ($id == 0) {
                    $insert_data['created_by'] = get_userdata('user_id');
                    $insert_data['created_date'] = time();

                    $res = $this->db->insert($this->table, $insert_data);
                } else {
                    $insert_data['updated_by'] = get_userdata('user_id');
                    $insert_data['updated_date'] = time();

                    $this->db->where('id', $id);
                    $this->db->where('package_id', $package_id);
                    $res = $this->db->update($this->table, $insert_data);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/package_departure/' . $package_id);
            } else {
                $this->form($id, 'package_departure', $package_name);
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js','assets/' . BACKENDFOLDER . '/dist/js/calendar.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'package_departure', $package_name);
        }
    }

    public function delete()
    {
        $package_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "tbl_package_booking_status.departure_date_id";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->package_departure->delete(array('id' => $selected_id, 'package_id' => $package_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->package_departure->delete(array('id' => $id, 'package_id' => $package_id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/package_departure/' . $package_id);
    }

    public function status()
    {
        $post = $_POST;
        $package_id = segment(5);
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->package_departure->changeStatus('package_departure', $status, $selected_id, $package_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(6);
            $res = $this->package_departure->changeStatus('package_departure', $status, $id, $package_id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/package_departure/' . $package_id);
    }

    public function fixed_departure()
    {
        $post = $_POST;
        $package_id = segment(5);
        $departure = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->package_departure->changeDeparture('package_departure', $departure, $selected_id, $package_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/package_departure/' . $package_id);
    }

}