<?php

class Import_form_data extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('import_form_data_model', 'import_form');
        $this->data['module_name'] = 'Import Form Manager';
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
         if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Import Data';
            $this->data['rows'] = $this->import_form->getEventsRacewitForm('event');
            $this->data['addJs'] = array('assets/'.BACKENDFOLDER.'/dist/js/import_form_data.js');
            $this->data['body'] = BACKENDFOLDER.'/import_form_data/import_form.php';
            $this->render();
         }else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function import() {
        $post = $_POST;
        $insert_post['type'] = $post['type'];
        $insert_post['id'] = $post['id'];

        $query = "SELECT id FROM tbl_google_form_links WHERE type='".$post['type']."' AND type_id = " . $post['id'] . " ORDER BY id DESC LIMIT 1";
        $google_form_id = $this->db->query($query)->row();
        $google_form_id = $google_form_id->id;

        $this->db->select('id, column_name');
        $this->db->where('type', $post['type']);
        $this->db->where('type_id', $post['id']);
        $this->db->where('google_form_id', $google_form_id);
        $prev_columns = $this->db->get('tbl_form_column_names')->result();

        if (!empty($_FILES['excel']['name'])) {
            $file = $_FILES['excel'];
            $temp_file_path = $file['tmp_name'];
            $ext = explode(".", $file['name']);
            $ext = end($ext);
            if (!file_exists('temp')) {
                mkdir('temp');
            }
            $upload_path = 'temp/' . time() . '.' . $ext;
            if (move_uploaded_file($temp_file_path, $upload_path)) {
                include APPPATH . "third_party/phpexcel/PHPExcel.php";
                $objPHPExcel = PHPExcel_IOFactory::load($upload_path);

                $sheet = $objPHPExcel->getSheet(0);
                $raw_data = $sheet->toArray('No Data', TRUE, TRUE, FALSE);
                $row_column_data = $this->_clean_data($raw_data);
                $row_data = (array_slice($row_column_data, 1));
                $column_data = $row_column_data[0];
                $column_ind_arr = array();

                $this->db->trans_begin();

                if(isset($prev_columns) && empty($prev_columns)) {
                    foreach($column_data as $ind => $column){
                        $insert_column['type'] = $post['type'];
                        $insert_column['type_id'] = $post['id'];
                        $insert_column['google_form_id'] = $google_form_id;
                        $insert_column['column_name'] = $column;
                        $insert_column['column_name_key'] = $ind;

                        $this->db->insert('tbl_form_column_names', $insert_column);
                        $form_column_name_id = $this->db->insert_id();

                        array_push($column_ind_arr, $form_column_name_id);
                    }
                } else {
                    foreach($prev_columns as $val) {
                        $this->db->where('type', $post['type']);
                        $this->db->where('type_id', $post['id']);
                        $this->db->where('google_form_id', $google_form_id);
                        $this->db->delete('tbl_form_data_submissions');
                        $this->db->where('form_column_name_id', $val->id);
                        $this->db->delete('tbl_form_data');
                        array_push($column_ind_arr, $val->id);
                    }

                }

                foreach($row_data as $ind => $val) {
                    $insert_submissions['type'] = $post['type'];
                    $insert_submissions['type_id'] = $post['id'];
                    $insert_submissions['google_form_id'] = $google_form_id;
                    $this->db->insert('tbl_form_data_submissions', $insert_submissions);
                    $form_data_submission_id = $this->db->insert_id();

                    foreach($val as $index => $value) {
                        $insert_batch[] = array(
                            'form_column_name_id' => $column_ind_arr[$index],
                            'form_data_submission_id' => $form_data_submission_id,
                            'data' => $value
                        );
                    }
                }

                if(isset($insert_batch) && !empty($insert_batch)) {
                    $this->db->insert_batch('tbl_form_data', $insert_batch);
                }
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    $res = set_flash('msg', 'Data could not be saved');
                } else {
                    $this->db->trans_commit();
                    $res = set_flash('msg', 'Data saved');
                }
                redirect(BACKENDFOLDER.'/import_form_data');
            }
        }
    }

    function _clean_data($raw_data)
    {
        foreach ($raw_data as $key => $data) {
            $is_empty = false;
            for ($i = 0; $i < count($data); $i++) {
                if ($data[$i] == 'No Data') {
                    $is_empty = true;
                } else {
                    $is_empty = false;
                    break;
                }
            }
            if ($is_empty) unset($raw_data[$key]);
        }
        return $raw_data;
    }

    public function loadData() {
        $post = $_POST;
        $html['data'] = '';
        $result = $this->import_form->getEventsRacewitForm($post['type']);

        if(isset($result) && !empty($result)) {
            foreach($result as $val) {
                $html['data'] .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }
        }

        echo json_encode($html);
        die;
    }

    public function formdata(){
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
        $type = segment(4);
        $id = segment(5);
        $this->data['sub_module_name'] = ucfirst($type) . ' Google Form Data';
        /* get form data id */
        $query = "SELECT id FROM tbl_google_form_links WHERE type='".$type."' AND type_id = " . $id . " ORDER BY id DESC LIMIT 1";
        $google_form_id = $this->db->query($query)->row();
        $google_form_id = $google_form_id->id;
        /* get form data id */
        /* select column names */
        $this->db->select('id, column_name');
        $this->db->where('type', $type);
        $this->db->where('type_id', $id);
        $this->db->where('google_form_id', $google_form_id);
        $columns = $this->db->get('tbl_form_column_names')->result();
        /* select column names */
        /* select saved form data ids */
        $this->db->select('id');
        $this->db->where('type', $type);
        $this->db->where('type_id', $id);
        $this->db->where('google_form_id', $google_form_id);
        $saved_data_ids = $this->db->get('tbl_form_data_submissions')->result();
        /* select saved form data ids */
        $array = '';
        foreach($saved_data_ids as $val) {
            foreach($columns as $value) {
                $this->db->select('data');
                $this->db->where('form_column_name_id', $value->id);
                $this->db->where('form_data_submission_id', $val->id);
                $row = $this->db->get('tbl_form_data')->row();
                $array[$val->id][$value->id] = $row->data;
            }
        }

        $this->data['columns'] = $columns;
        $this->data['rows'] = $array;
        $this->data['body'] = BACKENDFOLDER.'/import_form_data/_data';
        $this->render();

    }

}