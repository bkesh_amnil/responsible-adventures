<?php

class Smtp_Settings extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['module_name'] = 'Smtp Settings Manager';
        $this->data['show_add_link'] = false;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
         if($this->data['activeModulePermission']['view']) {
            $id = 1;
            $this->load->model('smtp_model', 'smtp_settings');
            $this->data['smtp_settings'] = $this->smtp_settings->get();

            if($_POST) {
                $post = $_POST;

                $this->smtp_settings->id = $id;

                $this->form_validation->set_rules($this->smtp_settings->rules($id));
                if($this->form_validation->run()) {
                    if(isset($this->data['smtp_settings']) && !empty($this->data['smtp_settings'])) {
                        $condition = array('id' => $id);
                        $res = $this->smtp_settings->save($post, $condition);
                    } else {
                        $res = $this->smtp_settings->save($post);
                    }
                    $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                    redirect(BACKENDFOLDER.'/smtp_settings');
                } else {
                    $this->form($id, 'smtp_settings');
                }
            } else {
                $this->form($id, 'smtp_settings');
            } 
            
            } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

}