<?php

class Form_data extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dynamic_form/dynamic_form_model');
        $this->load->model('dynamic_form/form_model', 'form_fields');
        $this->load->model('dynamic_form/form_datas', 'form_data');
        $this->data['module_name'] = 'Form Data Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {

        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($form_id, $action = '', $id = '')
    { 
         if($this->data['activeModulePermission']['view']) {
            $form_name = $this->form_data->get_single_data('tbl_form', 'form_title', $form_id, 'id');
            $this->data['sub_module_name'] = 'Form Data List for <b>' . $form_name . '</b>';
            $this->_grid($form_id);
             } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
        }

    public function _grid($form_id) {
        $this->data['form_id'] = $form_id;
        $this->data['fields'] = $this->form_fields->getFormFields($form_id, 1);

        $submitted_forms = $this->form_data->getSubmittedForms($form_id);
        $array = array();
        $i = 0;
        if(isset($submitted_forms) && !empty($submitted_forms) && is_array($submitted_forms)) {
            foreach($submitted_forms as $submitted_form){
                foreach($this->data['fields'] as $field) {
                    $array[$submitted_form->id][$field->id] = $this->form_data->getSubmittedFormData($submitted_form->id, $field->id);
                }
                $i++;
            }
        }

        $this->data['submitted_forms'] =  array_slice($array, 0, 200, true);
        
        $this->data['body'] = BACKENDFOLDER.'/form_data/_list';
        $this->render();
    }

    public function create()
    {
        $form_id = segment(4);
        $id = segment(5);

        $this->data['form_id'] = $form_id;
        $this->data['form_action'] = $this->dynamic_form_model->getFormAction($form_id);
        $this->data['fields'] = $this->form_fields->getFormFields($form_id, 0);
        
        if($id)
        {
            $i = 0; 
            foreach($this->data['fields'] as $field)
            {
                $this->data['fields'][$i]->default_value = $this->form_data->get_submitted_data($id, $field->id);
                $i++;
                
            }
        }
        
        if($_POST) {
            $post = $_POST;
            $this->form_data->id = $id;

            $this->form_validation->set_rules($this->form_data->rules($id));
            if($this->form_validation->run()) {
                $validation_rules = $post['validation_rule'];

                foreach($validation_rules as $key => $validation_rule) {
                    $value = $post[$validation_rule];
                    if(isset($value)) {
                        if(!empty($value)) {
                            $validation_rules[$key] = $validation_rule.'['.$value.']';
                        } else {
                            unset($validation_rules[$key]);
                        }
                    } else {
                        $validation_rules[$key] = $validation_rule;
                    }
                }

                $insert_post['form_id'] = $form_id;
                $insert_post['field_type'] = $post['field_type'];
                $insert_post['field_label'] = $post['field_label'];
                $insert_post['field_name'] = $post['field_name'];
                $insert_post['field_placeholder'] = $post['field_placeholder'];
                $insert_post['default_value'] = $post['default_value'];
                $insert_post['field_attribute'] = $post['field_attribute'];
                $insert_post['field_class'] = $post['field_class'];
                $insert_post['position'] = $post['position'];
                $insert_post['show_in_grid'] = $post['show_in_grid'];
                $insert_post['front_display'] = $post['front_display'];
                $insert_post['validation_rule'] = is_array($validation_rules) ? implode('|', $validation_rules) : "";

                if($id == '0') {
                    $res = $this->form_data->save($insert_post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->form_data->save($insert_post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/form_data/' . $form_id);
            } else {
                $this->form($id, 'form_data');
            }
        } else {
            $this->form($id, 'form_data');
        }
    }

    public function delete()
    {
        $form_id = segment(4);
        $form_submission_id = segment(5);
        $this->db->where('form_id', $form_id);
        $this->db->where('id', $form_submission_id);
        $res = $this->db->delete('tbl_form_submissions');

        if($res) {
            $this->db->where('form_submission_id', $form_submission_id);
            $this->db->delete('tbl_form_submission_fields');
        }
        
        $res ? set_flash('msg', 'Data deleted') : set_flash('msg', 'Error in deleting data');
        redirect(BACKENDFOLDER.'/form_data/' . $form_id);
    }

}