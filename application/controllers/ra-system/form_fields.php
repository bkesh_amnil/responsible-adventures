<?php

class Form_fields extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        //$this->load->model('dynamic_form/dynamic_form_model', 'dynamic_form');
        $this->load->model('dynamic_form/form_model', 'form_fields');
        $this->data['module_name'] = 'Form Fields Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {

        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($id = '')
    {
        if($this->data['activeModulePermission']['view']) {
        $form_id = $this->uri->segment(3);
        $form_name = $this->form_fields->get_single_data('tbl_form', 'form_title', $form_id, 'id');
        $this->data['sub_module_name'] = 'Form Fields List for <b>' . $form_name . '</b>';
        $this->data['rows'] = $this->form_fields->getFormFields($form_id, 0);
        $this->data['body'] = BACKENDFOLDER.'/form_fields/_list';
        $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $form_id = segment(4);
        $id = segment(5);

        $this->data['form_id'] = $form_id;
        $this->data['field_types'] = $this->form_fields->getFormFieldTypes();
        $this->data['form_validations'] = $this->form_fields->getFormValidations();
        if($_POST) {
            $post = $_POST;
            $this->form_fields->id = $id;

            $this->form_validation->set_rules($this->form_fields->rules($id));
            if($this->form_validation->run()) {
                $validation_rules = $post['validation_rule'];

                foreach($validation_rules as $key => $validation_rule) {
                    $value = $post[$validation_rule];
                    if(isset($value)) {
                        if(!empty($value)) {
                            $validation_rules[$key] = $validation_rule.'['.$value.']';
                        } else {
                            unset($validation_rules[$key]);
                        }
                    } else {
                        $validation_rules[$key] = $validation_rule;
                    }
                }

                $insert_post['form_id'] = $form_id;
                $insert_post['field_type'] = $post['field_type'];
                $insert_post['field_label'] = $post['field_label'];
                $insert_post['field_name'] = $post['field_name'];
                $insert_post['field_placeholder'] = $post['field_placeholder'];
                $insert_post['default_value'] = $post['default_value'];
                $insert_post['field_attribute'] = $post['field_attribute'];
                $insert_post['field_class'] = $post['field_class'];
                $insert_post['position'] = $post['position'];
                $insert_post['show_in_grid'] = $post['show_in_grid'];
                $insert_post['front_display'] = $post['front_display'];
                $insert_post['validation_rule'] = is_array($validation_rules) ? implode('|', $validation_rules) : "";

                if($id == '0') {
                    $res = $this->form_fields->save($insert_post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->form_fields->save($insert_post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/form_fields/' . $form_id);
            } else {
                $this->form($id, 'form_fields');
            }
        } else {
            $this->form($id, 'form_fields');
        }
    }

    public function delete()
    {
        $form_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->form_fields->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->form_fields->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/form_fields/' . $form_id);
    }

}