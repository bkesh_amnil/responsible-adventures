<?php 

class Event extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('event_model', 'event');
        $this->data['module_name'] = 'Event Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Events List';
            $this->data['events'] = $this->event->get('', '', 'position');
            $this->data['body'] = BACKENDFOLDER.'/event/_list';
            $this->render();
         } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
         redirect(BACKENDFOLDER.'/dashboard');
         
         }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;
        if($id == '')
            $this->data['event'] = $this->event->get();
        else
            $this->data['event'] = $this->event->get('', array('id !=' => $id));

        if($_POST) {
            $post = $_POST;

            $this->event->id = $id;
            $this->form_validation->set_rules($this->event->rules($id));
            if($this->form_validation->run()) {
                $insert_post['name'] = $post['name'];
                $insert_post['slug'] = $post['slug'];
                $insert_post['image'] = $post['image'];
                $insert_post['short_description'] = $post['short_description'];
                $insert_post['description'] = $post['description'];
                $insert_post['event_date'] = $post['event_date'];
                $insert_post['publish_date'] = $post['publish_date'];
                $insert_post['unpublish_date'] = $post['unpublish_date'];
                $insert_post['is_highlighted'] = $post['is_highlighted'];
                $insert_post['google_form_link'] = (isset($post['google_form_link']) && !empty($post['google_form_link'])) ? $post['google_form_link'] : $post['google_form_link_edit'];
                $insert_post['meta_keywords'] = $post['meta_keywords'];
                $insert_post['meta_description'] = $post['meta_description'];
                $insert_post['status'] = $post['status'];

                if($id == '') {
                    if($post['google_form_link'] != '') {
                        $res = $this->event->save($insert_post, '', true);
                        $event_id = $res;

                        $insert_form_link['type'] = 'event';
                        $insert_form_link['type_id'] = $event_id;
                        $insert_form_link['google_form_link'] = $post['google_form_link'];
                        $this->db->insert('tbl_google_form_links', $insert_form_link);
                    } else {
                        $res = $this->event->save($insert_post);
                    }
                } else {
                    $condition = array('id' => $id);
                    $res = $this->event->save($insert_post, $condition);

                    if(isset($post['edit_google_form'])) {
                        $insert_form_link['type'] = 'event';
                        $insert_form_link['type_id'] = $id;
                        $insert_form_link['google_form_link'] = $post['google_form_link'];
                        $this->db->insert('tbl_google_form_links', $insert_form_link);
                    }
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/event');
            } else {
                $this->form($id, 'event');
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js', 'assets/'.BACKENDFOLDER.'/dist/js/calendar.js', 'assets/'.BACKENDFOLDER.'/dist/js/jquery.textarea-counter.js', 'assets/'.BACKENDFOLDER.'/dist/js/events.js', 'assets/'.BACKENDFOLDER.'/dist/js/google_form.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'event');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->event->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->event->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/event');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->event->changeStatus('event', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->event->changeStatus('event', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/event');
    }

    function checkHighlightedEvents() {
        $this->db->select('id, name');
        $this->db->where('is_highlighted', 1);
        $row = $this->db->get($this->event)->row();

        if(isset($row) && !empty($row)) {
            $data['action'] = 'error';
            $data['msg'] = 'Event ' . $row->name . ' is highlighted. Do you want to remove it?';
            $data['id'] = $row->id;
        } else {
            $data['action'] = 'success';
        }

        echo json_encode($data);
    }

    function removeHighlight() {
        $event_id = $_POST['id'];

        $edit_data['is_highlighted'] = '0';
        $this->db->where('id', $event_id);

        if($this->db->update($this->event, $edit_data)) {
            $data['result'] = 'sucess';
            $data['msg'] = 'Highlight removed successfully';
        } else {
            $data['result'] = 'error';
            $data['msg'] = 'Error in updating event';
        }

        echo json_encode($data);
    }

    public function formdata() {
        $this->data['show_add_link'] = false;
        $this->data['show_sort_link'] = false;
        $id = segment(4);
        $this->data['sub_module_name'] = 'Event Google Form Data';
        /* get form data id */
        $query = "SELECT id FROM tbl_google_form_links WHERE type='event' AND type_id = " . $id . " ORDER BY id DESC LIMIT 1";
        $google_form_id = $this->db->query($query)->row();
        $google_form_id = $google_form_id->id;
        /* get form data id */
        /* select column names */
        $this->db->select('id, column_name');
        $this->db->where('type', 'event');
        $this->db->where('type_id', $id);
        $this->db->where('google_form_id', $google_form_id);
        $columns = $this->db->get('tbl_form_column_names')->result();
        /* select column names */
        /* select saved form data ids */
        $this->db->select('id');
        $this->db->where('type', 'event');
        $this->db->where('type_id', $id);
        $this->db->where('google_form_id', $google_form_id);
        $saved_data_ids = $this->db->get('tbl_form_data_submissions')->result();
        /* select saved form data ids */
        $array = '';
        foreach($saved_data_ids as $val) {
            foreach($columns as $value) {
                $this->db->select('data');
                $this->db->where('form_column_name_id', $value->id);
                $this->db->where('form_data_submission_id', $val->id);
                $row = $this->db->get('tbl_form_data')->row();
                $array[$val->id][$value->id] = $row->data;
            }
        }

        $this->data['columns'] = $columns;
        $this->data['rows'] = $array;
        $this->data['body'] = BACKENDFOLDER.'/event/_data';
        $this->render();
    }

}