<?php

class Package_Downloads extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_download_model', 'package_download');
        $this->load->model('package_model', 'package');
        $this->data['module_name'] = 'Package Download Manager';
        $this->data['show_add_link'] = false;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Package Download List';
            $this->data['packages'] = $this->package->getPackageDownloadNames();
            $this->data['rows'] = $this->package_download->getPackageDownloads();
            $this->data['body'] = BACKENDFOLDER.'/package_download/_list';
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/package_downloads.js');
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }


    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;

        $this->data['package_destinations'] = $this->destination->getAllDestinations();
        $this->data['package_experiences'] = $this->experience->getExperiences();
        $this->data['packages'] = $this->package->getPackages();

        if ($_POST) {
            $post = $_POST;
            if ($post['note'] != '') {
                $condition = array('id' => $id);
                $res = $this->package_download->save($post, $condition);

                if ($res) {
                    $email_admin = '';
                    /* sending email code */
                    $footer_contact = $this->public_model->getContent('5');
                    $subject = 'Package Download';

                    $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                    $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="'.base_url('img/logo.png').'"></td></tr></thead>';
                    $email_admin .= '<tbody>';
                    $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                    $email_admin .= $subject;
                    $email_admin .= '</td></tr>';
                    $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                    $email_admin .= $post['note'];
                    $email_admin .= '</td></tr>';
                    $email_admin .= '<tr><td>';
                    $email_admin .= 'Regards<br/>';
                    $email_admin .= 'Responsible Adventures<br/>';
                    $email_admin .= '<img src="'.base_url('img/logo.png').'">';
                    $email_admin .= '</td>';
                    $email_admin .= '<td>';
                    $email_admin .= $footer_contact[0]->long_description;
                    $email_admin .= '</td></tr>';
                    $message = $email_admin;

                    $res = $this->email_send->sendMail($subject, $post['email_address'], $message);
                    /* sending email code */
                    if (isset($res) && ($res == 'success')) {
                        set_flash('msg', 'Data saved and mail sent to user.');
                    } else {
                        set_flash('msg', 'Data saved');
                    }
                } else {
                    set_flash('msg', 'Data not saved');
                }
            } else {
                set_flash('msg', 'No changes made.');
            }

            redirect(BACKENDFOLDER . '/package_download');
        } else {
            $this->form($id, 'package_download');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->package_download->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->package_download->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/package_download');
    }

    public function export() {
        $post['search_field_type'] = $_POST['export_filter_id'];
        $post['search_field'] = 'all';
        if($post['search_field_type'] == 'package') {
            $post['search_field'] = $_POST['export_filter_package'];
        } else if($post['search_field_type'] == 'download_date') {
            $post['search_field'] = $_POST['export_filter_download_date'];
        }

        $result = $this->package_download->getExportData($post['search_field_type'], $post['search_field']);

        if (isset($result) && !empty($result)) {
            include APPPATH . 'third_party/phpexcel/PHPExcel.php';
            $excel = new PHPExcel();
            $sheet_title = "Package-Download- " . date('Y F d');
            //activate worksheet number 1
            $excel->setActiveSheetIndex(0);
            //name the worksheet
            $excel->getActiveSheet()->setTitle($sheet_title);
            $k = 1;
            $count = 2;
            $excel->getActiveSheet()->setCellValue('A' . $k, 'SN');
            $excel->getActiveSheet()->setCellValue('B' . $k, 'Package Name');
            $excel->getActiveSheet()->setCellValue('C' . $k, 'Customer Full Name');
            $excel->getActiveSheet()->setCellValue('D' . $k, 'Customer Email Address');
            $excel->getActiveSheet()->setCellValue('E' . $k, 'Download Date');

            foreach ($result as $key => $v) {
                $excel->getActiveSheet()->setCellValueExplicit('A' . $count, $key + 1, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('B' . $count, $v->package_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('C' . $count, $v->full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('D' . $count, $v->email, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('E' . $count, $v->download_date, PHPExcel_Cell_DataType::TYPE_STRING);
                $count++;
            }
            //make the font become bold
            //$excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

            $filename = $sheet_title . '.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            die;
        }  else {
            set_flash('msg', 'Sorry, no result found.');
            redirect(BACKENDFOLDER.'/package_download');
        }
    }

}