<?php

class Banner extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model', 'menu');
        $this->load->model('destination_model', 'destination');
        $this->load->model('experience_model', 'experience');
        $this->load->model('area_model', 'area');
        $this->load->model('package_model', 'package');
        $this->load->model('banner_model', 'banner');
        $this->data['module_name'] = 'Banner Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Banner List';
            $this->data['rows'] = $this->banner->get('', '', 'position');
            $this->data['body'] = BACKENDFOLDER.'/banner/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['menus'] = $this->menu->getMenus();
        $this->data['destinations'] = $this->destination->getAllDestinations();
        $this->data['experiences'] = $this->experience->getExperiences();
        $this->data['areas'] = $this->area->getAreas();
        $this->data['packages'] = $this->package->getPackages();

        if($_POST) {
            $post = $_POST;
            $this->banner->id = $id;

            $this->form_validation->set_rules($this->banner->rules($id));
            if($this->form_validation->run()) {
                $POST['name'] = $post['name'];
                $POST['description'] = $post['description'];
                $POST['image'] = $post['image'];
                $POST['category_id'] = '1'; // by default Uncategorised
                $POST['link_url'] = $POST['link_id'] = '';
                $POST['link_type'] = $post['link_type'];

                if($post['link_type'] == 'menu'){
                    $POST['link_id'] = $post['menu_link_id'];
                } else if($post['link_type'] == 'url') {
                    $POST['link_id'] = $post['link_url'];
                } else if($post['link_type'] == 'destination') {
                    $POST['link_id'] = $post['destination_link_id'];
                } else if($post['link_type'] == 'area') {
                    $POST['link_id'] = $post['area_link_id'];
                } else if($post['link_type'] == 'experience') {
                    $POST['link_id'] = $post['experience_link_id'];
                } else if($post['link_type'] == 'package') {
                    $POST['link_id'] = $post['package_link_id'];
                }

                $POST['opens'] = $post['opens'];
                $POST['status'] = $post['status'];

                if($id == '') {
                    $res = $this->banner->save($POST);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->banner->save($POST, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/banner');
            } else {
                $this->form($id, 'banner');
            }
        } else {
            $this->data['addJs'] = array('assets/'.BACKENDFOLDER.'/dist/js/banner.js');
            $this->form($id, 'banner');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->banner->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->banner->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/banner');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->banner->changeStatus('banner', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->banner->changeStatus('banner', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/banner');
    }

}