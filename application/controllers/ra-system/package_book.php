<?php

class Package_Book extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('email_send_model', 'email_send');
        $this->load->model('booking_status_model', 'book_status');
        $this->load->model('destination_model', 'destination');
        $this->load->model('experience_model', 'experience');
        $this->load->model('package_model', 'package');
        $this->load->model('package_departure_model', 'package_departure');
        $this->load->model('public_model', 'public');
        $this->load->model('package_book_model', 'package_book');
        $this->load->model('package_book_status_model', 'package_book_status');
        $this->data['module_name'] = 'Package Book Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Package Book List';
            $this->data['package_destinations'] = $this->destination->getPackageBookingDestination();
            $this->data['package_experiences'] = $this->experience->getPackageBookingExperiences();
            $this->data['packages'] = $this->package->getPackageBookingNames();
            $this->data['customers'] = $this->package_book_status->getPackageBookingCustomers();
            $this->data['booking_status'] = $this->book_status->getBookingStatus('package_booking');
            $this->data['rows'] = $this->package_book->getPackageBookings();
            $this->data['body'] = BACKENDFOLDER.'/package_book/_list';
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/package_book.js');
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;
        $this->data['destinations'] = $this->destination->getAllDestinations();
        $this->data['experiences'] = $this->experience->getExperiences($this->data['destinations'][0]->id);
        $this->data['package_book_status'] = $this->_format_data($id);
        $this->data['book_status'] = $this->book_status->getBookStatus();

        if($id != '') {
            $this->data['logs'] = $this->package_book_status->getSavedData($id, 'logs');
            $experience_id = $this->package_book->get_single_data('tbl_package_booking', 'package_experience_id', $id, 'id');
            $this->data['packages'] = $this->package->getPackages($experience_id);
            $package_id = $this->data['logs'][0]->package_id;
        } else {
            $this->data['packages'] = $this->package->getPackages($this->data['experiences'][0]->id);
            $package_id = $this->data['packages'][0]->id;
        }
        if (isset($this->data['packages']) && !empty($this->data['packages'])) {
            $this->data['departure_dates'] = $this->public->getPackageDepartureDates($package_id);
        } else {
            $this->data['departure_dates'] = '';
        }

        if($_POST) {
            $post = $_POST;

            $this->package_book->id = $id;

            $this->form_validation->set_rules($this->package_book->rules($id));
            if($this->form_validation->run()) {
                $this->form_validation->set_rules($this->package_book_status->rules($id));
                if($this->form_validation->run()) {
                    $insert_book['package_destination_id'] = $post['package_destination_id'];
                    $insert_book['package_experience_id'] = $post['package_experience_id'];
                    $insert_book['package_id'] = $post['package_id'];
                    $insert_book['remarks'] = '';
                    $insert_book['last_booking_status_id'] = $post['booking_status_id'];

                    $insert_status['departure_date_id'] = $post['departure_date_id'];
                    $insert_status['number_of_pax'] = $post['number_of_pax'];
                    $insert_status['price_per_person'] = $post['price_per_person'];
                    $insert_status['total_price'] = $post['total_price'];
                    $insert_status['full_name'] = $post['full_name'];
                    $insert_status['email_address'] = $post['email_address'];
                    $insert_status['contact_number'] = $post['contact_number'];
                    $insert_status['contact_address'] = $post['contact_address'];
                    $insert_status['booking_status_id'] = $post['booking_status_id'];
                    $insert_status['note'] = $post['note'];

                    $this->db->trans_begin();

                    if ($id == '') {
                        $res = $this->package_book->save($insert_book, '', true);
                        $package_booking_id = $res;

                        $insert_status['package_booking_id'] = $package_booking_id;

                        $this->package_book_status->save($insert_status);
                        /* change package booking count according to booking status starts */
                        if($post['booking_status_id'] == '2') {
                            $this->changeAllocationBookedAvailable($post['package_id'], /*$post['departure_date_id'],*/ $id, '', '', $post['departure_date_id'], '');
                        }
                        /* change package booking count according to booking status ends */
                    } else {
                        $condition = array('id' => $id);
                        $this->package_book->save($insert_book, $condition);

                        $insert_status['package_booking_id'] = $id;

                        $this->package_book_status->save($insert_status);

                        $this->changeAllocationBookedAvailable($post['package_id'], /*$post['departure_date_id'],*/ $id, $post['previous_departure_date_id'], $post['previous_book_status'], $post['departure_date_id'], $post['booking_status_id']);
                    }

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        /* sending mail code */
                        if(!empty($post['note'])) {
                            $footer_contact = $this->public_model->getContent('5');
                            $booking_status = $this->db->select('name')->where('id', $post['booking_status_id'])->get('tbl_booking_status')->row();
                            $email_admin = '';
                            $subject = 'Package Booking (' . $booking_status->name . ')';

                            $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                            $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="'.base_url('img/logo.png').'"></td></tr></thead>';
                            $email_admin .= '<tbody>';
                            $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                            $email_admin .= $subject;
                            $email_admin .= '</td></tr>';
                            $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                            $email_admin .= $post['note'];
                            $email_admin .= '</td></tr>';
                            $email_admin .= '<tr><td>';
                            $email_admin .= 'Regards<br/>';
                            $email_admin .= 'Responsible Adventures<br/>';
                            $email_admin .= '<img src="'.base_url('img/logo.png').'">';
                            $email_admin .= '</td>';
                            $email_admin .= '<td>';
                            $email_admin .= $footer_contact[0]->long_description;
                            $email_admin .= '</td></tr>';

                            $message = $email_admin;

                            $res = $this->email_send->sendMail($subject, $post['email_address'], $message);
                        }
                        /* sending mail code */
                        if(isset($res) && ($res == 'success')) {
                            set_flash('msg', 'Data saved and mail sent to user.');
                        } else {
                            set_flash('msg', 'Data saved');
                        }
                    }

                    redirect(BACKENDFOLDER . '/package_book');
                } else {
                    $this->form($id, 'package_book');
                }
            } else {
                $this->form($id, 'package_book');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/package_book.js');
            $this->form($id, 'package_book');
        }
    }

    public function changeAllocationBookedAvailable($package_id, /*$departure_date_id, */$id, $previous_departure_date_id, $previous_booking_status_id, $current_departure_date_id, $current_booking_status_id) {
        if($id == '') { /* create status only confirmed*/
            $this->db->query("UPDATE tbl_package_departure_detail
                                        SET booked = booked + 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
            $this->db->query("UPDATE tbl_package_departure_detail
                                        SET available = available - 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
        } else { /* edit */
            /* prev date = cur date, prev status = cur status ---> no change @ all*/
            if($previous_departure_date_id == $current_departure_date_id) {
                if($previous_booking_status_id != $current_booking_status_id) {
                    if($previous_booking_status_id == '1') { /* Pending */
                        if($current_booking_status_id == '2') { /* to Confirmed */
                            $this->db->query("UPDATE tbl_package_departure_detail
                                        SET booked = booked + 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                            $this->db->query("UPDATE tbl_package_departure_detail
                                        SET available = available - 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                        }
                        /* Pending to Cancelled ---> No change */
                    } else if($previous_booking_status_id == '2') { /* Confirmed */
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET booked = booked - 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET available = available + 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                    } else { /* Cancelled*/
                        if($current_booking_status_id == '2') { /* Confirmed */
                            $this->db->query("UPDATE tbl_package_departure_detail
                                        SET booked = booked + 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                            $this->db->query("UPDATE tbl_package_departure_detail
                                        SET available = available - 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                        }
                        /* Cancelled to Pending ---> No change */
                    }
                }
            } else {
                /* prev date !=  cur date */
                if($previous_booking_status_id != $current_booking_status_id) { /* prev status != curr status */
                    if(($previous_booking_status_id == '1' && $current_booking_status_id == '2') || ($previous_booking_status_id == '3' && $current_booking_status_id == '2')) { /* prev - pending; curr - confirmed OR prev - cancelled;curr - confirmed */
                        /* add booking to current date id */
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET booked = booked + 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET available = available - 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                    } elseif(($previous_booking_status_id == '2' && $current_booking_status_id == '1') || ($previous_booking_status_id == '2' && $current_booking_status_id == '3')) { /* prev - confirmed;curr - pending OR prev - confirmed; curr - cancelled*/
                        /* subtract booking from previous date id */
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET booked = booked - 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $previous_departure_date_id);
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET available = available + 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $previous_departure_date_id);
                    }
                } else { /* prev status == curr status */
                    /* Pending n Cancelled Status ---> no change */
                    if($previous_booking_status_id == '2') { /* Confirmed */
                        /* subtract booking from previous date id */
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET booked = booked - 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $previous_departure_date_id);
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET available = available + 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $previous_departure_date_id);
                        /* add booking to current date id */
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET booked = booked + 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                        $this->db->query("UPDATE tbl_package_departure_detail
                                        SET available = available - 1
                                        WHERE package_id = " . $package_id . "
                                        AND id = " . $current_departure_date_id);
                    }
                }
            }
        }
    }

    public function _format_data($id) {
        if($id != '') {
            $package_booking_status = $this->package_book_status->getSavedData($id, 'edit');

            $data['departure_date_id'] = $package_booking_status->departure_date_id;
            $data['number_of_pax'] = $package_booking_status->number_of_pax;
            $data['price_per_person'] = $package_booking_status->price_per_person;
            $data['total_price'] = $package_booking_status->total_price;
            $data['full_name'] = $package_booking_status->full_name;
            $data['email_address'] = $package_booking_status->email_address;
            $data['contact_number'] = $package_booking_status->contact_number;
            $data['contact_address'] = $package_booking_status->contact_address;
            $data['booking_status_id'] = $package_booking_status->booking_status_id;
            $data['note'] = $package_booking_status->note;

            $data['package_name'] = $package_booking_status->package_name;
            $data['departure_date'] = $package_booking_status->departure_date;
            $data['allocation'] = $package_booking_status->all_allocation;
            $data['available'] = $package_booking_status->available;
            $data['booked'] = $package_booking_status->booked;
        } else {
            $data['departure_date_id'] = '';
            $data['number_of_pax'] = '';
            $data['price_per_person'] = '';
            $data['total_price'] = '';
            $data['full_name'] = '';
            $data['email_address'] = '';
            $data['contact_number'] = '';
            $data['contact_address'] = '';
            $data['booking_status_id'] = '';
            $data['note'] = '';
        }

        $data = (object)$data;

        return $data;
    }

    public function load_data() {
        $post = $_POST;
        $data['experience_html'] = '';
        $data['package_html'] = '';
        $data['package_dates'] = '';
        $data['package_pax'] = '';
        $data['package_unit_price'] = '';
        $data['package_name'] = '';
        $data['departure_date'] = '';
        $data['all_allocation'] = '';
        $data['booked'] = '';
        $data['available'] = '';

        if($post['type'] == 'package_destination') { /* change in package destinatin */
            $packageExperiences = $this->experience->getExperiences($post['package_destination_id']);
        }else if($post['type'] == 'package_experience') { /* change in package_experience */
            $packages = $this->package->getPackages($post['package_experience_id']);
        } else if($post['type'] == 'package') { /* change in package */
            $data['package_name'] = $this->package->get_single_data('tbl_package', 'name', $post['package_id'], 'id');
            $departure_dates = $this->public->getPackageDepartureDates($post['package_id']);
        } else if($post['type'] == 'departure_date') { /* change in departure date */
            $date_detail = $this->public->getDateDetail($post['sel_date'], $post['package_id']);
            if($post['nos_of_pax'] != 'Select') {
                $price_detail = $this->public->getPriceDetail($post['nos_of_pax'], $post['sel_date'], $post['package_id']);
            }
        } else { /* change in nos of pax */
            $price_detail = $this->public->getPriceDetail($post['nos_of_pax'], $post['sel_date'], $post['package_id']);
        }

        if(isset($packageExperiences) && !empty($packageExperiences)) {
            $packagesbyExperience = $this->package->getPackages($packageExperiences[0]->id);
            if(isset($packagesbyExperience) && !empty($packagesbyExperience)) {
                $data['package_name'] = $packagesbyExperience[0]->name;
                $departure_dates = $this->public->getPackageDepartureDates($packagesbyExperience[0]->id);

                foreach ($packagesbyExperience as $package) {
                    $data['package_html'] .= '<option value="' . $package->id . '">' . $package->name . '</option>';
                }
            }

            foreach($packageExperiences as $packageExperience) {
                $data['experience_html'] .= '<option value="' . $packageExperience->id . '">' . $packageExperience->name . '</option>';
            }
        }

        if(isset($packages) && !empty($packages)) { /* start from loading packages */
            $data['package_name'] = $packages[0]->name;
            $departure_dates = $this->public->getPackageDepartureDates($packages[0]->id);

            foreach($packages as $package) {
                $data['package_html'] .= '<option value="'.$package->id.'">'.$package->name.'</option>';
            }
        }

        if(isset($departure_dates) && !empty($departure_dates)) {
            $data['all_allocation'] = $departure_dates[0]->all_allocation;
            $data['booked'] = $departure_dates[0]->booked;
            $data['available'] = $departure_dates[0]->available;
            foreach($departure_dates as $val) {
                $data['package_dates'] .= '<option value="'.$val->id.'">'.$val->departure_date.'</option>';
            }
        } else if($post['type'] == 'departure_date') {
            $data['all_allocation'] = $date_detail->all_allocation;
            $data['booked'] = $date_detail->booked;
            $data['available'] = $date_detail->available;
        }

        if(isset($price_detail) && !empty($price_detail)) {
            $data['package_unit_price'] = $price_detail->unit_price;
        }

        echo json_encode($data);
        die;
    }

    public function export() {
        $post['search_field_type'] = $_POST['export_filter_id'];
        $post['search_field'] = 'all';
        if($post['search_field_type'] == 'package_destination') {
            $post['search_field'] = $_POST['export_filter_package_destination'];
        } else if($post['search_field_type'] == 'package_experience') {
            $post['search_field'] = $_POST['export_filter_package_experience'];
        } else if($post['search_field_type'] == 'package') {
            $post['search_field'] = $_POST['export_filter_package'];
        } else if($post['search_field_type'] == 'customer_name') {
            $post['search_field'] = $_POST['export_filter_customer_name'];
        } else if($post['search_field_type'] == 'booking_status') {
            $post['search_field'] = $_POST['export_filter_booking_status'];
        } else if($post['search_field_type'] == 'booking_date') {
            $post['search_field'] = $_POST['export_filter_booking_date'];
        } else if($post['search_field_type'] == 'departure_date') {
            $post['search_field'] = $_POST['export_filter_departure_date'];
        }

        $result = $this->package_book->getExportData($post['search_field_type'], $post['search_field']);

        if (isset($result) && !empty($result)) {
            include APPPATH . 'third_party/phpexcel/PHPExcel.php';
            $excel = new PHPExcel();
            $sheet_title = "Package-Booking- " . date('Y F d');
            //activate worksheet number 1
            $excel->setActiveSheetIndex(0);
            //name the worksheet
            $excel->getActiveSheet()->setTitle($sheet_title);
            $k = 1;
            $count = 2;
            $excel->getActiveSheet()->setCellValue('A' . $k, 'SN');
            $excel->getActiveSheet()->setCellValue('B' . $k, 'Package Destination');
            $excel->getActiveSheet()->setCellValue('C' . $k, 'Package Experience');
            $excel->getActiveSheet()->setCellValue('D' . $k, 'Package Name');
            $excel->getActiveSheet()->setCellValue('E' . $k, 'Package Code');
            $excel->getActiveSheet()->setCellValue('F' . $k, 'Customer Name');
            $excel->getActiveSheet()->setCellValue('G' . $k, 'Customer Email Address');
            $excel->getActiveSheet()->setCellValue('H' . $k, 'Customer Contact Number');
            $excel->getActiveSheet()->setCellValue('I' . $k, 'Customer Contact Address');
            $excel->getActiveSheet()->setCellValue('J' . $k, 'Package Departure Date');
            $excel->getActiveSheet()->setCellValue('K' . $k, 'Number of Pax');
            $excel->getActiveSheet()->setCellValue('L' . $k, 'Price Per Person($)');
            $excel->getActiveSheet()->setCellValue('M' . $k, 'Total Price($)');
            $excel->getActiveSheet()->setCellValue('N' . $k, 'Booking Status');
            $excel->getActiveSheet()->setCellValue('O' . $k, 'Booking Date');
            $excel->getActiveSheet()->setCellValue('P' . $k, 'Booked By');

            foreach ($result as $key => $v) {
                $excel->getActiveSheet()->setCellValueExplicit('A' . $count, $key + 1, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('B' . $count, $v->destination_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('C' . $count, $v->experience_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('D' . $count, $v->package_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('E' . $count, $v->code, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('F' . $count, $v->full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('G' . $count, $v->email_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('H' . $count, $v->contact_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('I' . $count, $v->contact_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('J' . $count, $v->departure_date, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('K' . $count, $v->number_of_pax, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('L' . $count, $v->price_per_person, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('M' . $count, $v->total_price, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('N' . $count, $v->booking_status, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('O' . $count, date('Y-m-d', $v->created_date), PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('P' . $count, ($v->created_by != '0') ? get_userdata('name') : $v->full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $count++;
            }
            //make the font become bold
            //$excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

            $filename = $sheet_title . '.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            die;
        }  else {
            set_flash('msg', 'Sorry, no result found.');
            redirect(BACKENDFOLDER.'/package_book');
        }
    }

}