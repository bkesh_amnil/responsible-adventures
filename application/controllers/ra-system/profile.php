<?php

class Profile extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('role_model', 'role');
        $this->load->model('profile_model', 'profile');
        $this->data['module_name'] = 'My Profile Manager';
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
         if($this->data['activeModulePermission']['view']) {
            $id = get_userdata('user_id');
            $this->data['sub_module_name'] = 'My_profile List';
            $this->data['profile'] = $this->user->get(1, array('id' => get_userdata('user_id')));
            $this->data['profile_roles'] = $this->role->getRoles();
            if($_POST) {
                $post = $_POST;
                $this->form_validation->set_rules($this->profile->rules);
                if($this->form_validation->run()) {
                    $POST['name'] = $post['name'];
                    $POST['email'] = $post['email'];
                    $POST['username'] = $post['username'];

                    $condition = array('id' => $id);
                    $res = $this->user->save($POST, $condition);

                    $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                    redirect(BACKENDFOLDER.'/profile');
                } else {
                    $this->form($id, 'profile');
                }
            } else {
                $this->form($id, 'profile');
            }
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

}