<?php

class Package_Enquire extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('email_send_model', 'email_send');
        $this->load->model('destination_model', 'destination');
        $this->load->model('experience_model', 'experience');
        $this->load->model('package_model', 'package');
        $this->load->model('package_enquire_model', 'package_enquire');
        $this->data['module_name'] = 'Package Enquire Manager';
        $this->data['show_add_link'] = false;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Package Enquire List';
             $this->data['package_destinations'] = $this->destination->getPackageEnquireDestination();
             $this->data['package_experiences'] = $this->experience->getPackageEnquireExperiences();
             $this->data['packages'] = $this->package->getPackageEnquireNames();
             $this->data['customers'] = $this->package_enquire->getPackageEnquireCustomers();
            $this->data['rows'] = $this->package_enquire->getPackageEnquiries();
            $this->data['body'] = BACKENDFOLDER.'/package_enquire/_list';
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/package_enquire.js');
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }
   

    public function create()
    {
        $id = segment(4);
        $this->data['id'] = $id;

        $this->data['package_destinations'] = $this->destination->getAllDestinations();
        $this->data['package_experiences'] = $this->experience->getExperiences();
        $this->data['packages'] = $this->package->getPackages();

        if ($_POST) {
            $post = $_POST;
            if ($post['note'] != '') {
                $condition = array('id' => $id);
                $res = $this->package_enquire->save($post, $condition);

                if ($res) {
                    $email_admin = '';
                    /* sending email code */
                    $footer_contact = $this->public_model->getContent('5');
                    $subject = 'Package Enquire';

                    $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                    $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="'.base_url('img/logo.png').'"></td></tr></thead>';
                    $email_admin .= '<tbody>';
                    $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                    $email_admin .= $subject;
                    $email_admin .= '</td></tr>';
                    $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                    $email_admin .= $post['note'];
                    $email_admin .= '</td></tr>';
                    $email_admin .= '<tr><td>';
                    $email_admin .= 'Regards<br/>';
                    $email_admin .= 'Responsible Adventures<br/>';
                    $email_admin .= '<img src="'.base_url('img/logo.png').'">';
                    $email_admin .= '</td>';
                    $email_admin .= '<td>';
                    $email_admin .= $footer_contact[0]->long_description;
                    $email_admin .= '</td></tr>';
                    $message = $email_admin;

                    $res = $this->email_send->sendMail($subject, $post['email_address'], $message);
                    /* sending email code */
                    if (isset($res) && ($res == 'success')) {
                        set_flash('msg', 'Data saved and mail sent to user.');
                    } else {
                        set_flash('msg', 'Data saved');
                    }
                } else {
                    set_flash('msg', 'Data not saved');
                }
            } else {
                set_flash('msg', 'No changes made.');
            }

            redirect(BACKENDFOLDER . '/package_enquire');
        } else {
            $this->form($id, 'package_enquire');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->package_enquire->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->package_enquire->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/package_enquire');
    }

    public function export() {
        $post['search_field_type'] = $_POST['export_filter_id'];
        $post['search_field'] = 'all';
        if($post['search_field_type'] == 'package_destination') {
            $post['search_field'] = $_POST['export_filter_package_destination'];
        } else if($post['search_field_type'] == 'package_experience') {
            $post['search_field'] = $_POST['export_filter_package_experience'];
        } else if($post['search_field_type'] == 'package') {
            $post['search_field'] = $_POST['export_filter_package'];
        } else if($post['search_field_type'] == 'customer_name') {
            $post['search_field'] = $_POST['export_filter_customer_name'];
        } else if($post['search_field_type'] == 'enquire_date') {
            $post['search_field'] = $_POST['export_filter_enquire_date'];
        }

        $result = $this->package_enquire->getExportData($post['search_field_type'], $post['search_field']);

        if (isset($result) && !empty($result)) {
            include APPPATH . 'third_party/phpexcel/PHPExcel.php';
            $excel = new PHPExcel();
            $sheet_title = "Package-Enquire- " . date('Y F d');
            //activate worksheet number 1
            $excel->setActiveSheetIndex(0);
            //name the worksheet
            $excel->getActiveSheet()->setTitle($sheet_title);
            $k = 1;
            $count = 2;
            $excel->getActiveSheet()->setCellValue('A' . $k, 'SN');
            $excel->getActiveSheet()->setCellValue('B' . $k, 'Package Destination');
            $excel->getActiveSheet()->setCellValue('C' . $k, 'Package Experience');
            $excel->getActiveSheet()->setCellValue('D' . $k, 'Package Name');
            $excel->getActiveSheet()->setCellValue('E' . $k, 'Package Code');
            $excel->getActiveSheet()->setCellValue('F' . $k, 'Customer Name');
            $excel->getActiveSheet()->setCellValue('G' . $k, 'Customer Email Address');
            $excel->getActiveSheet()->setCellValue('H' . $k, 'Customer Contact Number');
            $excel->getActiveSheet()->setCellValue('I' . $k, 'Customer Contact Address');
            $excel->getActiveSheet()->setCellValue('J' . $k, 'Departure Date');
            $excel->getActiveSheet()->setCellValue('K' . $k, 'Number of Pax');
            $excel->getActiveSheet()->setCellValue('L' . $k, 'Enquiry Date');
            $excel->getActiveSheet()->setCellValue('M' . $k, 'Customer Enquiry');

            foreach ($result as $key => $v) {
                $excel->getActiveSheet()->setCellValueExplicit('A' . $count, $key + 1, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('B' . $count, $v->destination_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('C' . $count, $v->experience_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('D' . $count, $v->package_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('E' . $count, $v->code, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('F' . $count, $v->full_name, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('G' . $count, $v->email_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('H' . $count, $v->contact_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('I' . $count, $v->contact_address, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('J' . $count, $v->departure_date, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('K' . $count, $v->number_of_pax, PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('L' . $count, date('Y-m-d', $v->created_date), PHPExcel_Cell_DataType::TYPE_STRING);
                $excel->getActiveSheet()->setCellValueExplicit('M' . $count, $v->enquiry, PHPExcel_Cell_DataType::TYPE_STRING);
                $count++;
            }
            //make the font become bold
            //$excel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

            $filename = $sheet_title . '.xls'; //save our workbook as this file name
            header('Content-Type: application/vnd.ms-excel'); //mime type
            header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name
            header('Cache-Control: max-age=0'); //no cache

            //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
            //if you want to save it as .XLSX Excel 2007 format
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            //force user to download the Excel file without writing it to server's HD
            $objWriter->save('php://output');
            die;
        }  else {
            set_flash('msg', 'Sorry, no result found.');
            redirect(BACKENDFOLDER.'/package_enquire');
        }
    }

}