<?php

class Package extends My_Controller
{
    var $table = 'tbl_package';
    var $package_departure_detail = 'tbl_package_departure_detail';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('destination_model', 'destination');
        $this->load->model('experience_model', 'experience');
        $this->load->model('area_model', 'area');
        $this->load->model('difficulty_model', 'difficulty');
        $this->load->model('departure_type_model', 'departure');
        $this->load->model('package_model', 'package');
        $this->data['module_name'] = 'Package Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Package List';
            $this->data['rows'] = $this->package->get('', '', 'id DESC');
            $this->data['body'] = BACKENDFOLDER.'/package/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['destinations'] = $this->destination->getAllDestinations();
        if(!empty($id)) {
            $destination_id = $this->package->get_single_data($this->table, 'destination_id', $id, 'id');
            $this->data['experiences'] = $this->experience->getExperiences($destination_id);
        } else {
            $this->data['experiences'] = $this->experience->getExperiences($this->data['destinations'][0]->id);
        }
        $this->data['areas'] = $this->area->getAreas();
        $this->data['package_difficulty'] = $this->difficulty->getPackageDifficulty();
        $this->data['package_departure'] = $this->departure->getPackageDepartures();
        if($_POST) {
            $post = $_POST;
            $this->package->id = $id;

            $this->form_validation->set_rules($this->package->rules($id));
            if($this->form_validation->run()) {
                $insert_package['destination_id'] = $post['destination_id'];
                $insert_package['experience_id'] = $post['experience_id'];
                $insert_package['area_id'] = $post['area_id'];
                $insert_package['code'] = $post['code'];
                $insert_package['name'] = $post['name'];
                $insert_package['slug'] = $post['slug'];
                $insert_package['cover_image'] = $post['cover_image'];
                $insert_package['file'] = $post['file'];
                $insert_package['map_file'] = $post['map_file'];
                $insert_package['duration'] = $post['duration'];
                $min_group_size = $insert_package['minimum_group_size'] = $post['minimum_group_size'];
                $insert_package['best_season'] = $post['best_season'];
                $insert_package['altitude']= $post['altitude'];
                $insert_package['difficulty_id'] = $post['difficulty_id'];
                $insert_package['support'] = $post['support'];
                $insert_package['is_recommended'] = $post['is_recommended'];
                $insert_package['is_highlighted'] = $post['is_highlighted'];
                $insert_package['short_description'] = $post['short_description'];
                $insert_package['description'] = $post['description'];
                $insert_package['itinerary'] = $post['itinerary'];
                $departure_type = $insert_package['departure_type'] = $post['departure_type'];
                //$departure_years = $post['departure_year'];
                //$insert_package['departure_year'] = implode(',', $post['departure_year']);
                if($id == '') {
                    $departure_years = $post['departure_year'];
                    $insert_package['departure_year'] = implode(',', $post['departure_year']);
                }
                $insert_package['meta_keywords'] = $post['meta_keywords'];
                $insert_package['meta_description'] = $post['meta_description'];
                $insert_package['status'] = $post['status'];

                $current_month = date('m');
                $current_year = date('Y');

                $this->db->trans_begin();

                if($id == '') {
                    $insert_package['created_by'] = get_userdata('user_id');
                    $insert_package['created_date'] = time();
                    $this->db->insert($this->table, $insert_package);
                    $package_id = $this->db->insert_id();
                    if( $departure_type != 'anyday') {
                        if( $departure_type != 'everyday') {
                            foreach($departure_years as $departure_year) {
                                $start_month = 01;
                                if($departure_year == $current_year) {
                                    $start_month = $current_month;
                                }
                                for($month = $start_month; $month <= 12; $month ++) {
                                    $days[] = $this->getAllSpecificDaysInAMonth($departure_year, $month,  $departure_type);
                                }
                            }
                            if(isset($days) && !empty($days)) {
                                foreach($days as $day) {
                                    foreach($day as $val) {
                                        $insert_batch[] = array(
                                            'package_id' => $package_id,
                                            'departure_date' => $val->format('Y-m-d'),
                                            'all_allocation' => $min_group_size,
                                            'available' => $min_group_size,
                                            'booked' => 0,
                                            'is_fixed_departure' => 1,
                                            'status' => 1,
                                            'created_by' => get_userdata('user_id'),
                                            'created_date' => time(),
                                        );
                                    }
                                }

                                if(isset($insert_batch) && !empty($insert_batch)) {
                                    $this->db->insert_batch($this->package_departure_detail, $insert_batch);
                                }
                            }
                        } else {
                            foreach($departure_years as $departure_year) {
                                $start_month = 01;
                                if($departure_year == $current_year) {
                                    $start_month = $current_month;
                                }
                                for($month = $start_month; $month <= 12; $month ++) {
                                    $days[] = $this->saveAllDaysInAMonth($departure_year, $month);
                                }
                            }
                            if(isset($days) && !empty($days)) {
                                foreach($days as $day) {
                                    foreach($day as $val) {
                                        $insert_batch[] = array(
                                            'package_id' => $package_id,
                                            'departure_date' => $val,
                                            'all_allocation' => $min_group_size,
                                            'available' => $min_group_size,
                                            'booked' => 0,
                                            'is_fixed_departure' => 1,
                                            'status' => 1,
                                            'created_by' => get_userdata('user_id'),
                                            'created_date' => time(),
                                        );
                                    }
                                }
                                if(isset($insert_batch) && !empty($insert_batch)) {
                                    $this->db->insert_batch($this->package_departure_detail, $insert_batch);
                                }
                            }
                        }
                    }
                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        set_flash('msg', 'Data saved');
                    }
                } else {
                    $saved_departure_year = $post['saved_departure_year'];
                    $insert_package['departure_year'] = $saved_departure_year;
                    if(isset($post['departure_year']) && !empty($post['departure_year'])) {
                        $departure_years = $post['departure_year'];
                        $insert_package['departure_year'] = $saved_departure_year . ',' . implode(',', $post['departure_year']);
                    }
                    $insert_package['updated_by']	 = get_userdata('user_id');
                    $insert_package['updated_date'] = time();
                    $this->db->where('id', $id);
                    $this->db->update($this->table, $insert_package);

                    if(isset($departure_years) && !empty($departure_years)) {
                        if( $departure_type != 'anyday') {
                            if( $departure_type != 'everyday') {
                                foreach($departure_years as $departure_year) {
                                    $start_month = 01;
                                    if($departure_year == $current_year) {
                                        $start_month = $current_month;
                                    }
                                    for($month = $start_month; $month <= 12; $month ++) {
                                        $days[] = $this->getAllSpecificDaysInAMonth($departure_year, $month, $departure_type);
                                    }
                                }
                                if(isset($days) && !empty($days)) {
                                    foreach($days as $day) {
                                        foreach($day as $val) {
                                            $insert_batch[] = array(
                                                'package_id' => $id,
                                                'departure_date' => $val->format('Y-m-d'),
                                                'all_allocation' => $min_group_size,
                                                'available' => $min_group_size,
                                                'booked' => 0,
                                                'is_fixed_departure' => 1,
                                                'status' => 1,
                                                'created_by' => get_userdata('user_id'),
                                                'created_date' => time()
                                            );
                                        }
                                    }

                                    if(isset($insert_batch) && !empty($insert_batch)) {
                                        $this->db->insert_batch($this->package_departure_detail, $insert_batch);
                                    }
                                }
                            } else {
                                foreach($departure_years as $departure_year) {
                                    $start_month = 01;
                                    if($departure_year == $current_year) {
                                        $start_month = $current_month;
                                    }
                                    for($month = $start_month; $month <= 12; $month ++) {
                                        $days[] = $this->saveAllDaysInAMonth($departure_year, $month);
                                    }
                                }
                                if(isset($days) && !empty($days)) {
                                    foreach($days as $day) {
                                        foreach($day as $val) {
                                            $insert_batch[] = array(
                                                'package_id' => $id,
                                                'departure_date' => $val,
                                                'all_allocation' => $min_group_size,
                                                'available' => $min_group_size,
                                                'booked' => 0,
                                                'is_fixed_departure' => 1,
                                                'status' => 1,
                                                'created_by' => get_userdata('user_id'),
                                                'created_date' => time()
                                            );
                                        }
                                    }
                                    if(isset($insert_batch) && !empty($insert_batch)) {
                                        $this->db->insert_batch($this->package_departure_detail, $insert_batch);
                                    }
                                }
                            }
                        }
                    }

                    if ($this->db->trans_status() === FALSE) {
                        $this->db->trans_rollback();
                        set_flash('msg', 'Data could not be saved');
                    } else {
                        $this->db->trans_commit();
                        set_flash('msg', 'Data saved');
                    }
                }

                redirect(BACKENDFOLDER.'/package');
            } else {
                $this->form($id, 'package');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/package.js');
            $this->form($id, 'package');
        }
    }

    public function getAllSpecificDaysInAMonth($year, $month, $day, $daysError = 3)
    {
        $dateString = 'first '.$day.' of '.$year.'-'.$month;
        if (!strtotime($dateString))
        {
            throw new \Exception('"'.$dateString.'" is not a valid strtotime');
        }
        $startDay = new \DateTime($dateString);
        $days = array();
        while ($startDay->format('Y-m') <= $year.'-'.str_pad($month, 2, 0, STR_PAD_LEFT))
        {
            $days[] = clone($startDay);
            $startDay->modify('+ 7 days');
        }

        return $days;
    }

    public function saveAllDaysInAMonth($year, $month)
    {
        $days = array();
        $num_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        for($d = 01; $d <= $num_of_days; $d++)
        {
            $time = mktime(12, 0, 0, $month, $d, $year);
            $monthName = date('F', mktime(0, 0, 0, $month, 10));
            if (date('m', $time) == $month) {
                $days[] = date('Y-m-d', $time);
            }
        }

        return $days;
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "tbl_package_booking.package_id";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->package->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->package->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/package');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->package->changeStatus('package', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->package->changeStatus('package', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/package');
    }

    public function getExperiences() {
        $post = $_POST;
        $html = '';

        $data = $this->experience->getExperiences($post['destination_id']);

        if(isset($data) && !empty($data)) {
            foreach($data as $val) {
                $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }
        }

        echo json_encode($html);
        die;
    }

}