<?php

class Dynamic_form extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('dynamic_form/dynamic_form_model', 'dynamic_form');
        $this->data['module_name'] = 'Dynamic Form Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Dynamic Form List';
            $this->data['rows'] = $this->dynamic_form->get_forms(0, TRUE, 0, 'form_title');
            $this->data['body'] = BACKENDFOLDER.'/dynamic_form/_list';
            $this->render();
             } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
             }
    }

    public function create()
    {
        $id = segment(4);
        $this->data['availableForms'] = $this->dynamic_form->get_available_forms($id);
        if($_POST) {
            $post = $_POST;
            $this->dynamic_form->id = $id;

            $this->form_validation->set_rules($this->dynamic_form->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->dynamic_form->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->dynamic_form->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/dynamic_form');
            } else {
                $this->form($id, 'dynamic_form');
            }
        } else {
            $this->form($id, 'dynamic_form');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->dynamic_form->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->dynamic_form->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/dynamic_form');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->dynamic_form->changeStatus('dynamic_form', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->dynamic_form->changeStatus('dynamic_form', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/dynamic_form');
    }

}