<?php

class Package_map extends My_Controller
{
    var $table = 'tbl_package_map';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_map_model', 'package_map');
        $this->data['module_name'] = 'Package Map Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {

        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($id = '')
    {
          if($this->data['activeModulePermission']['view']) {
            $package_id = $this->uri->segment(3);
            $package_name = $this->package_map->get_single_data('tbl_package', 'name', $package_id, 'id');
            $this->data['sub_module_name'] = 'Package Map List for <b>' . $package_name .'</b>';
            $this->data['rows'] = $this->package_map->getPackageMaps($package_id);
            $this->data['body'] = BACKENDFOLDER.'/package_map/_list';
            $this->render();
         } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $package_id = segment(4);
        $package_name = $this->package_map->get_single_data('tbl_package', 'name', $package_id, 'id');
        $id = segment(5);
        $this->data['package_id'] = $package_id;
        $this->data['rows'] = $this->package_map->getPackageMaps($package_id);
        if($_POST) {
            $post = $_POST;
            $alphas = range('A', 'Z');
            $title_arr = $post['title'];
            $address_arr = $post['address'];
            $description_arr = $post['description'];
            $latitude_arr = $post['latitude'];
            $longitude_arr = $post['longitude'];
            $order_arr = $post['order'];

            if(isset($title_arr) && !empty($title_arr)) {
                foreach($title_arr as $ind => $val) {
                    $array[] = array(
                        'title' => $val,
                        'address' => $address_arr[$ind],
                        'description' => $description_arr[$ind],
                        'latitude' => $latitude_arr[$ind],
                        'longitude' => $longitude_arr[$ind],
                        'order' => $order_arr[$ind]
                    );
                }
            }

            function sortByOrder($a, $b) {
                return $a['order'] - $b['order'];
            }

            usort($array, 'sortByOrder');

            if(isset($array) && !empty($array)) {
                foreach($array as $ind => $val) {
                    $order = $ind+1;
                    $insert_batch[] = array(
                        'package_id' => $package_id,
                        'latitude' => $val['latitude'],
                        'longitude' => $val['longitude'],
                        'marker_title' => $val['title'],
                        'marker_address' => $val['address'],
                        'marker_description' => $val['description'],
                        'marker_label' => $alphas[$ind],
                        'marker_sort_order' => $order,
                        'created_by' => get_userdata('user_id'),
                        'created_date' => time()
                    );
                }
            }

            if(isset($insert_batch) && !empty($insert_batch)) {
                $this->db->where('package_id', $package_id);
                $this->db->delete($this->table);

                if($this->db->insert_batch($this->table, $insert_batch)) {
                    set_flash('msg', 'Data saved');
                } else {
                    set_flash('msg', 'Data could not be saved');
                }
            }
            redirect(BACKENDFOLDER.'/package_map/' . $package_id);
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/map.js');
            $this->form($id, 'package_map', $package_name);
        }
    }

    public function delete()
    {
        $package_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->package_map->delete(array('id' => $selected_id, 'package_id' => $package_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->package_map->delete(array('id' => $id, 'package_id' => $package_id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/package_map/' . $package_id);
    }

}