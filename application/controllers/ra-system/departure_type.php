<?php

class Departure_type extends My_Controller
{
    var $table = 'tbl_departure_type';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('departure_type_model', 'departure');
        $this->data['module_name'] = 'Booking Status Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
         if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Booking Status List';
            $this->data['rows'] = $this->departure->get('', '', 'position');
            $this->data['body'] = BACKENDFOLDER.'/departure/_list';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        if($_POST) {
            $post = $_POST;
            $this->departure->id = $id;

            $this->form_validation->set_rules($this->departure->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->departure->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->departure->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/departure_type');
            } else {
                $this->form($id, 'departure');
            }
        } else {
            $this->form($id, 'departure');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "tbl_tour.departure_type|tbl_bike.rent_type";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                $val = $this->departure->get_single_data($this->table, "code", $selected_id);
                if($this->restrict_delete->check_for_delete($params, $val)) {
                    $res = $this->departure->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            $val = $this->departure->get_single_data($this->table, "code", $id);

            if($this->restrict_delete->check_for_delete($params, $val)) {
                $id = segment(4);
                $res = $this->departure->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/departure_type');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->departure->changeStatus('departure', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->departure->changeStatus('departure', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/departure_type');
    }

}