<?php

class Package_inclusion_exclusion extends My_Controller
{
    var $table = 'tbl_package_inclusion_exclusion';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('package_inclusion_exclusion_model', 'package_inclusion_exclusion');
        $this->data['module_name'] = 'Package Inclusion Exclusion Manager';
        $this->data['show_add_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    function _remap($method)
    {

        $param_offset = 2;
        // Default to index
        if ( ! method_exists($this, $method))
        {
            // We need one more param
            $param_offset = 1;
            $method = 'index';
        }
        // Since all we get is $method, load up everything else in the URI
        $params = array_slice($this->uri->rsegment_array(), $param_offset);
        // Call the determined method with all params
        call_user_func_array(array($this, $method), $params);
    }

    public function index($id = '')
    {
         if($this->data['activeModulePermission']['view']) {
            $package_id = $this->uri->segment(3);
            $package_name = $this->package_inclusion_exclusion->get_single_data('tbl_package', 'name', $package_id, 'id');
            $this->data['sub_module_name'] = 'Package Inclusion Exclusion List for <b>' . $package_name .'</b>';
            $this->data['rows'] = $this->package_inclusion_exclusion->getPackageInclusionExclustion($package_id);
            $this->data['body'] = BACKENDFOLDER.'/package_inclusion_exclusion/_list';
            $this->render();
         } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $package_id = segment(4);
        $package_name = $this->package_inclusion_exclusion->get_single_data('tbl_package', 'name', $package_id, 'id');
        $id = segment(5);
        $this->data['package_id'] = $package_id;
        if($_POST) {
            $post = $_POST;
             $this->package_inclusion_exclusion->id = $id;
            if($id == 0) {
                $package_data_type_arr = $_POST['hidden_package_data_type'];
                $package_data_arr = $_POST['hidden_package_data'];
                if (empty($package_data_type_arr) && empty($package_data_arr)) {
                    $this->form_validation->set_rules('package_data', 'Package Data', "trim|required");
                }
            }
            $this->form_validation->set_rules($this->package_inclusion_exclusion->rules($id));

            if($this->form_validation->run()) {
                if($id == '0') {
                    foreach($package_data_type_arr  as $ind => $val) {
                        $insert_batch[] = array(
                            'package_id' => $package_id,
                            'data_type' => $val,
                            'data' => $package_data_arr[$ind],
                            'status' => $this->input->post('status'),
                            'created_by' => get_userdata('user_id'),
                            'created_date' => time()
                        );
                    }
                    $res = $this->db->insert_batch($this->table, $insert_batch);
                } else {
                    $insert_data['data_type'] = $this->input->post('data_type');
                    $insert_data['data'] = $this->input->post('data');
                    $insert_data['status'] = $this->input->post('status');
                    $insert_data['updated_by'] = get_userdata('user_id');
                    $insert_data['updated_date'] = time();
                    $this->db->where('id', $id);
                    $this->db->where('package_id', $package_id);

                    $res = $this->db->update($this->table, $insert_data);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/package_inclusion_exclusion/' . $package_id);
            } else {
                $this->form($id, 'package_inclusion_exclusion', $package_name);
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/dynamic_add.js');
            $this->form($id, 'package_inclusion_exclusion', $package_name);
        }
    }

    public function delete()
    {
        $package_id = segment(4);
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->package_inclusion_exclusion->delete(array('id' => $selected_id, 'package_id' => $package_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(5);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->package_inclusion_exclusion->delete(array('id' => $id, 'package_id' => $package_id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/package_inclusion_exclusion/' . $package_id);
    }

    public function status()
    {
        $post = $_POST;
        $package_id = segment(5);
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->package_inclusion_exclusion->changeStatus('package_inclusion_exclusion', $status, $selected_id, $package_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(6);
            $res = $this->package_inclusion_exclusion->changeStatus('package_inclusion_exclusion', $status, $id, $package_id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/package_inclusion_exclusion/' . $package_id);
    }

}