<?php
class Menu extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('category_model', 'category');
        $this->load->model('menu_model', 'menu');
        $this->data['module_name'] = 'Menu Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['menus'] = $this->menu->getAllData();
            $this->data['menu_types'] = $this->menu->getMenuTypes();
            $this->data['sub_module_name'] = 'Menu List';
            $this->data['body'] = BACKENDFOLDER.'/menu/_list';
            $this->render();
            } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }

    }

    public function create()
    {
        $id = segment(4);
        if(!empty($id)) {
            $this->data['settings'] = $this->menu->get_menu_page_settings($id);
        } else {
            $this->data['settings'] = $this->getSettings();
        }
        $this->data['modules'] = $this->menu->get_enabled_module(1);
        $this->data['menu_type'] = $this->menu->get_menutypes();
        $this->data['menu_parents'] = $this->menu->getAllData();
        $this->data['menu_link_type']['linktypes'] = $this->menu->get_menulinktype();
        foreach ($this->data['menu_link_type']['linktypes'] as $key =>$data){
            $this->data['menu_link_type']['link'][$key] = $this->menu->get_menulinks($key);
        }
        $this->data['menu_opens']  =  array('same' => 'Same Window/Tab', 'new' => 'New Window/Tab');
        $this->data['addJs'] = array('assets/'.BACKENDFOLDER.'/dist/js/menu.js');
        $id = segment(4);
        if($_POST) {
            $post = $_POST;

            $this->menu->id = $id;
            $this->form_validation->set_rules($this->menu->rules($id));
            if($this->form_validation->run()) {
                $insert_data['menu_type_id']    = $post['menu_type'];
                $insert_data['name']      = $post['name'];
                $insert_data['slug']      = $post['slug'];
                $insert_data['menu_parent'] = $post['menu_parent'];
                $insert_data['page_title']          = $post['page_title'];
                $insert_data['meta_keywords']       = $post['meta_keywords'];
                $insert_data['menu_heading']        = $post['menu_heading'];
                $insert_data['meta_description']    = $post['meta_description'];
                $insert_data['status']              = $post['status'];
                $insert_data['relate_destination']  = '0';
                //Menu Page Settings
                $insert_data['menu_link_type']  = $post['menu_link_type'];
                $insert_data['menu_opens']      = $post['menu_opens'];
                $insert_data['menu_url']        = $post['menu_url'];
                $insert_data['menu_site']       = '';
                if($id == '') {
                    $res = $this->db->insert('tbl_menu', $insert_data);
                    $id = $this->db->insert_id();
                }else{
                    $this->db->where('id', $id);
                    $res = $this->db->update('tbl_menu', $insert_data);
                }
                if($post['menu_link_type']=="page"){
                    $modulesIdAdded = $this->input->post('module_id');
                    $selectedModuleContent = $this->input->post('selectedModulesIds');
                    $module_title = $this->input->post('module_title');
                    $allSelectedModuleContent = $this->input->post('allSelectedModuleContent');
                    $categorysName = $this->input->post('category_id');
                    if(!empty($modulesIdAdded) && is_array($modulesIdAdded)){
                        $count = 1;
                        $this->db->where("menu_id", $id);
                        $this->db->delete('tbl_menu_page_settings');
                        foreach($modulesIdAdded as $modInd=>$modVal){
                            unset($insert_data_menupage);
                            $category_id = 0;
                            if(isset($categorysName[$modInd]) && !empty($categorysName[$modInd])){
                                $category_id = $categorysName[$modInd];
                            }else{
                                $category_id = $categorysName[$modInd];
                            }
                            $insert_data_menupage['menu_id']    = $id;
                            $insert_data_menupage['module_id']  = $modVal;
                            if($modVal == '75') {
                                $insert_data_menupage['content_ids'] = '';
                            } else {
                                $insert_data_menupage['content_ids']    = $selectedModuleContent[$modInd];
                            }
                            $insert_data_menupage['position']   = $modInd;
                            $insert_data_menupage['module_title']   =  $module_title[$modInd];
                            $insert_data_menupage['category_id']    =  $category_id;
                            if(isset($allSelectedModuleContent[$modInd]) && $allSelectedModuleContent[$modInd] == "true"){
                                $insert_data_menupage['all_within_category'] =  'yes';
                            }else{
                                $insert_data_menupage['all_within_category'] =  'no';
                            }

                            $this->db->insert("tbl_menu_page_settings", $insert_data_menupage);
                        }
                    }
                }
                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/menu');
            } else {
                $this->form($id, 'menu');
            }
        } else {
            $this->form($id, 'menu');
        }
    }

    public function getSettings() {
        $module_title = $this->input->post('module_title');
        $module_id = $this->input->post('module_id');
        $category_id = $this->input->post('category_id');
        $selectedModulesIds = $this->input->post('selectedModulesIds');
        $settings = array();
        if(!empty($module_title) && is_array($module_title) && !empty($module_id) && is_array($module_id)){
            foreach($module_title as $ind=>$val){
                $list = array("id" => "", "module_title"=>$module_title[$ind], "menu_id"=>"", "module_id"=>$module_id[$ind], "content_ids"=>$selectedModulesIds[$ind], "category_id"=>$category_id[$ind], "position"=>$ind);
                array_push($settings, $list);
            }
        }
        return $settings;
    }

    public function getModuleContents() {
        if ($this->input->is_ajax_request()) {
            $siteId = $this->input->post('siteId');
            $moduleId = $this->input->post('moduleId');
            $position = $this->input->post('position');
            $category = $this->input->post('category_id');
            $selected = $this->input->post('selecteds');
            $this->administrator_model->get_module_contents($siteId, $moduleId, $position, $selected, $category);
        }
    }

    public function getAvailableMenus($level = 0) {
        $menuType = $this->input->post('menuType');

        if (empty($menuType)) {
            $options = "";
        } else {
            $this->db->where("id", $menuType);
            $settingData = $this->db->get('tbl_site_menus')->row();
            if ($settingData->submenus == "yes") {
                if ($settingData->depth == 0) {
                    $depth = 999;
                } else {
                    $depth = $settingData->depth;
                }
                $options = $this->getMenus($settingData->id, $depth);
            } else {
                $options = "";
            }
        }

        $html = '<option value="">Parent</option>';
        if (!empty($options) && is_array($options)) {
            $html .= $this->generateHtml($options);
        }
        echo $html;
    }

    function generateHtml($options, $maxDepth = 0) {
        $html = "";
        foreach ($options as $ind => $val) {
            $depth = $val['depth'];
            if ($maxDepth == 0) {
                $maxDepth = $depth;
            }
            $actDepth = $maxDepth - $depth;
            $spaces = "";
            for ($i = 0; $i < $actDepth; $i++) {
                $spaces .= '&nbsp;';
            }
            $html .= '<option value="' . $val['id'] . '">' . $spaces . $val['name'] . '</option>';
            if (!empty($val['childs']) && is_array($val['childs'])) {
                $html .= $this->generateHtml($val['childs'], $maxDepth);
            }
        }
        return $html;
    }

    public function getMenus($typeId, $depth = 0, $parent = 0) {
        if ($depth == 0) {
            return false;
        }
        $this->db->where('menu_type_id', $typeId);
        $this->db->where('status', "1");
        $this->db->where('menu_parent', $parent);
        $menuData = $this->db->get('tbl_menu')->result();

        if (!empty($menuData) && is_array($menuData)) {
            foreach ($menuData as $ind => $val) {
                $data[] = array("id" => $val->id, "name" => $val->name, "depth" => $depth, "childs" => $this->getMenus($typeId, ($depth - 1), $val->id));
            }
        }
        if (!empty($data) && is_array($data)) {
            return $data;
        }
        return false;
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "tbl_menu_page_settings.menu_id";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->menu->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->menu->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/menu');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->menu->changeStatus('menu', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->menu->changeStatus('menu', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/menu');
    }

}