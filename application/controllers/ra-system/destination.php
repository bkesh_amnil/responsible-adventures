<?php

class Destination extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('menu_model', 'menu');
        $this->load->model('destination_model', 'destination');
        $this->data['module_name'] = 'Destination Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
         if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Destination List';
            $this->data['rows'] = $this->destination->get('', '', 'position');
            $this->data['body'] = BACKENDFOLDER.'/destination/_list';
            $this->render();
            } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);

        if($_POST) {
            $post = $_POST;

            $this->destination->id = $id;

            $this->form_validation->set_rules($this->destination->rules($id));
            if($this->form_validation->run()) {
                if(isset($post['media']) && !empty($post['media'])) {
                    $medias = $post['media'];
                    $mediaTitles = $post['title'];
                    $mediaDescriptions = $post['description'];
                    unset($post['media'], $post['title'], $post['description']);
                }
                unset($post['images']);
                if($id == '') {
                    $res = $this->destination->save($post, '', true);
                    $id = $res;
                    /* add destination as menu under Destination menu */
                    $insert_menu['menu_type_id'] = '2';
                    $insert_menu['name'] = $post['name'];
                    $insert_menu['slug'] = $post['slug'];
                    $insert_menu['sub_title'] = $post['sub_title'];
                    $insert_menu['menu_link_type'] = 'none';
                    $insert_menu['menu_parent'] = '2';
                    $insert_menu['page_title'] = $post['name'];
                    $insert_menu['menu_heading'] = $post['name'];
                    $insert_menu['meta_keywords'] = $post['meta_keywords'];
                    $insert_menu['meta_description'] = $post['meta_description'];
                    $insert_menu['menu_opens'] = 'same';
                    $insert_menu['menu_site'] = '0';
                    $insert_menu['status'] = '1';
                    $insert_menu['relate_destination'] = '1';
                    $insert_menu['destination_id'] = $id;

                    $this->db->insert('tbl_menu', $insert_menu);
                    /* add destination as menu under Destination menu */
                } else {
                    $condition = array('id' => $id);
                    $res = $this->destination->save($post, $condition);
                    /* update related destination in menu */
                    $update_menu['name'] = $post['name'];
                    $update_menu['slug'] = $post['slug'];
                    $update_menu['meta_keywords'] = $post['meta_keywords'];
                    $update_menu['meta_description'] = $post['meta_description'];

                    $this->db->where('destination_id', $id);
                    $this->db->update('tbl_menu', $update_menu);
                    /* update related destination in menu */
                }
                // saving destination media
                $this->load->model('destinationmedia_model', 'destinationmedia');
                if($id != '') {
                    $this->destinationmedia->delete(array('destination_id' => $id));
                }
                foreach($medias as $key => $singleMedia) {
                    $mediaInsertData = array(
                        'destination_id' => $id,
                        'media' => $singleMedia,
                        'title' => isset($mediaTitles) ? $mediaTitles[$key] : '',
                        'caption' => isset($mediaDescriptions) ? $mediaDescriptions[$key] : ''
                    );
                    $this->destinationmedia->save($mediaInsertData);
                }
                
                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/destination');
            } else {
                $this->form($id, 'destination');
            }
        } else {
            $this->data['addJs'] = array('assets/' . BACKENDFOLDER . '/dist/js/gallery.js');
            if($id != '') {
                $this->data['savedMedia'] = $this->destination->getSavedMedia($id);
            }
            $this->form($id, 'destination');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "tbl_activity.destination_id|tbl_experience.destination_id";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->destination->delete(array('id' => $selected_id));
                    if ($res) {
                        $this->menu->delete(array('destination_id' => $selected_id));
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->destination->delete(array('id' => $id));
                $this->menu->delete(array('destination_id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/destination');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->destination->changeStatus('destination', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->destination->changeStatus('destination', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/destination');
    }

}