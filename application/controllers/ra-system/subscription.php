<?php

class Subscription extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('email_send_model', 'email_send');
        $this->load->model('subscription_model', 'subscription');
        $this->data['module_name'] = 'Subscription Manager';
        $this->data['show_add_link'] = false;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Subscription List';
            $this->data['rows'] = $this->subscription->get();
            $this->data['body'] = BACKENDFOLDER.'/subscription/_list';
            $this->render();
         } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->subscription->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $res = $this->subscription->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/subscription');
    }

    public function send_mail() {
        $post = $_POST;
        if(isset($post['selected']) && !empty($post['selected'])) {
            $to_emails = $post['selected'];
        } else {
            $to_emails = explode(',', $post['to_emails']);
        }
        $this->data['subcriptions'] = $this->_format_data($to_emails);

        if(isset($post['message']) && !empty($post['message'])) {
            $footer_contact = $this->public_model->getContent('5');
            $email_user = '';
            $subject = (isset($post['subject']) && !empty($post['subject'])) ? $post['subject'] : 'Subscription';

            $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
            $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="'.base_url('img/logo.png').'"></td></tr></thead>';
            $email_user .= '<tbody>';
            $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
            $email_user .= $subject;
            $email_user .= '</td></tr>';
            $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
            $email_user .= $post['message'];
            $email_user .= '</td></tr>';
            $email_user .= '<tr><td>';
            $email_user .= 'Regards<br/>';
            $email_user .= 'Responsible Adventures<br/>';
            $email_user .= '<img src="'.base_url('img/logo.png').'">';
            $email_user .= '</td>';
            $email_user .= '<td>';
            $email_user .= $footer_contact[0]->long_description;
            $email_user .= '</td></tr>';

            $message = $email_user;

            $res = $this->email_send->sendMail($subject, $post['to_emails'], $message, $post['file']);

            if(isset($res) && ($res == 'success')) {
                set_flash('msg', 'Mail sent successfully.');
            } else {
                set_flash('msg', 'Error in sending mail');
            }
            redirect(BACKENDFOLDER . '/subscription');
        } else {
            $this->data['sub_module_name'] = 'Subscription Mail List';
            $this->data['body'] = BACKENDFOLDER . '/subscription/_send_mail';
            $this->render();
        }
    }

    public function _format_data($emails) {
        if(isset($_POST) && (!empty($_POST['subjcet']) || !empty($_POST['message']))) {
            $post = $_POST;
            $data['to_emails'] = $emails;
            $data['subject'] = $post['subject'];
            $data['message'] = $post['message'];
        } else {
            $data['to_emails'] = $emails;
            $data['subject'] = '';
            $data['message'] = '';
        }

        $data = (object)$data;

        return $data;
    }

    public function export_excel() {
        $result = $this->subscription->getExcelData();

        require_once(APPPATH . 'third_party/excel_export/export-xls.class.php');
        #The file name you want any resulting file to be called.
        $filename = date('Y-m-d_H:i:j') . '.xls';
        #create an instance of the class
        $xls = new ExportXLS($filename);
        # Lets add some sample data
        #
        # Of course this can be from a SQL query or anyother data source
        #
        if(count($result) > 0 )
        {
            $headers =  $result[0];
            $header = array();

            $header[] = 'ID';
            $header[] = 'Email';
            $header[] = 'Date';

            $xls->addHeader($header);

            $count = 1;
            foreach($result as $r)
            {
                $row = array();
                foreach($headers as $index => $value)
                {

                    $row[] = ucfirst($r[$index]);
                }

                $xls->addRow($row);

            }
        }

        # You can return the xls as a variable to use with;
        # $sheet = $xls->returnSheet();
        #
        # OR
        #
        # You can send the sheet directly to the browser as a file
        #
        $xls->sendFile();
    }

}