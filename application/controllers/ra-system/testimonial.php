<?php

class Testimonial extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('testimonial_model', 'testimonial');
        $this->data['module_name'] = 'Testimonial Manager';
        $this->data['show_add_link'] = true;
        $this->data['show_sort_link'] = true;
        $this->header['page_name']	= $this->router->fetch_class();
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = 'Testimonials List';
            $this->data['rows'] = $this->testimonial->get_testimonials(0, "all");
            $this->data['body'] = BACKENDFOLDER.'/testimonial/_list';
            $this->render();
         } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

    public function create()
    {
        $id = segment(4);
        if($id == '')
            $this->data['testimonial'] = $this->testimonial->get();
        else
            $this->data['testimonial'] = $this->testimonial->get('', array('id !=' => $id));
        $this->data['countries'] = $this->db->select('id, printable_name')->get('tbl_countries')->result();
        if($_POST) {
            $post = $_POST;
            $this->testimonial->id = $id;
            $this->form_validation->set_rules($this->testimonial->rules($id));
            if($this->form_validation->run()) {
                if($id == '') {
                    $res = $this->testimonial->save($post);
                } else {
                    $condition = array('id' => $id);
                    $res = $this->testimonial->save($post, $condition);
                }

                $res ? set_flash('msg', 'Data saved') : set_flash('msg', 'Data could not be saved');
                redirect(BACKENDFOLDER.'/testimonial');
            } else {
                $this->form($id, 'testimonial');
            }
        } else {
            $this->data['addJs'] = array('assets/datepicker/bootstrap-datepicker.js','assets/'.BACKENDFOLDER.'/dist/js/jquery.textarea-counter.js');
            $this->data['addCss'] = array('assets/datepicker/datepicker3.css');
            $this->form($id, 'testimonial');
        }
    }

    public function delete()
    {
        $post = $_POST;

        $this->load->library('restrict_delete');
        $params = "";
        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $deleted = 0;
            foreach($selected_ids as $selected_id){
                if($this->restrict_delete->check_for_delete($params, $selected_id)) {
                    $res = $this->testimonial->delete(array('id' => $selected_id));
                    if ($res) {
                        $deleted++;
                    }
                }
            }

            $deleted ? set_flash('msg', $deleted . ' out of ' . count($selected_ids) . ' data deleted successfully') : set_flash('msg', 'Data could not be deleted');

        } else {
            $id = segment(4);
            if($this->restrict_delete->check_for_delete($params, $id)) {
                $id = segment(4);
                $res = $this->testimonial->delete(array('id' => $id));

                $msg = $res ? 'Data deleted' : 'Error in deleting data';
            } else {
                $msg = 'This data cannot be deleted. It is being used in system.';
            }

            set_flash('msg', $msg);
        }

        redirect(BACKENDFOLDER.'/testimonial');
    }

    public function status()
    {
        $post = $_POST;
        $status = segment(4) == '0' ? '1' : '0';

        if(isset($post) && !empty($post)) {
            $selected_ids = $post['selected'];
            $changed = 0;
            foreach($selected_ids as $selected_id) {
                $res = $this->testimonial->changeStatus('testimonial', $status, $selected_id);
                if($res) {
                    $changed++;
                }
            }
            $changed ? set_flash('msg', $changed . ' out of ' . count($selected_ids) . ' data status changed successfully') : set_flash('msg', 'Status could not be changed');
        } else {
            $id = segment(5);
            $res = $this->testimonial->changeStatus('testimonial', $status, $id);

            $res ? set_flash('msg', 'Status changed') : set_flash('msg', 'Status could not be changed');
        }

        redirect(BACKENDFOLDER.'/testimonial');
    }
}