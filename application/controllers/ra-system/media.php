<?php

class Media extends My_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['module_name'] = 'Media Manager';
        $this->data['show_add_link'] = false;
    }

    public function index()
    {
        if($this->data['activeModulePermission']['view']) {
            $this->data['sub_module_name'] = $this->data['module_name'];
            $this->data['body'] = BACKENDFOLDER.'/media/_index';
            $this->render();
        } else {
            set_flash('msg', 'Sorry, you don\'t have the necessary permission.');
            redirect(BACKENDFOLDER.'/dashboard');
        }
    }

}