<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dynamic_form extends CI_Controller {

    function __Construct() {
        parent::__Construct();
        $this->load->model('dynamic_form/public_form_model');
        $this->load->model('dynamic_form/form_model', 'form_fields');
        $this->load->model('email_send_model', 'email_send');
    }

    /**
     * just for test
     */
    function form_submit_test() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {
            $data['action'] = 'success';
            $data['msg'] = 'yo success';
        } else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
    }

    function form_submit() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {

            $form_id = $this->input->post('form_id');

            $form = $this->public_form_model->get_forms($form_id);
            $data['action'] = 'success';
            $fields = $this->input->post('field_name');

            $result = $this->form_fields->getFormFields($form_id);

            foreach ($result as $row) {
                if ($row->field_type != 'upload' && isset($fields[$row->id])) {
                    $_POST['field_' . $row->id] = $fields[$row->id];
                    $this->form_validation->set_rules('field_' . $row->id, $row->field_label, $row->validation_rule);
                }
            }
            if (isset($result) && !empty($result)) {
                foreach ($result as $field) {
                    $field_arr[$field->field_name] = $field->id;
                }
            }

            $submission['form_id'] = $form->id;

            if ($form->submit_action == 'email' || $form->submit_action == 'both') {
                if ($form->admin_email) {
                    $footer_contact = $this->public_model->getContent('5');

                    if ($form_id == '1') {
                        $email_admin = $email_user = '';
                        $subject = $form->admin_email_subject;

                        $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                        $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                        $email_admin .= '<tbody>';
                        $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                        $email_admin .= $subject;
                        $email_admin .= '</td></tr>';
                        $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                        $link_arr = array(
                            '{full_name}' => ucwords($fields[$field_arr['full_name']]),
                            '{email}' => $fields[$field_arr['email']],
                            '{mobile}' => $fields[$field_arr['phone']],
                            '{message}' => $fields[$field_arr['message']]
                        );
                        $email_admin .= strtr($form->admin_email_msg, $link_arr);
                        $email_admin .= '</td></tr>';
                        $email_admin .= '<tr><td>';
                        $email_admin .= 'Regards<br/>';
                        $email_admin .= 'Responsible Adventures<br/>';
                        $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                        $email_admin .= '</td>';
                        $email_admin .= '<td>';
                        $email_admin .= $footer_contact[0]->long_description;
                        $email_admin .= '</td></tr>';

                        $body = $email_admin;
                        $this->email_send->sendMail($subject, '', $body);

                        if ($form->email_to_user) {
                            $subject1 = $form->user_email_subject;
                            $to_email = $fields[$field_arr['email_address']];

                            $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                            $email_user .= '<thead><tr><td style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                            $email_user .= '<tbody>';
                            $email_user .= '<tr><td style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                            $email_user .= $subject1;
                            $email_user .= '</td></tr>';
                            $email_user .= '<tr><td style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                            $link_arr1 = array(
                                '{full_name}' => ucwords($fields[$field_arr['full_name']])
                            );
                            $email_user .= strtr($form->user_email_msg, $link_arr1);
                            $email_user .= '</td></tr>';
                            $email_user .= '<tr><td>';
                            $email_user .= 'Regards<br/>';
                            $email_user .= 'Responsible Adventures<br/>';
                            $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                            $email_user .= '</td>';
                            $email_user .= '<td>';
                            $email_user .= $footer_contact[0]->long_description;
                            $email_user .= '</td></tr>';

                            $body1 = $email_user;
                            $this->email_send->sendMail($subject1, $to_email, $body1);
                        }
                    }
                }
            }

            if (!($form->submit_action == 'email' )) {
                $this->db->insert('tbl_form_submissions', $submission);
                $submission_fields['form_submission_id'] = $this->db->insert_id();
                foreach ($fields as $index => $value) {
                    $submission_fields['form_field_id'] = $index;
                    $submission_fields['form_field_value'] = $value;
                    $this->db->insert('tbl_form_submission_fields', $submission_fields);
                }
                $this->load->library('upload_files');
                $this->upload_files->upload($submission_fields['form_submission_id']);
            }

            if ($form->success_msg)
                $data['msg'] = $form->success_msg;
            else
                $data['msg'] = 'An Email has been sent. We will respond to you shortly.';
        }else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
    }

    function verify_captcha($response_code) {
        $verify_url = 'https://www.google.com/recaptcha/api/siteverify?secret=6Lcq6QsUAAAAALJywLNL-U00T0MsSquAv6nP-gBp&response=' . $response_code;
        $response = json_decode(file_get_contents($verify_url), true);
        return $response['success'] ? true : false;
    }

    function package_booking() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {
            /* backend model */
            $this->load->model('package_book_model', 'package_book');
            $this->load->model('package_book_status_model', 'package_book_status');
            /* backend model */
            $post = $_POST;

            $package_name = $post['hidden_package_name'];
            $package_code = $post['hidden_package_code'];
            $departure_date = $post['departure_date'];
            $departure_date_id = $this->db->select('id')->where(array('departure_date' => $post['departure_date'], 'package_id' => $post['hidden_package_id']))->get('tbl_package_departure_detail')->row();

            if (!$departure_date_id) {
                $data['class'] = 'error';
                $data['msg'] = 'Booking is not available for selected date';
                echo json_encode($data);
                exit;
            }
            $booking_status = $this->db->select('name')->where('id', 1)->get('tbl_booking_status')->row();

            $insert_book['package_destination_id'] = $post['hidden_package_destination_id'];
            $insert_book['package_experience_id'] = $post['hidden_package_experience_id'];
            $insert_book['package_id'] = $post['hidden_package_id'];
            $insert_book['remarks'] = $post['remarks'];
            $insert_book['last_booking_status_id'] = 1;

            $this->db->trans_begin();

            $res = $this->package_book->save($insert_book, '', true);
            $package_booking_id = $res;

            $insert_status['package_booking_id'] = $package_booking_id;
            $insert_status['departure_date_id'] = $departure_date_id->id;
            $insert_status['number_of_pax'] = $post['nos_of_pax'];
            $insert_status['price_per_person'] = $post['price_per_person'];
            $insert_status['total_price'] = $post['total_cost'];
            $insert_status['full_name'] = ucwords($post['full_name']);
            $insert_status['email_address'] = $post['email_address'];
            $insert_status['contact_number'] = $post['contact_nos'];
            $insert_status['contact_address'] = $post['contact_address'];
            $insert_status['booking_status_id'] = 1;
            $insert_status['note'] = '';

            $this->package_book_status->save($insert_status);

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $data['class'] = 'error';
                $data['msg'] = 'Error occured, Please try again';
            } else {
                $footer_contact = $this->public_model->getContent('5');
                //echo $footer_contact[0]->long_description;die;
                $this->db->trans_commit();
                /* sending mail code */
                $email_admin = $email_user = '';
                $subject = 'Package Booking (' . $booking_status->name . ')';

                $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_admin .= '<tbody>';
                $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_admin .= $subject;
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_admin .= '<p>Greetings,</p>';
                $email_admin .= '<p>A new Package Booking has been made for <b>' . $package_name . '</b>.</p>';
                $email_admin .= '<p>Following are the details:</p>';
                $email_admin .= '<ul style="padding-left: 20px;">';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Package Code :</strong>';
                $email_admin .= '<span>' . $package_code . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Name :</strong>';
                $email_admin .= '<span>' . ucwords($post['full_name']) . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Email :</strong>';
                $email_admin .= '<span>' . $post['email_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Contact :</strong>';
                $email_admin .= '<span>' . $post['contact_nos'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Address :</strong>';
                $email_admin .= '<span>' . $post['contact_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '</ul>';
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td>';
                $email_admin .= 'Regards<br/>';
                $email_admin .= 'Responsible Adventures<br/>';
                $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                $email_admin .= '</td>';
                $email_admin .= '<td>';
                $email_admin .= $footer_contact[0]->long_description;
                $email_admin .= '</td></tr>';

                $body = $email_admin;

                $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_user .= '<tbody>';
                $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_user .= $subject;
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_user .= '<p>Namaste ' . ucwords($post['full_name']) . '</p>';
                $email_user .= '<p>We have received your booking request as follows.</p>';
                $email_user .= '<ul style="padding-left: 20px;">';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Package Name :</strong>';
                $email_user .= '<span>' . $package_name . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Package Code :</strong>';
                $email_user .= '<span>' . $package_code . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Departure Date :</strong>';
                $email_user .= '<span>' . $departure_date . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Nos of Pax :</strong>';
                $email_user .= '<span>' . $post['nos_of_pax'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Price per Pax :</strong>';
                $email_user .= '<span> $ ' . $post['price_per_person'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Total Price :</strong>';
                $email_user .= '<span> $ ' . $post['total_cost'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '</ul>';
                $email_user .= '<p>We will confirm your booking shortly after verification.</p>';
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td>';
                $email_user .= 'Regards<br/>';
                $email_user .= 'Responsible Adventures<br/>';
                $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                $email_user .= '</td>';
                $email_user .= '<td>';
                $email_user .= $footer_contact[0]->long_description;
                $email_user .= '</td></tr>';

                $body_user = $email_user;

                $res = $this->email_send->sendMail($subject, '', $body);
                $res1 = $this->email_send->sendMail($subject, $post['email_address'], $body_user);
                /* sending mail code */
                if ($res == 'success' && $res1 == 'success') {
                    $data['class'] = 'success';
                    $data['msg'] = 'An Email has been sent. We will respond to you shortly.';
                } else {
                    $data['class'] = 'success';
                    $data['msg'] = 'Package Booked Successfully';
                }
            }
        } else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
    }

    function package_enquiry() {
        $recaptcha = $_POST['g-recaptcha-response'];
        if ($this->verify_captcha($recaptcha) == true) {
            /* backend model */
            $this->load->model('package_enquire_model', 'package_enquire');
            /* backend model */
            $post = $_POST;

            $package_name = $post['hidden_package_name'];
            $package_code = $post['hidden_package_code'];

            $insert_post['package_destination_id'] = $post['hidden_package_destination_id'];
            $insert_post['package_experience_id'] = $post['hidden_package_experience_id'];
            $insert_post['package_id'] = $post['hidden_package_id'];
            $insert_post['departure_date'] = $post['departure_date'];
            $insert_post['number_of_pax'] = $post['nos_of_pax'];
            $insert_post['full_name'] = $post['full_name'];
            $insert_post['email_address'] = $post['email_address'];
            $insert_post['contact_number'] = $post['contact_number'];
            $insert_post['contact_address'] = $post['contact_address'];
            $insert_post['enquiry'] = $post['enquiry'];
            $insert_post['note'] = '';

            $res = $this->package_enquire->save($insert_post, '', true);

            if ($res) {
                $footer_contact = $this->public_model->getContent('5');
                /* sending mail code */
                $email_admin = $email_user = '';
                $subject = 'Package Enquire';

                $email_admin .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_admin .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_admin .= '<tbody>';
                $email_admin .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_admin .= $subject;
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_admin .= '<p>Greetings,</p>';
                $email_admin .= '<p>A new Package Enquire has been made for ' . $package_name . '.</p>';
                $email_admin .= '<p>Following are the details:</p>';
                $email_admin .= '<ul style="padding-left: 20px;">';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Name :</strong>';
                $email_admin .= '<span>' . ucwords($post['full_name']) . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Email :</strong>';
                $email_admin .= '<span>' . $post['email_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Contact :</strong>';
                $email_admin .= '<span>' . $post['contact_number'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_admin .= '<strong style="width: 140px; display: inline-block;">Customer Address :</strong>';
                $email_admin .= '<span>' . $post['contact_address'] . '</span>';
                $email_admin .= '</li>';
                $email_admin .= '</ul>';
                $email_admin .= '</td></tr>';
                $email_admin .= '<tr><td>';
                $email_admin .= 'Regards<br/>';
                $email_admin .= 'Responsible Adventures<br/>';
                $email_admin .= '<img src="' . base_url('img/logo.png') . '">';
                $email_admin .= '</td>';
                $email_admin .= '<td>';
                $email_admin .= $footer_contact[0]->long_description;
                $email_admin .= '</td></tr>';

                $body = $email_admin;

                $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_user .= '<thead><tr><td colspan="2" style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_user .= '<tbody>';
                $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_user .= $subject;
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_user .= '<p>Namaste ' . ucwords($post['full_name']) . '</p>';
                $email_user .= '<p>We have received your enquiry for the following.</p>';
                $email_user .= '<ul style="padding-left: 20px;">';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Package Name :</strong>';
                $email_user .= '<span>' . $package_name . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Package Code :</strong>';
                $email_user .= '<span>' . $package_code . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Departure Date :</strong>';
                $email_user .= '<span>' . $post['departure_date'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Nos of Pax :</strong>';
                $email_user .= '<span>' . $post['nos_of_pax'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '<li style="list-style: none; margin: 0 0 10px;">';
                $email_user .= '<strong style="width: 140px; display: inline-block;">Enquiry :</strong>';
                $email_user .= '<span>' . $post['enquiry'] . '</span>';
                $email_user .= '</li>';
                $email_user .= '</ul>';
                $email_user .= '<p>We will get back to you shortly.</p>';
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td>';
                $email_user .= 'Regards<br/>';
                $email_user .= 'Responsible Adventures<br/>';
                $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                $email_user .= '</td>';
                $email_user .= '<td>';
                $email_user .= $footer_contact[0]->long_description;
                $email_user .= '</td></tr>';

                $body_user = $email_user;

                $res = $this->email_send->sendMail($subject, '', $body);
                $res1 = $this->email_send->sendMail($subject, $post['email_address'], $body_user);
                /* sending mail code */
                if ($res == 'success' && $res1 == 'success') {
                    $data['class'] = 'success';
                    $data['msg'] = 'An Email has been sent. We will respond to you shortly.';
                } else {
                    $data['class'] = 'success';
                    $data['msg'] = 'Package Enquiry made Successfully';
                }
            } else {
                $data['class'] = 'error';
                $data['msg'] = 'Error';
            }
        } else {
            $data['class'] = 'error';
            $data['msg'] = 'Captcha verification error';
        }
        echo json_encode($data);
    }

    function mail_pdf() {
        if ($this->input->post()) {
            $result = $this->public_model->insert_data();
            if ($result == 'success') {

                $footer_contact = $this->public_model->getContent('5');
                $attached_file = $this->input->post('filename');

                $email_user = '';

                $subject = "Package Itinerary";
                $to_email = $this->input->post('email');

                $email_user .= '<table style="width: 600px; margin: 0 auto; font-family:Arial;">';
                $email_user .= '<thead><tr><td style="text-align: center;"><img src="' . base_url('img/logo.png') . '"></td></tr></thead>';
                $email_user .= '<tbody>';
                $email_user .= '<tr><td colspan="2" style="padding: 30px;  font-size: 20px; font-weight: 700; color: #fff; background-color:#FFA200; padding-left: 30px;">';
                $email_user .= $subject;
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td colspan="2" style=" font-weight: 400; color: #777; font-size: 14px; background-color: #f7f7f7; padding: 30px; line-height: 21px; ">';
                $email_user .= '<p>Dear ' . $this->input->post('name') . ',</p>';
                $email_user .= '<p>Please find attached package details you have requested for. Let us know how we can help you plan your holiday.</p>';
                $email_user .= '</td></tr>';
                $email_user .= '<tr><td>';
                $email_user .= 'Regards<br/>';
                $email_user .= 'Responsible Adventures<br/>';
                $email_user .= '<img src="' . base_url('img/logo.png') . '">';
                $email_user .= '</td>';
                $email_user .= '<td>';
                $email_user .= $footer_contact[0]->long_description;
                $email_user .= '</td></tr>';

                $body = $email_user;
                $res = $this->email_send->sendMail($subject, $to_email, $body, $attached_file);
                if ($res == 'success') {
                    $data['response'] = 'success';
                }

                echo json_encode($data);
            }
        }
    }

}

?>