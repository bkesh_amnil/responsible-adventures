<?php
    Class Home extends BASE_Controller{

        function __construct() {
            parent::__construct();
        }
        
        function index(){
            $data['active_menu'] = 'home';
            $data['count'] = 0;
            $data['module_name'] = 'home';
            $item_per_page = 1;

            $data['banners'] = $this->public_model->getBanner();
            //$data['destinations'] = $this->public_model->getDestinationwithActivity();
            $data['destinations'] = $this->public_model->getDestinations();
            $data['activities'] = $this->public_model->getActivities();
            if(isset($data['activities']) && !empty($data['activities'])) {
                foreach($data['activities'] as $activity) {
                    $data1[] = array('url' => base_url($activity['info']->image), 'country' => $activity['info']->destination_id, 'link' => $activity['link'], 'title' => $activity['info']->name, 'description' => $activity['info']->description);
                }
            }
            $data['activity_sliders'] = json_encode($data1);
            $data['search_experiences'] = $this->public_model->getExperiences();

            $count_data = count($this->template['featured_packages']);
            $total_pages = ceil($count_data/$item_per_page);
            $data['total_pages'] = $total_pages;
            $data['item_per_page'] = $this->template['item_per_page'];
            $data['type'] = 'experience';

            $this->template['content'] = $this->load->view('home', $data, TRUE);
        }

    }
?>