<?php

function debug($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die();
}

function logged_in($key)
{
    return (get_userdata($key)) ? true : false;
}

function get_userdata($key)
{
    $ci = &get_instance();

    $value = $ci->session->userdata($key);
    return $value ? $value : false;
}

function segment($segment)
{
    $ci = &get_instance();

    $value = $ci->uri->segment($segment);
    return $value;
}

function set_userdata($session_array)
{
    $ci = &get_instance();

    $ci->session->set_userdata($session_array);
}

function set_flash($key, $message)
{
    $ci = &get_instance();

    $ci->session->set_flashdata($key, $message);
}

function flash()
{
    $ci = &get_instance();

    $flash_message = $ci->session->flashdata('msg');

    if($flash_message) : ?>
        <div class="alert alert-info alert-dismissable fade in">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $flash_message ?>
        </div>
    <?php endif;
}

function get_mac_address()
{
    // Turn on output buffering
    ob_start();
    //Get the ipconfig details using system commond
    system('ipconfig /all');
    // Capture the output into a variable
    $mycom=ob_get_contents();
    // Clean (erase) the output buffer
    ob_clean();
    $findme = "Physical";
    //Search the "Physical" | Find the position of Physical text
    $pmac = strpos($mycom, $findme);
    // Get Physical Address
    $mac=substr($mycom,($pmac+36),17);
    //Display Mac Address
    return $mac;
}

function swiftsend($params)
{
    require_once APPPATH.'third_party/swiftmailer/swift_required.php';

    try {
        if($_SERVER['SERVER_NAME'] === 'localhost') {
            // Create the Transport
            $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername('satish.localmail@gmail.com')
                ->setPassword('neversaydie');
        } else {
            $transport = Swift_SendmailTransport::newInstance();
        }

        // Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        // Create a message
        $message = Swift_Message::newInstance($params['subject'])
            ->setFrom(array($params['from'] => $params['fromname']))
            ->setTo(array($params['to'] => $params['toname']))
            ->setBody($params['message'], 'text/html');

        // Send the message
        $result = $mailer->send($message);

        return $result;
    } catch(Exception $e) {
        if(ENVIRONMENT == 'development') {
            debug($e);
        } else {
            redirect();
        }
    }
}

function check_parent_active($children, $current_segment)
{
    foreach($children as $child) {
        if($child->slug == $current_segment) {
            return true;
            die;
        }
    }
}

function get_select($data, $default_value = '', $name_column = '', $id_column = '')
{
    $result = array();
    if(!empty($data)) {
        if($default_value != '')
            $result[''] = $default_value;
        if($name_column == '')
            $name_column = 'name';
        if($id_column == '')
            $id_column = 'id';
        foreach($data as $row) {
            $result[$row->$id_column] = $row->$name_column;
        }
    }
    return $result;
}

function getBrowser($u_agent)
{
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }

    // check if we have a number
    if ($version==null || $version=="") {$version="?";}
    $data = array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => ucwords($platform),
        'pattern'    => $pattern
    );
    return $data;
}

function image_thumb($file_path, $width, $height, $class = "")
{
    $file_path = urlencode(base_url($file_path));
    $image_tag = '';
    /*$image_tag .= "<img src='";*/
    $image_tag .= base_url() . 'assets/phpthumb/phpthumb.php?src=';
    $image_tag .= $file_path . "&amp;w=" . $width . "&amp;h=" . $height . "&amp;zc=1&f=png'";
    /*if($class != "" && !is_array($class))
        $image_tag .= $class;
    elseif($class != "" && is_array($class)) {
        foreach($class as $attribute => $property) {
            $image_tag .= $attribute . '="' . $property . '"';
        }
    }
    $image_tag .= " />";*/

    return $image_tag;
}

function get_city_name($cities)
{
    $name = 'Kathmandu';
    foreach($cities as  $city) {
        if($city->id == get_userdata('current_city'))
            $name = $city->name;
    }
    return $name;
}

function getYoutubeVideoId($url)
{
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
    return $matches[0];
}

